﻿using UnityEngine;

public class ArenaTextMeshFontData
{
    private string m_text;

    private float m_charSize;

    private float m_charSpacing;

    private TextAnchor m_anchor;
    
    private float m_lineSpacing;

    private TextAlignment m_alignment;

    private Color m_color;

    private Font m_font;

    private int m_fontSize;

    private float m_tabSize;

    private FontStyle m_fontStyle;

    private float offsetZ;

    private bool richText;

    public Vector3 LocalPositionOffset { get; set; }

    public Vector3 LocalRotation { get; set; }

    public Shader m_shader;

    public string Text
    {
        get
        {
            return this.m_text;
        }
    }

    public float CharSize
    {
        get
        {
            return this.m_charSize;
        }
    }

    public float CharSpacing
    {
        get
        {
            return this.m_charSpacing;
        }
    }

    public float LineSpacing
    {
        get
        {
            return this.m_lineSpacing;
        }
    }

    public TextAnchor Anchor
    {
        get
        {
            return this.m_anchor;
        }
    }

    public TextAlignment Alignment
    {
        get
        {
            return this.m_alignment;
        }
    }

    public float TabSize
    {
        get
        {
            return this.m_tabSize;
        }
    }
    
    public Color Color
    {
        get
        {
            return this.m_color;
        }
    }

    public Shader Shader
    {
        get
        {
            return this.m_shader;
        }
    }

    public Font Font
    {
        get
        {
            return this.m_font;
        }
    }

    public int FontSize
    {
        get
        {
            return this.m_fontSize;
        }
        set
        {
            this.m_fontSize = value;
        }
    }
    public FontStyle FontStyle
    {
        get
        {
            return this.m_fontStyle;
        }
       }

    public float OffsetZ
    {
        get
        {
            return this.offsetZ;
        }
    }

    public bool RichText
    {
        get
        {
            return this.richText;
        }
    }

    public ArenaTextMeshFontData(
        string i_Text,
        float i_OffsetZ,
        float i_CharSize,
        float i_LineSpacing,
        TextAnchor i_Anchor,
        TextAlignment i_Alignment,
        int i_TabSize,
        int i_FontSize,
        FontStyle i_FontStyle,
        bool i_RichText,
        Font i_Font,
        Color i_Color,
        Shader i_Shader,
        Vector3 i_LocalPositionOffset,
        Vector3 i_LocalRotation
        )
    {
        //Debug.Log("Text is " + i_Text);
        this.m_text = i_Text;
        this.offsetZ = i_OffsetZ;
        this.m_charSize = i_CharSize;
        this.m_lineSpacing = i_LineSpacing;
        this.m_anchor = i_Anchor;
        this.m_alignment = i_Alignment;
        this.m_tabSize = i_TabSize;
        this.m_font = i_Font;
        this.m_fontSize = i_FontSize;
        this.m_fontStyle = i_FontStyle;
        this.richText = i_RichText;
        this.m_color = i_Color;
        this.m_shader = i_Shader;
        this.LocalPositionOffset = i_LocalPositionOffset;
        this.LocalRotation = i_LocalRotation;
    }
}
