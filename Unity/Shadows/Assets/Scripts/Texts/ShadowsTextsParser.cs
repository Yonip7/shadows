﻿using Newtonsoft.Json;

using UnityEngine;
using System.Collections;

public class ShadowsTextsParser : MonoBehaviour {

    private const string k_nameOfENTextsJSON = "Texts/AppTextsEN";
    private const string k_nameOfPRTextsJSON = "Texts/AppTextsPR";

    public void ParseTexts(System.Action i_Action)
    {
        this.StartCoroutine(this.parseLevels(i_Action));
    }

    private IEnumerator parseLevels(System.Action i_Action)
    {
        var resource = Resources.Load(k_nameOfENTextsJSON);
        yield return resource;

        yield return this.StartCoroutine(TextService.Instance.ParseJson(k_nameOfENTextsJSON));

        Debug.Log("Texts Parsing completed");

        if (i_Action != null)
        {
            i_Action();
        }
    }
}
