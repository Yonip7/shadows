﻿using ShadowsModel.Model;

using UnityEngine;
using System.Collections;

public interface IShadowsGameModel
{
    void Init(LogicalBoard i_InitialLogicalBoard);

    void LoadLevel(int i_LevelNum, LogicalBoard i_NewLevelLogicalBoard);

    void ApplyMove(Move move);

    void RestartLevel();
}
