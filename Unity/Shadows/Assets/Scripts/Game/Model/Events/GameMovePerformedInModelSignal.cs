﻿using ShadowsModel.Model;

using strange.extensions.signal.impl;

public class GameMovePerformedInModelSignal : Signal<Result>
{
}
