﻿namespace ShadowsModel.Model
{
    public enum Move
    {
        Right,
        Left,
        Jump,
        Flip,
    }
}
