﻿public class ExitPosition 
{

    public readonly int Row, Column;
    public readonly bool IsUpRight;

    public ExitPosition(int i_Row, int i_Column, bool i_IsUpright)
    {
        Row = i_Row;
        Column = i_Column;
        IsUpRight = i_IsUpright;
    }
}
