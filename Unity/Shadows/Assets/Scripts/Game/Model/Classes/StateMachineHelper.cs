﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ShadowsModel.Model
{
    public class StateMachineHelper
    {
        private readonly LogicalBoard m_logicalBoard;

        public StateMachineHelper(LogicalBoard i_LogicalBoard)
        {
            this.m_logicalBoard = i_LogicalBoard;
        }

        public StateMachineCell At(int row, int column)
        {
            return new StateMachineCell(this.m_logicalBoard.Config, row, column);
        }

        public StateMachineCell GetCurrent()
        {
            return new StateMachineCell(this.m_logicalBoard.Config, this.m_logicalBoard.CurrentPosition.Row, this.m_logicalBoard.CurrentPosition.Column);
        }

        public bool IsHeroUpright
        {
            get { return this.m_logicalBoard.CurrentPosition.IsMimpi; }
        }

        public bool IsHeroFacingRight
        {
            get { return this.m_logicalBoard.CurrentPosition.IsFacingRight; }
        }

        public StateMachineHelper WithHeroAt(StateMachineCell i_StateMachineCell)
        {
            return new StateMachineHelper(new LogicalBoard(this.m_logicalBoard.Config, new HeroPosition(i_StateMachineCell.Row, i_StateMachineCell.Column, this.m_logicalBoard.CurrentPosition.IsMimpi, this.m_logicalBoard.CurrentPosition.IsFacingRight)));
        }

        public StateMachineHelper Direct(Direction dir)
        {
            return new StateMachineHelper(new LogicalBoard(this.m_logicalBoard.Config, new HeroPosition(this.m_logicalBoard.CurrentPosition.Row, this.m_logicalBoard.CurrentPosition.Column, this.m_logicalBoard.CurrentPosition.IsMimpi, dir == Direction.Right)));
        }

        public StateMachineHelper Wimpi()
        {
            return new StateMachineHelper(new LogicalBoard(this.m_logicalBoard.Config, new HeroPosition(this.m_logicalBoard.CurrentPosition.Row, this.m_logicalBoard.CurrentPosition.Column, false, this.m_logicalBoard.CurrentPosition.IsFacingRight)));
        }

        public StateMachineHelper Mimpi()
        {
            return new StateMachineHelper(new LogicalBoard(this.m_logicalBoard.Config, new HeroPosition(this.m_logicalBoard.CurrentPosition.Row, this.m_logicalBoard.CurrentPosition.Column, true, this.m_logicalBoard.CurrentPosition.IsFacingRight)));
        }

        public Result MakeResult(Move move, eResultTypes i_ResultType) {
            return new Result(this.m_logicalBoard, move, i_ResultType);
        }

        public class StateMachineCell
        {
            private readonly BoardConfig _board;
            public readonly int Row, Column;

            public StateMachineCell(BoardConfig board, int row, int column)
            {
                _board = board;
                Row = row;
                Column = column;
            }

            public StateMachineCell Up()
            {
                if (Row + 1 >= _board.Rows)
                {
                    return null;
                }

                return new StateMachineCell(_board, Row + 1, Column); 
            }

            public StateMachineCell Down()
            {
                if (Row - 1 <= 0)
                {
                    return null;
                }

                return new StateMachineCell(_board, Row - 1, Column);
            }

            public StateMachineCell Right()  
            {
                if (Column + 1 >= _board.Columns)
                {
                    return null;
                }

                return new StateMachineCell(_board, Row, Column + 1);
            }

            public StateMachineCell Left()
            {
                if (Column - 1 <= 0)
                {
                    return null;
                }

                return new StateMachineCell(_board, Row, Column - 1);
            }

            public bool HasFloor
            {
                get { return _board.Layout[Row - 1, Column - 1].HasFloor; }
            }

            public bool HasWall
            {
                get { return _board.Layout[Row - 1, Column - 1].HasWall; }
            }

            public bool RightmostColumn
            {
                get
                {
                    return _board.Columns == Column;
                }
            }

            public bool BeforeRightmostColumn
            {
                get
                {
                    return _board.Columns - 1 == Column;
                }
            }

            public bool LeftmostColumn
            {
                get
                {
                    return Column == 1;
                }
            }

            public bool BeforeLeftmostColumn
            {
                get
                {
                    return Column == 2;
                }
            }
            
            public bool TopmostRow
            {
                get { return _board.Rows == Row; }
            }

            public bool BottomRow
            {
                get { return 1 == Row; }
            }

            public bool TwoTopmostRows // so that Mimpi can stand there
            {
                get { return _board.Rows - 1 <= Row;  }
            }

            public bool BelowBottomRow
            {
                get { return Row < 1; }
            }

            //private bool IsValidForMimpi() // ignores droppability
            //{
            //    return !BelowBottomRow
            //        && !TopmostRow
            //        && !Up().HasFloor;
            //}

            //public bool IsValidForWimpi() // ignores droppability
            //{
            //    return !BelowBottomRow
            //        && !BottomRow
            //        && !HasFloor;
            //}
        }

    }
}
