﻿namespace ShadowsModel.Model
{
    public enum eResultTypes
    {
        SuccessfulMove,
        InvalidMove,
        Death
    }

    public class Result
    {
        public readonly LogicalBoard LogicalBoard;
        public readonly Move Move;
        public readonly eResultTypes ResultType;

        public Result(LogicalBoard i_LogicalBoard, Move move,
            eResultTypes i_ResultType)
        {
            this.LogicalBoard = i_LogicalBoard;
            this.ResultType = i_ResultType;
            this.Move = move;
        }
    }
}
