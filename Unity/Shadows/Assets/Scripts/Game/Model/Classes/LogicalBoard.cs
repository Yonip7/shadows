﻿namespace ShadowsModel.Model
{
    public class LogicalBoard
    {
        public readonly BoardConfig Config;
        public readonly HeroPosition CurrentPosition;

        public LogicalBoard(BoardConfig boardConfig, HeroPosition currentPosition)
        {
            Config = boardConfig;
            CurrentPosition = currentPosition;
        }
    }
}
