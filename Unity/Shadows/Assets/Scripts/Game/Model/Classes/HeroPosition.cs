﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ShadowsModel.Model
{
    public class HeroPosition
    {
        public readonly int Row, Column;
        public readonly bool IsMimpi;
        public readonly bool IsFacingRight;

        public HeroPosition(int row, int column, bool isUpright, bool isFacingRight)
        {
            Row = row;
            Column = column;
            IsMimpi = isUpright;
            IsFacingRight = isFacingRight;
        }

        public override bool Equals(object obj)
        {
            if (obj == null || !(obj is HeroPosition)) return false;
            var other = obj as HeroPosition;
            return Row == other.Row
                && Column == other.Column
                && IsMimpi == other.IsMimpi
                && IsFacingRight == other.IsFacingRight;
        }

        public override int GetHashCode()
        {
            return Row.GetHashCode() ^ (Column * 13).GetHashCode() ^ (IsMimpi.GetHashCode() * 101) ^ (IsFacingRight.GetHashCode() * 203);
        }

    }
}
