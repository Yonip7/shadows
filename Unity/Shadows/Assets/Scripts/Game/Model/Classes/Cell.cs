﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ShadowsModel.Model
{
    // Configuration is based on Mimpi perspective. Floors turn into the floor for adjacent cells when in Pipmi position.
    public class Cell
    {
        public readonly bool HasWall; // wall is always on left side
        public readonly bool HasFloor; // the enemy's gate is always down

        public Cell(bool hasWall, bool hasFloor)
        {
            HasWall = hasWall;
            HasFloor = hasFloor;
        }
    }
}
