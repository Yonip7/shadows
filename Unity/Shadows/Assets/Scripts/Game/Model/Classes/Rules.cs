﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ShadowsModel.Model
{
    public class Rules
    {
        public Result ApplyMove(LogicalBoard prev, Move move)
        {
            StateMachineHelper board = new StateMachineHelper(prev);

            try
            {
                switch (move)
                {
                    case Move.Flip:
                        return applyFlip(board, move);
                    case Move.Right:
                        return applyRight(board, move);
                    case Move.Left:
                        return applyLeft(board, move);
                    case Move.Jump:
                        return applyJump(board, move);
                }
            }
            catch (NullReferenceException ex)
            {
                return new Result(null, move, eResultTypes.InvalidMove); 
            }

            throw new ArgumentException();
        }

        private Result applyFlip(StateMachineHelper board, Move move)
        {
            var cell = board.GetCurrent();
            if (board.IsHeroUpright)
            {
                cell = cell.Down();
                if (!cell.HasFloor & !cell.BottomRow)
                {
                    return board.WithHeroAt(cell).Wimpi().MakeResult(move, eResultTypes.SuccessfulMove);
                }
                //return Result.InvalidMove;
                return new Result(null, move, eResultTypes.InvalidMove); 
            }
            else
            {
                if (cell.TwoTopmostRows)
                {
                    return new Result(null, move, eResultTypes.InvalidMove); 
                }
                if (cell.Up().Up().HasFloor)
                {
                    return new Result(null, move, eResultTypes.InvalidMove); 
                }
                return board.WithHeroAt(cell.Up()).Mimpi().MakeResult(move, eResultTypes.SuccessfulMove);
            }
        }

        private Result applyRight(StateMachineHelper board, Move move)
        {
            if (!board.IsHeroFacingRight)
            { // simple left/right switch - always doable
                return board.Direct(Direction.Right).MakeResult(move, eResultTypes.SuccessfulMove);
            }

            var cell = board.GetCurrent();

            if (cell.RightmostColumn)
            {
                if (cell.Right().HasWall)
                {
                    return new Result(null, move, eResultTypes.InvalidMove);
                }

                return new Result(null, move, eResultTypes.Death);
            }

            //Mimpi Logic
            if (board.IsHeroUpright && cell.Right().HasWall)
            {
                return new Result(null, move, eResultTypes.InvalidMove); 
            }

            if (board.IsHeroUpright && cell.Right().Up().HasWall)
            {
                return new Result(null, move, eResultTypes.InvalidMove);
            }
            
            if (board.IsHeroUpright && cell.Right().Up().HasFloor)
            {
                return new Result(null, move, eResultTypes.InvalidMove);
            }

            //Wimpi Logic
            if (!board.IsHeroUpright && cell.Right().HasWall)
            {
                return new Result(null, move, eResultTypes.InvalidMove);
            }

            if (!board.IsHeroUpright && cell.Right().Down().HasWall)
            {
                return new Result(null, move, eResultTypes.InvalidMove);
            }

            if (!board.IsHeroUpright && cell.Right().HasFloor)
            {
                return new Result(null, move, eResultTypes.InvalidMove);
            }

            return dropHero(board, cell.Right(), move);
        }

        private Result applyLeft(StateMachineHelper board, Move move)
        {
            if (board.IsHeroFacingRight)
            { // simple left/right switch - always doable
                return board.Direct(Direction.Left).MakeResult(move, eResultTypes.SuccessfulMove);
            }

            var cell = board.GetCurrent();

            if (cell.LeftmostColumn)
            {
                if (cell.HasWall)
                {
                    return new Result(null, move, eResultTypes.InvalidMove);
                }

                return new Result(null, move, eResultTypes.Death); 
            }

            //Mimpi Logic
            if (board.IsHeroUpright && cell.HasWall)
            {
                return new Result(null, move, eResultTypes.InvalidMove);
            }

            if (board.IsHeroUpright && cell.Up().HasWall)
            {
                return new Result(null, move, eResultTypes.InvalidMove);
            }

            if (board.IsHeroUpright && cell.Left().Up().HasFloor)
            {
                return new Result(null, move, eResultTypes.InvalidMove);
            }

            //Wimpi Logic
            if (!board.IsHeroUpright && cell.HasWall)
            {
                return new Result(null, move, eResultTypes.InvalidMove);
            }
            if (!board.IsHeroUpright && cell.Down().HasWall)
            {
                return new Result(null, move, eResultTypes.InvalidMove);
            }

            if (!board.IsHeroUpright && cell.Left().HasFloor)
            {
                return new Result(null, move, eResultTypes.InvalidMove);
            }

            return dropHero(board, cell.Left(), move);
        }

        // drop test - see the hero is dropped (gravity-wise) once they are put in a cell
        private Result dropHero(StateMachineHelper board, StateMachineHelper.StateMachineCell i_StateMachineCell,
            Move move)
        {
            for (int droppedRows = 0; droppedRows < 3; droppedRows++)
            {
                if ((board.IsHeroUpright && i_StateMachineCell.HasFloor)
                    || (!board.IsHeroUpright && i_StateMachineCell.Up().HasFloor))
                {
                    return board.WithHeroAt(i_StateMachineCell).MakeResult(move, eResultTypes.SuccessfulMove);
                }
                i_StateMachineCell = board.IsHeroUpright ? i_StateMachineCell.Down() : i_StateMachineCell.Up();
                if (i_StateMachineCell.BelowBottomRow || i_StateMachineCell.TopmostRow)
                {
                    return board.WithHeroAt(i_StateMachineCell).MakeResult(move, eResultTypes.Death);
                }
            }
           
            return board.WithHeroAt(i_StateMachineCell).MakeResult(move, eResultTypes.Death);
        }

        private Result applyJump(StateMachineHelper board, Move move)
        {
            var cell = board.GetCurrent();

            if (board.IsHeroFacingRight)
            {
                return this.handleJumpRight(board, cell, move);
            }
            else // facing left
            {
                return this.handleJumpLeft(board, cell, move);
            }
        }

        private Result handleJumpRight(StateMachineHelper board, StateMachineHelper.StateMachineCell cell, Move move)
        {
            if (cell.RightmostColumn && this.isDecreasedJumpRight(board, cell))
            {
                return new Result(null, move, eResultTypes.Death);
            }

            if (cell.BeforeRightmostColumn && this.isLongJumpRightAvailable(board, cell))
            {
                return new Result(null, move, eResultTypes.Death);
            }

            if (this.isShortJumpRightAvailable(board, cell))
            {
                return this.jumpRightShort(board, cell, move);
            }
            else
            {
                if (this.isLongJumpRightAvailable(board, cell))
                {
                    return this.jumpRightLong(board, cell, move);
                }
                else
                {
                    if (this.isDecreasedJumpRight(board, cell))
                    {
                        return this.jumpRightDecreased(board, cell, move);
                    }
                    else
                    {
                        return new Result(null, move, eResultTypes.InvalidMove);
                    }
                }
            }
        }

        private bool isShortJumpRightAvailable(StateMachineHelper board, StateMachineHelper.StateMachineCell cell)
        {
            bool isShortAvailable = false;

            if (board.IsHeroUpright)
            {
                if (this.isMimpiUnblockedForShortJumpRight(cell))
                {
                    isShortAvailable = true;
                }
            }

            if (!board.IsHeroUpright)
            {
                if (this.isWimpiUnblockedForShortJumpRight(cell))
                {
                    isShortAvailable = true;
                }
            }

            return isShortAvailable;
        }

        private bool isShortJumpLeftAvailable(StateMachineHelper board, StateMachineHelper.StateMachineCell cell)
        {
            bool isShortAvailable = false;

            if (board.IsHeroUpright)
            {
                if (this.isMimpiUnblockedForShortJumpLeft(cell))
                {
                    isShortAvailable = true;
                }
            }

            if (!board.IsHeroUpright)
            {
                if (this.isWimpiUnblockedForShortJumpLeft(cell))
                {
                    isShortAvailable = true;
                }
            }

            return isShortAvailable;
        }

        private Result handleJumpLeft(StateMachineHelper board, StateMachineHelper.StateMachineCell cell, Move move)
        {
            if (cell.LeftmostColumn && this.isDecreasedJumpLeft(board, cell))
            {
                return new Result(null, move, eResultTypes.Death);
            }

            if (cell.BeforeLeftmostColumn && this.isLongJumpLeftAvailable(board, cell))
            {
                return new Result(null, move, eResultTypes.Death);
            }

            if (this.isShortJumpLeftAvailable(board, cell))
            {
                return this.jumpLeftShort(board, cell, move);
            }
            else
            {
                if (this.isLongJumpLeftAvailable(board, cell))
                {
                    return this.jumpLeftLong(board, cell, move);
                }
                else
                {
                    if (this.isDecreasedJumpLeft(board, cell))
                    {
                        return this.jumpLeftDecreased(board, cell, move);
                    }
                    else
                    {
                        return new Result(null, move, eResultTypes.InvalidMove);
                    }
                }
            }
        }

        private Result jumpRightShort(StateMachineHelper board,
            StateMachineHelper.StateMachineCell i_OriginCell, Move move)
        {
            var jumpedToCell = i_OriginCell.Right();
            if (board.IsHeroUpright)
            {
                return this.jumpMimpiRightShort(board, jumpedToCell, move);
            }
            else // Wimpi
            {
                return this.jumpWimpiRightShort(board, jumpedToCell, move);
            }
        }

        private Result jumpLeftShort(StateMachineHelper board, StateMachineHelper.StateMachineCell i_OriginCell, Move move)
        {
            var jumpedToCell = i_OriginCell.Left();
            if (board.IsHeroUpright)
            {
                return this.jumpMimpiLeftShort(board, jumpedToCell, move);
            }
            else // Wimpi
            {
                return this.jumpWimpiLeftShort(board, jumpedToCell, move);
            }
        }

        private bool isLongJumpRightAvailable(StateMachineHelper board, StateMachineHelper.StateMachineCell i_OriginCell)
        {
            bool isAvailable;
           
            if (board.IsHeroUpright)
            {
                isAvailable = this.isLongJumpRightAvailableForMimpi(i_OriginCell);
            }
            else
            {
                isAvailable = this.isLongJumpRightAvailableForWimpi(i_OriginCell);
            }

            return isAvailable;
        }

        private bool isLongJumpLeftAvailable(StateMachineHelper board, StateMachineHelper.StateMachineCell i_OriginCell)
        {
            bool isAvailable;

            if (board.IsHeroUpright)
            {
                isAvailable = this.isLongJumpLeftAvailableForMimpi(i_OriginCell);
            }
            else
            {
                isAvailable = this.isLongJumpLeftAvailableForWimpi(i_OriginCell);
            }

            return isAvailable;
        }

        private bool isLongJumpRightAvailableForMimpi(StateMachineHelper.StateMachineCell i_OriginCell)
        {
            bool isAvailable = false;

            try
            {
                bool isUnblocked = this.isMimpiUnblockedForLongJumpRight(i_OriginCell);

                var oneCellRight = i_OriginCell.Right();
                var twoCellsRight = i_OriginCell.Right().Right();
                var oneCellRightUp = oneCellRight.Up();

                var twoCellsRightUp = twoCellsRight.Up();

                bool lowerCellsAreValidForJump = !oneCellRight.HasWall;

                bool upperCellsAreValidForJump = !oneCellRightUp.HasFloor && !twoCellsRightUp.HasFloor
                                                 && !oneCellRightUp.HasWall && !twoCellsRightUp.HasWall;

                if (lowerCellsAreValidForJump && upperCellsAreValidForJump && isUnblocked)
                {
                    isAvailable = true;
                }
            }
            catch (NullReferenceException ex)
            {
                var twoCellsUp = i_OriginCell.Up().Up();
                var twoCellupRight = twoCellsUp.Right();

                if (twoCellsUp == null && twoCellupRight == null)
                {
                    isAvailable = true;
                }
                else
                {
                    if (!twoCellsUp.HasFloor && !twoCellupRight.HasFloor)
                    {
                        isAvailable = true;
                    }
                }

                return isAvailable;
            }

            return isAvailable;
        }

        private bool isDecreasedJumpRight(StateMachineHelper board, StateMachineHelper.StateMachineCell i_OriginCell)
        {
            bool isDecreased;

            if (board.IsHeroUpright)
            {
                isDecreased = this.isDecreasedJumpRightForMimpi(i_OriginCell);
            }
            else
            {
                isDecreased = this.isDecreasedJumpRightForWimpi(i_OriginCell);
            }

            return isDecreased;
        }

        private bool isDecreasedJumpLeft(StateMachineHelper board, StateMachineHelper.StateMachineCell i_OriginCell)
        {
            bool isDecreased;

            if (board.IsHeroUpright)
            {
                isDecreased = this.isDecreasedJumpLeftForMimpi(i_OriginCell);
            }
            else
            {
                isDecreased = this.isDecreasedJumpLeftForWimpi(i_OriginCell);
            }

            return isDecreased;
        }



        private bool isDecreasedJumpRightForMimpi(StateMachineHelper.StateMachineCell i_OriginCell)
        {
            bool isDecreased = false;

            try
            {
                bool isUnblocked = this.isMimpiUnblockedForDecreasedJumpRight(i_OriginCell);

                var oneCellRight = i_OriginCell.Right();
                var oneCellRightUp = oneCellRight.Up();

                bool isAvailable = !oneCellRight.HasWall && !oneCellRightUp.HasWall && !oneCellRightUp.HasFloor;
                
                isDecreased = isAvailable && isUnblocked;
            }
            catch (NullReferenceException ex)
            {
                var twoCellsUp = i_OriginCell.Up().Up();

                if (twoCellsUp == null)
                {
                    isDecreased = true;
                }
                else
                {
                    if (!twoCellsUp.HasFloor)
                    {
                        isDecreased = true;
                    }
                }

                return isDecreased;
            }

            return isDecreased;
        }

        #region Mimpi Unblocked

            private bool isMimpiUnblockedForShortJumpRight(StateMachineHelper.StateMachineCell i_OriginCell)
            {
                bool isUnblocked = false;

                try
                {
                    var oneCellUp = i_OriginCell.Up();
                    var oneCellUpRight = oneCellUp.Right();
                    var twoCellsUp = oneCellUp.Up();
                    var twoCellsUpRight = twoCellsUp.Right();

                    isUnblocked = !twoCellsUp.HasFloor && oneCellUpRight.HasFloor &&
                        !twoCellsUpRight.HasFloor;

                }
                catch (NullReferenceException ex)
                {
                    return isUnblocked;
                }

                return isUnblocked;
            }

            private bool isMimpiUnblockedForDecreasedJumpRight(StateMachineHelper.StateMachineCell i_OriginCell)
            {
                bool isUnblocked = false;

                try
                {
                    var oneCellUp = i_OriginCell.Up();
                    var oneCellUpRight = oneCellUp.Right();
                    var twoCellsUp = oneCellUp.Up();
                    var twoCellsUpRight = twoCellsUp.Right();

                    isUnblocked = !twoCellsUp.HasFloor && !twoCellsUpRight.HasFloor;

                }
                catch (NullReferenceException ex)
                {
                    isUnblocked = true;
                    return isUnblocked;
                }

                return isUnblocked;
            }

            private bool isMimpiUnblockedForLongJumpRight(StateMachineHelper.StateMachineCell i_OriginCell)
            {
                bool isUnblocked = false;

                try
                {
                    var oneCellUp = i_OriginCell.Up();
                    var oneCellUpRight = oneCellUp.Right();
                    var twoCellsUp = oneCellUp.Up();
                    var twoCellsUpRight = twoCellsUp.Right();
                    var threeCellsUpRight = twoCellsUpRight.Right();

                    isUnblocked = !twoCellsUp.HasFloor && !twoCellsUpRight.HasFloor && !threeCellsUpRight.HasFloor;
                }
                catch (NullReferenceException ex)
                {
                    var oneCellUp = i_OriginCell.Up();

                    if (oneCellUp == null)
                    {
                        isUnblocked = true;
                    }
                    else
                    {
                        var twoCellsUp = oneCellUp.Up();

                        if (twoCellsUp == null)
                        {
                            isUnblocked = true;
                        }
                        else
                        {
                            var twoCellsUpRight = twoCellsUp.Right();
                            var threeCellsUpRight = twoCellsUpRight.Right();
                            if (twoCellsUp == null && twoCellsUpRight == null && threeCellsUpRight == null)
                            {
                                isUnblocked = true;
                            }
                        }
                    }

                    return isUnblocked;
                }

                return isUnblocked;
            }

            private bool isMimpiUnblockedForShortJumpLeft(StateMachineHelper.StateMachineCell i_OriginCell)
            {
                bool isUnblocked = false;

                try
                {
                    var oneCellUp = i_OriginCell.Up();
                    var oneCellUpLeft = oneCellUp.Left();
                    var twoCellsUp = oneCellUp.Up();
                    var twoCellsUpLeft = twoCellsUp.Left();

                    isUnblocked = !twoCellsUp.HasFloor && oneCellUpLeft.HasFloor && !twoCellsUpLeft.HasFloor;
                }
                catch (NullReferenceException ex)
                {
                    return isUnblocked;
                }

                return isUnblocked;
            }

            private bool isMimpiUnblockedForDecreasedJumpLeft(StateMachineHelper.StateMachineCell i_OriginCell)
            {
                bool isUnblocked = false;

                try
                {
                    var oneCellUp = i_OriginCell.Up();
                    var oneCellUpLeft = oneCellUp.Left();
                    var twoCellsUp = oneCellUp.Up();
                    var twoCellsUpLeft = twoCellsUp.Left();

                    isUnblocked = !twoCellsUp.HasFloor && !twoCellsUpLeft.HasFloor;
                }
                catch (NullReferenceException ex)
                {
                    isUnblocked = true;
                    return isUnblocked;
                }

                return isUnblocked;
            }

            private bool isMimpiUnblockedForLongJumpLeft(StateMachineHelper.StateMachineCell i_OriginCell)
            {
                bool isUnblocked = false;

                try
                {
                    var oneCellUp = i_OriginCell.Up();
                    var twoCellsUp = oneCellUp.Up();
                    var twoCellsUpLeft = twoCellsUp.Left();
                    var threeCellsUpLeft = twoCellsUpLeft.Left();


                    isUnblocked = !twoCellsUp.HasFloor && !twoCellsUpLeft.HasFloor && !threeCellsUpLeft.HasFloor;
                }
                catch (NullReferenceException ex)
                {
                    return isUnblocked;
                }

                return isUnblocked;
            }

            private bool isWimpiUnblockedForShortJumpRight(StateMachineHelper.StateMachineCell i_OriginCell)
            {
                bool isUnblocked = false;

                try
                {
                    var oneCellDown = i_OriginCell.Down();
                    var oneCellRight = i_OriginCell.Right();
                    var oneCellDownRight = oneCellDown.Right();

                    isUnblocked = !oneCellDown.HasFloor && oneCellRight.HasFloor
                                  && !oneCellDownRight.HasWall && !oneCellDownRight.HasFloor;
                }
                catch (NullReferenceException ex)
                {
                    return isUnblocked;
                }

                return isUnblocked;
            }

            private bool isWimpiUnblockedForDecreasedJumpRight(StateMachineHelper.StateMachineCell i_OriginCell)
            {
                bool isUnblocked = false;

                try
                {
                    var oneCellDown = i_OriginCell.Down();
                    var oneCellDownRight = oneCellDown.Right();

                    isUnblocked = !oneCellDown.HasFloor && !oneCellDownRight.HasFloor;
                }
                catch (NullReferenceException ex)
                {
                    return isUnblocked;
                }

                return isUnblocked;
            }

            private bool isWimpiUnblockedForLongJumpRight(StateMachineHelper.StateMachineCell i_OriginCell)
            {
                bool isUnblocked = false;

                try
                {
                    var oneCellDown = i_OriginCell.Down();
                    var oneCellDownRight = oneCellDown.Right();
                    var twoCellsDownRight = oneCellDownRight.Right();

                    isUnblocked = !oneCellDown.HasFloor && !oneCellDownRight.HasFloor && !twoCellsDownRight.HasFloor;
                }
                catch (NullReferenceException ex)
                {
                    return isUnblocked;
                }

                return isUnblocked;
            }

            private bool isWimpiUnblockedForShortJumpLeft(StateMachineHelper.StateMachineCell i_OriginCell)
            {
                bool isUnblocked = false;

                try
                {
                    var oneCellDown = i_OriginCell.Down();
                    var oneCellLeft = i_OriginCell.Left();
                    var oneCellDownLeft = oneCellDown.Left();

                    isUnblocked = !oneCellDown.HasFloor && oneCellLeft.HasFloor
                                  && !oneCellDown.HasWall && !oneCellDownLeft.HasFloor;
                }
                catch (NullReferenceException ex)
                {
                    return isUnblocked;
                }

                return isUnblocked;
            }

            private bool isWimpiUnblockedForDecreasedJumpLeft(StateMachineHelper.StateMachineCell i_OriginCell)
            {
                bool isUnblocked = false;

                try
                {
                    var oneCellDown = i_OriginCell.Down();
                    var oneCellDownLeft = oneCellDown.Left();

                    isUnblocked = !oneCellDown.HasFloor && !oneCellDownLeft.HasFloor;
                }
                catch (NullReferenceException ex)
                {
                    return isUnblocked;
                }

                return isUnblocked;
            }

            private bool isWimpiUnblockedForLongJumpLeft(StateMachineHelper.StateMachineCell i_OriginCell)
            {
                bool isUnblocked = false;

                try
                {
                    var oneCellDown = i_OriginCell.Down();
                    var oneCellDownRight = oneCellDown.Left();
                    var twoCellsDownRight = oneCellDownRight.Left();

                    isUnblocked = !oneCellDown.HasFloor && !oneCellDownRight.HasFloor && !twoCellsDownRight.HasFloor;
                }
                catch (NullReferenceException ex)
                {
                    return isUnblocked;
                }

                return isUnblocked;
            }

        #endregion

        

        private bool isDecreasedJumpRightForWimpi(StateMachineHelper.StateMachineCell i_OriginCell)
        {
            bool isDecreased = false;

            try
            {
                bool isUnblocked = this.isWimpiUnblockedForDecreasedJumpRight(i_OriginCell);

                var oneCellRight = i_OriginCell.Right();
                var oneCellRightDown = oneCellRight.Down();

                bool isAvailable = !oneCellRight.HasWall && !oneCellRightDown.HasWall && !oneCellRightDown.HasFloor;

                isDecreased = isAvailable && isUnblocked;
            }
            catch (NullReferenceException ex)
            {
                var twoCellsDown = i_OriginCell.Down().Down();

                if (twoCellsDown == null)
                {
                    isDecreased = true;
                }
                else
                {
                    if (!twoCellsDown.HasFloor)
                    {
                        isDecreased = true;
                    }
                }

                return isDecreased;
            }

            return isDecreased;
        }

        private bool isDecreasedJumpLeftForMimpi(StateMachineHelper.StateMachineCell i_OriginCell)
        {
            bool isDecreased = false;

            try
            {
                bool isUnblocked = this.isMimpiUnblockedForDecreasedJumpLeft(i_OriginCell);

                var oneCellLeft = i_OriginCell.Left();
                var oneCellLeftUp = oneCellLeft.Up();

                bool isAvailable = !oneCellLeft.HasWall && !oneCellLeftUp.HasWall && !oneCellLeftUp.HasFloor;

                isDecreased = isAvailable && isUnblocked;
            }
            catch (NullReferenceException ex)
            {
                var twoCellsUp = i_OriginCell.Up().Up();

                if (twoCellsUp == null)
                {
                    isDecreased = true;
                }
                else
                {
                    if (!twoCellsUp.HasFloor)
                    {
                        isDecreased = true;
                    }
                }

                return isDecreased;
            }

            return isDecreased;
        }

        private bool isDecreasedJumpLeftForWimpi(StateMachineHelper.StateMachineCell i_OriginCell)
        {
            bool isDecreased = false;

            try
            {
                bool isUnblocked = this.isWimpiUnblockedForDecreasedJumpLeft(i_OriginCell);

                var oneCellLeft = i_OriginCell.Left();
                var oneCellLeftDown = oneCellLeft.Down();

                bool isAvailable = !oneCellLeft.HasWall && !oneCellLeftDown.HasWall && !oneCellLeftDown.HasFloor;

                isDecreased = isAvailable && isUnblocked;
            }
            catch (NullReferenceException ex)
            {
                var twoCellsDown = i_OriginCell.Down().Down();

                if (twoCellsDown == null)
                {
                    isDecreased = true;
                }
                else
                {
                    if (!twoCellsDown.HasFloor)
                    {
                        isDecreased = true;
                    }
                }

                return isDecreased;
            }

            return isDecreased;
        }

        private bool isLongJumpRightAvailableForWimpi(StateMachineHelper.StateMachineCell i_OriginCell)
        {
            bool isAvailable = false;

            try
            {
                bool isUnblocked = this.isWimpiUnblockedForLongJumpRight(i_OriginCell);

                var oneCellRight = i_OriginCell.Right();
                var twoCellsRight = i_OriginCell.Right().Right();
                var oneCellRightDown = oneCellRight.Down();

                var twoCellsRightDown = twoCellsRight.Down();

                bool upperCellsAreValidForJump = !oneCellRight.HasWall && !twoCellsRight.HasWall
                                                 && !twoCellsRight.HasFloor;

                bool lowerCellsAreValidForJump = !oneCellRightDown.HasFloor && !twoCellsRightDown.HasFloor
                                                 && !oneCellRightDown.HasWall && !twoCellsRightDown.HasWall;

                if (lowerCellsAreValidForJump && upperCellsAreValidForJump && isUnblocked)
                {
                    isAvailable = true;
                }
            }
            catch (NullReferenceException ex)
            {
                isAvailable = true;
                return isAvailable;
//                var twoCellsDown = i_OriginCell.Down().Down();
//                var twoCellupRight = twoCellsDown.Right();
//
//                if (twoCellsDown == null && twoCellupRight == null)
//                {
//                    isAvailable = true;
//                }
//                else
//                {
//                    if (!twoCellsDown.HasFloor && !twoCellupRight.HasFloor)
//                    {
//                        isAvailable = true;
//                    }
//                }

                return isAvailable;
            }

            return isAvailable;
        }

        private bool isLongJumpLeftAvailableForMimpi(StateMachineHelper.StateMachineCell i_OriginCell)
        {
            bool isAvailable = false;

            try
            {
                bool isUnblocked = this.isMimpiUnblockedForLongJumpLeft(i_OriginCell);

                var oneCellLeft = i_OriginCell.Left();
                var twoCellsLeft = i_OriginCell.Left().Left();
                var oneCellLeftUp = oneCellLeft.Up();

                var twoCellsLeftUp = twoCellsLeft.Up();

                bool lowerCellsAreValidForJump = !oneCellLeft.HasWall;

                bool upperCellsAreValidForJump = !oneCellLeftUp.HasFloor && !twoCellsLeftUp.HasFloor
                                                 && !oneCellLeftUp.HasWall && !twoCellsLeftUp.HasWall;

                if (lowerCellsAreValidForJump && upperCellsAreValidForJump && isUnblocked)
                {
                    isAvailable = true;
                }
            }
            catch (NullReferenceException ex)
            {
                var twoCellsUp = i_OriginCell.Up().Up();
                var twoCellupLeft = twoCellsUp.Left();

                if (twoCellsUp == null && twoCellupLeft == null)
                {
                    isAvailable = true;
                }
                else
                {
                    if (!twoCellsUp.HasFloor && !twoCellupLeft.HasFloor)
                    {
                        isAvailable = true;
                    }
                }

                return isAvailable;
            }

            return isAvailable;
        }

        private bool isLongJumpLeftAvailableForWimpi(StateMachineHelper.StateMachineCell i_OriginCell)
        {
            bool isAvailable = false;

            try
            {
                bool isUnblocked = this.isWimpiUnblockedForLongJumpLeft(i_OriginCell);

                var oneCellLeft = i_OriginCell.Left();
                var twoCellsLeft = i_OriginCell.Left().Left();
                var oneCellLeftDown = oneCellLeft.Down();

                if (twoCellsLeft != null)
                {
                    var twoCellsLeftDown = twoCellsLeft.Down();

                    bool upperCellsAreValidForJump = !oneCellLeft.HasWall && !twoCellsLeft.HasWall
                                                     && !twoCellsLeft.HasFloor;

                    bool lowerCellsAreValidForJump = !oneCellLeftDown.HasFloor && !twoCellsLeftDown.HasFloor
                                                     && !oneCellLeftDown.HasWall && !twoCellsLeftDown.HasWall;

                    if (lowerCellsAreValidForJump && upperCellsAreValidForJump && isUnblocked)
                    {
                        isAvailable = true;
                    }
                }
            }
            catch (NullReferenceException ex)
            {
                var twoCellsDown = i_OriginCell.Down().Down();
                var twoCellDownLeft = twoCellsDown.Left();

                if (twoCellsDown == null && twoCellDownLeft == null)
                {
                    isAvailable = true;
                }
                else
                {
                    if (!twoCellsDown.HasFloor && !twoCellDownLeft.HasFloor)
                    {
                        isAvailable = true;
                    }
                }

                return isAvailable;
            }

            return isAvailable;
        }

        private Result jumpRightLong(StateMachineHelper board, StateMachineHelper.StateMachineCell i_OriginCell, Move move)
        {
            var jumpedOnCell = i_OriginCell.Right().Right();

            if (jumpedOnCell.HasFloor)
            {
                return board.WithHeroAt(jumpedOnCell).MakeResult(move, eResultTypes.SuccessfulMove);
            }
            else
            {
                return dropHero(board, jumpedOnCell, move);
            }            
        }

        private Result jumpRightDecreased(StateMachineHelper board, StateMachineHelper.StateMachineCell i_OriginCell, Move move)
        {
            var jumpedOnCell = i_OriginCell.Right();

            if (jumpedOnCell.HasFloor)
            {
                return board.WithHeroAt(jumpedOnCell).MakeResult(move, eResultTypes.SuccessfulMove);
            }
            else
            {
                return dropHero(board, jumpedOnCell, move);
            }
        }

        private Result jumpLeftDecreased(StateMachineHelper board,
            StateMachineHelper.StateMachineCell i_OriginCell, Move move)
        {
            var jumpedOnCell = i_OriginCell.Left();

            if (jumpedOnCell.HasFloor)
            {
                return board.WithHeroAt(jumpedOnCell).MakeResult(move, eResultTypes.SuccessfulMove);
            }
            else
            {
                return dropHero(board, jumpedOnCell, move);
            }
        }

        private Result jumpLeftLong(StateMachineHelper board, StateMachineHelper.StateMachineCell i_OriginCell, Move move)
        {
            var jumpedOnCell = i_OriginCell.Left().Left();

            if (jumpedOnCell.HasFloor)
            {
                return board.WithHeroAt(jumpedOnCell).MakeResult(move, eResultTypes.SuccessfulMove);
            }
            else
            {
                return dropHero(board, jumpedOnCell, move);
            }
        }

        private Result jumpMimpiRightShort(StateMachineHelper board, StateMachineHelper.StateMachineCell i_StateMachineCell,
            Move move)
        {
            var landing = i_StateMachineCell.Up();

            if (landing.HasFloor
                && !landing.HasWall
                && !landing.TopmostRow
                && !landing.Up().HasWall
                && !landing.Up().HasFloor) // can jump up+right, not blocked
            {
                return board.WithHeroAt(landing).MakeResult(move, eResultTypes.SuccessfulMove);
            }

            var twoCellsRight = i_StateMachineCell.Right();
            if (!landing.HasFloor
                && !landing.HasWall
                && !i_StateMachineCell.HasWall
                && !i_StateMachineCell.HasFloor
                && !i_StateMachineCell.RightmostColumn
                && !twoCellsRight.HasWall
                && !twoCellsRight.Up().HasWall
                && !twoCellsRight.Up().HasFloor) // can jump right over a pothole
            {
                return dropHero(board, twoCellsRight, move);
            }
            return new Result(null, move, eResultTypes.InvalidMove);
        }

        private Result jumpWimpiRightShort(StateMachineHelper board, StateMachineHelper.StateMachineCell i_StateMachineCell,
            Move move)
        {
            var landing = i_StateMachineCell.Down();
            if (i_StateMachineCell.HasFloor
                && !landing.HasWall
                && !landing.BottomRow
                && !landing.HasFloor
                && !landing.Down().BottomRow
                && !landing.Down().HasWall) // can jump down+right, not blocked
            {
                return board.WithHeroAt(landing).MakeResult(move, eResultTypes.SuccessfulMove);
            }
            var twoCellsRight = i_StateMachineCell.Right();
            if (!i_StateMachineCell.RightmostColumn
                && !i_StateMachineCell.HasFloor
                && !i_StateMachineCell.Up().HasFloor
                && !i_StateMachineCell.HasWall
                && !landing.HasWall
                && !twoCellsRight.HasWall
                && !twoCellsRight.Down().HasWall) // can jump right over a pothole
            {
                return dropHero(board, twoCellsRight, move);
            }
            return new Result(null, move, eResultTypes.InvalidMove); 
        }

        private Result jumpMimpiLeftShort(StateMachineHelper board, StateMachineHelper.StateMachineCell i_StateMachineCell,
            Move move)
        {
            var landing = i_StateMachineCell.Up();
            if (landing.HasFloor
                && !landing.Right().HasWall
                && !landing.TopmostRow
                && !landing.Up().Right().HasWall
                && !landing.Up().HasWall
                && !landing.Up().HasFloor) // can jump up+left, not blocked
            {
                return board.WithHeroAt(landing).MakeResult(move, eResultTypes.SuccessfulMove);
            }
            var twoCellsLeft = i_StateMachineCell.Left();
            if (!landing.HasFloor
                && !landing.HasWall
                && !i_StateMachineCell.HasWall
                && !i_StateMachineCell.HasFloor
                && !i_StateMachineCell.Right().HasWall
                && !i_StateMachineCell.Right().Up().HasWall
                && !i_StateMachineCell.LeftmostColumn
                && !twoCellsLeft.Up().HasFloor) // can jump left over a pothole
            {
                return dropHero(board, twoCellsLeft, move);
            }
            return new Result(null, move, eResultTypes.InvalidMove); 
        }

        private Result jumpWimpiLeftShort(StateMachineHelper board, StateMachineHelper.StateMachineCell i_StateMachineCell,
            Move move)
        {
            var landing = i_StateMachineCell.Down();
            if (i_StateMachineCell.HasFloor
                && !landing.HasFloor
                && !landing.BottomRow
                && !landing.Down().BottomRow
                && !landing.Right().HasWall
                && !landing.Right().Down().HasWall) // can jump down+left, not blocked
            {
                return board.WithHeroAt(landing).MakeResult(move, eResultTypes.SuccessfulMove);
            }
            var twoCellsLeft = i_StateMachineCell.Left();
            if (!i_StateMachineCell.LeftmostColumn
                && !i_StateMachineCell.HasFloor
                && !i_StateMachineCell.Up().HasFloor
                && !i_StateMachineCell.HasWall
                && !landing.HasWall
                && !i_StateMachineCell.Right().HasWall
                && !twoCellsLeft.HasFloor
                && !i_StateMachineCell.Right().Down().HasWall) // can jump left over a pothole
            {
                return dropHero(board, twoCellsLeft, move);
            }
            return new Result(null, move, eResultTypes.InvalidMove); 
        }

    }

}
