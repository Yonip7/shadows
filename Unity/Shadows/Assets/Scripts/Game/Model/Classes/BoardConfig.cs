﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ShadowsModel.Model
{
    public class BoardConfig
    {
        public readonly Cell[,] Layout; // rows are first indexed, then columns
        public readonly ExitPosition ExitPosition;
        public readonly HeroPosition InitialPosition;
        public readonly int Rows;
        public readonly int Columns;

        public BoardConfig(Cell[,] i_Layout, ExitPosition i_ExitPosition, HeroPosition i_InitialPosition)
        {
            Layout = i_Layout;
            ExitPosition = i_ExitPosition;
            InitialPosition = i_InitialPosition;
            Rows = Layout.GetLength(0);
            Columns = Layout.GetLength(1);
        }

        public Cell GetConfigForCell(int row, int col) {
            if (row > Layout.GetLength(0)) throw new ArgumentOutOfRangeException("row", row, "number too large");
            if (col > Layout.GetLength(1)) throw new ArgumentOutOfRangeException("col", col, "number too large");
            return Layout[row-1, col-1];
        }
    }
}
