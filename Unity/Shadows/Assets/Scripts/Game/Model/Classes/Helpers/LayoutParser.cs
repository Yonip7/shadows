﻿using ShadowsModel.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ShadowsModel.Helpers
{
    public class LayoutParser
    {
        // parses board layout/configuration and returns a board object, primed for usage
        // format is "dim:pppp...p:mimpi:exit"
        // description:
        // - whitespace is ignored.
        // - ":" (colon) is the top-level separator
        // - dim : format is "w/h": width and height of the board
        // - pppppp : cell configuration. rows are sequentially described, within rows individual cells. each "p" is:
        //    -- "L" - both (left) wall and (bottom) floor.
        //    -- "|" - only wall.
        //    -- "_" - only floor.
        //    -- "." - nothing (space).
        // - mimpi : initial mimpi position. format is "col/row" - 1-based indexing, (1,1) is at bottom right.
        // - exit : defined the exit/portal location. "col/row/inverse". inverse can be T/F, T if the inverse (shadow) position is also a valid portal.
        public static BoardConfig parse(string layoutString)
        {
            var parts = trim(layoutString.Split(':'));
            var size = trim(parts[0].Split('/'));
            var width = int.Parse(size[0]);
            var height = int.Parse(size[1]);
            var grid = new Cell[height, width];
            var config = parts[1].Replace(" ","").Replace("\t","").Replace("\n","").Replace("\r","");
            var gridPos = 0;
            for (int i=height - 1; i>=0; i--) {
                for (int j=0; j<width; j++) {
                    grid[i, j] = new Cell(
                        (config[gridPos] == 'L' || config[gridPos] == '|'),
                        (config[gridPos] == 'L' || config[gridPos] == '_')
                    );
                    gridPos++;
                }
            }
            var pos = trim(parts[2].Split('/'));
            var initialPosition = new HeroPosition(int.Parse(pos[1]), int.Parse(pos[0]), true, true);
            var exit = trim(parts[3].Split('/'));

            ExitPosition newExit = new ExitPosition(int.Parse(exit[1]), int.Parse(exit[0]),
                    false);

            if (exit[2] == "T")
            {
                newExit = new ExitPosition(int.Parse(exit[1]), int.Parse(exit[0]), true);
            }
            
//            HeroPosition[] exits = new [] {
//                new HeroPosition(int.Parse(exit[1]), int.Parse(exit[0]), true, true),            
//                new HeroPosition(int.Parse(exit[1]), int.Parse(exit[0]), false, true)
//            };
//            if (exit[2] != "T") exits = new [] { exits.First() };
            return new BoardConfig(grid, newExit, initialPosition);
        }

        private static String[] trim(IEnumerable<String> y)
        {
            return y.Select<String,String>(x => x.Trim()).ToArray();
        }
    }
}
