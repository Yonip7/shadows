﻿using Assets.Scripts.strange.extensions;

using ShadowsModel.Model;

public class ShadowsGameModel : Model, IShadowsGameModel
{
    private Rules m_gameRules;

    [Inject]
    public GameInitCompletedSignal GameInitCompletedSignal { get; set; }

    [Inject]
    public GameLevelLoadedSignal GameLevelLoadedSignal { get; set; }

    [Inject]
    public GameMovePerformedInModelSignal GameMovePerformedInModelSignal { get; set; }

    [Inject]
    public GameRestartedInModelSignal GameRestartedInModel { get; set; }

    public LogicalBoard CurrentLogicalBoard { get; private set; }

    private LogicalBoard m_initialLogicalBoard;

    public void Init(LogicalBoard i_InitialLogicalBoard)
    {
        GameInitCompletedSignal.Dispatch(i_InitialLogicalBoard);
    }

    public void LoadLevel(int i_LevelNum, LogicalBoard i_NewLevelLogicalBoard)
    {
        this.m_initialLogicalBoard = i_NewLevelLogicalBoard;
        this.CurrentLogicalBoard = i_NewLevelLogicalBoard;
        this.m_gameRules = new Rules();
        GameLevelLoadedSignal.Dispatch(i_LevelNum, this.CurrentLogicalBoard);
    }

    public void ApplyMove(Move move)
    {
        Result result = 
            this.m_gameRules.ApplyMove(this.CurrentLogicalBoard, move);

        if (result.ResultType == eResultTypes.Death)
        {
            if (result.LogicalBoard == null)
            {
                result = new Result(this.CurrentLogicalBoard, move, eResultTypes.Death);
            }
            
            this.CurrentLogicalBoard = new LogicalBoard(this.m_initialLogicalBoard.Config,
                this.m_initialLogicalBoard.CurrentPosition);
        }
        else
        {
            if (result.ResultType == eResultTypes.InvalidMove)
            {
                result = new Result(this.CurrentLogicalBoard, result.Move, eResultTypes.InvalidMove);
            }
            else
            {
                this.CurrentLogicalBoard = result.LogicalBoard;
            }
            
        }
        this.GameMovePerformedInModelSignal.Dispatch(result);
    }

    public void RestartLevel()
    {
        this.CurrentLogicalBoard = this.m_initialLogicalBoard;
        this.GameRestartedInModel.Dispatch();
    }
}
