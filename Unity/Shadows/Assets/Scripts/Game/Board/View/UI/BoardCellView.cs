﻿using UnityEngine;
using System.Collections;

public class BoardCellView : MonoBehaviour
{
    public enum eSectionsDirection
    {
        Left,
        Bottom,
    }

    public int Row { get; private set; }
    public int Col { get; private set; }

    //Direction Containers
    public SectionView mr_Left;
    public SectionView mr_Bottom;

    public const int k_BoardCellSize = 42;

    private bool m_bothSectionsAreOn;

    public void Init(int i_RowNum, int i_ColNum)
    {
        this.Row = i_RowNum;
        this.Col = i_ColNum;
        this.transform.name =
            "BoardCell: (" + i_RowNum + "," + i_ColNum + ")";
        mr_Bottom.Init();
        mr_Left.Init();
        mr_Bottom.transform.gameObject.SetActive(false);
        mr_Left.transform.gameObject.SetActive(false);
    }

    public void ShowSection(eSectionsDirection i_Direction)
    {
        switch (i_Direction)
        {
                case eSectionsDirection.Bottom:
                     mr_Bottom.transform.gameObject.SetActive(true);

                     if (mr_Left.transform.gameObject.activeInHierarchy)
                     {
                         this.m_bothSectionsAreOn = true;
                     }
                     break;

                case eSectionsDirection.Left:
                     mr_Left.transform.gameObject.SetActive(true);

                     if (mr_Bottom.transform.gameObject.activeInHierarchy)
                     {
                         this.m_bothSectionsAreOn = true;
                     }

                     break;
        }

        if (this.m_bothSectionsAreOn)
        {
            this.EnableLeftOverlapingBoardCellPart();
        }
    }

    public void EnableLeftOverlapingBoardCellPart()
    {
        mr_Left.EnableLeftOverlapingSectionPart();
    }

    public void SwitchState()
    {
        mr_Left.ToggleStateSwitch();
        mr_Bottom.ToggleStateSwitch();
    }
}
