﻿using UnityEngine;
using System.Collections;

public class SectionView : MonoBehaviour
{
    public GameObject mr_LightSide;
    public GameObject mr_DarkSide;

    public GameObject mr_LeftLight;
    public GameObject mr_MiddleLight;
    public GameObject mr_RightLight;

    public GameObject mr_LeftDark;
    public GameObject mr_MiddleDark;
    public GameObject mr_RightDark;

    public void ToggleStateSwitch()
    {
        if (mr_LightSide.activeInHierarchy)
        {
            this.switchToDark();
        }
        else
        {
            this.switchToLight();
        }
    }

    private void switchToLight()
    {
        this.mr_LightSide.gameObject.SetActive(true);
        this.mr_DarkSide.gameObject.SetActive(false);
    }

    private void switchToDark()
    {
        this.mr_DarkSide.gameObject.SetActive(true);
        this.mr_LightSide.gameObject.SetActive(false);
    }

    public void Init()
    {
        //Light
        mr_LeftLight.SetActive(false);
        mr_MiddleLight.SetActive(true);
        mr_RightLight.SetActive(true);

        //Dark
        mr_LeftDark.SetActive(false);
        mr_MiddleDark.SetActive(true);
        mr_RightDark.SetActive(true);

        this.switchToLight();
    }

    public void EnableLeftOverlapingSectionPart()
    {
        mr_LeftLight.SetActive(true);
        mr_LeftDark.SetActive(true);
    }
}
