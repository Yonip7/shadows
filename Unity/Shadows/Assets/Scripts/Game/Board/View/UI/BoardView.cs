﻿using System.Collections.Generic;
using ShadowsModel.Model;
using UnityEngine;
using System.Linq;

public class BoardView : MonoBehaviour
{
    public BoardCellView mr_BoardCellPrefab;

    private List<BoardCellView> m_boardCells; 

    private const int k_boardCellSize = 42;

    private int m_numOfRows;// = 16;

    private int m_numOfCol;// = 20;

    public void Init(LogicalBoard i_LogicalBoardModel)
    {
        this.m_numOfRows = i_LogicalBoardModel.Config.Rows;
        this.m_numOfCol = i_LogicalBoardModel.Config.Columns;
        this.createBoardCells();
        this.updateBoardCellsFromModel(i_LogicalBoardModel);
    }

    private void createBoardCells()
    {
        this.m_boardCells = new List<BoardCellView>();

        for (int i = 1; i <= this.m_numOfRows; i++)
        {
            for (int j = 1; j <= this.m_numOfCol; j++)
            {
                this.createBoardCell(i, j);
            }
        }
    }

    private void createBoardCell(int i_RowNum, int i_ColNum)
    {
        BoardCellView newBoardCell =
            Instantiate(mr_BoardCellPrefab) as BoardCellView;

        newBoardCell.Init(i_RowNum, i_ColNum);

        newBoardCell.transform.parent = this.transform;
        newBoardCell.transform.localPosition = new Vector3(i_ColNum * k_boardCellSize,
             i_RowNum * k_boardCellSize, 0);

        this.m_boardCells.Add(newBoardCell);
    }

    private void updateBoardCellsFromModel(LogicalBoard i_LogicalBoardModel)
    {
        StateMachineHelper helper = new StateMachineHelper(i_LogicalBoardModel);
        StateMachineHelper.StateMachineCell currentCellModel;
        
        for (int i = 1; i <= this.m_numOfRows; i++)
        {
            for (int j = 1; j <= this.m_numOfCol; j++)
            {
                currentCellModel = helper.At(i, j);
                this.updateBoardCellFromModel(currentCellModel);
            }
        }
    }

    private void updateBoardCellFromModel(StateMachineHelper.StateMachineCell
        i_CellModel)
    {
        BoardCellView currentBoardCellView = 
            this.getBoardCellAtIndices(i_CellModel.Row, i_CellModel.Column);

        if (i_CellModel.HasFloor)
        {
            currentBoardCellView.ShowSection
                (BoardCellView.eSectionsDirection.Bottom);
        }

        if (i_CellModel.HasWall)
        {
            currentBoardCellView.ShowSection
                (BoardCellView.eSectionsDirection.Left);
        }
    }

    private BoardCellView getBoardCellAtIndices(int i_RowIndex, int i_ColIndex)
    {
        BoardCellView boardCellAtIndices = null;

        IEnumerable<BoardCellView> boardCellsOfPosition =
            m_boardCells.Where(x => x.Row == i_RowIndex && x.Col == i_ColIndex);

        if (boardCellsOfPosition != null)
        {
            boardCellAtIndices = boardCellsOfPosition.First();;
        }

        return boardCellAtIndices;
    }

    public void SwitchState()
    {
        foreach (BoardCellView boardCellView in m_boardCells)
        {
            boardCellView.SwitchState();
        }
    }

    public void Reset()
    {
        if (this.m_boardCells != null)
        {
            foreach (BoardCellView boardCellView in m_boardCells)
            {
                Destroy(boardCellView.gameObject);
            }

            this.m_boardCells.Clear();
        }
    }
}
