﻿using System.Collections;
using System.Collections.Generic;

using ShadowsModel.Model;
using UnityEngine;
using strange.extensions.mediation.impl;

public class GameView : View
{
    public enum eGameStates
    {
        Light,

        Dark
    }

    public enum eTexts
    {
        BackToLevelsViewButton,
    }

    //Events
    public delegate void MoveButtonClickedDelegate(Move i_Move);

    public event MoveButtonClickedDelegate MoveButtonClicked;

    public delegate void MovePerformedDelegate();

    public event MovePerformedDelegate MovePerformed;

    public delegate void ResetButtonClickedDelegate();

    public event ResetButtonClickedDelegate ResetButtonClicked;

    //Win Test Event
    public delegate void WinTestDelegate();

    public event WinTestDelegate WinTest;

    public Camera mr_GameCamera;

    public BoardView mr_BoardView;

    public GameButtonsView mr_GameButtons;

    public GameCharacterView mr_CharacterView;

    public WinningPortalView mr_WinningPortal;

    public CloudsView mr_CloudsView;

    public SparklingStarsView mr_SparklingStarsView;

    public GameObject mr_PurpleBackLayer;

    public GameObject mr_GreyBackLayer;

    public MLTextMesh mr_LevelLabelText;

    public MLTextMesh mr_ResetLabelText;

    public SystemButtons mr_SystemButtons;

    private eGameStates m_gameState;

    private LogicalBoard m_initialCurrentLevelBoardModel;

    private bool m_controlsEnabled;

    private int m_currentLevelNumber;

    private Dictionary<eTexts, string> m_texts;

    public void Init(LogicalBoard i_LogicalBoardModel)
    {
        mr_SystemButtons.Init();
        mr_SparklingStarsView.Init();
        this.listenToCharacterEvents();
        //this.startLevel(i_LogicalBoardModel);
        Debug.Log("Init Completed!!!");
    }

    #region Texts

    private void initTexts()
    {
        this.initTextsFromJSON();
        this.assignTexts();
    }

    private void initTextsFromJSON()
    {
        m_texts = new Dictionary<eTexts, string>();
        m_texts[eTexts.BackToLevelsViewButton] = string.Format(TextService.GetTextById("3.3.3.2"), m_currentLevelNumber);
    }

    private void assignTexts()
    {
        mr_LevelLabelText.ChangeText(this.getTextByType(eTexts.BackToLevelsViewButton));
        this.mr_ResetLabelText.Text = TextService.GetTextById("3.3.2.1");
    }

    private string getTextByType(eTexts i_TextType)
    {
        return m_texts[i_TextType];
    }

    #endregion



    private void listenToCharacterEvents()
    {
        mr_CharacterView.CharacterChangedDirection += this.handleDirectionChange;
    }

    public void LoadLevel(int i_LevelNum, LogicalBoard i_LogicalBoardModel)
    {
        this.m_currentLevelNumber = i_LevelNum;
        this.initTexts();
        mr_GameButtons.Init();
        mr_CloudsView.Init();
        mr_SparklingStarsView.ClearStars();
        this.m_initialCurrentLevelBoardModel = i_LogicalBoardModel;
        this.startLevel(i_LogicalBoardModel);
        Debug.Log("Level Loaded!!!");
    }

    public void ResetLevel ()
    {
        this.resetLevel(this.m_initialCurrentLevelBoardModel);
    }

    private void initPortal(LogicalBoard i_LogicalBoardModel)
    {
        mr_WinningPortal.gameObject.SetActive(true);
        BoardConfig initialBoardConfig = i_LogicalBoardModel.Config;
        ExitPosition portalPosition = initialBoardConfig.ExitPosition;
        WinningPortalView.ePortalTypes portalType = portalPosition.IsUpRight
                                                        ? WinningPortalView.ePortalTypes.Dark
                                                        : WinningPortalView.ePortalTypes.Light;

        mr_WinningPortal.PositionPortal(portalType, portalPosition.Row, portalPosition.Column);
    }

    public IEnumerator PerformMove(Result i_Result)
    {
        this.DisableControls();

        //Todo : perform move in view

        if (i_Result.ResultType == eResultTypes.SuccessfulMove)
        {
            if (i_Result.Move == Move.Flip)
            {
                this.switchState(i_Result.LogicalBoard);
            }
            else
            {
                yield return this.StartCoroutine(this.makeMove(i_Result));
            }
        }
        else
        {
            if (i_Result.ResultType == eResultTypes.InvalidMove)
            {
                Debug.Log("Invalid Move");
                this.StartCoroutine(mr_CharacterView.PlayInvalidMoveAnimation());

            }
                //Character Died
            else
            {
                Debug.Log("Character died");
                yield return this.StartCoroutine(this.makeMove(i_Result));
                yield return this.StartCoroutine(mr_CharacterView.DropCharacterToDeath());
                yield return 0;
                this.resetLevel(this.m_initialCurrentLevelBoardModel);
            }
        }

        this.enableControls();
        this.dispatchMovePerformedEvent();
    }

    private IEnumerator makeMove(Result i_Result)
    {
        Move currentMove = i_Result.Move;

        int newCharacterRowPosition = i_Result.LogicalBoard.CurrentPosition.Row;
        int newCharacterColPosition = i_Result.LogicalBoard.CurrentPosition.Column;

        yield return
            this.StartCoroutine(
                this.mr_CharacterView.MoveToPosition(newCharacterRowPosition, newCharacterColPosition, currentMove));
    }

    private void handleDirectionChange(GameCharacterView.eMovementOrientation i_NewMovementOrientation)
    {
        mr_GameButtons.ChangeJumpButtonOrientation(i_NewMovementOrientation);
    }

    private void startLevel(LogicalBoard i_LogicalBoardModel)
    {
        mr_BoardView.Reset();
        mr_BoardView.Init(i_LogicalBoardModel);
        this.resetLevel(i_LogicalBoardModel);
    }

    private void resetLevel(LogicalBoard i_LogicalBoardModel)
    {
        if (this.m_gameState == eGameStates.Dark)
        {
            mr_BoardView.SwitchState();
        }

        this.m_gameState = eGameStates.Light;
        BoardConfig currentBoardConfig = i_LogicalBoardModel.Config;
        this.mr_CharacterView.Init(currentBoardConfig.InitialPosition.Row, currentBoardConfig.InitialPosition.Column);
        this.initPortal(i_LogicalBoardModel);
        this.switchCameraToLightSide();
        this.handleDirectionChange(GameCharacterView.eMovementOrientation.Right);
        this.setBackLayerMode();
        Debug.Log("Level Restarted!!!");
    }

    private void switchState(LogicalBoard i_LogicalBoard)
    {
        //To Dark
        if (m_gameState == eGameStates.Light)
        {
            this.switchCameraToDarkSide();
            this.mr_CharacterView.SwitchToWimpiMode(
                i_LogicalBoard.CurrentPosition.Row, i_LogicalBoard.CurrentPosition.Column);
            this.m_gameState = eGameStates.Dark;
            mr_CloudsView.SwitchToDarkMode();
        }
            //To Light
        else
        {
            this.switchCameraToLightSide();
            this.mr_CharacterView.SwitchToMimpiMode(
                i_LogicalBoard.CurrentPosition.Row, i_LogicalBoard.CurrentPosition.Column);
            this.m_gameState = eGameStates.Light;
            mr_CloudsView.SwitchToLightMode();
        }

        this.setBackLayerMode();
        mr_BoardView.SwitchState();
    }

    private void setBackLayerMode()
    {
        if (this.m_gameState == eGameStates.Light)
        {
            mr_PurpleBackLayer.gameObject.SetActive(true);
            mr_GreyBackLayer.gameObject.SetActive(false);
        }
        else
        {
            mr_GreyBackLayer.gameObject.SetActive(true);
            mr_PurpleBackLayer.gameObject.SetActive(false);
        }
    }

    private void switchCameraToLightSide()
    {
        mr_GameCamera.transform.localPosition = new Vector3(0, 0, -376);
        mr_GameCamera.transform.localEulerAngles = new Vector3(2, -2, 0);
    }

    private void switchCameraToDarkSide()
    {
        mr_GameCamera.transform.localPosition = new Vector3(0, -8, -376);

        mr_GameCamera.transform.localEulerAngles = new Vector3(358, -2, 0);
    }

    private void dispatchMoveEvent(Move i_Move)
    {
        if (this.MoveButtonClicked != null)
        {
            Debug.Log("Firing button click");
            this.MoveButtonClicked(i_Move);
        }
    }

    private void dispatchWinTestEvent()
    {
        if (this.WinTest != null)
        {
            this.WinTest();
        }
    }

    private void dispatchMovePerformedEvent()
    {
        if (MovePerformed != null)
        {
            MovePerformed();
        }
    }

    public void TransitionToGame()
    {
        mr_BoardView.gameObject.SetActive(true);
        mr_CharacterView.gameObject.SetActive(true);
        mr_WinningPortal.gameObject.SetActive(true);

        this.enableControls();
        this.enableGameButtons();
    }

    public void TransitionToLevelsScreen()
    {
        mr_BoardView.gameObject.SetActive(false);
        mr_CharacterView.gameObject.SetActive(false);
        mr_WinningPortal.gameObject.SetActive(false);
    }

    private void enableGameButtons()
    {
        mr_GameButtons.gameObject.SetActive(true);
    }

    public void DisableGameButtons()
    {
        mr_GameButtons.gameObject.SetActive(false);
    }

    #region Buttons Handling

    public void FlipButtonClicked()
    {
        if (this.m_controlsEnabled)
        {
            Debug.Log("Try Flip!!!");
            this.dispatchMoveEvent(Move.Flip);
        }
    }

    public void MoveRightButtonClicked()
    {
        if (this.m_controlsEnabled)
        {
            Debug.Log("Try Move Right!!!");
            this.dispatchMoveEvent(Move.Right);
        }
    }

    public void MoveLeftButtonClicked()
    {
        if (this.m_controlsEnabled)
        {
            Debug.Log("Try Move Left!!!");
            this.dispatchMoveEvent(Move.Left);
        }
    }

    public void JumpButtonClicked()
    {
        if (this.m_controlsEnabled)
        {
            Debug.Log("Try Jump");
            this.dispatchMoveEvent(Move.Jump);
        }
    }

    public void ResetButtonClickedHandler()
    {
        Debug.Log("Restarting board.");
        if (this.ResetButtonClicked != null)
        {
            this.ResetButtonClicked();
        }

    }

    public void Update()
    {
        this.checkKeyboardPresses();

        //Win Test
        if (Input.GetKeyDown(KeyCode.W))
        {
            Debug.Log("Win Test!!!");
            this.dispatchWinTestEvent();
        }
    }

    private void checkKeyboardPresses()
    {
        if (this.m_controlsEnabled)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                Debug.Log("Try Flip!!!");
                this.dispatchMoveEvent(Move.Flip);
            }

            if (Input.GetKey(KeyCode.RightArrow))
            {
                Debug.Log("Try Move Right!!!");
                this.dispatchMoveEvent(Move.Right);
            }

            if (Input.GetKey(KeyCode.LeftArrow))
            {
                Debug.Log("Try Move Left!!!");
                this.dispatchMoveEvent(Move.Left);
            }

            if (Input.GetKey(KeyCode.UpArrow))
            {
                Debug.Log("Try Jump");
                this.dispatchMoveEvent(Move.Jump);
            }
        }
    }

    #endregion

    private void enableControls()
    {
        Debug.Log("Enabling controls.");
        this.m_controlsEnabled = true;
    }

    public void DisableControls()
    {
        Debug.Log("Disabling controls.");
        this.m_controlsEnabled = false;
    }
}
