﻿using UnityEngine;
using System.Collections;

public class GameCharacterAnimator : MonoBehaviour
{
    public GameObject mr_Mimpi;
    public GameObject mr_Wimpi;

    public Animator mr_Animator;

    private const float k_moveAnimationDuration = 0.5f;
    private const float k_jumpAnimationDuration = 0.5f;
    private const float k_dropAnimationDuration = 0.3f;
    private const float k_invalidMoveAnimationDuration = 0.15f;

    public void PlayIdleAnimation(GameCharacterView.eCharacterTypes i_CharacterType)
    {
        if (i_CharacterType == GameCharacterView.eCharacterTypes.Mimpi)
        {
            mr_Animator.Play("MimpiIdle");
        }
        else
        {
            mr_Animator.Play("WimpiIdle");
        }
    }

    public IEnumerator MoveCharacter(GameCharacterView.eCharacterTypes i_CharacterType, 
        GameCharacterView.eMovementOrientation i_Direction)
    {
        if (i_CharacterType == GameCharacterView.eCharacterTypes.Mimpi)
        {
            mr_Animator.Play("MimpiMove");
            
        }
        else
        {
            mr_Animator.Play("WimpiMove");
        }

        yield return this.StartCoroutine(this.move(i_CharacterType, i_Direction));
        this.PlayIdleAnimation(i_CharacterType);
    }


    public IEnumerator JumpCharacter(GameCharacterView.eCharacterTypes i_CharacterType,
        GameCharacterView.eMovementOrientation i_Direction, int i_NumOfSquaresToJump)
    {
        if (i_CharacterType == GameCharacterView.eCharacterTypes.Mimpi)
        {
            mr_Animator.Play("MimpiMove");

        }
        else
        {
            mr_Animator.Play("WimpiMove");
        }

        yield return this.StartCoroutine(this.jump(i_CharacterType, i_Direction, i_NumOfSquaresToJump));
        this.PlayIdleAnimation(i_CharacterType);
    }


    public IEnumerator DropCharacter(GameCharacterView.eCharacterTypes i_CharacterType,
        int i_NumOfSquaresToDrop)
    {
        if (i_CharacterType == GameCharacterView.eCharacterTypes.Mimpi)
        {
            mr_Animator.Play("MimpiFall");

        }
        else
        {
            mr_Animator.Play("WimpiFall");
        }

        yield return this.StartCoroutine(this.drop(i_CharacterType, i_NumOfSquaresToDrop));
        this.PlayIdleAnimation(i_CharacterType);
    }

    public IEnumerator PlayInvalidMoveAnimation(GameCharacterView.eCharacterTypes i_CharacterType)
    {
        if (i_CharacterType == GameCharacterView.eCharacterTypes.Mimpi)
        {
            mr_Animator.Play("MimpiInvalidMove");
        }
        else
        {
            mr_Animator.Play("WimpiInvalidMove");
        }

        yield return new WaitForSeconds(k_invalidMoveAnimationDuration);
        this.PlayIdleAnimation(i_CharacterType);
    }

    private IEnumerator move(GameCharacterView.eCharacterTypes i_CharacterType,
        GameCharacterView.eMovementOrientation i_Direction)
    {
        float boardCellSize = BoardCellView.k_BoardCellSize;

        GameObject characterToMove = mr_Mimpi;

        if (i_CharacterType == GameCharacterView.eCharacterTypes.Wimpi)
        {
            characterToMove = mr_Wimpi;
        }

        float xDirectionModifier = (int)i_Direction;

        float yDirectionModifier = (int)i_CharacterType;

        Hashtable ht = new Hashtable();

        Vector3[] path = new Vector3[3];
        Vector3 origin = characterToMove.transform.localPosition;
        float topX = origin.x + xDirectionModifier * boardCellSize / 2;
        float topY = origin.y + yDirectionModifier * boardCellSize / 2;
        Vector3 top = new Vector3(topX, topY, 0);
        Vector3 destination = origin + new Vector3(xDirectionModifier * boardCellSize, 0, 0);
        path[0] = origin;
        path[1] = top;
        path[2] = destination;

        ht.Add("path", path);
        ht.Add("time", k_moveAnimationDuration);
        ht.Add("easetype", "linear");
        ht.Add("islocal", true);
        ht.Add("movetopath", false);

        iTween.MoveTo(characterToMove.gameObject, ht);

        yield return new WaitForSeconds(k_moveAnimationDuration);
    }

    private IEnumerator jump(GameCharacterView.eCharacterTypes i_CharacterType,
        GameCharacterView.eMovementOrientation i_Direction, int i_NumOfSquaresToJump)
    {
        float boardCellSize = BoardCellView.k_BoardCellSize;

        GameObject characterToMove = mr_Mimpi;

        if (i_CharacterType == GameCharacterView.eCharacterTypes.Wimpi)
        {
            characterToMove = mr_Wimpi;
        }

        float xDirectionModifier = (int)i_Direction;

        float yDirectionModifier = (int)i_CharacterType;

        Hashtable ht = new Hashtable();

        Vector3[] path = new Vector3[3];
        Vector3 origin = characterToMove.transform.localPosition;
        float topX = origin.x + xDirectionModifier * boardCellSize / 2;
        float topY = origin.y + 2 * yDirectionModifier * boardCellSize / 2;
        Vector3 top = new Vector3(topX, topY, 0);
        Vector3 destination = origin + new Vector3(xDirectionModifier * i_NumOfSquaresToJump * boardCellSize,
            yDirectionModifier * boardCellSize , 0);
        path[0] = origin;
        path[1] = top;
        path[2] = destination;

        ht.Add("path", path);
        ht.Add("time", k_jumpAnimationDuration);
        ht.Add("easetype", "linear");
        ht.Add("islocal", true);
        ht.Add("movetopath", false);

        iTween.MoveTo(characterToMove.gameObject, ht);

        yield return new WaitForSeconds(k_jumpAnimationDuration);
    }

    private IEnumerator drop(GameCharacterView.eCharacterTypes i_CharacterType,
       int i_NumOfSquaresToDrop)
    {
        float boardCellSize = BoardCellView.k_BoardCellSize;

        GameObject characterToMove = mr_Mimpi;

        int characterDropModifier = -1;

        if (i_CharacterType == GameCharacterView.eCharacterTypes.Wimpi)
        {
            characterToMove = mr_Wimpi;
            characterDropModifier = 1;
        }

        Vector3 origin = characterToMove.transform.localPosition;
        Vector3 destination = origin + characterDropModifier * new Vector3(0, i_NumOfSquaresToDrop * boardCellSize, 0);


        Hashtable ht = new Hashtable();

        ht.Add("position", destination);
        ht.Add("time", k_dropAnimationDuration);
        ht.Add("easetype", "linear");
        ht.Add("islocal", true);
        ht.Add("movetopath", false);

        iTween.MoveTo(characterToMove.gameObject, ht);

        yield return new WaitForSeconds(k_dropAnimationDuration);
    }
}

