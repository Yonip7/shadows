﻿using ShadowsModel.Model;
using UnityEngine;
using System.Collections;

public class GameCharacterView : MonoBehaviour
{
    public delegate void CharacterChangedDirectionDelegate(eMovementOrientation i_NewDirection);
    public event CharacterChangedDirectionDelegate CharacterChangedDirection;

    public enum eCharacterTypes
    {
        Mimpi = 1,
        Wimpi = -1
    }

    public enum eMovementOrientation
    {
        Right = 1,
        Left = -1
    }

    public int mr_Row;
    public int mr_Col;

    public GameCharacterAnimator mr_CharacterAnimator;

    public GameObject mr_Mimpi;
    public GameObject mr_Wimpi;

    private eCharacterTypes m_currentCharacter;
    private eMovementOrientation m_currentMovementOrientation;

    private const int k_shortJumpNumOfSquares = 1;
    private const int k_longJumpNumOfSquares = 2;
    private const int k_deathDropNumOfSquares = 3;

    private const int k_firstBoardColumnIndex = 1;

    public void Init(int i_Row, int i_Col)
    {
        mr_Row = i_Row;
        mr_Col = i_Col;
        this.m_currentMovementOrientation = eMovementOrientation.Right;
        this.SwitchToMimpiMode(i_Row, i_Col);
        this.changeDirection(eMovementOrientation.Right);
    }

    public void SwitchToMimpiMode(int i_Row, int i_Col)
    {
        int boardCellSize = BoardCellView.k_BoardCellSize;

        mr_Row = i_Row;
        mr_Col = i_Col;

        mr_Mimpi.gameObject.SetActive(true);
        mr_Wimpi.gameObject.SetActive(false);

        mr_Mimpi.transform.localPosition = new Vector3(boardCellSize * (mr_Col - 1),
            boardCellSize * (mr_Row - 1), 0);

        this.m_currentCharacter = eCharacterTypes.Mimpi;
        mr_CharacterAnimator.PlayIdleAnimation(eCharacterTypes.Mimpi);
    }

    public void SwitchToWimpiMode(int i_Row, int i_Col)
    {
        int boardCellSize = BoardCellView.k_BoardCellSize;

        mr_Row = i_Row;
        mr_Col = i_Col;

        mr_Wimpi.gameObject.SetActive(true);
        mr_Mimpi.gameObject.SetActive(false);

        mr_Wimpi.transform.localPosition = new Vector3(boardCellSize * (mr_Col - 1),
           boardCellSize * (mr_Row - 2), 0);

        this.m_currentCharacter = eCharacterTypes.Wimpi;
        mr_CharacterAnimator.PlayIdleAnimation(eCharacterTypes.Wimpi);
    }

    public IEnumerator MoveToPosition(int i_NewRow, int i_NewCol, Move i_Move)
    {
        bool didChangeDirection = false;

        if (i_Move == Move.Right && this.m_currentMovementOrientation == eMovementOrientation.Left)
        {
            Debug.Log("Changed to right direction");
            this.changeDirection(eMovementOrientation.Right);
            this.dispatchCharacterChangedDirectionEvent();
            didChangeDirection = true;
        }

        if (i_Move == Move.Left && this.m_currentMovementOrientation == eMovementOrientation.Right)
        {
            Debug.Log("Changed to left direction");
            this.changeDirection(eMovementOrientation.Left);
            this.dispatchCharacterChangedDirectionEvent();
            didChangeDirection = true;
        }

        if (!didChangeDirection)
        {
            yield return this.StartCoroutine(this.moveToNewPosition(i_NewRow, i_NewCol, i_Move));
            //Debug.Log("Mimpi New Position: " + mr_Mimpi.transform.localPosition);
        }
    }

    private void changeDirection(eMovementOrientation i_MovementOrientation)
    {
        float originalMimpiXScale = Mathf.Abs(mr_Mimpi.transform.localScale.x);
        float originalMimpiYScale = mr_Mimpi.transform.localScale.y;
        float originalWimpiXScale = Mathf.Abs(mr_Wimpi.transform.localScale.x);
        float originalWimpiYScale = mr_Wimpi.transform.localScale.y;

        int direction = (int)eMovementOrientation.Left;
        this.m_currentMovementOrientation = eMovementOrientation.Left;

        switch (i_MovementOrientation)
        {
            case eMovementOrientation.Right:
                direction = (int)eMovementOrientation.Right;
                this.m_currentMovementOrientation = eMovementOrientation.Right;
                break;
        }

        mr_Mimpi.transform.localScale = new Vector3(direction * originalMimpiXScale
        , originalMimpiYScale, 1);
        mr_Wimpi.transform.localScale = new Vector3(direction *
        originalWimpiXScale, originalWimpiYScale, 1);
    }

    private IEnumerator moveToNewPosition(int i_NewRow, int i_NewCol, Move i_Move)
    {
        if (i_Move == Move.Jump)
        {
            yield return this.StartCoroutine(this.handleJumping(i_NewRow ,i_NewCol));
        }
        else
        {
            yield return this.StartCoroutine(this.handleMovement(i_NewRow, i_NewCol));
        }

        mr_Row = i_NewRow;
        mr_Col = i_NewCol;

//        mr_Mimpi.transform.localPosition = new Vector3(boardCellSize * (mr_Col - 1),
//            boardCellSize * (mr_Row - 1), 0);
//        
//        mr_Wimpi.transform.localPosition = new Vector3(boardCellSize * (mr_Col - 1),
//            boardCellSize * (mr_Row - 2), 0);
    }

    private IEnumerator handleMovement(int i_NewRow, int i_NewCol)
    {
        eMovementOrientation movementOrientation = eMovementOrientation.Right;

        if (i_NewCol < mr_Col)
        {
            movementOrientation = eMovementOrientation.Left;
        }

        if (i_NewCol == mr_Col)
        {
            movementOrientation = m_currentMovementOrientation;
        }

        yield return
                this.StartCoroutine(mr_CharacterAnimator.MoveCharacter
                    (this.m_currentCharacter, movementOrientation));

        yield return this.StartCoroutine(this.tryDrop(i_NewRow, false, false));
    }

    private IEnumerator handleJumping(int i_NewRow, int i_NewCol)
    {
        int numOfSquaresToJump = k_shortJumpNumOfSquares;

        bool isLongJump;
        bool isDecreasedJump = this.isDecreasedJump(i_NewRow, mr_Row);

        if (i_NewCol == mr_Col)
        {
            numOfSquaresToJump = k_longJumpNumOfSquares;
        }
        else
        {
            if (i_NewCol > mr_Col)
            {
                if (i_NewCol - mr_Col >= k_longJumpNumOfSquares)
                {
                    numOfSquaresToJump = k_longJumpNumOfSquares;
                }
            }
            else
            {
                if (mr_Col - i_NewCol >= k_longJumpNumOfSquares)
                {
                    numOfSquaresToJump = k_longJumpNumOfSquares;
                }
            }
        }

        yield return this.StartCoroutine(
                           mr_CharacterAnimator.JumpCharacter(this.m_currentCharacter,
                           this.m_currentMovementOrientation, numOfSquaresToJump));

        isLongJump = numOfSquaresToJump == k_longJumpNumOfSquares;

        yield return this.StartCoroutine(this.tryDrop(i_NewRow, isLongJump, isDecreasedJump));
    }

    private IEnumerator tryDrop(int i_NewRow, bool i_IsLongJump, bool i_IsDecreasedJump)
    {
        int numOfSquaresToDrop;
        int originalRow = mr_Row;

        if (this.m_currentCharacter == eCharacterTypes.Mimpi)
        {
            if (i_IsLongJump)
            {
                originalRow++;
            }
            else
            {
                if (i_IsDecreasedJump)
                {
                    originalRow++;
                }
            }

            numOfSquaresToDrop = originalRow - i_NewRow;
        }
        else
        {
            if (i_IsLongJump)
            {
                originalRow--;
            }
            else
            {
                if (i_IsDecreasedJump)
                {
                    originalRow--;
                }
            }

            numOfSquaresToDrop = i_NewRow - originalRow;
        }

        if (numOfSquaresToDrop > 0)
        {
            yield return
                this.StartCoroutine(
                    mr_CharacterAnimator.DropCharacter(this.m_currentCharacter, numOfSquaresToDrop));
        }
    }

    private bool isDecreasedJump(int i_CurrentRow, int i_OriginalRow)
    {
        bool isDecreasedJump;

        if (this.m_currentCharacter == eCharacterTypes.Mimpi)
        {
            isDecreasedJump = i_OriginalRow - i_CurrentRow >= 0;
        }
        else
        {
            isDecreasedJump = i_CurrentRow - i_OriginalRow >= 0;
        }

        return isDecreasedJump;
    }

    public IEnumerator PlayInvalidMoveAnimation()
    {
        yield return this.StartCoroutine(mr_CharacterAnimator.PlayInvalidMoveAnimation(this.m_currentCharacter));
    }

    public IEnumerator DropCharacterToDeath()
    {
        yield return
            this.StartCoroutine
                (mr_CharacterAnimator.DropCharacter(this.m_currentCharacter, k_deathDropNumOfSquares));
    }

    private void dispatchCharacterChangedDirectionEvent()
    {
       if (CharacterChangedDirection != null)
       {
           CharacterChangedDirection(this.m_currentMovementOrientation);
       }
    }
}
