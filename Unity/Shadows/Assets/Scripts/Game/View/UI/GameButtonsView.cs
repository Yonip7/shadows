﻿using UnityEngine;
using System.Collections;

public class GameButtonsView : MonoBehaviour
{
    public ArenaUIButton mr_MoveRightButton;

    public ArenaUIButton mr_MoveLeftButton;

    public ArenaUIButton mr_JumpButton;

    public ArenaUIButton mr_FlipButton;

    public ArenaUIButton mr_ResetButton;

    public void Init()
    {
        this.showButtons();
    }

    private void showButtons()
    {
        mr_MoveRightButton.gameObject.SetActive(true);
        mr_MoveLeftButton.gameObject.SetActive(true);
        mr_JumpButton.gameObject.SetActive(true);
        mr_FlipButton.gameObject.SetActive(true);
        mr_ResetButton.gameObject.SetActive(true);
    }

    private void hideButtons()
    {
        mr_MoveRightButton.gameObject.SetActive(false);
        mr_MoveLeftButton.gameObject.SetActive(false);
        mr_JumpButton.gameObject.SetActive(false);
        mr_FlipButton.gameObject.SetActive(false);
        mr_ResetButton.gameObject.SetActive(false);
    }

    public void ChangeJumpButtonOrientation(GameCharacterView.eMovementOrientation i_CharacterOrientation)
    {
        if (i_CharacterOrientation == GameCharacterView.eMovementOrientation.Right)
        {
            mr_JumpButton.transform.localScale = new Vector3(1, 1, 1);
        }
        else
        {
            mr_JumpButton.transform.localScale = new Vector3(-1, 1, 1);
        }
    }
}
