﻿using UnityEngine;

public class WinningPortalView : MonoBehaviour
{
    public enum ePortalTypes
    {
        Light,
        Dark
    }

    public GameObject mr_LightPortal;
    public GameObject mr_DarkPortal;

    public Animator mr_PortalAnimator;

    private ePortalTypes m_portalState;

    private const string k_portalLightIdleAnimation = "PortalLightIdleAnimation";
    private const string k_portalDarkIdleAnimation = "PortalDarkIdleAnimation";

    private const int k_portalOffsetFromMiddle = 10;

    public void PositionPortal(ePortalTypes i_PortalType,
        int i_BoardCellRow, int i_BoardCellColumn)
    {
        if (i_PortalType == ePortalTypes.Light)
        {
            this.switchToLightPortal();
        }
        else
        {
            this.switchToDarkPortal();
        }

        int boardCellSize = BoardCellView.k_BoardCellSize;

        mr_LightPortal.transform.localPosition = new Vector3(boardCellSize * (i_BoardCellColumn - 1),
            boardCellSize * (i_BoardCellRow - 1) + k_portalOffsetFromMiddle, 0);

        mr_DarkPortal.transform.localPosition = new Vector3(boardCellSize * (i_BoardCellColumn - 1),
           boardCellSize * (i_BoardCellRow - 2) - k_portalOffsetFromMiddle, 0);
    }

    private void switchToLightPortal()
    {
        mr_LightPortal.gameObject.SetActive(true);
        mr_DarkPortal.gameObject.SetActive(false);
        this.m_portalState = ePortalTypes.Light;
        this.playPortalIdleAnimation();
    }

    private void switchToDarkPortal()
    {
        mr_DarkPortal.gameObject.SetActive(true);
        mr_LightPortal.gameObject.SetActive(false);
        this.m_portalState = ePortalTypes.Dark;
        this.playPortalIdleAnimation();
    }

    private void playPortalIdleAnimation()
    {
        if (m_portalState == ePortalTypes.Light)
        {
            mr_PortalAnimator.Play(k_portalLightIdleAnimation);
        }
        else
        {
            mr_PortalAnimator.Play(k_portalDarkIdleAnimation);
        }
    }
}
