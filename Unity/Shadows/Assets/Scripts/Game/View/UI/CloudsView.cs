﻿using UnityEngine;
using System.Collections;

public class CloudsView : MonoBehaviour
{
    public GameObject mr_LightCloudsContainer;
    public GameObject mr_DarkCloudsContainer;

    public GameObject mr_LightBigCloud;
    public GameObject mr_LightMediumCloud;
    public GameObject mr_LightSmallCloud;
    public GameObject mr_LightTinyCloud;

    public GameObject mr_DarkBigCloud;
    public GameObject mr_DarkMediumCloud;
    public GameObject mr_DarkSmallCloud;
    public GameObject mr_DarkTinyCloud;

    private bool m_moveClouds;

    private const float k_distanceToMove = 1500f;

    public void Init()
    {
        this.m_moveClouds = true;
        this.SwitchToLightMode();
        this.StartCoroutine(this.moveClouds());
    }

    private IEnumerator moveClouds()
    {
        this.StartCoroutine(this.moveLightClouds());
        yield return this.StartCoroutine(this.moveDarkClouds());
    }

    private IEnumerator moveLightClouds()
    {
        this.StartCoroutine(this.moveCloud(mr_LightBigCloud, 20f));
        this.StartCoroutine(this.moveCloud(mr_LightMediumCloud, 17f));
        this.StartCoroutine(this.moveCloud(mr_LightSmallCloud, 15f));
        yield return this.StartCoroutine(this.moveCloud(mr_LightTinyCloud, 13f));
    }

    private IEnumerator moveDarkClouds()
    {
        this.StartCoroutine(this.moveCloud(mr_DarkBigCloud, 20f));
        this.StartCoroutine(this.moveCloud(mr_DarkMediumCloud, 17f));
        this.StartCoroutine(this.moveCloud(mr_DarkSmallCloud, 15f));
        yield return this.StartCoroutine(this.moveCloud(mr_DarkTinyCloud, 13f));
    }

    private IEnumerator moveCloud(GameObject i_Cloud, float i_Duration)
    {
        Vector3 startPosition = i_Cloud.gameObject.transform.localPosition;

        Vector3 destinationPosition = startPosition + new Vector3(-k_distanceToMove, 0, 0);

        while (m_moveClouds)
        {
            yield return
            this.StartCoroutine(
                GenericAnimator.Instance.MoveLocal(i_Cloud, startPosition, destinationPosition, i_Duration));

            startPosition = new Vector3(k_distanceToMove,
                i_Cloud.gameObject.transform.localPosition.y,
                i_Cloud.gameObject.transform.localPosition.z);

            destinationPosition = startPosition + new Vector3(-2 * k_distanceToMove, 0, 0);
        }
    }

    public void SwitchToLightMode()
    {
        mr_LightCloudsContainer.gameObject.SetActive(true);
        mr_DarkCloudsContainer.gameObject.SetActive(false);
    }

    public void SwitchToDarkMode()
    {
        mr_DarkCloudsContainer.gameObject.SetActive(true);
        mr_LightCloudsContainer.gameObject.SetActive(false);
    }
}
