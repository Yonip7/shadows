﻿using System.Collections.Generic;

using UnityEngine;
using System.Collections;

public class SparklingStarsView : MonoBehaviour
{
    public List<GameObject> mr_StarsPrefabs;

    private bool m_makeStarsSparkle;

    private List<GameObject> m_activeStars;

    private const float k_zeroAlpha = 0;
    private const float k_fullAlpha = 1;

    private const float k_fadeAnimationDuration = 1f;
    private const float k_fadeOutAnimationDuration = 2f;

    private const float k_minTimeTillNextSparkle = 1f;
    private const float k_maxTimeTillNextSparkle = 2f;

    private const float k_minStarVisibleTime = 1f;
    private const float k_maxStarVisibleTime = 1.5f;

    private const float k_minXPosition = 30f;
    private const float k_maxXPosition = 1250;

    private const float k_minYPosition = -720f;
    private const float k_maxYosition = 30f;

    private const int k_numOfStarsPerIteration = 5;
 
    public void Init()
    {
        this.m_makeStarsSparkle = true;
        this.m_activeStars = new List<GameObject>();
        this.StartCoroutine(this.initEnumerator());
    }

    private IEnumerator initEnumerator()
    {
        yield return this.StartCoroutine(this.Show());
        //yield return this.StartCoroutine(this.destroyStarsEnumerator());
    }

    public IEnumerator Show()
    {
        float randomTimeToWaitTillNextSparkle;
        int randomStarIndex;
        GameObject randomStarToDisplay;

        randomTimeToWaitTillNextSparkle = Random.Range(k_minTimeTillNextSparkle, k_maxTimeTillNextSparkle);
        
        randomStarIndex = Random.Range(0, mr_StarsPrefabs.Count);
        randomStarToDisplay = mr_StarsPrefabs[randomStarIndex];

        while (this.m_makeStarsSparkle)
        {
            for (int i = 0; i < k_numOfStarsPerIteration; i++)
            {
                this.StartCoroutine(this.createRandomStar(randomStarToDisplay));
            }

            yield return new WaitForSeconds(randomTimeToWaitTillNextSparkle);
            randomTimeToWaitTillNextSparkle = Random.Range(k_minTimeTillNextSparkle, k_maxTimeTillNextSparkle);

            randomStarIndex = Random.Range(0, mr_StarsPrefabs.Count);
            randomStarToDisplay = mr_StarsPrefabs[randomStarIndex];
        }
    }

    private IEnumerator createRandomStar(GameObject i_RandomStarToDisplay)
    {
        float randomXPosition = Random.Range(k_minXPosition, k_maxXPosition);
        float randomYPosition = Random.Range(k_minYPosition, k_maxYosition);
        float randomZPosition = 0;

        Vector3 randomStarPosition = new Vector3(randomXPosition, randomYPosition, randomZPosition);


        GameObject randomStar = Instantiate(i_RandomStarToDisplay) as GameObject;

        randomStar.transform.parent = this.transform;

        randomStar.transform.localPosition = randomStarPosition;

        randomStar.transform.name = "Star: " + this.m_activeStars.Count;

        this.m_activeStars.Add(randomStar);

        yield return this.StartCoroutine(GenericAnimator.Instance.Fade(randomStar,
            k_zeroAlpha, k_fullAlpha, k_fadeAnimationDuration));

        yield return this.StartCoroutine(GenericAnimator.Instance.Fade(randomStar,
                k_fullAlpha, k_zeroAlpha, k_fadeOutAnimationDuration));

        Destroy(randomStar.gameObject);
        m_activeStars.Remove(randomStar);
    }

    private IEnumerator destroyStarsEnumerator()
    {
        while (this.m_makeStarsSparkle)
        {
            float randomTimeForStarToRemain = Random.Range(k_minStarVisibleTime, k_maxStarVisibleTime);

            yield return new WaitForSeconds(randomTimeForStarToRemain);

            if (m_activeStars.Count > 0)
            {
                GameObject currentStarToFadeOut = m_activeStars[0];

                yield return this.StartCoroutine(GenericAnimator.Instance.Fade(currentStarToFadeOut,
                    k_fullAlpha, k_zeroAlpha, k_fadeOutAnimationDuration));

                Destroy(currentStarToFadeOut.gameObject);
                m_activeStars.Remove(currentStarToFadeOut);
            }
        }
    }

    public void ClearStars()
    {
        if (this.m_activeStars != null)
        {
            foreach (GameObject activeStar in m_activeStars)
            {
                Destroy(activeStar);
            }

            this.m_activeStars.Clear();
        }
    }
}
