﻿using ShadowsModel.Model;

using UnityEngine;

using strange.extensions.mediation.impl;

public class GameViewMediator : EventMediator
{
    [Inject]
    public GameView GameView { get; set; }

    [Inject]
    public GameInitCompletedSignal GameInitCompletedSignal { get; set; }

    [Inject]
    public GameLevelLoadedSignal GameLevelLoadedSignal { get; set; }

    [Inject]
    public GamePerformMoveSignal GamePerformMoveSignal { get; set; }

    [Inject]
    public GameMovePerformedInModelSignal GameMovePerformedInModelSignal { get; set; }

    [Inject]
    public GameMovePerformedInViewSignal GameMovePerformedInViewSignal { get; set; }

    [Inject]
    public MoveToGameSignal MoveToGameSignal { get; set; }

    [Inject]
    public MoveToSuccessPopupSignal MoveToSuccessPopupSignal { get; set; }

    [Inject]
    public SuccessLevelsButtonWasClickedSignal SuccessLevelsButtonWasClickedSignal { get; set; }

    [Inject]
    public GameResetClickedSignal ResetButtonClickedSignal { get; set; }

    //Win Test Signal
    [Inject]
    public GameWinTestSignal GameWinTestSignal { get; set; }

    [Inject]
    public GameRestartedInModelSignal GameRestartedInModel { get; set; }

    public override void OnRegister()
    {
        GameView.gameObject.SetActive(false);
        this.listenToModelEvents();
        this.listenToViewEvents();
    }

    public void RestartLevel()
    {
        this.GameView.ResetLevel();
    }

    private void onGameLevelLoaded(int i_LevelNum, LogicalBoard i_LogicalBoard)
    {
        GameView.LoadLevel(i_LevelNum, i_LogicalBoard);
    }

    private void onGameInitCompletion(LogicalBoard i_LogicalBoard)
    {
        GameView.Init(i_LogicalBoard);
    }

    private void onGameMovePerformedInModel(Result i_Result)
    {
        this.StartCoroutine(GameView.PerformMove(i_Result));
    }

    private void listenToViewEvents()
    {
        GameView.MoveButtonClicked += this.handleMoveButtonClicked;
        GameView.MovePerformed += this.handleViewMovePerformed;
        GameView.ResetButtonClicked += this.handlViewResetButtonClicked;

        //Win Test
        GameView.WinTest += this.handleWinTest;
    }

    private void handlViewResetButtonClicked()
    {
        this.ResetButtonClickedSignal.Dispatch();
    }

    private void handleWinTest()
    {
        GameWinTestSignal.Dispatch();
    }

    private void handleViewMovePerformed()
    {
        this.GameMovePerformedInViewSignal.Dispatch();
    }

    private void listenToModelEvents()
    {
        GameInitCompletedSignal.AddListener(this.onGameInitCompletion);
        GameLevelLoadedSignal.AddListener(this.onGameLevelLoaded);
        GameMovePerformedInModelSignal.AddListener(this.onGameMovePerformedInModel);
        MoveToGameSignal.AddListener(this.onMoveToGame);
        SuccessLevelsButtonWasClickedSignal.AddListener(this.onMoveToLevelsScreen);
        MoveToSuccessPopupSignal.AddListener(this.onMoveToSuccessPopup);
        GameRestartedInModel.AddListener(this.onGameRestarted);
    }

    private void onGameRestarted()
    {
        this.RestartLevel();
    }

    private void handleMoveButtonClicked(Move i_Move)
    {
        Debug.Log("Dispatching button click");
        GamePerformMoveSignal.Dispatch(i_Move);
    }

    private void onMoveToGame()
    {
        GameView.gameObject.SetActive(true);
        //todo: perform game view in animation here
        GameView.TransitionToGame();
    }

    private void onMoveToLevelsScreen()
    {
        GameView.gameObject.SetActive(false);
        //todo: perform game view out animation here
        GameView.TransitionToLevelsScreen();
    }

    private void onMoveToSuccessPopup()
    {
        GameView.DisableControls();
        GameView.DisableGameButtons();
    }
}
