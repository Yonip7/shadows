﻿using ShadowsModel.Helpers;
using ShadowsModel.Model;

using strange.extensions.command.impl;

public class GameMovePeformedInViewCommand : Command 
{
    [Inject]
    public ApplicationModel ApplicationModel { get; set; }

    [Inject]
    public IShadowsGameModel ShadowsGameModel { get; set;}

    [Inject]
    public ShadowsLevelsModel ShadowsLevelsModel { get; set; }

    public override void Execute()
    {
        //Todo: handle winning here

        if (this.victoryOccured())
        {
            ApplicationModel.MoveToSuccessPopup();
        }

        //this.moveToNextLevel();

        //Todo : display success popup
    }

    private bool victoryOccured()
    {
        bool victoryOccured = false;

        ShadowsGameModel gameModel = ShadowsGameModel as ShadowsGameModel;
        int characterRow = gameModel.CurrentLogicalBoard.CurrentPosition.Row;
        int characterColumn = gameModel.CurrentLogicalBoard.CurrentPosition.Column;

        int exitRow = gameModel.CurrentLogicalBoard.Config.ExitPosition.Row;
        int exitColumn = gameModel.CurrentLogicalBoard.Config.ExitPosition.Column;

        if (characterRow == exitRow && characterColumn == exitColumn)
        {
            victoryOccured = true;
        }

        return victoryOccured;
    }

    private void moveToNextLevel()
    {
        ShadowsLevelsModel.MoveToNextLevel();
        BoardConfig newConfig = LayoutParser.parse(ShadowsLevelsModel.GetCurrentLevel());

        LogicalBoard nextLevelLogicalBoard =
            new LogicalBoard(newConfig, newConfig.InitialPosition);
        ShadowsGameModel.LoadLevel(ShadowsLevelsModel.GetCurrentLevelIndex(), nextLevelLogicalBoard);
    }
}
