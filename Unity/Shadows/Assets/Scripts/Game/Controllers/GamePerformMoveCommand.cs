﻿using ShadowsModel.Model;

using strange.extensions.command.impl;

public class GamePerformMoveCommand : Command 
{
    [Inject]
    public Move CurrentMove { get; set; }

    [Inject]
    public IShadowsGameModel ShadowsGameModel { get; set;}

    public override void Execute()
    {
        ShadowsGameModel.ApplyMove(CurrentMove);
    }
}
