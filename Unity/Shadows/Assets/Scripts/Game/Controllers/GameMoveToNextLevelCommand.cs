﻿using System;

using ShadowsModel.Helpers;
using ShadowsModel.Model;

using UnityEngine;
using strange.extensions.command.impl;

public class GameMoveToNextLevelCommand : Command 
{
    [Inject]
    public ApplicationModel ApplicationModel { get; set; }

    [Inject]
    public ShadowsLevelsModel ShadowsLevelsModel { get; set; }

    [Inject]
    public IShadowsGameModel ShadowsGameModel { get; set; }

    public override void Execute()
    {
        ApplicationModel.MoveToGame();
        ShadowsLevelsModel.MoveToNextLevel();
        BoardConfig newConfig = LayoutParser.parse(ShadowsLevelsModel.GetCurrentLevel());

        LogicalBoard nextLevelLogicalBoard =
            new LogicalBoard(newConfig, newConfig.InitialPosition);
        ShadowsGameModel.LoadLevel(ShadowsLevelsModel.GetCurrentLevelIndex(), nextLevelLogicalBoard);
    }
}
