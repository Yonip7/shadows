﻿using ShadowsModel.Helpers;
using ShadowsModel.Model;

using strange.extensions.command.impl;

public class GameRestartCurrentLevelCommand : Command 
{
    [Inject]
    public ApplicationModel ApplicationModel { get; set; }

    [Inject]
    public ShadowsLevelsModel ShadowsLevelsModel { get; set; }

    [Inject]
    public IShadowsGameModel ShadowsGameModel { get; set; }

    public override void Execute()
    {
        ApplicationModel.MoveToGame();
        BoardConfig newConfig = LayoutParser.parse(ShadowsLevelsModel.GetCurrentLevel());

        LogicalBoard nextLevelLogicalBoard =
            new LogicalBoard(newConfig, newConfig.InitialPosition);
        ShadowsGameModel.LoadLevel(ShadowsLevelsModel.GetCurrentLevelIndex(), nextLevelLogicalBoard);
    }
}
