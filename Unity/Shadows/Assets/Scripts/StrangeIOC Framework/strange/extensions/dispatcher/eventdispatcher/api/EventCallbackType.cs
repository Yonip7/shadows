// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EventCallbackType.cs" company="">
//   
// </copyright>
// <summary>
//   The event callback type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace strange.extensions.dispatcher.eventdispatcher.api
{
    /// <summary>
    /// The event callback type.
    /// </summary>
    public enum EventCallbackType
    {
        /// <summary>
        /// The n o_ arguments.
        /// </summary>
        /// Indicates an EventCallback with no arguments
        NO_ARGUMENTS, 

        /// <summary>
        /// The on e_ argument.
        /// </summary>
        /// Indicates an EventCallback with one argument
        ONE_ARGUMENT, 

        /// <summary>
        /// The no t_ found.
        /// </summary>
        /// Indicates no matching EventCallback could be found
        NOT_FOUND
    }
}