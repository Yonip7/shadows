// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IEventDispatcher.cs" company="">
//   
// </copyright>
// <summary>
//   The EventDispatcher interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace strange.extensions.dispatcher.eventdispatcher.api
{
    using strange.extensions.dispatcher.api;

    /// <summary>
    /// The EventDispatcher interface.
    /// </summary>
    public interface IEventDispatcher : IDispatcher
    {
        #region Public Methods and Operators

        /// <summary>
        /// The add listener.
        /// </summary>
        /// <param name="evt">
        /// The evt.
        /// </param>
        /// <param name="callback">
        /// The callback.
        /// </param>
        /// Add an observer with exactly one argument to this Dispatcher
        void AddListener(object evt, EventCallback callback);

        /// <summary>
        /// The add listener.
        /// </summary>
        /// <param name="evt">
        /// The evt.
        /// </param>
        /// <param name="callback">
        /// The callback.
        /// </param>
        /// Add an observer with exactly no arguments to this Dispatcher
        void AddListener(object evt, EmptyCallback callback);

        /// <summary>
        /// The bind.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <returns>
        /// The <see cref="IEventBinding"/>.
        /// </returns>
        IEventBinding Bind(object key);

        /// <summary>
        /// The has listener.
        /// </summary>
        /// <param name="evt">
        /// The evt.
        /// </param>
        /// <param name="callback">
        /// The callback.
        /// </param>
        /// Returns true if the provided observer is already registered
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        bool HasListener(object evt, EventCallback callback);

        /// <summary>
        /// The has listener.
        /// </summary>
        /// <param name="evt">
        /// The evt.
        /// </param>
        /// <param name="callback">
        /// The callback.
        /// </param>
        /// Returns true if the provided observer is already registered
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        bool HasListener(object evt, EmptyCallback callback);

        /// <summary>
        /// The remove listener.
        /// </summary>
        /// <param name="evt">
        /// The evt.
        /// </param>
        /// <param name="callback">
        /// The callback.
        /// </param>
        /// Remove a previously registered observer with exactly one argument from this Dispatcher
        void RemoveListener(object evt, EventCallback callback);

        /// <summary>
        /// The remove listener.
        /// </summary>
        /// <param name="evt">
        /// The evt.
        /// </param>
        /// <param name="callback">
        /// The callback.
        /// </param>
        /// Remove a previously registered observer with exactly no arguments from this Dispatcher
        void RemoveListener(object evt, EmptyCallback callback);

        /// <summary>
        /// The update listener.
        /// </summary>
        /// <param name="toAdd">
        /// The to Add.
        /// </param>
        /// <param name="evt">
        /// The evt.
        /// </param>
        /// <param name="callback">
        /// The callback.
        /// </param>
        /// By passing true, an observer with exactly one argument will be added to this Dispatcher
        void UpdateListener(bool toAdd, object evt, EventCallback callback);

        /// <summary>
        /// The update listener.
        /// </summary>
        /// <param name="toAdd">
        /// The to Add.
        /// </param>
        /// <param name="evt">
        /// The evt.
        /// </param>
        /// <param name="callback">
        /// The callback.
        /// </param>
        /// By passing true, an observer with exactly no arguments will be added to this Dispatcher
        void UpdateListener(bool toAdd, object evt, EmptyCallback callback);

        #endregion
    }
}