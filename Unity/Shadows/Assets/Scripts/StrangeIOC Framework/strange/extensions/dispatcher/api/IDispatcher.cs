// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IDispatcher.cs" company="">
//   
// </copyright>
// <summary>
//   The Dispatcher interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace strange.extensions.dispatcher.api
{
    /// <summary>
    /// The Dispatcher interface.
    /// </summary>
    public interface IDispatcher
    {
        #region Public Methods and Operators

        /// <summary>
        /// The dispatch.
        /// </summary>
        /// <param name="eventType">
        /// The event Type.
        /// </param>
        /// Send a notification of type eventType. No data.
        /// In MVCSContext this dispatches an IEvent.
        void Dispatch(object eventType);

        /// <summary>
        /// The dispatch.
        /// </summary>
        /// <param name="eventType">
        /// The event Type.
        /// </param>
        /// <param name="data">
        /// The data.
        /// </param>
        /// Send a notification of type eventType and the provided data payload.
        /// In MVCSContext this dispatches an IEvent.
        void Dispatch(object eventType, object data);

        #endregion
    }
}