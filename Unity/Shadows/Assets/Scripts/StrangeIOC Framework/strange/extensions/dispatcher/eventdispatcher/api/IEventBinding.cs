// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IEventBinding.cs" company="">
//   
// </copyright>
// <summary>
//   The event callback.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace strange.extensions.dispatcher.eventdispatcher.api
{
    using strange.framework.api;

    /// <summary>
    /// The event callback.
    /// </summary>
    /// Delegate for adding a listener with a single argument
    public delegate void EventCallback(IEvent payload);

    /// <summary>
    /// The empty callback.
    /// </summary>
    /// Delegate for adding a listener with a no arguments
    public delegate void EmptyCallback();

    /// <summary>
    /// The EventBinding interface.
    /// </summary>
    public interface IEventBinding : IBinding
    {
        #region Public Methods and Operators

        /// <summary>
        /// The key.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <returns>
        /// The <see cref="IEventBinding"/>.
        /// </returns>
        new IEventBinding Key(object key);

        /// <summary>
        /// The to.
        /// </summary>
        /// <param name="callback">
        /// The callback.
        /// </param>
        /// <returns>
        /// The <see cref="IEventBinding"/>.
        /// </returns>
        IEventBinding To(EventCallback callback);

        /// <summary>
        /// The to.
        /// </summary>
        /// <param name="callback">
        /// The callback.
        /// </param>
        /// <returns>
        /// The <see cref="IEventBinding"/>.
        /// </returns>
        IEventBinding To(EmptyCallback callback);

        /// <summary>
        /// The type for callback.
        /// </summary>
        /// <param name="callback">
        /// The callback.
        /// </param>
        /// Retrieve the type of the provided callback
        /// <returns>
        /// The <see cref="EventCallbackType"/>.
        /// </returns>
        EventCallbackType typeForCallback(EventCallback callback);

        /// <summary>
        /// The type for callback.
        /// </summary>
        /// <param name="callback">
        /// The callback.
        /// </param>
        /// Retrieve the type of the provided callback
        /// <returns>
        /// The <see cref="EventCallbackType"/>.
        /// </returns>
        EventCallbackType typeForCallback(EmptyCallback callback);

        #endregion
    }
}