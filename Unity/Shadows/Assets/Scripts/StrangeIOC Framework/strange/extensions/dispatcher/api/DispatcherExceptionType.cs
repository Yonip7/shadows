// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DispatcherExceptionType.cs" company="">
//   
// </copyright>
// <summary>
//   The dispatcher exception type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace strange.extensions.dispatcher.api
{
    /// <summary>
    /// The dispatcher exception type.
    /// </summary>
    public enum DispatcherExceptionType
    {
        /// <summary>
        /// The nul l_ factory.
        /// </summary>
        /// Injector Factory not found
        NULL_FACTORY, 

        /// <summary>
        /// The illega l_ callbac k_ handler.
        /// </summary>
        /// Callback must be a Delegate with zero or one argument
        ILLEGAL_CALLBACK_HANDLER
    }
}