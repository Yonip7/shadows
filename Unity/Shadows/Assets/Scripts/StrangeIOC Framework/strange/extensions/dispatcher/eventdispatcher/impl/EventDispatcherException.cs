// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EventDispatcherException.cs" company="">
//   
// </copyright>
// <summary>
//   The event dispatcher exception.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace strange.extensions.dispatcher.eventdispatcher.impl
{
    using System;

    using strange.extensions.dispatcher.eventdispatcher.api;

    /// <summary>
    /// The event dispatcher exception.
    /// </summary>
    public class EventDispatcherException : Exception
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="EventDispatcherException"/> class.
        /// </summary>
        public EventDispatcherException()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="EventDispatcherException"/> class. 
        /// The event dispatcher exception.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        /// <param name="exceptionType">
        /// The exception Type.
        /// </param>
        /// Constructs an EventDispatcherException with a message and EventDispatcherExceptionType
        public EventDispatcherException(string message, EventDispatcherExceptionType exceptionType)
            : base(message)
        {
            this.type = exceptionType;
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        public EventDispatcherExceptionType type { get; set; }

        #endregion
    }
}