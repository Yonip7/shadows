// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EventBinding.cs" company="">
//   
// </copyright>
// <summary>
//   The event binding.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace strange.extensions.dispatcher.eventdispatcher.impl
{
    using System;
    using System.Collections.Generic;
    using System.Reflection;

    using strange.extensions.dispatcher.api;
    using strange.extensions.dispatcher.eventdispatcher.api;
    using strange.extensions.dispatcher.impl;
    using strange.framework.api;
    using strange.framework.impl;

    using Binder = strange.framework.impl.Binder;

    /// <summary>
    /// The event binding.
    /// </summary>
    public class EventBinding : Binding, IEventBinding
    {
        #region Fields

        /// <summary>
        /// The callback types.
        /// </summary>
        private Dictionary<Delegate, EventCallbackType> callbackTypes;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="EventBinding"/> class.
        /// </summary>
        public EventBinding()
            : this(null)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="EventBinding"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver.
        /// </param>
        public EventBinding(Binder.BindingResolver resolver)
            : base(resolver)
        {
            this.keyConstraint = BindingConstraintType.ONE;
            this.valueConstraint = BindingConstraintType.MANY;
            this.callbackTypes = new Dictionary<Delegate, EventCallbackType>();
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The key.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <returns>
        /// The <see cref="IEventBinding"/>.
        /// </returns>
        public new IEventBinding Key(object key)
        {
            return base.Key(key) as IEventBinding;
        }

        /// <summary>
        /// The remove value.
        /// </summary>
        /// <param name="value">
        /// The value.
        /// </param>
        public override void RemoveValue(object value)
        {
            base.RemoveValue(value);
            this.callbackTypes.Remove(value as Delegate);
        }

        /// <summary>
        /// The to.
        /// </summary>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <returns>
        /// The <see cref="IEventBinding"/>.
        /// </returns>
        public IEventBinding To(EventCallback value)
        {
            this.To(value);
            this.storeMethodType(value);
            return this;
        }

        /// <summary>
        /// The to.
        /// </summary>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <returns>
        /// The <see cref="IEventBinding"/>.
        /// </returns>
        public IEventBinding To(EmptyCallback value)
        {
            this.To(value);
            this.storeMethodType(value);
            return this;
        }

        /// <summary>
        /// The to.
        /// </summary>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <returns>
        /// The <see cref="IEventBinding"/>.
        /// </returns>
        public new IEventBinding To(object value)
        {
            this.To(value);
            this.storeMethodType(value as Delegate);
            return this;
        }

        /// <summary>
        /// The type for callback.
        /// </summary>
        /// <param name="callback">
        /// The callback.
        /// </param>
        /// <returns>
        /// The <see cref="EventCallbackType"/>.
        /// </returns>
        public EventCallbackType typeForCallback(EmptyCallback callback)
        {
            if (this.callbackTypes.ContainsKey(callback))
            {
                return this.callbackTypes[callback];
            }

            return EventCallbackType.NOT_FOUND;
        }

        /// <summary>
        /// The type for callback.
        /// </summary>
        /// <param name="callback">
        /// The callback.
        /// </param>
        /// <returns>
        /// The <see cref="EventCallbackType"/>.
        /// </returns>
        public EventCallbackType typeForCallback(EventCallback callback)
        {
            if (this.callbackTypes.ContainsKey(callback))
            {
                return this.callbackTypes[callback];
            }

            return EventCallbackType.NOT_FOUND;
        }

        #endregion

        #region Methods

        /// <summary>
        /// The store method type.
        /// </summary>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <exception cref="DispatcherException">
        /// </exception>
        private void storeMethodType(Delegate value)
        {
            if (value == null)
            {
                throw new DispatcherException(
                    "EventDispatcher can't map something that isn't a delegate'", 
                    DispatcherExceptionType.ILLEGAL_CALLBACK_HANDLER);
            }

            MethodInfo methodInfo = value.Method;
            int argsLen = methodInfo.GetParameters().Length;
            switch (argsLen)
            {
                case 0:
                    this.callbackTypes[value] = EventCallbackType.NO_ARGUMENTS;
                    break;
                case 1:
                    this.callbackTypes[value] = EventCallbackType.ONE_ARGUMENT;
                    break;
                default:
                    throw new DispatcherException(
                        "Event callbacks must have either one or no arguments", 
                        DispatcherExceptionType.ILLEGAL_CALLBACK_HANDLER);
            }
        }

        #endregion
    }
}