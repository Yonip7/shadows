// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EventDispatcher.cs" company="">
//   
// </copyright>
// <summary>
//   The event dispatcher.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace strange.extensions.dispatcher.eventdispatcher.impl
{
    using System;
    using System.Collections.Generic;

    using strange.extensions.dispatcher.api;
    using strange.extensions.dispatcher.eventdispatcher.api;
    using strange.framework.api;
    using strange.framework.impl;

    /// <summary>
    /// The event dispatcher.
    /// </summary>
    public class EventDispatcher : Binder, IEventDispatcher, ITriggerProvider, ITriggerable
    {
        #region Fields

        /// <summary>
        /// The is triggering clients.
        /// </summary>
        protected bool isTriggeringClients;

        /// <summary>
        /// The trigger client removals.
        /// </summary>
        protected HashSet<ITriggerable> triggerClientRemovals;

        /// <summary>
        /// The trigger clients.
        /// </summary>
        /// The list of clients that will be triggered as a consequence of an Event firing.
        protected HashSet<ITriggerable> triggerClients;

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets the triggerables.
        /// </summary>
        public int Triggerables
        {
            get
            {
                if (this.triggerClients == null)
                {
                    return 0;
                }

                return this.triggerClients.Count;
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The add listener.
        /// </summary>
        /// <param name="evt">
        /// The evt.
        /// </param>
        /// <param name="callback">
        /// The callback.
        /// </param>
        public void AddListener(object evt, EventCallback callback)
        {
            IBinding binding = this.GetBinding(evt);
            if (binding == null)
            {
                this.Bind(evt).To(callback);
            }
            else
            {
                binding.To(callback);
            }
        }

        /// <summary>
        /// The add listener.
        /// </summary>
        /// <param name="evt">
        /// The evt.
        /// </param>
        /// <param name="callback">
        /// The callback.
        /// </param>
        public void AddListener(object evt, EmptyCallback callback)
        {
            IBinding binding = this.GetBinding(evt);
            if (binding == null)
            {
                this.Bind(evt).To(callback);
            }
            else
            {
                binding.To(callback);
            }
        }

        /// <summary>
        /// The add triggerable.
        /// </summary>
        /// <param name="target">
        /// The target.
        /// </param>
        public void AddTriggerable(ITriggerable target)
        {
            if (this.triggerClients == null)
            {
                this.triggerClients = new HashSet<ITriggerable>();
            }

            this.triggerClients.Add(target);
        }

        /// <summary>
        /// The bind.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <returns>
        /// The <see cref="IEventBinding"/>.
        /// </returns>
        public new IEventBinding Bind(object key)
        {
            return base.Bind(key) as IEventBinding;
        }

        /// <summary>
        /// The dispatch.
        /// </summary>
        /// <param name="eventType">
        /// The event type.
        /// </param>
        public void Dispatch(object eventType)
        {
            this.Dispatch(eventType, null);
        }

        /// <summary>
        /// The dispatch.
        /// </summary>
        /// <param name="eventType">
        /// The event type.
        /// </param>
        /// <param name="data">
        /// The data.
        /// </param>
        /// <exception cref="EventDispatcherException">
        /// </exception>
        public void Dispatch(object eventType, object data)
        {
            // Scrub the data to make eventType and data conform if possible
            if (eventType == null)
            {
                throw new EventDispatcherException(
                    "Attempt to Dispatch to null.\ndata: " + data, EventDispatcherExceptionType.EVENT_KEY_NULL);
            }
            else if (eventType is IEvent)
            {
                // Client provided a full-formed event
                data = eventType;
                eventType = (data as IEvent).type;
            }
            else if (data == null)
            {
                // Client provided just an event ID. Create an event for injection
                data = new TmEvent(eventType, this, null);
            }
            else if (data is IEvent)
            {
                // Client provided both an evertType and a full-formed IEvent
                (data as IEvent).type = eventType;
            }
            else
            {
                // Client provided an eventType and some data which is not a IEvent.
                data = new TmEvent(eventType, this, data);
            }

            bool continueDispatch = true;
            if (this.triggerClients != null)
            {
                this.isTriggeringClients = true;
                foreach (ITriggerable trigger in this.triggerClients)
                {
                    if (!trigger.Trigger(eventType, data))
                    {
                        continueDispatch = false;
                    }
                }

                if (this.triggerClientRemovals != null)
                {
                    this.flushRemovals();
                }

                this.isTriggeringClients = false;
            }

            if (!continueDispatch)
            {
                return;
            }

            IEventBinding binding = this.GetBinding(eventType) as IEventBinding;
            if (binding == null)
            {
                return;
            }

            object[] callbacks = binding.value as object[];

            if (callbacks == null)
            {
                return;
            }

            int bb = callbacks.Length;
            for (int b = 0; b < bb; b++)
            {
                object callback = callbacks[b];
                object[] parameters = null;
                if (callback is EventCallback)
                {
                    parameters = new object[1];
                    parameters[0] = data;
                    EventCallback evtCb = callback as EventCallback;

                    try
                    {
                        evtCb(parameters[0] as IEvent);
                    }
                    catch (InvalidCastException)
                    {
                        object tgt = evtCb.Target;
                        string methodName = (callback as Delegate).Method.Name;
                        string message =
                            "An EventCallback is attempting an illegal cast. One possible reason is not typing the payload to IEvent in your callback. Another is illegal casting of the data.\nTarget class: "
                            + tgt + " method: " + methodName;
                        throw new EventDispatcherException(message, EventDispatcherExceptionType.TARGET_INVOCATION);
                    }
                }
                else if (callback is EmptyCallback)
                {
                    parameters = new object[0];
                    EmptyCallback emptyCb = callback as EmptyCallback;
                    emptyCb();
                }
            }
        }

        /// <summary>
        /// The get raw binding.
        /// </summary>
        /// <returns>
        /// The <see cref="IBinding"/>.
        /// </returns>
        public override IBinding GetRawBinding()
        {
            return new EventBinding(this.resolver);
        }

        /// <summary>
        /// The has listener.
        /// </summary>
        /// <param name="evt">
        /// The evt.
        /// </param>
        /// <param name="callback">
        /// The callback.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool HasListener(object evt, EventCallback callback)
        {
            IEventBinding binding = this.GetBinding(evt) as IEventBinding;
            if (binding == null)
            {
                return false;
            }

            return binding.typeForCallback(callback) != EventCallbackType.NOT_FOUND;
        }

        /// <summary>
        /// The has listener.
        /// </summary>
        /// <param name="evt">
        /// The evt.
        /// </param>
        /// <param name="callback">
        /// The callback.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool HasListener(object evt, EmptyCallback callback)
        {
            IEventBinding binding = this.GetBinding(evt) as IEventBinding;
            if (binding == null)
            {
                return false;
            }

            return binding.typeForCallback(callback) != EventCallbackType.NOT_FOUND;
        }

        /// <summary>
        /// The remove listener.
        /// </summary>
        /// <param name="evt">
        /// The evt.
        /// </param>
        /// <param name="callback">
        /// The callback.
        /// </param>
        public void RemoveListener(object evt, EventCallback callback)
        {
            IBinding binding = this.GetBinding(evt);
            this.RemoveValue(binding, callback);
        }

        /// <summary>
        /// The remove listener.
        /// </summary>
        /// <param name="evt">
        /// The evt.
        /// </param>
        /// <param name="callback">
        /// The callback.
        /// </param>
        public void RemoveListener(object evt, EmptyCallback callback)
        {
            IBinding binding = this.GetBinding(evt);
            this.RemoveValue(binding, callback);
        }

        /// <summary>
        /// The remove triggerable.
        /// </summary>
        /// <param name="target">
        /// The target.
        /// </param>
        public void RemoveTriggerable(ITriggerable target)
        {
            if (this.triggerClients.Contains(target))
            {
                if (this.triggerClientRemovals == null)
                {
                    this.triggerClientRemovals = new HashSet<ITriggerable>();
                }

                this.triggerClientRemovals.Add(target);
                if (!this.isTriggeringClients)
                {
                    this.flushRemovals();
                }
            }
        }

        /// <summary>
        /// The trigger.
        /// </summary>
        /// <param name="data">
        /// The data.
        /// </param>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool Trigger<T>(object data)
        {
            return this.Trigger(typeof(T), data);
        }

        /// <summary>
        /// The trigger.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <param name="data">
        /// The data.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool Trigger(object key, object data)
        {
            this.Dispatch(key, data);
            return true;
        }

        /// <summary>
        /// The update listener.
        /// </summary>
        /// <param name="toAdd">
        /// The to add.
        /// </param>
        /// <param name="evt">
        /// The evt.
        /// </param>
        /// <param name="callback">
        /// The callback.
        /// </param>
        public void UpdateListener(bool toAdd, object evt, EventCallback callback)
        {
            if (toAdd)
            {
                AddListener(evt, callback);
            }
            else
            {
                RemoveListener(evt, callback);
            }
        }

        /// <summary>
        /// The update listener.
        /// </summary>
        /// <param name="toAdd">
        /// The to add.
        /// </param>
        /// <param name="evt">
        /// The evt.
        /// </param>
        /// <param name="callback">
        /// The callback.
        /// </param>
        public void UpdateListener(bool toAdd, object evt, EmptyCallback callback)
        {
            if (toAdd)
            {
                AddListener(evt, callback);
            }
            else
            {
                RemoveListener(evt, callback);
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// The flush removals.
        /// </summary>
        protected void flushRemovals()
        {
            if (this.triggerClientRemovals == null)
            {
                return;
            }

            foreach (ITriggerable target in this.triggerClientRemovals)
            {
                if (this.triggerClients.Contains(target))
                {
                    this.triggerClients.Remove(target);
                }
            }

            this.triggerClientRemovals = null;
        }

        #endregion
    }
}