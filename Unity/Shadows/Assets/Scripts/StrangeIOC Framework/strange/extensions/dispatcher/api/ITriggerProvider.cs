// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ITriggerProvider.cs" company="">
//   
// </copyright>
// <summary>
//   The TriggerProvider interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace strange.extensions.dispatcher.api
{
    /// <summary>
    /// The TriggerProvider interface.
    /// </summary>
    public interface ITriggerProvider
    {
        #region Public Properties

        /// <summary>
        /// The triggerables.
        /// </summary>
        /// Count of the current number of trigger clients.
        int Triggerables { get; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The add triggerable.
        /// </summary>
        /// <param name="target">
        /// The target.
        /// </param>
        /// Register a Triggerable client with this provider.
        void AddTriggerable(ITriggerable target);

        /// <summary>
        /// The remove triggerable.
        /// </summary>
        /// <param name="target">
        /// The target.
        /// </param>
        /// Remove a previously registered Triggerable client from this provider.
        void RemoveTriggerable(ITriggerable target);

        #endregion
    }
}