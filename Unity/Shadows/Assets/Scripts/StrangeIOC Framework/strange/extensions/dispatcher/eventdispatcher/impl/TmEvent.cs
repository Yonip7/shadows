// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TmEvent.cs" company="">
//   
// </copyright>
// <summary>
//   The tm event.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace strange.extensions.dispatcher.eventdispatcher.impl
{
    using strange.extensions.dispatcher.eventdispatcher.api;

    /// <summary>
    /// The tm event.
    /// </summary>
    public class TmEvent : IEvent
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="TmEvent"/> class. 
        /// The tm event.
        /// </summary>
        /// <param name="type">
        /// The type.
        /// </param>
        /// <param name="target">
        /// The target.
        /// </param>
        /// <param name="data">
        /// The data.
        /// </param>
        /// Construct a TmEvent
        public TmEvent(object type, IEventDispatcher target, object data)
        {
            this.type = type;
            this.target = target;
            this.data = data;
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets the data.
        /// </summary>
        public object data { get; set; }

        /// <summary>
        /// Gets or sets the target.
        /// </summary>
        public IEventDispatcher target { get; set; }

        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        public object type { get; set; }

        #endregion
    }
}