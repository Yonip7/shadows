// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ITriggerable.cs" company="">
//   
// </copyright>
// <summary>
//   The Triggerable interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace strange.extensions.dispatcher.api
{
    /// <summary>
    /// The Triggerable interface.
    /// </summary>
    public interface ITriggerable
    {
        #region Public Methods and Operators

        /// <summary>
        /// The trigger.
        /// </summary>
        /// <param name="data">
        /// The data.
        /// </param>
        /// Cause this ITriggerable to access any provided Key in its Binder by the provided generic and data.
        /// <returns>
        /// false if the originator should abort dispatch
        /// </returns>
        bool Trigger<T>(object data);

        /// <summary>
        /// The trigger.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <param name="data">
        /// The data.
        /// </param>
        /// Cause this ITriggerable to access any provided Key in its Binder by the provided key and data.
        /// <returns>
        /// false if the originator should abort dispatch
        /// </returns>
        bool Trigger(object key, object data);

        #endregion
    }
}