// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IEvent.cs" company="">
//   
// </copyright>
// <summary>
//   The Event interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace strange.extensions.dispatcher.eventdispatcher.api
{
    /// <summary>
    /// The Event interface.
    /// </summary>
    public interface IEvent
    {
        #region Public Properties

        /// <summary>
        /// The data.
        /// </summary>
        /// An arbitrary data payloaf
        object data { get; set; }

        /// <summary>
        /// The target.
        /// </summary>
        /// The IEventDispatcher that sent the event
        IEventDispatcher target { get; set; }

        /// <summary>
        /// The type.
        /// </summary>
        /// The Event key
        object type { get; set; }

        #endregion
    }
}