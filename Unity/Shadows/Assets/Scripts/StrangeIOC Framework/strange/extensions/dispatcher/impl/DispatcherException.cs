// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DispatcherException.cs" company="">
//   
// </copyright>
// <summary>
//   The dispatcher exception.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace strange.extensions.dispatcher.impl
{
    using System;

    using strange.extensions.dispatcher.api;

    /// <summary>
    /// The dispatcher exception.
    /// </summary>
    public class DispatcherException : Exception
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="DispatcherException"/> class.
        /// </summary>
        public DispatcherException()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DispatcherException"/> class. 
        /// The dispatcher exception.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        /// <param name="exceptionType">
        /// The exception Type.
        /// </param>
        /// Constructs a DispatcherException with a message and DispatcherExceptionType
        public DispatcherException(string message, DispatcherExceptionType exceptionType)
            : base(message)
        {
            this.type = exceptionType;
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        public DispatcherExceptionType type { get; set; }

        #endregion
    }
}