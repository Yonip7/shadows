// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EventDispatcherExceptionType.cs" company="">
//   
// </copyright>
// <summary>
//   The event dispatcher exception type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace strange.extensions.dispatcher.eventdispatcher.api
{
    /// <summary>
    /// The event dispatcher exception type.
    /// </summary>
    public enum EventDispatcherExceptionType
    {
        /// <summary>
        /// The even t_ ke y_ null.
        /// </summary>
        /// Indicates that an event was fired with null as the key.
        EVENT_KEY_NULL, 

        /// <summary>
        /// The even t_ typ e_ mismatch.
        /// </summary>
        /// Indicates that the type of Event in the call and the type of Event in the payload don't match.
        EVENT_TYPE_MISMATCH, 

        /// <summary>
        /// The targe t_ invocation.
        /// </summary>
        /// When attempting to fire a callback, the callback was discovered to be casting illegally.
        TARGET_INVOCATION
    }
}