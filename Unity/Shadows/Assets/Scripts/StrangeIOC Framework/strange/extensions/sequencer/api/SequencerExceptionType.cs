// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SequencerExceptionType.cs" company="">
//   
// </copyright>
// <summary>
//   The sequencer exception type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace strange.extensions.sequencer.api
{
    /// <summary>
    /// The sequencer exception type.
    /// </summary>
    public enum SequencerExceptionType
    {
        /// <summary>
        /// The execut e_ override.
        /// </summary>
        /// SequenceCommands must always override the Execute() method.
        EXECUTE_OVERRIDE, 

        /// <summary>
        /// The comman d_ use d_ i n_ sequence.
        /// </summary>
        /// This exception is raised if the mapped Command doesn't implement ISequenceCommand.
        COMMAND_USED_IN_SEQUENCE
    }
}