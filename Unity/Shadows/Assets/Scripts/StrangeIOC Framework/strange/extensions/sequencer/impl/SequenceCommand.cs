// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SequenceCommand.cs" company="">
//   
// </copyright>
// <summary>
//   The sequence command.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace strange.extensions.sequencer.impl
{
    using strange.extensions.command.impl;
    using strange.extensions.sequencer.api;

    /// <summary>
    /// The sequence command.
    /// </summary>
    public class SequenceCommand : Command, ISequenceCommand
    {
        #region Public Properties

        /// <summary>
        /// Gets or sets the sequencer.
        /// </summary>
        [Inject]
        public ISequencer sequencer { get; set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The execute.
        /// </summary>
        /// <exception cref="SequencerException">
        /// </exception>
        public new virtual void Execute()
        {
            throw new SequencerException(
                "You must override the Execute method in every SequenceCommand", SequencerExceptionType.EXECUTE_OVERRIDE);
        }

        /// <summary>
        /// The fail.
        /// </summary>
        public new void Fail()
        {
            if (this.sequencer != null)
            {
                this.sequencer.Stop(this);
            }
        }

        /// <summary>
        /// The release.
        /// </summary>
        public new void Release()
        {
            this._retain = false;
            if (this.sequencer != null)
            {
                this.sequencer.ReleaseCommand(this);
            }
        }

        #endregion
    }
}