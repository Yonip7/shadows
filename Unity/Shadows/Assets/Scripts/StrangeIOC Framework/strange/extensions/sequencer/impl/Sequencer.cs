// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Sequencer.cs" company="">
//   
// </copyright>
// <summary>
//   The sequencer.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace strange.extensions.sequencer.impl
{
    using System;

    using strange.extensions.command.impl;
    using strange.extensions.dispatcher.api;
    using strange.extensions.injector.api;
    using strange.extensions.sequencer.api;
    using strange.framework.api;

    /// <summary>
    /// The sequencer.
    /// </summary>
    public class Sequencer : CommandBinder, ISequencer, ITriggerable
    {
        #region Public Properties

        /// <summary>
        /// Gets or sets the injection binder.
        /// </summary>
        [Inject]
        public new IInjectionBinder injectionBinder { get; set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The bind.
        /// </summary>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// The <see cref="ISequenceBinding"/>.
        /// </returns>
        public new virtual ISequenceBinding Bind<T>()
        {
            return base.Bind<T>() as ISequenceBinding;
        }

        /// <summary>
        /// The bind.
        /// </summary>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <returns>
        /// The <see cref="ISequenceBinding"/>.
        /// </returns>
        public new virtual ISequenceBinding Bind(object value)
        {
            return base.Bind(value) as ISequenceBinding;
        }

        /// <summary>
        /// The get raw binding.
        /// </summary>
        /// <returns>
        /// The <see cref="IBinding"/>.
        /// </returns>
        public override IBinding GetRawBinding()
        {
            return new SequenceBinding(this.resolver);
        }

        /// <summary>
        /// The react to.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <param name="data">
        /// The data.
        /// </param>
        public override void ReactTo(object key, object data)
        {
            ISequenceBinding binding = this.GetBinding(key) as ISequenceBinding;
            if (binding != null)
            {
                this.nextInSequence(binding, data, 0);
            }
        }

        /// <summary>
        /// The release command.
        /// </summary>
        /// <param name="command">
        /// The command.
        /// </param>
        public void ReleaseCommand(ISequenceCommand command)
        {
            if (command.retain == false)
            {
                if (this.activeSequences.ContainsKey(command))
                {
                    ISequenceBinding binding = this.activeSequences[command] as ISequenceBinding;
                    object data = command.data;
                    this.activeSequences.Remove(command);
                    this.nextInSequence(binding, data, command.sequenceId + 1);
                }
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// The create command.
        /// </summary>
        /// <param name="cmd">
        /// The cmd.
        /// </param>
        /// <param name="data">
        /// The data.
        /// </param>
        /// Instantiate and Inject the ISequenceCommand.
        /// <returns>
        /// The <see cref="ISequenceCommand"/>.
        /// </returns>
        protected new virtual ISequenceCommand createCommand(object cmd, object data)
        {
            this.injectionBinder.Bind<ISequenceCommand>().To(cmd);
            ISequenceCommand command = this.injectionBinder.GetInstance<ISequenceCommand>() as ISequenceCommand;
            command.data = data;
            this.injectionBinder.Unbind<ISequenceCommand>();
            return command;
        }

        /// <summary>
        /// The execute command.
        /// </summary>
        /// <param name="command">
        /// The command.
        /// </param>
        private void executeCommand(ISequenceCommand command)
        {
            if (command == null)
            {
                return;
            }

            command.Execute();
        }

        /// <summary>
        /// The fail if.
        /// </summary>
        /// <param name="condition">
        /// The condition.
        /// </param>
        /// <param name="message">
        /// The message.
        /// </param>
        /// <param name="type">
        /// The type.
        /// </param>
        /// <exception cref="SequencerException">
        /// </exception>
        private void failIf(bool condition, string message, SequencerExceptionType type)
        {
            if (condition)
            {
                throw new SequencerException(message, type);
            }
        }

        /// <summary>
        /// The invoke command.
        /// </summary>
        /// <param name="cmd">
        /// The cmd.
        /// </param>
        /// <param name="binding">
        /// The binding.
        /// </param>
        /// <param name="data">
        /// The data.
        /// </param>
        /// <param name="depth">
        /// The depth.
        /// </param>
        private void invokeCommand(Type cmd, ISequenceBinding binding, object data, int depth)
        {
            ISequenceCommand command = this.createCommand(cmd, data);
            command.sequenceId = depth;
            trackCommand(command, binding);
            executeCommand(command);
            ReleaseCommand(command);
        }

        /// <summary>
        /// The next in sequence.
        /// </summary>
        /// <param name="binding">
        /// The binding.
        /// </param>
        /// <param name="data">
        /// The data.
        /// </param>
        /// <param name="depth">
        /// The depth.
        /// </param>
        private void nextInSequence(ISequenceBinding binding, object data, int depth)
        {
            object[] values = binding.value as object[];
            if (depth < values.Length)
            {
                Type cmd = values[depth] as Type;
                invokeCommand(cmd, binding, data, depth);
            }
            else
            {
                if (binding.isOneOff)
                {
                    Unbind(binding);
                }
            }
        }

        /// <summary>
        /// The remove sequence.
        /// </summary>
        /// <param name="command">
        /// The command.
        /// </param>
        private void removeSequence(ISequenceCommand command)
        {
            if (this.activeSequences.ContainsKey(command))
            {
                command.Cancel();
                this.activeSequences.Remove(command);
            }
        }

        /// <summary>
        /// The track command.
        /// </summary>
        /// <param name="command">
        /// The command.
        /// </param>
        /// <param name="binding">
        /// The binding.
        /// </param>
        private void trackCommand(ISequenceCommand command, ISequenceBinding binding)
        {
            this.activeSequences[command] = binding;
        }

        #endregion
    }
}