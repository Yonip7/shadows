// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SequenceBinding.cs" company="">
//   
// </copyright>
// <summary>
//   The sequence binding.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace strange.extensions.sequencer.impl
{
    using System;

    using strange.extensions.command.impl;
    using strange.extensions.sequencer.api;
    using strange.framework.impl;

    /// <summary>
    /// The sequence binding.
    /// </summary>
    public class SequenceBinding : CommandBinding, ISequenceBinding
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="SequenceBinding"/> class.
        /// </summary>
        public SequenceBinding()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SequenceBinding"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver.
        /// </param>
        public SequenceBinding(Binder.BindingResolver resolver)
            : base(resolver)
        {
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets a value indicating whether is one off.
        /// </summary>
        public new bool isOneOff { get; set; }

        #endregion

        // Everything below this point is simply facade on Binding to ensure fluent interface
        #region Public Methods and Operators

        /// <summary>
        /// The bind.
        /// </summary>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// The <see cref="ISequenceBinding"/>.
        /// </returns>
        public new ISequenceBinding Bind<T>()
        {
            return this.Key<T>();
        }

        /// <summary>
        /// The bind.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <returns>
        /// The <see cref="ISequenceBinding"/>.
        /// </returns>
        public new ISequenceBinding Bind(object key)
        {
            return this.Key(key);
        }

        /// <summary>
        /// The key.
        /// </summary>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// The <see cref="ISequenceBinding"/>.
        /// </returns>
        public new ISequenceBinding Key<T>()
        {
            return base.Key<T>() as ISequenceBinding;
        }

        /// <summary>
        /// The key.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <returns>
        /// The <see cref="ISequenceBinding"/>.
        /// </returns>
        public new ISequenceBinding Key(object key)
        {
            return base.Key(key) as ISequenceBinding;
        }

        /// <summary>
        /// The named.
        /// </summary>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// The <see cref="ISequenceBinding"/>.
        /// </returns>
        public new ISequenceBinding Named<T>()
        {
            return base.Named<T>() as ISequenceBinding;
        }

        /// <summary>
        /// The named.
        /// </summary>
        /// <param name="o">
        /// The o.
        /// </param>
        /// <returns>
        /// The <see cref="ISequenceBinding"/>.
        /// </returns>
        public new ISequenceBinding Named(object o)
        {
            return base.Named(o) as ISequenceBinding;
        }

        /// <summary>
        /// The once.
        /// </summary>
        /// <returns>
        /// The <see cref="ISequenceBinding"/>.
        /// </returns>
        public new ISequenceBinding Once()
        {
            this.isOneOff = true;
            return this;
        }

        /// <summary>
        /// The to.
        /// </summary>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// The <see cref="ISequenceBinding"/>.
        /// </returns>
        public new ISequenceBinding To<T>()
        {
            return this.To(typeof(T));
        }

        /// <summary>
        /// The to.
        /// </summary>
        /// <param name="o">
        /// The o.
        /// </param>
        /// <returns>
        /// The <see cref="ISequenceBinding"/>.
        /// </returns>
        /// <exception cref="SequencerException">
        /// </exception>
        public new ISequenceBinding To(object o)
        {
            Type oType = o as Type;
            Type sType = typeof(ISequenceCommand);

            if (sType.IsAssignableFrom(oType) == false)
            {
                throw new SequencerException(
                    "Attempt to bind a non SequenceCommand to a Sequence. Perhaps your command needs to extend SequenceCommand or implement ISequenCommand?\n\tType: "
                    + oType, 
                    SequencerExceptionType.COMMAND_USED_IN_SEQUENCE);
            }

            return base.To(o) as ISequenceBinding;
        }

        /// <summary>
        /// The to name.
        /// </summary>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// The <see cref="ISequenceBinding"/>.
        /// </returns>
        public new ISequenceBinding ToName<T>()
        {
            return base.ToName<T>() as ISequenceBinding;
        }

        /// <summary>
        /// The to name.
        /// </summary>
        /// <param name="o">
        /// The o.
        /// </param>
        /// <returns>
        /// The <see cref="ISequenceBinding"/>.
        /// </returns>
        public new ISequenceBinding ToName(object o)
        {
            return base.ToName(o) as ISequenceBinding;
        }

        #endregion
    }
}