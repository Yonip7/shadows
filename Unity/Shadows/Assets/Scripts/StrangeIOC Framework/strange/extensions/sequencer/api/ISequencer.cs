// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ISequencer.cs" company="">
//   
// </copyright>
// <summary>
//   The Sequencer interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace strange.extensions.sequencer.api
{
    using strange.extensions.command.api;

    /// <summary>
    /// The Sequencer interface.
    /// </summary>
    public interface ISequencer : ICommandBinder
    {
        #region Public Methods and Operators

        /// <summary>
        /// The bind.
        /// </summary>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// The <see cref="ISequenceBinding"/>.
        /// </returns>
        new ISequenceBinding Bind<T>();

        /// <summary>
        /// The bind.
        /// </summary>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <returns>
        /// The <see cref="ISequenceBinding"/>.
        /// </returns>
        new ISequenceBinding Bind(object value);

        /// <summary>
        /// The release command.
        /// </summary>
        /// <param name="command">
        /// The command.
        /// </param>
        /// Release a previously retained SequenceCommand.
        /// By default, a Command is garbage collected at the end of its `Execute()` method. 
        /// But the Command can be retained for asynchronous calls.
        void ReleaseCommand(ISequenceCommand command);

        #endregion
    }
}