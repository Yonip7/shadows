// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EventSequencer.cs" company="">
//   
// </copyright>
// <summary>
//   The event sequencer.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace strange.extensions.sequencer.impl
{
    using strange.extensions.dispatcher.eventdispatcher.api;
    using strange.extensions.sequencer.api;

    /// <summary>
    /// The event sequencer.
    /// </summary>
    public class EventSequencer : Sequencer
    {
        #region Methods

        /// <summary>
        /// The create command.
        /// </summary>
        /// <param name="cmd">
        /// The cmd.
        /// </param>
        /// <param name="data">
        /// The data.
        /// </param>
        /// Instantiate and Inject the command, incling an IEvent to data.
        /// <returns>
        /// The <see cref="ISequenceCommand"/>.
        /// </returns>
        protected override ISequenceCommand createCommand(object cmd, object data)
        {
            this.injectionBinder.Bind<ISequenceCommand>().To(cmd);
            if (data is IEvent)
            {
                this.injectionBinder.Bind<IEvent>().ToValue(data).ToInject(false);
                
            }

            ISequenceCommand command = this.injectionBinder.GetInstance<ISequenceCommand>() as ISequenceCommand;
            command.data = data;
            if (data is IEvent)
            {
                this.injectionBinder.Unbind<IEvent>();
            }

            this.injectionBinder.Unbind<ISequenceCommand>();
            return command;
        }

        #endregion
    }
}