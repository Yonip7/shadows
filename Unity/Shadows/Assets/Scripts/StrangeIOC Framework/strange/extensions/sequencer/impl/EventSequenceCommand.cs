// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EventSequenceCommand.cs" company="">
//   
// </copyright>
// <summary>
//   The event sequence command.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace strange.extensions.sequencer.impl
{
    using strange.extensions.context.api;
    using strange.extensions.dispatcher.eventdispatcher.api;

    /// <summary>
    /// The event sequence command.
    /// </summary>
    public class EventSequenceCommand : SequenceCommand
    {
        #region Public Properties

        /// <summary>
        /// The dispatcher.
        /// </summary>
        /// The context-wide Event bus
        [Inject(ContextKeys.CONTEXT_DISPATCHER)]
        public IEventDispatcher dispatcher { get; set; }

        /// <summary>
        /// The evt.
        /// </summary>
        /// The injected IEvent
        [Inject]
        public IEvent evt { get; set; }

        #endregion
    }
}