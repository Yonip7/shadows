// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ISequenceBinding.cs" company="">
//   
// </copyright>
// <summary>
//   The SequenceBinding interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace strange.extensions.sequencer.api
{
    using strange.extensions.command.api;

    /// <summary>
    /// The SequenceBinding interface.
    /// </summary>
    public interface ISequenceBinding : ICommandBinding
    {
        #region Public Properties

        /// <summary>
        /// The is one off.
        /// </summary>
        /// Get/set the property set to `true` by `Once()`
        new bool isOneOff { get; set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The key.
        /// </summary>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// The <see cref="ISequenceBinding"/>.
        /// </returns>
        new ISequenceBinding Key<T>();

        /// <summary>
        /// The key.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <returns>
        /// The <see cref="ISequenceBinding"/>.
        /// </returns>
        new ISequenceBinding Key(object key);

        /// <summary>
        /// The named.
        /// </summary>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// The <see cref="ISequenceBinding"/>.
        /// </returns>
        new ISequenceBinding Named<T>();

        /// <summary>
        /// The named.
        /// </summary>
        /// <param name="o">
        /// The o.
        /// </param>
        /// <returns>
        /// The <see cref="ISequenceBinding"/>.
        /// </returns>
        new ISequenceBinding Named(object o);

        /// <summary>
        /// The once.
        /// </summary>
        /// Declares that the Binding is a one-off. As soon as it's satisfied, it will be unmapped.
        /// <returns>
        /// The <see cref="ISequenceBinding"/>.
        /// </returns>
        new ISequenceBinding Once();

        /// <summary>
        /// The to.
        /// </summary>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// The <see cref="ISequenceBinding"/>.
        /// </returns>
        new ISequenceBinding To<T>();

        /// <summary>
        /// The to.
        /// </summary>
        /// <param name="o">
        /// The o.
        /// </param>
        /// <returns>
        /// The <see cref="ISequenceBinding"/>.
        /// </returns>
        new ISequenceBinding To(object o);

        /// <summary>
        /// The to name.
        /// </summary>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// The <see cref="ISequenceBinding"/>.
        /// </returns>
        new ISequenceBinding ToName<T>();

        /// <summary>
        /// The to name.
        /// </summary>
        /// <param name="o">
        /// The o.
        /// </param>
        /// <returns>
        /// The <see cref="ISequenceBinding"/>.
        /// </returns>
        new ISequenceBinding ToName(object o);

        #endregion
    }
}