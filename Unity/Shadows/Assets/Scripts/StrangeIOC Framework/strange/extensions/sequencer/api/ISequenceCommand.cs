// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ISequenceCommand.cs" company="">
//   
// </copyright>
// <summary>
//   The SequenceCommand interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace strange.extensions.sequencer.api
{
    using strange.extensions.command.api;

    /// <summary>
    /// The SequenceCommand interface.
    /// </summary>
    public interface ISequenceCommand : ICommand
    {
    }
}