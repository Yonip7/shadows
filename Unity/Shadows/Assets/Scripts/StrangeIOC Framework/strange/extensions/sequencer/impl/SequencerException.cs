// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SequencerException.cs" company="">
//   
// </copyright>
// <summary>
//   The sequencer exception.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace strange.extensions.sequencer.impl
{
    using System;

    using strange.extensions.sequencer.api;

    /// <summary>
    /// The sequencer exception.
    /// </summary>
    public class SequencerException : Exception
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="SequencerException"/> class.
        /// </summary>
        public SequencerException()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SequencerException"/> class. 
        /// The sequencer exception.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        /// <param name="exceptionType">
        /// The exception Type.
        /// </param>
        /// Constructs a SequencerException with a message and SequencerExceptionType
        public SequencerException(string message, SequencerExceptionType exceptionType)
            : base(message)
        {
            this.type = exceptionType;
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        public SequencerExceptionType type { get; set; }

        #endregion
    }
}