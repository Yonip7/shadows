﻿using global::strange.extensions.context.api;
using global::strange.extensions.dispatcher.eventdispatcher.api;

namespace Assets.Scripts.strange.extensions
{
    public class Model
    {
        /// <summary>
        /// Gets or sets the dispatcher.
        /// </summary>
        [Inject(ContextKeys.CONTEXT_DISPATCHER)]
        public IEventDispatcher dispatcher { get; set; }
    }
}
