// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ContextException.cs" company="">
//   
// </copyright>
// <summary>
//   The context exception.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace strange.extensions.context.impl
{
    using System;

    using strange.extensions.context.api;

    /// <summary>
    /// The context exception.
    /// </summary>
    public class ContextException : Exception
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ContextException"/> class.
        /// </summary>
        public ContextException()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ContextException"/> class. 
        /// The context exception.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        /// <param name="exceptionType">
        /// The exception Type.
        /// </param>
        /// Constructs a ContextException with a message and ContextExceptionType
        public ContextException(string message, ContextExceptionType exceptionType)
            : base(message)
        {
            this.type = exceptionType;
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        public ContextExceptionType type { get; set; }

        #endregion
    }
}