// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IContextView.cs" company="">
//   
// </copyright>
// <summary>
//   The ContextView interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace strange.extensions.context.api
{
    /// <summary>
    /// The ContextView interface.
    /// </summary>
    public interface IContextView
    {
        #region Public Properties

        /// <summary>
        /// The context.
        /// </summary>
        /// Get and set the Context
        IContext context { get; set; }

        #endregion
    }
}