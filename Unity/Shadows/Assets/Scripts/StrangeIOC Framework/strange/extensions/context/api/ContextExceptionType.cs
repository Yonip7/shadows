// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ContextExceptionType.cs" company="">
//   
// </copyright>
// <summary>
//   The context exception type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace strange.extensions.context.api
{
    /// <summary>
    /// The context exception type.
    /// </summary>
    public enum ContextExceptionType
    {
        /// <summary>
        /// The n o_ contex t_ view.
        /// </summary>
        /// MVCSContext requires a root ContextView
        NO_CONTEXT_VIEW, 

        /// <summary>
        /// The n o_ mediatio n_ binder.
        /// </summary>
        /// MVCSContext requires a mediationBinder
        NO_MEDIATION_BINDER
    }
}