// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Context.cs" company="">
//   
// </copyright>
// <summary>
//   The context.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace strange.extensions.context.impl
{
    using strange.extensions.context.api;
    using strange.framework.impl;

    /// <summary>
    /// The context.
    /// </summary>
    public class Context : Binder, IContext
    {
        #region Static Fields

        /// <summary>
        /// The first context.
        /// </summary>
        /// In a multi-Context app, this represents the first Context to instantiate.
        public static IContext firstContext;

        #endregion

        #region Fields

        /// <summary>
        /// The auto startup.
        /// </summary>
        /// If false, the `Launch()` method won't fire.
        public bool autoStartup;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="Context"/> class.
        /// </summary>
        public Context()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Context"/> class.
        /// </summary>
        /// <param name="view">
        /// The view.
        /// </param>
        /// <param name="autoStartup">
        /// The auto startup.
        /// </param>
        public Context(object view, bool autoStartup)
        {
            // If firstContext was unloaded, the contextView will be null. Assign the new context as firstContext.
            if (firstContext == null || firstContext.GetContextView() == null)
            {
                firstContext = this;
            }
            else
            {
                firstContext.AddContext(this);
            }

            this.autoStartup = autoStartup;
            this.SetContextView(view);
            this.addCoreComponents();

//            if (autoStartup)
//            {
//                this.Start();
//            }
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// The context view.
        /// </summary>
        /// The top of the View hierarchy.
        /// In MVCSContext, this is your top-level GameObject
        public object contextView { get; set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The add context.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        /// Add another Context to this one.
        /// <returns>
        /// The <see cref="IContext"/>.
        /// </returns>
        public virtual IContext AddContext(IContext context)
        {
            return this;
        }

        /// <summary>
        /// The add view.
        /// </summary>
        /// <param name="view">
        /// The view.
        /// </param>
        /// Register a View with this Context
        public virtual void AddView(object view)
        {
            // Override in subclasses
        }

        /// <summary>
        /// The get component.
        /// </summary>
        /// Retrieve a component from this Context by generic type
        /// <returns>
        /// The <see cref="object"/>.
        /// </returns>
        public virtual object GetComponent<T>()
        {
            return null;
        }

        /// <summary>
        /// The get component.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        /// Retrieve a component from this Context by generic type and name
        /// <returns>
        /// The <see cref="object"/>.
        /// </returns>
        public virtual object GetComponent<T>(object name)
        {
            return null;
        }

        /// <summary>
        /// The get context view.
        /// </summary>
        /// <returns>
        /// The <see cref="object"/>.
        /// </returns>
        public virtual object GetContextView()
        {
            return this.contextView;
        }

        /// <summary>
        /// The launch.
        /// </summary>
        /// The final method to fire after mappings.
        /// If autoStartup is false, you need to call this manually.
        public virtual void Launch()
        {
        }

        /// <summary>
        /// The remove context.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        /// Remove a context from this one.
        /// <returns>
        /// The <see cref="IContext"/>.
        /// </returns>
        public virtual IContext RemoveContext(IContext context)
        {
            context.OnRemove();
            return this;
        }

        /// <summary>
        /// The remove view.
        /// </summary>
        /// <param name="view">
        /// The view.
        /// </param>
        /// Remove a View from this Context
        public virtual void RemoveView(object view)
        {
            // Override in subclasses
        }

        /// <summary>
        /// The set context view.
        /// </summary>
        /// <param name="view">
        /// The view.
        /// </param>
        /// Set the object that represents the top of the Context hierarchy.
        /// In MVCSContext, this would be a GameObject.
        /// <returns>
        /// The <see cref="IContext"/>.
        /// </returns>
        public virtual IContext SetContextView(object view)
        {
            this.contextView = view;
            return this;
        }

        /// <summary>
        /// The start.
        /// </summary>
        /// Call this from your Root to set everything in action.
        /// <returns>
        /// The <see cref="IContext"/>.
        /// </returns>
        public virtual IContext Start()
        {
            this.instantiateCoreComponents();
            this.mapBindings();
            this.postBindings();
            if (this.autoStartup)
            {
                this.Launch();
            }

            return this;
        }

        #endregion

        #region Methods

        /// <summary>
        /// The add core components.
        /// </summary>
        /// Override to add componentry. Or just extend MVCSContext.
        protected virtual void addCoreComponents()
        {
        }

        /// <summary>
        /// The instantiate core components.
        /// </summary>
        /// Override to instantiate componentry. Or just extend MVCSContext.
        protected virtual void instantiateCoreComponents()
        {
        }

        /// <summary>
        /// The map bindings.
        /// </summary>
        /// Override to map project-specific bindings
        protected virtual void mapBindings()
        {
        }

        /// <summary>
        /// The post bindings.
        /// </summary>
        /// Override to do things after binding but before app launch
        protected virtual void postBindings()
        {
        }

        #endregion
    }
}