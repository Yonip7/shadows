// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ContextView.cs" company="">
//   
// </copyright>
// <summary>
//   The context view.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace strange.extensions.context.impl
{
    using strange.extensions.context.api;

    using UnityEngine;

    /// <summary>
    /// The context view.
    /// </summary>
    public class ContextView : MonoBehaviour, IContextView
    {
        #region Public Properties

        /// <summary>
        /// Gets or sets the context.
        /// </summary>
        public IContext context { get; set; }

        #endregion

        #region Methods

        /// <summary>
        ///     When a ContextView is Destroyed, automatically removes the associated Context.
        /// </summary>
        protected virtual void OnDestroy()
        {
            if (this.context != null)
            {
                Context.firstContext.RemoveContext(this.context);
            }
        }

        #endregion
    }
}