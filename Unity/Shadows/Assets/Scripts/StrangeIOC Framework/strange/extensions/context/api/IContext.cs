// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IContext.cs" company="">
//   
// </copyright>
// <summary>
//   The Context interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace strange.extensions.context.api
{
    using strange.framework.api;

    /// <summary>
    /// The Context interface.
    /// </summary>
    public interface IContext : IBinder
    {
        #region Public Methods and Operators

        /// <summary>
        /// The add context.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        /// Register a new context to this one
        /// <returns>
        /// The <see cref="IContext"/>.
        /// </returns>
        IContext AddContext(IContext context);

        /// <summary>
        /// The add view.
        /// </summary>
        /// <param name="view">
        /// The view.
        /// </param>
        /// Register a view with this context
        void AddView(object view);

        /// <summary>
        /// The get context view.
        /// </summary>
        /// Get the ContextView
        /// <returns>
        /// The <see cref="object"/>.
        /// </returns>
        object GetContextView();

        /// <summary>
        /// The remove context.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        /// Remove a context from this one
        /// <returns>
        /// The <see cref="IContext"/>.
        /// </returns>
        IContext RemoveContext(IContext context);

        /// <summary>
        /// The remove view.
        /// </summary>
        /// <param name="view">
        /// The view.
        /// </param>
        /// Remove a view from this context
        void RemoveView(object view);

        /// <summary>
        /// The start.
        /// </summary>
        /// Kick the context into action
        /// <returns>
        /// The <see cref="IContext"/>.
        /// </returns>
        IContext Start();

        #endregion
    }
}