﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ICrossContextCapable.cs" company="">
//   
// </copyright>
// <summary>
//   The CrossContextCapable interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace strange.extensions.context.api
{
    using strange.extensions.dispatcher.api;
    using strange.extensions.injector.api;

    /// <summary>
    /// The CrossContextCapable interface.
    /// </summary>
    public interface ICrossContextCapable
    {
        #region Public Properties

        /// <summary>
        /// The cross context dispatcher.
        /// </summary>
        /// Set and get the shared system bus for communicating across contexts
        IDispatcher crossContextDispatcher { get; set; }

        /// <summary>
        /// The injection binder.
        /// </summary>
        /// All cross-context capable contexts must implement an injectionBinder
        ICrossContextInjectionBinder injectionBinder { get; set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The assign cross context.
        /// </summary>
        /// <param name="childContext">
        /// The child Context.
        /// </param>
        /// Add cross-context functionality to a child context being added
        void AssignCrossContext(ICrossContextCapable childContext);

        /// <summary>
        /// The get component.
        /// </summary>
        /// Request a component from the context (might be useful in certain cross-context situations)
        /// This is technically a deprecated methodology. Bind using CrossContext() instead.
        /// <returns>
        /// The <see cref="object"/>.
        /// </returns>
        object GetComponent<T>();

        /// <summary>
        /// The get component.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        /// Request a component from the context (might be useful in certain cross-context situations)
        /// This is technically a deprecated methodology. Bind using CrossContext() instead.
        /// <returns>
        /// The <see cref="object"/>.
        /// </returns>
        object GetComponent<T>(object name);

        /// <summary>
        /// The remove cross context.
        /// </summary>
        /// <param name="childContext">
        /// The child Context.
        /// </param>
        /// Clean up cross-context functionality from a child context being removed
        void RemoveCrossContext(ICrossContextCapable childContext);

        #endregion
    }
}