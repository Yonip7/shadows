// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MVCSContext.cs" company="">
//   
// </copyright>
// <summary>
//   The mvcs context.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace strange.extensions.context.impl
{
    using strange.extensions.command.api;
    using strange.extensions.command.impl;
    using strange.extensions.context.api;
    using strange.extensions.dispatcher.api;
    using strange.extensions.dispatcher.eventdispatcher.api;
    using strange.extensions.dispatcher.eventdispatcher.impl;
    using strange.extensions.injector.api;
    using strange.extensions.mediation.api;
    using strange.extensions.mediation.impl;
    using strange.extensions.sequencer.api;
    using strange.extensions.sequencer.impl;
    using strange.framework.api;
    using strange.framework.impl;

    using UnityEngine;

    /// <summary>
    /// The mvcs context.
    /// </summary>
    public class MVCSContext : CrossContext
    {
        #region Static Fields

        /// <summary>
        /// The view cache.
        /// </summary>
        /// A list of Views Awake before the Context is fully set up.
        protected static ISemiBinding viewCache = new SemiBinding();

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="MVCSContext"/> class.
        /// </summary>
        public MVCSContext()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MVCSContext"/> class.
        /// </summary>
        /// <param name="view">
        /// The view.
        /// </param>
        /// <param name="autoStartup">
        /// The auto startup.
        /// </param>
        public MVCSContext(MonoBehaviour view, bool autoStartup)
            : base(view, autoStartup)
        {
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// The command binder.
        /// </summary>
        /// A Binder that maps Events to Commands
        public ICommandBinder commandBinder { get; set; }

        /// <summary>
        /// The dispatcher.
        /// </summary>
        /// A Binder that serves as the Event bus for the Context
        public IEventDispatcher dispatcher { get; set; }

        /// <summary>
        /// The mediation binder.
        /// </summary>
        /// A Binder that maps Views to Mediators
        public IMediationBinder mediationBinder { get; set; }

        /// <summary>
        /// The sequencer.
        /// </summary>
        /// A Binder that maps Events to Sequences
        public ISequencer sequencer { get; set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The add view.
        /// </summary>
        /// <param name="view">
        /// The view.
        /// </param>
        public override void AddView(object view)
        {
            if (this.mediationBinder != null)
            {
                this.mediationBinder.Trigger(MediationEvent.AWAKE, view as IView);
            }
            else
            {
                this.cacheView(view as MonoBehaviour);
            }
        }

        /// <summary>
        /// The get component.
        /// </summary>
        /// Gets an instance of the provided generic type.
        /// Always bear in mind that doing this risks adding
        /// dependencies that must be cleaned up when Contexts
        /// are removed.
        /// <returns>
        /// The <see cref="object"/>.
        /// </returns>
        public override object GetComponent<T>()
        {
            return this.GetComponent<T>(null);
        }

        /// <summary>
        /// The get component.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        /// Gets an instance of the provided generic type and name from the InjectionBinder
        /// Always bear in mind that doing this risks adding
        /// dependencies that must be cleaned up when Contexts
        /// are removed.
        /// <returns>
        /// The <see cref="object"/>.
        /// </returns>
        public override object GetComponent<T>(object name)
        {
            IInjectionBinding binding = this.injectionBinder.GetBinding<T>(name);
            if (binding != null)
            {
                return this.injectionBinder.GetInstance<T>(name);
            }

            return null;
        }

        /// <summary>
        /// The launch.
        /// </summary>
        /// Fires ContextEvent.START
        /// Whatever Command/Sequence you want to happen first should 
        /// be mapped to this event.
        public override void Launch()
        {
            this.dispatcher.Dispatch(ContextEvent.START);
        }

        /// <summary>
        /// The on remove.
        /// </summary>
        /// Clean up. Called by a ContextView in its OnDestroy method
        public override void OnRemove()
        {
            base.OnRemove();
            this.commandBinder.OnRemove();
        }

        /// <summary>
        /// The remove view.
        /// </summary>
        /// <param name="view">
        /// The view.
        /// </param>
        public override void RemoveView(object view)
        {
            this.mediationBinder.Trigger(MediationEvent.DESTROYED, view as IView);
        }

        /// <summary>
        /// The set context view.
        /// </summary>
        /// <param name="view">
        /// The view.
        /// </param>
        /// <returns>
        /// The <see cref="IContext"/>.
        /// </returns>
        /// <exception cref="ContextException">
        /// </exception>
        public override IContext SetContextView(object view)
        {
            this.contextView = (view as MonoBehaviour).gameObject;
            if (this.contextView == null)
            {
                throw new ContextException(
                    "MVCSContext requires a ContextView of type MonoBehaviour", ContextExceptionType.NO_CONTEXT_VIEW);
            }

            return this;
        }

        #endregion

        #region Methods

        /// <summary>
        /// The add core components.
        /// </summary>
        /// Map the relationships between the Binders.
        /// Although you can override this method, it is recommended
        /// that you provide all your application bindings in `mapBindings()`.
        protected override void addCoreComponents()
        {
            base.addCoreComponents();
            this.injectionBinder.Bind<IInjectionBinder>().ToValue(this.injectionBinder);
            this.injectionBinder.Bind<IContext>().ToValue(this).ToName(ContextKeys.CONTEXT);
            this.injectionBinder.Bind<ICommandBinder>().To<EventCommandBinder>().ToSingleton();

            // This binding is for local dispatchers
            this.injectionBinder.Bind<IEventDispatcher>().To<EventDispatcher>();

            // This binding is for the common system bus
            this.injectionBinder.Bind<IEventDispatcher>().To<EventDispatcher>().ToSingleton().ToName(
                ContextKeys.CONTEXT_DISPATCHER);
            this.injectionBinder.Bind<IMediationBinder>().To<MediationBinder>().ToSingleton();
            this.injectionBinder.Bind<ISequencer>().To<EventSequencer>().ToSingleton();
        }

        /// <summary>
        /// The cache view.
        /// </summary>
        /// <param name="view">
        /// The view.
        /// </param>
        /// Caches early-riser Views.
        /// 
        /// If a View is on stage at startup, it's possible for that
        /// View to be Awake before this Context has finished initing.
        /// `cacheView()` maintains a list of such 'early-risers'
        /// until the Context is ready to mediate them.
        protected virtual void cacheView(MonoBehaviour view)
        {
            if (viewCache.constraint.Equals(BindingConstraintType.ONE))
            {
                viewCache.constraint = BindingConstraintType.MANY;
            }

            viewCache.Add(view);
        }

        /// <summary>
        /// The instantiate core components.
        /// </summary>
        /// <exception cref="ContextException">
        /// </exception>
        protected override void instantiateCoreComponents()
        {
            base.instantiateCoreComponents();
            if (this.contextView == null)
            {
                throw new ContextException(
                    "MVCSContext requires a ContextView of type MonoBehaviour", ContextExceptionType.NO_CONTEXT_VIEW);
            }

            this.injectionBinder.Bind<GameObject>().ToValue(this.contextView).ToName(ContextKeys.CONTEXT_VIEW);
            this.commandBinder = this.injectionBinder.GetInstance<ICommandBinder>() as ICommandBinder;

            this.dispatcher =
                this.injectionBinder.GetInstance<IEventDispatcher>(ContextKeys.CONTEXT_DISPATCHER) as IEventDispatcher;
            this.mediationBinder = this.injectionBinder.GetInstance<IMediationBinder>() as IMediationBinder;
            this.sequencer = this.injectionBinder.GetInstance<ISequencer>() as ISequencer;

            (this.dispatcher as ITriggerProvider).AddTriggerable(this.commandBinder as ITriggerable);
            (this.dispatcher as ITriggerProvider).AddTriggerable(this.sequencer as ITriggerable);
        }

        /// <summary>
        /// The mediate view cache.
        /// </summary>
        /// Provide mediation for early-riser Views
        protected virtual void mediateViewCache()
        {
            if (this.mediationBinder == null)
            {
                throw new ContextException(
                    "MVCSContext cannot mediate views without a mediationBinder", 
                    ContextExceptionType.NO_MEDIATION_BINDER);
            }

            object[] values = viewCache.value as object[];
            if (values == null)
            {
                return;
            }

            int aa = values.Length;
            for (int a = 0; a < aa; a++)
            {
                this.mediationBinder.Trigger(MediationEvent.AWAKE, values[a] as IView);
            }

            viewCache = new SemiBinding();
        }

        /// <summary>
        /// The post bindings.
        /// </summary>
        protected override void postBindings()
        {
            // It's possible for views to fire their Awake before bindings. This catches any early risers and attaches their Mediators.
            this.mediateViewCache();
        }

        #endregion
    }
}