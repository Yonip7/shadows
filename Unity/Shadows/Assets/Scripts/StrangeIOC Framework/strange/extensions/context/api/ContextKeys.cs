// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ContextKeys.cs" company="">
//   
// </copyright>
// <summary>
//   The context keys.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace strange.extensions.context.api
{
    /// <summary>
    /// The context keys.
    /// </summary>
    public enum ContextKeys
    {
        /// <summary>
        /// The context.
        /// </summary>
        /// Marker for the named Injection of the Context
        CONTEXT, 

        /// <summary>
        /// The contex t_ view.
        /// </summary>
        /// Marker for the named Injection of the ContextView
        CONTEXT_VIEW, 

        /// <summary>
        /// The contex t_ dispatcher.
        /// </summary>
        /// Marker for the named Injection of the contextDispatcher
        CONTEXT_DISPATCHER, 

        /// <summary>
        /// The cros s_ contex t_ dispatcher.
        /// </summary>
        /// Marker for the named Injection of the crossContextDispatcher
        CROSS_CONTEXT_DISPATCHER
    }
}