﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CrossContext.cs" company="">
//   
// </copyright>
// <summary>
//   The cross context.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace strange.extensions.context.impl
{
    using strange.extensions.context.api;
    using strange.extensions.dispatcher.api;
    using strange.extensions.dispatcher.eventdispatcher.api;
    using strange.extensions.dispatcher.eventdispatcher.impl;
    using strange.extensions.injector.api;
    using strange.extensions.injector.impl;
    using strange.framework.api;

    /// <summary>
    /// The cross context.
    /// </summary>
    public class CrossContext : Context, ICrossContextCapable
    {
        #region Fields

        /// <summary>
        /// The _cross context dispatcher.
        /// </summary>
        /// A specific instance of EventDispatcher that communicates 
        /// across multiple contexts. An event sent across this 
        /// dispatcher will be re-dispatched by the various context-wide 
        /// dispatchers. So a dispatch to other contexts is simply 
        /// 
        /// `crossContextDispatcher.Dispatch(MY_EVENT, payload)`;
        /// 
        /// Other contexts don't need to listen to the cross-context dispatcher
        /// as such, just map the necessary event to your local context
        /// dispatcher and you'll receive it.
        protected IEventDispatcher _crossContextDispatcher;

        /// <summary>
        /// The _cross context bridge.
        /// </summary>
        private IBinder _crossContextBridge;

        /// <summary>
        /// The _injection binder.
        /// </summary>
        private ICrossContextInjectionBinder _injectionBinder;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="CrossContext"/> class.
        /// </summary>
        public CrossContext()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CrossContext"/> class.
        /// </summary>
        /// <param name="view">
        /// The view.
        /// </param>
        /// <param name="autoStartup">
        /// The auto startup.
        /// </param>
        public CrossContext(object view, bool autoStartup)
            : base(view, autoStartup)
        {
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets the cross context bridge.
        /// </summary>
        public virtual IBinder crossContextBridge
        {
            get
            {
                if (this._crossContextBridge == null)
                {
                    this._crossContextBridge = this.injectionBinder.GetInstance<CrossContextBridge>() as IBinder;
                }

                return this._crossContextBridge;
            }

            set
            {
                this._crossContextDispatcher = value as IEventDispatcher;
            }
        }

        /// <summary>
        /// Gets or sets the cross context dispatcher.
        /// </summary>
        public virtual IDispatcher crossContextDispatcher
        {
            get
            {
                return this._crossContextDispatcher;
            }

            set
            {
                this._crossContextDispatcher = value as IEventDispatcher;
            }
        }

        /// <summary>
        /// The injection binder.
        /// </summary>
        /// A Binder that handles dependency injection binding and instantiation
        public ICrossContextInjectionBinder injectionBinder
        {
            get
            {
                if (this._injectionBinder == null)
                {
                    this._injectionBinder = new CrossContextInjectionBinder();
                }

                return this._injectionBinder;
            }

            set
            {
                this._injectionBinder = value;
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The add context.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        /// <returns>
        /// The <see cref="IContext"/>.
        /// </returns>
        public override IContext AddContext(IContext context)
        {
            base.AddContext(context);
            if (context is ICrossContextCapable)
            {
                this.AssignCrossContext((ICrossContextCapable)context);
            }

            return this;
        }

        /// <summary>
        /// The assign cross context.
        /// </summary>
        /// <param name="childContext">
        /// The child context.
        /// </param>
        public virtual void AssignCrossContext(ICrossContextCapable childContext)
        {
            childContext.crossContextDispatcher = this.crossContextDispatcher;
            childContext.injectionBinder.CrossContextBinder = this.injectionBinder.CrossContextBinder;
        }

        /// <summary>
        /// The remove context.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        /// <returns>
        /// The <see cref="IContext"/>.
        /// </returns>
        public override IContext RemoveContext(IContext context)
        {
            if (context is ICrossContextCapable)
            {
                this.RemoveCrossContext((ICrossContextCapable)context);
            }

            return base.RemoveContext(context);
        }

        /// <summary>
        /// The remove cross context.
        /// </summary>
        /// <param name="childContext">
        /// The child context.
        /// </param>
        public virtual void RemoveCrossContext(ICrossContextCapable childContext)
        {
            if (childContext.crossContextDispatcher != null)
            {
                (childContext.crossContextDispatcher as ITriggerProvider).RemoveTriggerable(
                    childContext.GetComponent<IEventDispatcher>(ContextKeys.CONTEXT_DISPATCHER) as ITriggerable);
                childContext.crossContextDispatcher = null;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// The add core components.
        /// </summary>
        protected override void addCoreComponents()
        {
            base.addCoreComponents();
            if (this.injectionBinder.CrossContextBinder == null)
            {
                // Only null if it could not find a parent context / firstContext
                this.injectionBinder.CrossContextBinder = new CrossContextInjectionBinder();
            }

            if (firstContext == this)
            {
                this.injectionBinder.Bind<IEventDispatcher>().To<EventDispatcher>().ToSingleton().ToName(
                    ContextKeys.CROSS_CONTEXT_DISPATCHER).CrossContext();
                this.injectionBinder.Bind<CrossContextBridge>().ToSingleton().CrossContext();
            }
        }

        /// <summary>
        /// The instantiate core components.
        /// </summary>
        protected override void instantiateCoreComponents()
        {
            base.instantiateCoreComponents();

            IEventDispatcher dispatcher =
                this.injectionBinder.GetInstance<IEventDispatcher>(ContextKeys.CONTEXT_DISPATCHER) as IEventDispatcher;

            if (dispatcher != null)
            {
                this.crossContextDispatcher =
                    this.injectionBinder.GetInstance<IEventDispatcher>(ContextKeys.CROSS_CONTEXT_DISPATCHER) as
                    IEventDispatcher;
                (this.crossContextDispatcher as ITriggerProvider).AddTriggerable(dispatcher as ITriggerable);
                (dispatcher as ITriggerProvider).AddTriggerable(this.crossContextBridge as ITriggerable);
            }
        }

        #endregion
    }
}