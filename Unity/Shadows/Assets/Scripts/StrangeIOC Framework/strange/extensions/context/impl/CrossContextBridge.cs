// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CrossContextBridge.cs" company="">
//   
// </copyright>
// <summary>
//   The cross context bridge.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace strange.extensions.context.impl
{
    using System.Collections.Generic;

    using strange.extensions.context.api;
    using strange.extensions.dispatcher.api;
    using strange.extensions.dispatcher.eventdispatcher.api;
    using strange.framework.api;
    using strange.framework.impl;

    /// <summary>
    /// The cross context bridge.
    /// </summary>
    public class CrossContextBridge : Binder, ITriggerable
    {
        #region Fields

        /// <summary>
        /// The events in progress.
        /// </summary>
        /// Prevents the currently dispatching Event from cycling back on itself
        protected HashSet<object> eventsInProgress = new HashSet<object>();

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets the cross context dispatcher.
        /// </summary>
        [Inject(ContextKeys.CROSS_CONTEXT_DISPATCHER)]
        public IEventDispatcher crossContextDispatcher { get; set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The bind.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <returns>
        /// The <see cref="IBinding"/>.
        /// </returns>
        public override IBinding Bind(object key)
        {
            IBinding binding;
            binding = this.GetRawBinding();
            binding.Key(key);
            this.resolver(binding);
            return binding;
        }

        /// <summary>
        /// The trigger.
        /// </summary>
        /// <param name="data">
        /// The data.
        /// </param>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool Trigger<T>(object data)
        {
            return this.Trigger(typeof(T), data);
        }

        /// <summary>
        /// The trigger.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <param name="data">
        /// The data.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool Trigger(object key, object data)
        {
            IBinding binding = this.GetBinding(key, null);
            if (binding != null && !this.eventsInProgress.Contains(key))
            {
                this.eventsInProgress.Add(key);
                this.crossContextDispatcher.Dispatch(key, data);
                this.eventsInProgress.Remove(key);
                return false;
            }

            return true;
        }

        #endregion
    }
}