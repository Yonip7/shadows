// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ContextEvent.cs" company="">
//   
// </copyright>
// <summary>
//   The context event.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace strange.extensions.context.api
{
    /// <summary>
    /// The context event.
    /// </summary>
    public enum ContextEvent
    {
        /// <summary>
        /// The start.
        /// </summary>
        /// Fires immediately on conclusion of Context bootstrapping. Map this to your first Command.
        START
    }
}