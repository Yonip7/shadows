// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MediationException.cs" company="">
//   
// </copyright>
// <summary>
//   The mediation exception.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace strange.extensions.mediation.impl
{
    using System;

    using strange.extensions.mediation.api;

    /// <summary>
    /// The mediation exception.
    /// </summary>
    public class MediationException : Exception
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="MediationException"/> class.
        /// </summary>
        public MediationException()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MediationException"/> class. 
        /// The mediation exception.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        /// <param name="exceptionType">
        /// The exception Type.
        /// </param>
        /// Constructs a MediationException with a message and MediationExceptionType
        public MediationException(string message, MediationExceptionType exceptionType)
            : base(message)
        {
            this.type = exceptionType;
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        public MediationExceptionType type { get; set; }

        #endregion
    }
}