// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MediationBinding.cs" company="">
//   
// </copyright>
// <summary>
//   The mediation binding.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace strange.extensions.mediation.impl
{
    using strange.extensions.mediation.api;
    using strange.framework.impl;

    /// <summary>
    /// The mediation binding.
    /// </summary>
    public class MediationBinding : Binding, IMediationBinding
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="MediationBinding"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver.
        /// </param>
        public MediationBinding(Binder.BindingResolver resolver)
            : base(resolver)
        {
        }

        #endregion
    }
}