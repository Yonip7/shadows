// --------------------------------------------------------------------------------------------------------------------
// <copyright file="View.cs" company="">
//   
// </copyright>
// <summary>
//   The view.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace strange.extensions.mediation.impl
{
    using strange.extensions.context.api;
    using strange.extensions.context.impl;
    using strange.extensions.mediation.api;

    using UnityEngine;

    /// <summary>
    /// The view.
    /// </summary>
    public class View : MonoBehaviour, IView
    {
        #region Fields

        /// <summary>
        /// The _requires context.
        /// </summary>
        /// Leave this value true most of the time. If for some reason you want
        /// a view to exist outside a context you can set it to false. The only
        /// difference is whether an error gets generated.
        private bool _requiresContext = true;

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets a value indicating whether registered with context.
        /// </summary>
        public bool registeredWithContext { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether requires context.
        /// </summary>
        public bool requiresContext
        {
            get
            {
                return this._requiresContext;
            }

            set
            {
                this._requiresContext = value;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// The awake.
        /// </summary>
        /// A MonoBehaviour Awake handler.
        /// The View will attempt to connect to the Context at this moment.
        protected virtual void Awake()
        {
            if (!this.registeredWithContext)
            {
                this.bubbleToContext(this, true, false);
            }
        }

        /// <summary>
        /// The on destroy.
        /// </summary>
        /// A MonoBehaviour OnDestroy handler
        /// The View will inform the Context that it is about to be
        /// destroyed.
        protected virtual void OnDestroy()
        {
            this.bubbleToContext(this, false, false);
        }

        /// <summary>
        /// The start.
        /// </summary>
        /// A MonoBehaviour Start handler
        /// If the View is not yet registered with the Context, it will 
        /// attempt to connect again at this moment.
        protected virtual void Start()
        {
            if (!this.registeredWithContext)
            {
                this.bubbleToContext(this, true, true);
            }
        }

        /// <summary>
        /// The bubble to context.
        /// </summary>
        /// <param name="view">
        /// The view.
        /// </param>
        /// <param name="toAdd">
        /// The to Add.
        /// </param>
        /// <param name="finalTry">
        /// The final Try.
        /// </param>
        /// Recurses through Transform.parent to find the GameObject to which ContextView is attached
        /// Has a loop limit of 100 levels.
        /// By default, raises an Exception if no Context is found.
        protected virtual void bubbleToContext(MonoBehaviour view, bool toAdd, bool finalTry)
        {
            const int LOOP_MAX = 100;
            int loopLimiter = 0;
            Transform trans = view.gameObject.transform;
            while (trans.parent != null && loopLimiter < LOOP_MAX)
            {
                loopLimiter ++;
                trans = trans.parent;
                if (trans.gameObject.GetComponent<ContextView>() != null)
                {
                    ContextView contextView = trans.gameObject.GetComponent<ContextView>();
                    if (contextView.context != null)
                    {
                        IContext context = contextView.context;
                        if (toAdd)
                        {
                            context.AddView(view);
                            this.registeredWithContext = true;
                            return;
                        }
                        else
                        {
                            context.RemoveView(view);
                            return;
                        }
                    }
                }
            }

            if (this.requiresContext && finalTry)
            {
                // last ditch. If there's a Context anywhere, we'll use it!
                if (Context.firstContext != null)
                {
                    Context.firstContext.AddView(view);
                    this.registeredWithContext = true;
                    return;
                }

                string msg = (loopLimiter == LOOP_MAX)
                                 ? msg = "A view couldn't find a context. Loop limit reached."
                                 : msg =
                                   "A view was added with no context. Views must be added into the hierarchy of their ContextView lest all hell break loose.";
                msg += "\nView: " + view;
                throw new MediationException(msg, MediationExceptionType.NO_CONTEXT);
            }
        }

        #endregion
    }
}