// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MediationExceptionType.cs" company="">
//   
// </copyright>
// <summary>
//   The mediation exception type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace strange.extensions.mediation.api
{
    /// <summary>
    /// The mediation exception type.
    /// </summary>
    public enum MediationExceptionType
    {
        /// <summary>
        /// The n o_ context.
        /// </summary>
        /// Exception raised when a View can't locate a Context.
        /// Views contact the Context by "bubbling" their existence up 
        /// the display chain (recursively using transform.parent).
        /// If a View reaches the top of that chain without locating
        /// a Context, it will raise this Exception to warn you.
        /// 
        /// Note: to avoid infinite looping, there is a bubbling limit of 100
        /// layers. If your View needs to be more than 100 transforms deep, 
        /// that might signal a design problem.
        NO_CONTEXT, 

        /// <summary>
        /// The mediato r_ vie w_ stac k_ overflow.
        /// </summary>
        /// Exception raised when a View is mapped to itself.
        /// If a View is accidentally mapped to itself, the result will be an
        /// infinite loop of Mediation creation.
        MEDIATOR_VIEW_STACK_OVERFLOW
    }
}