// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MediationEvent.cs" company="">
//   
// </copyright>
// <summary>
//   The mediation event.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace strange.extensions.mediation.api
{
    /// <summary>
    /// The mediation event.
    /// </summary>
    public enum MediationEvent
    {
        /// <summary>
        /// The awake.
        /// </summary>
        /// The View is Awake
        AWAKE, 

        /// <summary>
        /// The destroyed.
        /// </summary>
        /// The View is about to be Destroyed
        DESTROYED, 

        /// <summary>
        /// The enabled.
        /// </summary>
        /// The View is being Enabled
        ENABLED, 

        /// <summary>
        /// The disabled.
        /// </summary>
        /// The View is being Disabled
        DISABLED
    }
}