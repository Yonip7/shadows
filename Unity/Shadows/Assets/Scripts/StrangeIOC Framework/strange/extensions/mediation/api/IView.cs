// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IView.cs" company="">
//   
// </copyright>
// <summary>
//   The View interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace strange.extensions.mediation.api
{
    /// <summary>
    /// The View interface.
    /// </summary>
    public interface IView
    {
        #region Public Properties

        /// <summary>
        /// The registered with context.
        /// </summary>
        /// Indicates whether this View  has been registered with a Context
        bool registeredWithContext { get; set; }

        /// <summary>
        /// The requires context.
        /// </summary>
        /// Indicates whether the View can work absent a context
        /// 
        /// Leave this value true most of the time. If for some reason you want
        /// a view to exist outside a context you can set it to false. The only
        /// difference is whether an error gets generated.
        bool requiresContext { get; set; }

        #endregion
    }
}