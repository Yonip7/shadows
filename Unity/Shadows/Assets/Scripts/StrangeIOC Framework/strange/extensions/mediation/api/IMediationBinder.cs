// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IMediationBinder.cs" company="">
//   
// </copyright>
// <summary>
//   The MediationBinder interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace strange.extensions.mediation.api
{
    using strange.framework.api;

    /// <summary>
    /// The MediationBinder interface.
    /// </summary>
    public interface IMediationBinder : IBinder
    {
        #region Public Methods and Operators

        /// <summary>
        /// The trigger.
        /// </summary>
        /// <param name="evt">
        /// The evt.
        /// </param>
        /// <param name="view">
        /// The view.
        /// </param>
        /// An event that just happened, and the View it happened to.
        /// If the event was Awake, it will trigger creation of a mapped Mediator.
        void Trigger(MediationEvent evt, IView view);

        #endregion
    }
}