// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IMediationBinding.cs" company="">
//   
// </copyright>
// <summary>
//   The MediationBinding interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace strange.extensions.mediation.api
{
    using strange.framework.api;

    /// <summary>
    /// The MediationBinding interface.
    /// </summary>
    public interface IMediationBinding : IBinding
    {
    }
}