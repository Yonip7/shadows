// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Mediator.cs" company="">
//   
// </copyright>
// <summary>
//   The mediator.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace strange.extensions.mediation.impl
{
    using strange.extensions.context.api;
    using strange.extensions.mediation.api;

    using UnityEngine;

    /// <summary>
    /// The mediator.
    /// </summary>
    public class Mediator : MonoBehaviour, IMediator
    {
        #region Public Properties

        /// <summary>
        /// Gets or sets the context view.
        /// </summary>
        [Inject(ContextKeys.CONTEXT_VIEW)]
        public GameObject contextView { get; set; }

        #endregion

        /**
		 * Fires directly after creation and before injection
		 */

        /**
		 * Fires after all injections satisifed.
		 *
		 * Override and place your initialization code here.
		 */
        #region Public Methods and Operators

        /// <summary>
        /// The on register.
        /// </summary>
        public virtual void OnRegister()
        {
        }

        /**
		 * Fires on removal of view.
		 *
		 * Override and place your cleanup code here
		 */

        /// <summary>
        /// The on remove.
        /// </summary>
        public virtual void OnRemove()
        {
        }

        /// <summary>
        /// The pre register.
        /// </summary>
        public virtual void PreRegister()
        {
        }

        #endregion
    }
}