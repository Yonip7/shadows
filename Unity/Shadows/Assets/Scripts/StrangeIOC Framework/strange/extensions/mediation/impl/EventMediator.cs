// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EventMediator.cs" company="">
//   
// </copyright>
// <summary>
//   The event mediator.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace strange.extensions.mediation.impl
{
    using strange.extensions.context.api;
    using strange.extensions.dispatcher.eventdispatcher.api;

    /// <summary>
    /// The event mediator.
    /// </summary>
    public class EventMediator : Mediator
    {
        #region Public Properties

        /// <summary>
        /// Gets or sets the dispatcher.
        /// </summary>
        [Inject(ContextKeys.CONTEXT_DISPATCHER)]
        public IEventDispatcher dispatcher { get; set; }

        protected void Dispatch(object eventType)
        {
            dispatcher.Dispatch(eventType);
        }

        protected void Dispatch(object eventType, object data)
        {
            dispatcher.Dispatch(eventType, data);
        }

        #endregion
    }
}