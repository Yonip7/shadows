// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MediationBinder.cs" company="">
//   
// </copyright>
// <summary>
//   The mediation binder.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace strange.extensions.mediation.impl
{
    using System;

    using strange.extensions.injector.api;
    using strange.extensions.mediation.api;
    using strange.framework.api;
    using strange.framework.impl;

    using UnityEngine;

    /// <summary>
    /// The mediation binder.
    /// </summary>
    public class MediationBinder : Binder, IMediationBinder
    {
        #region Public Properties

        /// <summary>
        /// Gets or sets the injection binder.
        /// </summary>
        [Inject]
        public IInjectionBinder injectionBinder { get; set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The bind.
        /// </summary>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// The <see cref="IBinding"/>.
        /// </returns>
        public override IBinding Bind<T>()
        {
            return base.Bind<T>();
        }

        /// <summary>
        /// The get raw binding.
        /// </summary>
        /// <returns>
        /// The <see cref="IBinding"/>.
        /// </returns>
        public override IBinding GetRawBinding()
        {
            return new MediationBinding(this.resolver);
        }

        /// <summary>
        /// The trigger.
        /// </summary>
        /// <param name="evt">
        /// The evt.
        /// </param>
        /// <param name="view">
        /// The view.
        /// </param>
        public void Trigger(MediationEvent evt, IView view)
        {
            Type viewType = view.GetType();
            IMediationBinding binding = this.GetBinding(viewType) as IMediationBinding;
            if (binding != null)
            {
                switch (evt)
                {
                    case MediationEvent.AWAKE:
                        this.injectViewAndChildren(view);
                        this.mapView(view, binding);
                        break;
                    case MediationEvent.DESTROYED:
                        this.unmapView(view, binding);
                        break;
                    default:
                        break;
                }
            }
            else if (evt == MediationEvent.AWAKE)
            {
                // Even if not mapped, Views (and their children) have potential to be injected
                this.injectViewAndChildren(view);
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// The inject view and children.
        /// </summary>
        /// <param name="view">
        /// The view.
        /// </param>
        /// Initialize all IViews within this view
        protected virtual void injectViewAndChildren(IView view)
        {
            MonoBehaviour mono = view as MonoBehaviour;
            Component[] views = mono.GetComponentsInChildren(typeof(IView), true);

            int aa = views.Length;
            for (int a = 0; a < aa; a++)
            {
                IView iView = views[a] as IView;
                if (iView != null)
                {
                    if (iView.registeredWithContext)
                    {
                        continue;
                    }

                    iView.registeredWithContext = true;
                    if (iView.Equals(mono) == false)
                    {
                        this.Trigger(MediationEvent.AWAKE, iView);
                    }
                }
            }

            this.injectionBinder.injector.Inject(mono, false);
        }

        /// <summary>
        /// The map view.
        /// </summary>
        /// <param name="view">
        /// The view.
        /// </param>
        /// <param name="binding">
        /// The binding.
        /// </param>
        /// Creates and registers one or more Mediators for a specific View instance.
        /// Takes a specific View instance and a binding and, if a binding is found for that type, creates and registers a Mediator.
        protected virtual void mapView(IView view, IMediationBinding binding)
        {
            Type viewType = view.GetType();

            if (this.bindings.ContainsKey(viewType))
            {
                object[] values = binding.value as object[];
                int aa = values.Length;
                for (int a = 0; a < aa; a++)
                {
                    MonoBehaviour mono = view as MonoBehaviour;
                    Type mediatorType = values[a] as Type;
                    if (mediatorType == viewType)
                    {
                        throw new MediationException(
                            viewType + "mapped to itself. The result would be a stack overflow.", 
                            MediationExceptionType.MEDIATOR_VIEW_STACK_OVERFLOW);
                    }

                    MonoBehaviour mediator = mono.gameObject.AddComponent(mediatorType) as MonoBehaviour;
                    if (mediator is IMediator)
                    {
                        ((IMediator)mediator).PreRegister();
                    }

                    this.injectionBinder.Bind(viewType).ToValue(view).ToInject(false);
                    this.injectionBinder.injector.Inject(mediator);
                    this.injectionBinder.Unbind(viewType);
                    if (mediator is IMediator)
                    {
                        ((IMediator)mediator).OnRegister();
                    }
                }
            }
        }

        /// <summary>
        /// The unmap view.
        /// </summary>
        /// <param name="view">
        /// The view.
        /// </param>
        /// <param name="binding">
        /// The binding.
        /// </param>
        /// Removes a mediator when its view is destroyed
        protected virtual void unmapView(IView view, IMediationBinding binding)
        {
            Type viewType = view.GetType();

            if (this.bindings.ContainsKey(viewType))
            {
                object[] values = binding.value as object[];
                int aa = values.Length;
                for (int a = 0; a < aa; a++)
                {
                    Type mediatorType = values[a] as Type;
                    MonoBehaviour mono = view as MonoBehaviour;
                    IMediator mediator = mono.GetComponent(mediatorType) as IMediator;
                    if (mediator != null)
                    {
                        mediator.OnRemove();
                    }
                }
            }
        }

        /// <summary>
        /// The disable view.
        /// </summary>
        /// <param name="view">
        /// The view.
        /// </param>
        private void disableView(IView view)
        {
        }

        /// <summary>
        /// The enable view.
        /// </summary>
        /// <param name="view">
        /// The view.
        /// </param>
        private void enableView(IView view)
        {
        }

        #endregion
    }
}