// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EventView.cs" company="">
//   
// </copyright>
// <summary>
//   The event view.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace strange.extensions.mediation.impl
{
    using strange.extensions.dispatcher.eventdispatcher.api;

    /// <summary>
    /// The event view.
    /// </summary>
    public class EventView : View
    {
        #region Public Properties

        /// <summary>
        /// Gets or sets the dispatcher.
        /// </summary>
        [Inject]
        public IEventDispatcher dispatcher { get; set; }

        #endregion
    }
}