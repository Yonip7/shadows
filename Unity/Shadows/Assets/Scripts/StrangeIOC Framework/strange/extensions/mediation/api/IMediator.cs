// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IMediator.cs" company="">
//   
// </copyright>
// <summary>
//   The Mediator interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace strange.extensions.mediation.api
{
    using UnityEngine;

    /// <summary>
    /// The Mediator interface.
    /// </summary>
    public interface IMediator
    {
        #region Public Properties

        /// <summary>
        /// The context view.
        /// </summary>
        /// Get/set the GameObject that represents the top-most item in this Context
        GameObject contextView { get; set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The on register.
        /// </summary>
        /// This method fires immediately after injection.
        /// Override to perform the actions you might normally perform in a constructor.
        void OnRegister();

        /// <summary>
        /// The on remove.
        /// </summary>
        /// This method fires just before a GameObject will be destroyed.
        /// Override to clean up any listeners, or anything else that might keep the View/Mediator pair from being garbage collected.
        void OnRemove();

        /// <summary>
        /// The pre register.
        /// </summary>
        /// This method fires immediately after instantiation, but before injection.
        /// Override to handle anything that needs to happen prior to injection.
        void PreRegister();

        #endregion
    }
}