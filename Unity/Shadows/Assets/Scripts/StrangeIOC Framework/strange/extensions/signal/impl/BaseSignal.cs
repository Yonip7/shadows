// --------------------------------------------------------------------------------------------------------------------
// <copyright file="BaseSignal.cs" company="">
//   
// </copyright>
// <summary>
//   The base signal.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace strange.extensions.signal.impl
{
    using System;
    using System.Collections.Generic;

    using strange.extensions.signal.api;

    /// <summary>
    /// The base signal.
    /// </summary>
    public class BaseSignal : IBaseSignal
    {
        #region Public Events

        /// <summary>
        /// The base listener.
        /// </summary>
        /// The delegate for repeating listeners
        public event Action<IBaseSignal, object[]> BaseListener = delegate { };

        /// <summary>
        /// The once base listener.
        /// </summary>
        /// The delegate for one-off listeners
        public event Action<IBaseSignal, object[]> OnceBaseListener = delegate { };

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The add listener.
        /// </summary>
        /// <param name="callback">
        /// The callback.
        /// </param>
        public void AddListener(Action<IBaseSignal, object[]> callback)
        {
            foreach (Delegate del in this.BaseListener.GetInvocationList())
            {
                Action<IBaseSignal, object[]> action = (Action<IBaseSignal, object[]>)del;
                if (callback.Equals(action))
                {
                    // If this callback exists already, ignore this addlistener
                    return;
                }
            }

            this.BaseListener += callback;
        }

        /// <summary>
        /// The add once.
        /// </summary>
        /// <param name="callback">
        /// The callback.
        /// </param>
        public void AddOnce(Action<IBaseSignal, object[]> callback)
        {
            foreach (Delegate del in this.OnceBaseListener.GetInvocationList())
            {
                Action<IBaseSignal, object[]> action = (Action<IBaseSignal, object[]>)del;
                if (callback.Equals(action))
                {
                    // If this callback exists already, ignore this addlistener
                    return;
                }
            }

            this.OnceBaseListener += callback;
        }

        /// <summary>
        /// The dispatch.
        /// </summary>
        /// <param name="args">
        /// The args.
        /// </param>
        public void Dispatch(object[] args)
        {
            this.BaseListener(this, args);
            this.OnceBaseListener(this, args);
            this.OnceBaseListener = delegate { };
        }

        /// <summary>
        /// The get types.
        /// </summary>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public virtual List<Type> GetTypes()
        {
            return new List<Type>();
        }

        /// <summary>
        /// The remove listener.
        /// </summary>
        /// <param name="callback">
        /// The callback.
        /// </param>
        public void RemoveListener(Action<IBaseSignal, object[]> callback)
        {
            this.BaseListener -= callback;
        }

        #endregion
    }
}