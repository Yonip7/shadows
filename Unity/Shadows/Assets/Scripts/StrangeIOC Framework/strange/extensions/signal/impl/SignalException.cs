﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SignalException.cs" company="">
//   
// </copyright>
// <summary>
//   The signal exception.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace strange.extensions.signal.impl
{
    using System;

    using strange.extensions.signal.api;

    /// <summary>
    /// The signal exception.
    /// </summary>
    public class SignalException : Exception
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="SignalException"/> class.
        /// </summary>
        public SignalException()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SignalException"/> class. 
        /// The signal exception.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        /// <param name="exceptionType">
        /// The exception Type.
        /// </param>
        /// Constructs a SignalException with a message and SignalExceptionType
        public SignalException(string message, SignalExceptionType exceptionType)
            : base(message)
        {
            this.type = exceptionType;
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        public SignalExceptionType type { get; set; }

        #endregion
    }
}