// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Signal.cs" company="">
//   
// </copyright>
// <summary>
//   The signal.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace strange.extensions.signal.impl
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// The signal.
    /// </summary>
    /// Base concrete form for a Signal with no parameters
    public class Signal : BaseSignal
    {
        #region Public Events

        /// <summary>
        /// The listener.
        /// </summary>
        public event Action Listener = delegate { };

        /// <summary>
        /// The once listener.
        /// </summary>
        public event Action OnceListener = delegate { };

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The add listener.
        /// </summary>
        /// <param name="callback">
        /// The callback.
        /// </param>
        public void AddListener(Action callback)
        {
            this.Listener += callback;
        }

        /// <summary>
        /// The add once.
        /// </summary>
        /// <param name="callback">
        /// The callback.
        /// </param>
        public void AddOnce(Action callback)
        {
            this.OnceListener += callback;
        }

        /// <summary>
        /// The dispatch.
        /// </summary>
        public void Dispatch()
        {
            this.Listener();
            this.OnceListener();
            this.OnceListener = delegate { };
            this.Dispatch(null);
        }

        /// <summary>
        /// The get types.
        /// </summary>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public override List<Type> GetTypes()
        {
            return new List<Type>();
        }

        /// <summary>
        /// The remove listener.
        /// </summary>
        /// <param name="callback">
        /// The callback.
        /// </param>
        public void RemoveListener(Action callback)
        {
            this.Listener -= callback;
        }

        #endregion
    }

    /// <summary>
    /// The signal.
    /// </summary>
    /// <typeparam name="T">
    /// </typeparam>
    /// Base concrete form for a Signal with one parameter
    public class Signal<T> : BaseSignal
    {
        #region Public Events

        /// <summary>
        /// The listener.
        /// </summary>
        public event Action<T> Listener = delegate { };

        /// <summary>
        /// The once listener.
        /// </summary>
        public event Action<T> OnceListener = delegate { };

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The add listener.
        /// </summary>
        /// <param name="callback">
        /// The callback.
        /// </param>
        public void AddListener(Action<T> callback)
        {
            this.Listener += callback;
        }

        /// <summary>
        /// The add once.
        /// </summary>
        /// <param name="callback">
        /// The callback.
        /// </param>
        public void AddOnce(Action<T> callback)
        {
            this.OnceListener += callback;
        }

        /// <summary>
        /// The dispatch.
        /// </summary>
        /// <param name="type1">
        /// The type 1.
        /// </param>
        public void Dispatch(T type1)
        {
            this.Listener(type1);
            this.OnceListener(type1);
            this.OnceListener = delegate { };
            object[] outv = { type1 };
            this.Dispatch(outv);
        }

        /// <summary>
        /// The get types.
        /// </summary>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public override List<Type> GetTypes()
        {
            List<Type> retv = new List<Type>();
            retv.Add(typeof(T));
            return retv;
        }

        /// <summary>
        /// The remove listener.
        /// </summary>
        /// <param name="callback">
        /// The callback.
        /// </param>
        public void RemoveListener(Action<T> callback)
        {
            this.Listener -= callback;
        }

        #endregion
    }

    /// <summary>
    /// The signal.
    /// </summary>
    /// <typeparam name="T">
    /// </typeparam>
    /// <typeparam name="U">
    /// </typeparam>
    /// Base concrete form for a Signal with two parameters
    public class Signal<T, U> : BaseSignal
    {
        #region Public Events

        /// <summary>
        /// The listener.
        /// </summary>
        public event Action<T, U> Listener = delegate { };

        /// <summary>
        /// The once listener.
        /// </summary>
        public event Action<T, U> OnceListener = delegate { };

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The add listener.
        /// </summary>
        /// <param name="callback">
        /// The callback.
        /// </param>
        public void AddListener(Action<T, U> callback)
        {
            this.Listener += callback;
        }

        /// <summary>
        /// The add once.
        /// </summary>
        /// <param name="callback">
        /// The callback.
        /// </param>
        public void AddOnce(Action<T, U> callback)
        {
            this.OnceListener += callback;
        }

        /// <summary>
        /// The dispatch.
        /// </summary>
        /// <param name="type1">
        /// The type 1.
        /// </param>
        /// <param name="type2">
        /// The type 2.
        /// </param>
        public void Dispatch(T type1, U type2)
        {
            this.Listener(type1, type2);
            this.OnceListener(type1, type2);
            this.OnceListener = delegate { };
            object[] outv = { type1, type2 };
            this.Dispatch(outv);
        }

        /// <summary>
        /// The get types.
        /// </summary>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public override List<Type> GetTypes()
        {
            List<Type> retv = new List<Type>();
            retv.Add(typeof(T));
            retv.Add(typeof(U));
            return retv;
        }

        /// <summary>
        /// The remove listener.
        /// </summary>
        /// <param name="callback">
        /// The callback.
        /// </param>
        public void RemoveListener(Action<T, U> callback)
        {
            this.Listener -= callback;
        }

        #endregion
    }

    /// <summary>
    /// The signal.
    /// </summary>
    /// <typeparam name="T">
    /// </typeparam>
    /// <typeparam name="U">
    /// </typeparam>
    /// <typeparam name="V">
    /// </typeparam>
    /// Base concrete form for a Signal with three parameters
    public class Signal<T, U, V> : BaseSignal
    {
        #region Public Events

        /// <summary>
        /// The listener.
        /// </summary>
        public event Action<T, U, V> Listener = delegate { };

        /// <summary>
        /// The once listener.
        /// </summary>
        public event Action<T, U, V> OnceListener = delegate { };

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The add listener.
        /// </summary>
        /// <param name="callback">
        /// The callback.
        /// </param>
        public void AddListener(Action<T, U, V> callback)
        {
            this.Listener += callback;
        }

        /// <summary>
        /// The add once.
        /// </summary>
        /// <param name="callback">
        /// The callback.
        /// </param>
        public void AddOnce(Action<T, U, V> callback)
        {
            this.OnceListener += callback;
        }

        /// <summary>
        /// The dispatch.
        /// </summary>
        /// <param name="type1">
        /// The type 1.
        /// </param>
        /// <param name="type2">
        /// The type 2.
        /// </param>
        /// <param name="type3">
        /// The type 3.
        /// </param>
        public void Dispatch(T type1, U type2, V type3)
        {
            this.Listener(type1, type2, type3);
            this.OnceListener(type1, type2, type3);
            this.OnceListener = delegate { };
            object[] outv = { type1, type2, type3 };
            this.Dispatch(outv);
        }

        /// <summary>
        /// The get types.
        /// </summary>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public override List<Type> GetTypes()
        {
            List<Type> retv = new List<Type>();
            retv.Add(typeof(T));
            retv.Add(typeof(U));
            retv.Add(typeof(V));
            return retv;
        }

        /// <summary>
        /// The remove listener.
        /// </summary>
        /// <param name="callback">
        /// The callback.
        /// </param>
        public void RemoveListener(Action<T, U, V> callback)
        {
            this.Listener -= callback;
        }

        #endregion
    }

    /// <summary>
    /// The signal.
    /// </summary>
    /// <typeparam name="T">
    /// </typeparam>
    /// <typeparam name="U">
    /// </typeparam>
    /// <typeparam name="V">
    /// </typeparam>
    /// <typeparam name="W">
    /// </typeparam>
    /// Base concrete form for a Signal with four parameters
    public class Signal<T, U, V, W> : BaseSignal
    {
        #region Public Events

        /// <summary>
        /// The listener.
        /// </summary>
        public event Action<T, U, V, W> Listener = delegate { };

        /// <summary>
        /// The once listener.
        /// </summary>
        public event Action<T, U, V, W> OnceListener = delegate { };

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The add listener.
        /// </summary>
        /// <param name="callback">
        /// The callback.
        /// </param>
        public void AddListener(Action<T, U, V, W> callback)
        {
            this.Listener += callback;
        }

        /// <summary>
        /// The add once.
        /// </summary>
        /// <param name="callback">
        /// The callback.
        /// </param>
        public void AddOnce(Action<T, U, V, W> callback)
        {
            this.OnceListener += callback;
        }

        /// <summary>
        /// The dispatch.
        /// </summary>
        /// <param name="type1">
        /// The type 1.
        /// </param>
        /// <param name="type2">
        /// The type 2.
        /// </param>
        /// <param name="type3">
        /// The type 3.
        /// </param>
        /// <param name="type4">
        /// The type 4.
        /// </param>
        public void Dispatch(T type1, U type2, V type3, W type4)
        {
            this.Listener(type1, type2, type3, type4);
            this.OnceListener(type1, type2, type3, type4);
            this.OnceListener = delegate { };
            object[] outv = { type1, type2, type3, type4 };
            this.Dispatch(outv);
        }

        /// <summary>
        /// The get types.
        /// </summary>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public override List<Type> GetTypes()
        {
            List<Type> retv = new List<Type>();
            retv.Add(typeof(T));
            retv.Add(typeof(U));
            retv.Add(typeof(V));
            retv.Add(typeof(W));
            return retv;
        }

        /// <summary>
        /// The remove listener.
        /// </summary>
        /// <param name="callback">
        /// The callback.
        /// </param>
        public void RemoveListener(Action<T, U, V, W> callback)
        {
            this.Listener -= callback;
        }

        #endregion
    }
}