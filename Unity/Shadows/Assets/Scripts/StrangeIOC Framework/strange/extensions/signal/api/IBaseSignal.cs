﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IBaseSignal.cs" company="">
//   
// </copyright>
// <summary>
//   The BaseSignal interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace strange.extensions.signal.api
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// The BaseSignal interface.
    /// </summary>
    public interface IBaseSignal
    {
        #region Public Methods and Operators

        /// <summary>
        /// The add listener.
        /// </summary>
        /// <param name="callback">
        /// The callback.
        /// </param>
        /// Attach a callback to this Signal
        /// The callback parameters must match the Types and order which were
        /// originally assigned to the Signal on its creation
        void AddListener(Action<IBaseSignal, object[]> callback);

        /// <summary>
        /// The add once.
        /// </summary>
        /// <param name="callback">
        /// The callback.
        /// </param>
        /// Attach a callback to this Signal for the duration of exactly one Dispatch
        /// The callback parameters must match the Types and order which were
        /// originally assigned to the Signal on its creation, and the callback
        /// will be removed immediately after the Signal dispatches
        void AddOnce(Action<IBaseSignal, object[]> callback);

        /// <summary>
        /// The dispatch.
        /// </summary>
        /// <param name="args">
        /// The args.
        /// </param>
        /// Instruct a Signal to call on all its registered listeners
        void Dispatch(object[] args);

        /// <summary>
        /// The get types.
        /// </summary>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        List<Type> GetTypes();

        /// <summary>
        /// The remove listener.
        /// </summary>
        /// <param name="callback">
        /// The callback.
        /// </param>
        /// Remove a callback from this Signal
        void RemoveListener(Action<IBaseSignal, object[]> callback);

        #endregion
    }
}