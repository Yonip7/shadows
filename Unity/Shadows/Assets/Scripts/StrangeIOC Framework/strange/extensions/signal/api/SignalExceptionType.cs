﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SignalExceptionType.cs" company="">
//   
// </copyright>
// <summary>
//   The signal exception type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace strange.extensions.signal.api
{
    /// <summary>
    /// The signal exception type.
    /// </summary>
    public enum SignalExceptionType
    {
        /// <summary>
        /// The comman d_ valu e_ conflict.
        /// </summary>
        /// Attempting to bind more than one value of the same type to a command
        COMMAND_VALUE_CONFLICT, 

        /// <summary>
        /// The comman d_ valu e_ no t_ found.
        /// </summary>
        /// A Signal mapped to a Command found no matching injectable Type to bind a parameter to.
        COMMAND_VALUE_NOT_FOUND, 

        /// <summary>
        /// The comman d_ nul l_ injection.
        /// </summary>
        /// SignalCommandBinder attempted to bind a null value from a signal to a Command
        COMMAND_NULL_INJECTION, 
    }
}