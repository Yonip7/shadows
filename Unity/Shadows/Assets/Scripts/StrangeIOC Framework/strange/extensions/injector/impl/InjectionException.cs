// --------------------------------------------------------------------------------------------------------------------
// <copyright file="InjectionException.cs" company="">
//   
// </copyright>
// <summary>
//   The injection exception.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace strange.extensions.injector.impl
{
    using System;

    using strange.extensions.injector.api;

    /// <summary>
    /// The injection exception.
    /// </summary>
    public class InjectionException : Exception
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="InjectionException"/> class.
        /// </summary>
        public InjectionException()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="InjectionException"/> class. 
        /// The injection exception.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        /// <param name="exceptionType">
        /// The exception Type.
        /// </param>
        /// Constructs an InjectionException with a message and InjectionExceptionType
        public InjectionException(string message, InjectionExceptionType exceptionType)
            : base(message)
        {
            this.type = exceptionType;
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        public InjectionExceptionType type { get; set; }

        #endregion
    }
}