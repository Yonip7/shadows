// --------------------------------------------------------------------------------------------------------------------
// <copyright file="InjectorFactory.cs" company="">
//   
// </copyright>
// <summary>
//   The injector factory.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace strange.extensions.injector.impl
{
    using System;

    using strange.extensions.injector.api;

    /// <summary>
    /// The injector factory.
    /// </summary>
    public class InjectorFactory : IInjectorFactory
    {
        #region Public Methods and Operators

        /// <summary>
        /// The get.
        /// </summary>
        /// <param name="binding">
        /// The binding.
        /// </param>
        /// <param name="args">
        /// The args.
        /// </param>
        /// <returns>
        /// The <see cref="object"/>.
        /// </returns>
        /// <exception cref="InjectionException">
        /// </exception>
        public object Get(IInjectionBinding binding, object[] args)
        {
            if (binding == null)
            {
                throw new InjectionException(
                    "InjectorFactory cannot act on null binding", InjectionExceptionType.NULL_BINDING);
            }

            InjectionBindingType type = binding.type;

            switch (type)
            {
                case InjectionBindingType.SINGLETON:
                    return this.singletonOf(binding, args);
                case InjectionBindingType.VALUE:
                    return this.valueOf(binding);
                default:
                    break;
            }

            return this.instanceOf(binding, args);
        }

        /// <summary>
        /// The get.
        /// </summary>
        /// <param name="binding">
        /// The binding.
        /// </param>
        /// <returns>
        /// The <see cref="object"/>.
        /// </returns>
        public object Get(IInjectionBinding binding)
        {
            return this.Get(binding, null);
        }

        #endregion

        #region Methods

        /// <summary>
        /// The create from value.
        /// </summary>
        /// <param name="o">
        /// The o.
        /// </param>
        /// <param name="args">
        /// The args.
        /// </param>
        /// Call the Activator to attempt instantiation the given object
        /// <returns>
        /// The <see cref="object"/>.
        /// </returns>
        protected object createFromValue(object o, object[] args)
        {
            Type value = (o is Type) ? o as Type : o.GetType();
            object retv = null;
            try
            {
                if (args == null || args.Length == 0)
                {
                    retv = Activator.CreateInstance(value);
                }
                else
                {
                    retv = Activator.CreateInstance(value, args);
                }
            }
            catch
            {
                // No-op
            }

            return retv;
        }

        /// <summary>
        /// The generate implicit.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <param name="args">
        /// The args.
        /// </param>
        /// <returns>
        /// The <see cref="object"/>.
        /// </returns>
        /// <exception cref="InjectionException">
        /// </exception>
        protected object generateImplicit(object key, object[] args)
        {
            Type type = key as Type;
            if (!type.IsInterface && !type.IsAbstract)
            {
                return this.createFromValue(key, args);
            }

            throw new InjectionException(
                "You can't create a class that can't be instantiated. Class: " + key, 
                InjectionExceptionType.NOT_INSTANTIABLE);
        }

        /// <summary>
        /// The instance of.
        /// </summary>
        /// <param name="binding">
        /// The binding.
        /// </param>
        /// <param name="args">
        /// The args.
        /// </param>
        /// Generate a new instance
        /// <returns>
        /// The <see cref="object"/>.
        /// </returns>
        protected object instanceOf(IInjectionBinding binding, object[] args)
        {
            if (binding.value != null)
            {
                return this.createFromValue(binding.value, args);
            }

            object value = this.generateImplicit((binding.key as object[])[0], args);
            return this.createFromValue(value, args);
        }

        /// <summary>
        /// The singleton of.
        /// </summary>
        /// <param name="binding">
        /// The binding.
        /// </param>
        /// <param name="args">
        /// The args.
        /// </param>
        /// Generate a Singleton instance
        /// <returns>
        /// The <see cref="object"/>.
        /// </returns>
        protected object singletonOf(IInjectionBinding binding, object[] args)
        {
            if (binding.value != null)
            {
                if (binding.value.GetType().IsInstanceOfType(typeof(Type)))
                {
                    binding.SetValue(this.createFromValue(binding.value, args));
                }
                else
                {
                    // no-op. We already have a binding value!
                }
            }
            else
            {
                binding.SetValue(this.generateImplicit((binding.key as object[])[0], args));
            }

            return binding.value;
        }

        /// <summary>
        /// The value of.
        /// </summary>
        /// <param name="binding">
        /// The binding.
        /// </param>
        /// The binding already has a value. Simply return it.
        /// <returns>
        /// The <see cref="object"/>.
        /// </returns>
        protected object valueOf(IInjectionBinding binding)
        {
            return binding.value;
        }

        #endregion
    }
}