// --------------------------------------------------------------------------------------------------------------------
// <copyright file="InjectionBindingType.cs" company="">
//   
// </copyright>
// <summary>
//   The injection binding type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace strange.extensions.injector.api
{
    /// <summary>
    /// The injection binding type.
    /// </summary>
    public enum InjectionBindingType
    {
        /// <summary>
        /// The default.
        /// </summary>
        /// The binding provides a new instance every time
        DEFAULT, 

        /// <summary>
        /// The singleton.
        /// </summary>
        /// The binding always provides the same instance
        SINGLETON, 

        /// <summary>
        /// The value.
        /// </summary>
        /// The binding always provides the same instance based on a provided value
        VALUE
    }
}