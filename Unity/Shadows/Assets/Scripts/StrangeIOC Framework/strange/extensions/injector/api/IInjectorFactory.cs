// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IInjectorFactory.cs" company="">
//   
// </copyright>
// <summary>
//   The InjectorFactory interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace strange.extensions.injector.api
{
    /// <summary>
    /// The InjectorFactory interface.
    /// </summary>
    public interface IInjectorFactory
    {
        #region Public Methods and Operators

        /// <summary>
        /// The get.
        /// </summary>
        /// <param name="binding">
        /// The binding.
        /// </param>
        /// Request instantiation based on the provided binding
        /// <returns>
        /// The <see cref="object"/>.
        /// </returns>
        object Get(IInjectionBinding binding);

        /// <summary>
        /// The get.
        /// </summary>
        /// <param name="binding">
        /// The binding.
        /// </param>
        /// <param name="args">
        /// The args.
        /// </param>
        /// Request instantiation based on the provided binding and an array of Constructor arguments
        /// <returns>
        /// The <see cref="object"/>.
        /// </returns>
        object Get(IInjectionBinding binding, object[] args);

        #endregion
    }
}