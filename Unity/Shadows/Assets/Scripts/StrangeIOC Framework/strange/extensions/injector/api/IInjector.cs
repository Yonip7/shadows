// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IInjector.cs" company="">
//   
// </copyright>
// <summary>
//   The Injector interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace strange.extensions.injector.api
{
    using strange.extensions.reflector.api;

    /// <summary>
    /// The Injector interface.
    /// </summary>
    public interface IInjector
    {
        #region Public Properties

        /// <summary>
        /// The binder.
        /// </summary>
        /// Get/set an InjectionBinder.
        IInjectionBinder binder { get; set; }

        /// <summary>
        /// The factory.
        /// </summary>
        /// Get/set an InjectorFactory.
        IInjectorFactory factory { get; set; }

        /// <summary>
        /// The reflector.
        /// </summary>
        /// Get/set a ReflectionBinder.
        IReflectionBinder reflector { get; set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The inject.
        /// </summary>
        /// <param name="target">
        /// The target.
        /// </param>
        /// Request that the provided target be injected.
        /// <returns>
        /// The <see cref="object"/>.
        /// </returns>
        object Inject(object target);

        /// <summary>
        /// The inject.
        /// </summary>
        /// <param name="target">
        /// The target.
        /// </param>
        /// <param name="attemptConstructorInjection">
        /// The attempt Constructor Injection.
        /// </param>
        /// Request that the provided target be injected.
        /// <returns>
        /// The <see cref="object"/>.
        /// </returns>
        object Inject(object target, bool attemptConstructorInjection);

        /// <summary>
        /// The instantiate.
        /// </summary>
        /// <param name="binding">
        /// The binding.
        /// </param>
        /// Request an instantiation based on the given binding.
        /// This request is made to the Injector, but it's really the InjectorFactory that does the instantiation.
        /// <returns>
        /// The <see cref="object"/>.
        /// </returns>
        object Instantiate(IInjectionBinding binding);

        #endregion
    }
}