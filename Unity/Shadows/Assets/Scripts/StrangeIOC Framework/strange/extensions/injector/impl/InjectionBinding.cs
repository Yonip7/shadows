// --------------------------------------------------------------------------------------------------------------------
// <copyright file="InjectionBinding.cs" company="">
//   
// </copyright>
// <summary>
//   The injection binding.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace strange.extensions.injector.impl
{
    using System;

    using strange.extensions.injector.api;
    using strange.framework.api;
    using strange.framework.impl;

    /// <summary>
    /// The injection binding.
    /// </summary>
    public class InjectionBinding : Binding, IInjectionBinding
    {
        #region Fields

        /// <summary>
        /// The _is cross context.
        /// </summary>
        private bool _isCrossContext;

        /// <summary>
        /// The _to inject.
        /// </summary>
        private bool _toInject = true;

        /// <summary>
        /// The _type.
        /// </summary>
        private InjectionBindingType _type = InjectionBindingType.DEFAULT;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="InjectionBinding"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver.
        /// </param>
        public InjectionBinding(Binder.BindingResolver resolver)
        {
            this.resolver = resolver;
            this.keyConstraint = BindingConstraintType.MANY;
            this.valueConstraint = BindingConstraintType.ONE;
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets a value indicating whether is cross context.
        /// </summary>
        public bool isCrossContext
        {
            get
            {
                return this._isCrossContext;
            }
        }

        /// <summary>
        /// Gets a value indicating whether to inject.
        /// </summary>
        public bool toInject
        {
            get
            {
                return this._toInject;
            }
        }

        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        public InjectionBindingType type
        {
            get
            {
                return this._type;
            }

            set
            {
                this._type = value;
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The bind.
        /// </summary>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// The <see cref="IInjectionBinding"/>.
        /// </returns>
        public IInjectionBinding Bind<T>()
        {
            return this.Key<T>();
        }

        /// <summary>
        /// The bind.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <returns>
        /// The <see cref="IInjectionBinding"/>.
        /// </returns>
        public IInjectionBinding Bind(object key)
        {
            return this.Key(key);
        }

        /// <summary>
        /// The cross context.
        /// </summary>
        /// <returns>
        /// The <see cref="IInjectionBinding"/>.
        /// </returns>
        public IInjectionBinding CrossContext()
        {
            this._isCrossContext = true;
            if (this.resolver != null)
            {
                this.resolver(this);
            }

            return this;
        }

        /// <summary>
        /// The key.
        /// </summary>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// The <see cref="IInjectionBinding"/>.
        /// </returns>
        public new IInjectionBinding Key<T>()
        {
            return base.Key<T>() as IInjectionBinding;
        }

        /// <summary>
        /// The key.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <returns>
        /// The <see cref="IInjectionBinding"/>.
        /// </returns>
        public new IInjectionBinding Key(object key)
        {
            return base.Key(key) as IInjectionBinding;
        }

        /// <summary>
        /// The named.
        /// </summary>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// The <see cref="IInjectionBinding"/>.
        /// </returns>
        public new IInjectionBinding Named<T>()
        {
            return base.Named<T>() as IInjectionBinding;
        }

        /// <summary>
        /// The named.
        /// </summary>
        /// <param name="o">
        /// The o.
        /// </param>
        /// <returns>
        /// The <see cref="IInjectionBinding"/>.
        /// </returns>
        public new IInjectionBinding Named(object o)
        {
            return base.Named(o) as IInjectionBinding;
        }

        /// <summary>
        /// The set value.
        /// </summary>
        /// <param name="o">
        /// The o.
        /// </param>
        /// <returns>
        /// The <see cref="IInjectionBinding"/>.
        /// </returns>
        /// <exception cref="InjectionException">
        /// </exception>
        public IInjectionBinding SetValue(object o)
        {
            Type objType = o.GetType();

            object[] keys = this.key as object[];
            int aa = keys.Length;

            // Check that value is legal for the provided keys
            for (int a = 0; a < aa; a++)
            {
                object aKey = keys[a];
                Type keyType = (aKey is Type) ? aKey as Type : aKey.GetType();
                if (keyType.IsAssignableFrom(objType) == false)
                {
                    throw new InjectionException(
                        "Injection cannot bind a value that does not extend or implement the binding type.", 
                        InjectionExceptionType.ILLEGAL_BINDING_VALUE);
                }
            }

            this.To(o);
            return this;
        }

        /// <summary>
        /// The to.
        /// </summary>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// The <see cref="IInjectionBinding"/>.
        /// </returns>
        public new IInjectionBinding To<T>()
        {
            return base.To<T>() as IInjectionBinding;
        }

        /// <summary>
        /// The to.
        /// </summary>
        /// <param name="o">
        /// The o.
        /// </param>
        /// <returns>
        /// The <see cref="IInjectionBinding"/>.
        /// </returns>
        public new IInjectionBinding To(object o)
        {
            return base.To(o) as IInjectionBinding;
        }

        /// <summary>
        /// The to inject.
        /// </summary>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <returns>
        /// The <see cref="IInjectionBinding"/>.
        /// </returns>
        public IInjectionBinding ToInject(bool value)
        {
            this._toInject = value;
            return this;
        }

        /// <summary>
        /// The to name.
        /// </summary>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// The <see cref="IInjectionBinding"/>.
        /// </returns>
        public new IInjectionBinding ToName<T>()
        {
            return base.ToName<T>() as IInjectionBinding;
        }

        /// <summary>
        /// The to name.
        /// </summary>
        /// <param name="o">
        /// The o.
        /// </param>
        /// <returns>
        /// The <see cref="IInjectionBinding"/>.
        /// </returns>
        public new IInjectionBinding ToName(object o)
        {
            return base.ToName(o) as IInjectionBinding;
        }

        /// <summary>
        /// The to singleton.
        /// </summary>
        /// <returns>
        /// The <see cref="IInjectionBinding"/>.
        /// </returns>
        public IInjectionBinding ToSingleton()
        {
            // If already a value, this mapping is redundant
            if (this.type == InjectionBindingType.VALUE)
            {
                return this;
            }

            this.type = InjectionBindingType.SINGLETON;
            if (this.resolver != null)
            {
                this.resolver(this);
            }

            return this;
        }

        /// <summary>
        /// The to value.
        /// </summary>
        /// <param name="o">
        /// The o.
        /// </param>
        /// <returns>
        /// The <see cref="IInjectionBinding"/>.
        /// </returns>
        public IInjectionBinding ToValue(object o)
        {
            this.type = InjectionBindingType.VALUE;
            this.SetValue(o);
            return this;
        }

        #endregion
    }
}