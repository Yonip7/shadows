// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IInjectionBinding.cs" company="">
//   
// </copyright>
// <summary>
//   The InjectionBinding interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace strange.extensions.injector.api
{
    using System;

    /// <summary>
    /// The InjectionBinding interface.
    /// </summary>
    public interface IInjectionBinding
    {
        #region Public Properties

        /// <summary>
        /// Gets a value indicating whether is cross context.
        /// </summary>
        bool isCrossContext { get; }

        /// <summary>
        /// Gets the key.
        /// </summary>
        object key { get; }

        /// <summary>
        /// Gets or sets the key constraint.
        /// </summary>
        Enum keyConstraint { get; set; }

        /// <summary>
        /// Gets the name.
        /// </summary>
        object name { get; }

        /// <summary>
        /// The to inject.
        /// </summary>
        /// Get the parameter that specifies whether this Binding allows an instance to be injected
        bool toInject { get; }

        /// <summary>
        /// The type.
        /// </summary>
        /// Get and set the InjectionBindingType
        /// @see InjectionBindingType
        InjectionBindingType type { get; set; }

        /// <summary>
        /// Gets the value.
        /// </summary>
        object value { get; }

        /// <summary>
        /// Gets or sets the value constraint.
        /// </summary>
        Enum valueConstraint { get; set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The bind.
        /// </summary>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// The <see cref="IInjectionBinding"/>.
        /// </returns>
        IInjectionBinding Bind<T>();

        /// <summary>
        /// The bind.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <returns>
        /// The <see cref="IInjectionBinding"/>.
        /// </returns>
        IInjectionBinding Bind(object key);

        /// <summary>
        /// The cross context.
        /// </summary>
        /// Map the binding and give access to all contexts in hierarchy
        /// <returns>
        /// The <see cref="IInjectionBinding"/>.
        /// </returns>
        IInjectionBinding CrossContext();

        /// <summary>
        /// The key.
        /// </summary>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// The <see cref="IInjectionBinding"/>.
        /// </returns>
        IInjectionBinding Key<T>();

        /// <summary>
        /// The key.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <returns>
        /// The <see cref="IInjectionBinding"/>.
        /// </returns>
        IInjectionBinding Key(object key);

        /// <summary>
        /// The named.
        /// </summary>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// The <see cref="IInjectionBinding"/>.
        /// </returns>
        IInjectionBinding Named<T>();

        /// <summary>
        /// The named.
        /// </summary>
        /// <param name="o">
        /// The o.
        /// </param>
        /// <returns>
        /// The <see cref="IInjectionBinding"/>.
        /// </returns>
        IInjectionBinding Named(object o);

        /// <summary>
        /// The set value.
        /// </summary>
        /// <param name="o">
        /// The o.
        /// </param>
        /// Map the Binding to a stated instance so that every `GetInstance()` on the Binder Key returns the provided imstance. Does not set type.
        /// <returns>
        /// The <see cref="IInjectionBinding"/>.
        /// </returns>
        IInjectionBinding SetValue(object o);

        /// <summary>
        /// The to.
        /// </summary>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// The <see cref="IInjectionBinding"/>.
        /// </returns>
        IInjectionBinding To<T>();

        /// <summary>
        /// The to.
        /// </summary>
        /// <param name="o">
        /// The o.
        /// </param>
        /// <returns>
        /// The <see cref="IInjectionBinding"/>.
        /// </returns>
        IInjectionBinding To(object o);

        /// <summary>
        /// The to inject.
        /// </summary>
        /// <param name="value">
        /// The value.
        /// </param>
        /// Boolean setter to optionally override injection. If false, the instance will not be injected after instantiation.
        /// <returns>
        /// The <see cref="IInjectionBinding"/>.
        /// </returns>
        IInjectionBinding ToInject(bool value);

        /// <summary>
        /// The to name.
        /// </summary>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// The <see cref="IInjectionBinding"/>.
        /// </returns>
        IInjectionBinding ToName<T>();

        /// <summary>
        /// The to name.
        /// </summary>
        /// <param name="o">
        /// The o.
        /// </param>
        /// <returns>
        /// The <see cref="IInjectionBinding"/>.
        /// </returns>
        IInjectionBinding ToName(object o);

        /// <summary>
        /// The to singleton.
        /// </summary>
        /// Map the Binding to a Singleton so that every `GetInstance()` on the Binder Key returns the same imstance.
        /// <returns>
        /// The <see cref="IInjectionBinding"/>.
        /// </returns>
        IInjectionBinding ToSingleton();

        /// <summary>
        /// The to value.
        /// </summary>
        /// <param name="o">
        /// The o.
        /// </param>
        /// Map the Binding to a stated instance so that every `GetInstance()` on the Binder Key returns the provided imstance. Sets type to Value
        /// <returns>
        /// The <see cref="IInjectionBinding"/>.
        /// </returns>
        IInjectionBinding ToValue(object o);

        #endregion
    }
}