// --------------------------------------------------------------------------------------------------------------------
// <copyright file="InjectAttribute.cs" company="">
//   
// </copyright>
// <summary>
//   The inject.
// </summary>
// --------------------------------------------------------------------------------------------------------------------



using System;

/// <summary>
/// The inject.
/// </summary>
[AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = true)]
public class Inject : Attribute
{
    #region Constructors and Destructors

    /// <summary>
    /// Initializes a new instance of the <see cref="Inject"/> class.
    /// </summary>
    public Inject()
    {
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="Inject"/> class.
    /// </summary>
    /// <param name="n">
    /// The n.
    /// </param>
    public Inject(object n)
    {
        this.name = n;
    }

    #endregion

    #region Public Properties

    /// <summary>
    /// Gets or sets the name.
    /// </summary>
    public object name { get; set; }

    #endregion
}

//Tag [PostConstruct] to perform post-injection construction actions
/// <summary>
/// The construct.
/// </summary>
[AttributeUsage(AttributeTargets.Constructor, AllowMultiple = false, Inherited = true)]
public class Construct : Attribute
{
}

//Tag [PostConstruct] to perform post-injection construction actions
/// <summary>
/// The post construct.
/// </summary>
[AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
public class PostConstruct : Attribute
{
}

/// <summary>
/// The deconstruct.
/// </summary>
[AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
public class Deconstruct : Attribute
{
}