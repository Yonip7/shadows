﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ICrossContextInjectionBinder.cs" company="">
//   
// </copyright>
// <summary>
//   The CrossContextInjectionBinder interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace strange.extensions.injector.api
{
    /// <summary>
    /// The CrossContextInjectionBinder interface.
    /// </summary>
    public interface ICrossContextInjectionBinder : IInjectionBinder
    {
        // Cross-context Injection Binder is shared across all child contexts
        #region Public Properties

        /// <summary>
        /// Gets or sets the cross context binder.
        /// </summary>
        IInjectionBinder CrossContextBinder { get; set; }

        #endregion
    }
}