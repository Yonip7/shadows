// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IInjectionBinder.cs" company="">
//   
// </copyright>
// <summary>
//   The InjectionBinder interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace strange.extensions.injector.api
{
    using System;
    using System.Collections.Generic;

    using strange.framework.api;

    /// <summary>
    /// The InjectionBinder interface.
    /// </summary>
    public interface IInjectionBinder
    {
        #region Public Properties

        /// <summary>
        /// The injector.
        /// </summary>
        /// Get or set an Injector to use. By default, Injector instantiates it's own, but that can be overridden.
        IInjector injector { get; set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The bind.
        /// </summary>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// The <see cref="IInjectionBinding"/>.
        /// </returns>
        IInjectionBinding Bind<T>();

        /// <summary>
        /// The bind.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <returns>
        /// The <see cref="IInjectionBinding"/>.
        /// </returns>
        IInjectionBinding Bind(Type key);

        /// <summary>
        /// The bind.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <returns>
        /// The <see cref="IBinding"/>.
        /// </returns>
        IBinding Bind(object key);

        /// <summary>
        /// The get binding.
        /// </summary>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// The <see cref="IInjectionBinding"/>.
        /// </returns>
        IInjectionBinding GetBinding<T>();

        /// <summary>
        /// The get binding.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// The <see cref="IInjectionBinding"/>.
        /// </returns>
        IInjectionBinding GetBinding<T>(object name);

        /// <summary>
        /// The get binding.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <returns>
        /// The <see cref="IInjectionBinding"/>.
        /// </returns>
        IInjectionBinding GetBinding(object key);

        /// <summary>
        /// The get binding.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <returns>
        /// The <see cref="IInjectionBinding"/>.
        /// </returns>
        IInjectionBinding GetBinding(object key, object name);

        /// <summary>
        /// The get instance.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// Retrieve an Instance based on the key.
        /// ex. `injectionBinder.Get(typeof(ISomeInterface));`
        /// <returns>
        /// The <see cref="object"/>.
        /// </returns>
        object GetInstance(Type key);

        /// <summary>
        /// The get instance.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <param name="name">
        /// The name.
        /// </param>
        /// Retrieve an Instance based on a key/name combo.
        /// ex. `injectionBinder.Get(typeof(ISomeInterface), SomeEnum.MY_ENUM);`
        /// <returns>
        /// The <see cref="object"/>.
        /// </returns>
        object GetInstance(Type key, object name);

        /// <summary>
        /// The get instance.
        /// </summary>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// The <see cref="object"/>.
        /// </returns>
        object GetInstance<T>();

        /// <summary>
        /// The get instance.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// The <see cref="object"/>.
        /// </returns>
        object GetInstance<T>(object name);

        /// <summary>
        /// The reflect.
        /// </summary>
        /// <param name="list">
        /// The list.
        /// </param>
        /// Reflect all the types in the list
        /// Return the number of types in the list, which should be equal to the list length
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        int Reflect(List<Type> list);

        /// <summary>
        /// The reflect all.
        /// </summary>
        /// Reflect all the types currently registered with InjectionBinder
        /// Return the number of types reflected, which should be equal to the number
        /// of concrete classes you've mapped.
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        int ReflectAll();

        /// <summary>
        /// Places individual Bindings into the bindings Dictionary as part of the resolving process
        /// </summary>
        /// <param name="binding">
        /// The binding.
        /// </param>
        /// <param name="key">
        /// The key.
        /// </param>
        /// Note that while some Bindings may store multiple keys, each key takes a unique position in the
        /// bindings Dictionary.
        /// 
        /// Conflicts in the course of fluent binding are expected, but GetBinding
        /// will throw an error if there are any unresolved conflicts.
        void ResolveBinding(IBinding binding, object key);

        /// <summary>
        /// The unbind.
        /// </summary>
        /// <typeparam name="T">
        /// </typeparam>
        void Unbind<T>();

        /// <summary>
        /// The unbind.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <typeparam name="T">
        /// </typeparam>
        void Unbind<T>(object name);

        /// <summary>
        /// The unbind.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        void Unbind(object key);

        /// <summary>
        /// The unbind.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <param name="name">
        /// The name.
        /// </param>
        void Unbind(object key, object name);

        /// <summary>
        /// The unbind.
        /// </summary>
        /// <param name="binding">
        /// The binding.
        /// </param>
        void Unbind(IBinding binding);

        #endregion
    }
}