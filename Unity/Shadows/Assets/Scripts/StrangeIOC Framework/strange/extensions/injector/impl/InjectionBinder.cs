// --------------------------------------------------------------------------------------------------------------------
// <copyright file="InjectionBinder.cs" company="">
//   
// </copyright>
// <summary>
//   The injection binder.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace strange.extensions.injector.impl
{
    using System;
    using System.Collections.Generic;

    using strange.extensions.injector.api;
    using strange.extensions.reflector.impl;
    using strange.framework.api;
    using strange.framework.impl;

    /// <summary>
    /// The injection binder.
    /// </summary>
    public class InjectionBinder : Binder, IInjectionBinder
    {
        #region Fields

        /// <summary>
        /// The _injector.
        /// </summary>
        private IInjector _injector;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="InjectionBinder"/> class.
        /// </summary>
        public InjectionBinder()
        {
            this.injector = new Injector();
            this.injector.binder = this;
            this.injector.reflector = new ReflectionBinder();
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets the injector.
        /// </summary>
        public IInjector injector
        {
            get
            {
                return this._injector;
            }

            set
            {
                if (this._injector != null)
                {
                    this._injector.binder = null;
                }

                this._injector = value;
                this._injector.binder = this;
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The bind.
        /// </summary>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// The <see cref="IInjectionBinding"/>.
        /// </returns>
        public new IInjectionBinding Bind<T>()
        {
            return base.Bind<T>() as IInjectionBinding;
        }

        /// <summary>
        /// The bind.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <returns>
        /// The <see cref="IInjectionBinding"/>.
        /// </returns>
        public IInjectionBinding Bind(Type key)
        {
            return base.Bind(key) as IInjectionBinding;
        }

        /// <summary>
        /// The get binding.
        /// </summary>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// The <see cref="IInjectionBinding"/>.
        /// </returns>
        public new virtual IInjectionBinding GetBinding<T>()
        {
            return base.GetBinding<T>() as IInjectionBinding;
        }

        /// <summary>
        /// The get binding.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// The <see cref="IInjectionBinding"/>.
        /// </returns>
        public new IInjectionBinding GetBinding<T>(object name)
        {
            return base.GetBinding<T>(name) as IInjectionBinding;
        }

        /// <summary>
        /// The get binding.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <returns>
        /// The <see cref="IInjectionBinding"/>.
        /// </returns>
        public new IInjectionBinding GetBinding(object key)
        {
            return base.GetBinding(key) as IInjectionBinding;
        }

        /// <summary>
        /// The get binding.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <returns>
        /// The <see cref="IInjectionBinding"/>.
        /// </returns>
        public new virtual IInjectionBinding GetBinding(object key, object name)
        {
            return base.GetBinding(key, name) as IInjectionBinding;
        }

        /// <summary>
        /// The get instance.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <returns>
        /// The <see cref="object"/>.
        /// </returns>
        public object GetInstance(Type key)
        {
            return this.GetInstance(key, null);
        }

        /// <summary>
        /// The get instance.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <returns>
        /// The <see cref="object"/>.
        /// </returns>
        /// <exception cref="InjectionException">
        /// </exception>
        public virtual object GetInstance(Type key, object name)
        {
            IInjectionBinding binding = this.GetBinding(key, name);
            if (binding == null)
            {
                throw new InjectionException(
                    "InjectionBinder has no binding for:\n\tkey: " + key + "\nname: " + name, 
                    InjectionExceptionType.NULL_BINDING);
            }

            object instance = this.GetInjectorForBinding(binding).Instantiate(binding);
            return instance;
        }

        /// <summary>
        /// The get instance.
        /// </summary>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// The <see cref="object"/>.
        /// </returns>
        public object GetInstance<T>()
        {
            return this.GetInstance(typeof(T));
        }

        /// <summary>
        /// The get instance.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// The <see cref="object"/>.
        /// </returns>
        public object GetInstance<T>(object name)
        {
            return this.GetInstance(typeof(T), name);
        }

        /// <summary>
        /// The get raw binding.
        /// </summary>
        /// <returns>
        /// The <see cref="IBinding"/>.
        /// </returns>
        public override IBinding GetRawBinding()
        {
            return new InjectionBinding(this.resolver);
        }

        /// <summary>
        /// The reflect.
        /// </summary>
        /// <param name="list">
        /// The list.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        public int Reflect(List<Type> list)
        {
            int count = 0;
            foreach (Type t in list)
            {
                // Reflector won't permit primitive types, so screen them
                if (t.IsPrimitive || t == typeof(Decimal) || t == typeof(string))
                {
                    continue;
                }

                count ++;
                this.injector.reflector.Get(t);
            }

            return count;
        }

        /// <summary>
        /// The reflect all.
        /// </summary>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        public int ReflectAll()
        {
            List<Type> list = new List<Type>();
            foreach (KeyValuePair<object, Dictionary<object, IBinding>> pair in this.bindings)
            {
                Dictionary<object, IBinding> dict = pair.Value;
                foreach (KeyValuePair<object, IBinding> bPair in dict)
                {
                    IBinding binding = bPair.Value;
                    Type t = (binding.value is Type) ? (Type)binding.value : binding.value.GetType();
                    if (list.IndexOf(t) == -1)
                    {
                        list.Add(t);
                    }
                }
            }

            return this.Reflect(list);
        }

        #endregion

        #region Methods

        /// <summary>
        /// The get injector for binding.
        /// </summary>
        /// <param name="binding">
        /// The binding.
        /// </param>
        /// <returns>
        /// The <see cref="IInjector"/>.
        /// </returns>
        protected virtual IInjector GetInjectorForBinding(IInjectionBinding binding)
        {
            return this.injector;
        }

        #endregion
    }
}