﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CrossContextInjectionBinder.cs" company="">
//   
// </copyright>
// <summary>
//   The cross context injection binder.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace strange.extensions.injector.impl
{
    using strange.extensions.injector.api;
    using strange.framework.api;

    /// <summary>
    /// The cross context injection binder.
    /// </summary>
    public class CrossContextInjectionBinder : InjectionBinder, ICrossContextInjectionBinder
    {
        #region Public Properties

        /// <summary>
        /// Gets or sets the cross context binder.
        /// </summary>
        public IInjectionBinder CrossContextBinder { get; set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The get binding.
        /// </summary>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// The <see cref="IInjectionBinding"/>.
        /// </returns>
        public override IInjectionBinding GetBinding<T>()
        {
            return this.GetBinding(typeof(T), null);
        }

        /// <summary>
        /// The get binding.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <returns>
        /// The <see cref="IInjectionBinding"/>.
        /// </returns>
        public override IInjectionBinding GetBinding(object key, object name)
        {
            IInjectionBinding binding = base.GetBinding(key, name);
            if (binding == null)
            {
                // Attempt to get this from the cross context. Cross context is always SECOND PRIORITY. Local injections always override
                if (this.CrossContextBinder != null)
                {
                    binding = this.CrossContextBinder.GetBinding(key, name);
                }
            }

            return binding;
        }

        /// <summary>
        /// The resolve binding.
        /// </summary>
        /// <param name="binding">
        /// The binding.
        /// </param>
        /// <param name="key">
        /// The key.
        /// </param>
        public override void ResolveBinding(IBinding binding, object key)
        {
            // Decide whether to resolve locally or not
            if (binding is IInjectionBinding)
            {
                InjectionBinding injectionBinding = (InjectionBinding)binding;
                if (injectionBinding.isCrossContext)
                {
                    if (this.CrossContextBinder == null)
                    {
                        // We are a crosscontextbinder
                        base.ResolveBinding(binding, key);
                    }
                    else
                    {
                        this.Unbind(key, binding.name); // remove this cross context binding from the local binder
                        this.CrossContextBinder.ResolveBinding(binding, key);
                    }
                }
                else
                {
                    base.ResolveBinding(binding, key);
                }
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// The get injector for binding.
        /// </summary>
        /// <param name="binding">
        /// The binding.
        /// </param>
        /// <returns>
        /// The <see cref="IInjector"/>.
        /// </returns>
        protected override IInjector GetInjectorForBinding(IInjectionBinding binding)
        {
            if (binding.isCrossContext && this.CrossContextBinder != null)
            {
                return this.CrossContextBinder.injector;
            }
            else
            {
                return this.injector;
            }
        }

        #endregion
    }
}