// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Injector.cs" company="">
//   
// </copyright>
// <summary>
//   The injector.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace strange.extensions.injector.impl
{
    using System;
    using System.Collections.Generic;
    using System.Reflection;

    using strange.extensions.injector.api;
    using strange.extensions.reflector.api;

    /// <summary>
    /// The injector.
    /// </summary>
    public class Injector : IInjector
    {
        #region Fields

        /// <summary>
        /// The infinit y_ limit.
        /// </summary>
        private int INFINITY_LIMIT = 10;

        /// <summary>
        /// The infinity lock.
        /// </summary>
        private Dictionary<IInjectionBinding, int> infinityLock;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="Injector"/> class.
        /// </summary>
        public Injector()
        {
            this.factory = new InjectorFactory();
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets the binder.
        /// </summary>
        public IInjectionBinder binder { get; set; }

        /// <summary>
        /// Gets or sets the factory.
        /// </summary>
        public IInjectorFactory factory { get; set; }

        /// <summary>
        /// Gets or sets the reflector.
        /// </summary>
        public IReflectionBinder reflector { get; set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The inject.
        /// </summary>
        /// <param name="target">
        /// The target.
        /// </param>
        /// <returns>
        /// The <see cref="object"/>.
        /// </returns>
        public object Inject(object target)
        {
            return this.Inject(target, true);
        }

        /// <summary>
        /// The inject.
        /// </summary>
        /// <param name="target">
        /// The target.
        /// </param>
        /// <param name="attemptConstructorInjection">
        /// The attempt constructor injection.
        /// </param>
        /// <returns>
        /// The <see cref="object"/>.
        /// </returns>
        public object Inject(object target, bool attemptConstructorInjection)
        {
            this.failIf(
                this.binder == null, 
                "Attempt to inject into Injector without a Binder", 
                InjectionExceptionType.NO_BINDER);
            this.failIf(
                this.reflector == null, "Attempt to inject without a reflector", InjectionExceptionType.NO_REFLECTOR);
            this.failIf(target == null, "Attempt to inject into null instance", InjectionExceptionType.NULL_TARGET);

            // Some things can't be injected into. Bail out.
            Type t = target.GetType();
            if (t.IsPrimitive || t == typeof(Decimal) || t == typeof(string))
            {
                return target;
            }

            IReflectedClass reflection = this.reflector.Get(t);

            if (attemptConstructorInjection)
            {
                target = this.performConstructorInjection(target, reflection);
            }

            this.performSetterInjection(target, reflection);
            this.postInject(target, reflection);
            return target;
        }

        /// <summary>
        /// The instantiate.
        /// </summary>
        /// <param name="binding">
        /// The binding.
        /// </param>
        /// <returns>
        /// The <see cref="object"/>.
        /// </returns>
        public object Instantiate(IInjectionBinding binding)
        {
            this.failIf(
                this.binder == null, 
                "Attempt to instantiate from Injector without a Binder", 
                InjectionExceptionType.NO_BINDER);
            this.failIf(
                this.factory == null, 
                "Attempt to inject into Injector without a Factory", 
                InjectionExceptionType.NO_FACTORY);

            this.armorAgainstInfiniteLoops(binding);

            object retv = this.factory.Get(binding);

            // Factory can return null in the case that there are no parameterless constructors.
            // In this case, the following routine attempts to generate based on a preferred constructor
            if (retv == null)
            {
                IReflectedClass reflection = this.reflector.Get(binding.value as Type);
                Type[] parameters = reflection.constructorParameters;
                int aa = parameters.Length;
                object[] args = new object[aa];
                for (int a = 0; a < aa; a++)
                {
                    args[a] = this.getValueInjection(parameters[a], null, retv);
                }

                retv = this.factory.Get(binding, args);
                if (retv == null)
                {
                    return null;
                }

                retv = this.Inject(retv, false);
            }
            else if (binding.toInject)
            {
                retv = this.Inject(retv, binding.type != InjectionBindingType.VALUE);
                if (binding.type == InjectionBindingType.SINGLETON || binding.type == InjectionBindingType.VALUE)
                {
                    // prevent double-injection
                    binding.ToInject(false);
                }
            }

            this.infinityLock = null;

            return retv;
        }

        #endregion

        #region Methods

        /// <summary>
        /// The armor against infinite loops.
        /// </summary>
        /// <param name="binding">
        /// The binding.
        /// </param>
        /// <exception cref="InjectionException">
        /// </exception>
        private void armorAgainstInfiniteLoops(IInjectionBinding binding)
        {
            if (binding == null)
            {
                return;
            }

            if (this.infinityLock == null)
            {
                this.infinityLock = new Dictionary<IInjectionBinding, int>();
            }

            if (this.infinityLock.ContainsKey(binding) == false)
            {
                this.infinityLock.Add(binding, 0);
            }

            this.infinityLock[binding] = this.infinityLock[binding] + 1;
            if (this.infinityLock[binding] > this.INFINITY_LIMIT)
            {
                throw new InjectionException(
                    "There appears to be a circular dependency. Terminating loop.", 
                    InjectionExceptionType.CIRCULAR_DEPENDENCY);
            }
        }

        /// <summary>
        /// The fail if.
        /// </summary>
        /// <param name="condition">
        /// The condition.
        /// </param>
        /// <param name="message">
        /// The message.
        /// </param>
        /// <param name="type">
        /// The type.
        /// </param>
        private void failIf(bool condition, string message, InjectionExceptionType type)
        {
            this.failIf(condition, message, type, null, null, null);
        }

        /// <summary>
        /// The fail if.
        /// </summary>
        /// <param name="condition">
        /// The condition.
        /// </param>
        /// <param name="message">
        /// The message.
        /// </param>
        /// <param name="type">
        /// The type.
        /// </param>
        /// <param name="t">
        /// The t.
        /// </param>
        /// <param name="name">
        /// The name.
        /// </param>
        private void failIf(bool condition, string message, InjectionExceptionType type, Type t, object name)
        {
            this.failIf(condition, message, type, t, name, null);
        }

        /// <summary>
        /// The fail if.
        /// </summary>
        /// <param name="condition">
        /// The condition.
        /// </param>
        /// <param name="message">
        /// The message.
        /// </param>
        /// <param name="type">
        /// The type.
        /// </param>
        /// <param name="t">
        /// The t.
        /// </param>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <param name="target">
        /// The target.
        /// </param>
        /// <exception cref="InjectionException">
        /// </exception>
        private void failIf(
            bool condition, string message, InjectionExceptionType type, Type t, object name, object target)
        {
            if (condition)
            {
                message += "\n\t\ttarget: " + target;
                message += "\n\t\ttype: " + t;
                message += "\n\t\tname: " + name;
                throw new InjectionException(message, type);
            }
        }

        /// <summary>
        /// The get value injection.
        /// </summary>
        /// <param name="t">
        /// The t.
        /// </param>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <param name="target">
        /// The target.
        /// </param>
        /// <returns>
        /// The <see cref="object"/>.
        /// </returns>
        private object getValueInjection(Type t, object name, object target)
        {
            IInjectionBinding binding = this.binder.GetBinding(t, name);
            this.failIf(
                binding == null, 
                "Attempt to Instantiate a null binding.", 
                InjectionExceptionType.NULL_BINDING, 
                t, 
                name, 
                target);
            if (binding.type == InjectionBindingType.VALUE)
            {
                if (!binding.toInject)
                {
                    return binding.value;
                }
                else
                {
                    return this.Inject(binding.value, false);
                }
            }
            else
            {
                return this.Instantiate(binding);
            }
        }

        // Inject the value into the target at the specified injection point
        /// <summary>
        /// The inject value into point.
        /// </summary>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <param name="target">
        /// The target.
        /// </param>
        /// <param name="point">
        /// The point.
        /// </param>
        private void injectValueIntoPoint(object value, object target, PropertyInfo point)
        {
            this.failIf(target == null, "Attempt to inject into a null target", InjectionExceptionType.NULL_TARGET);
            this.failIf(
                point == null, "Attempt to inject into a null point", InjectionExceptionType.NULL_INJECTION_POINT);
            this.failIf(
                value == null, 
                "Attempt to inject null into a target object", 
                InjectionExceptionType.NULL_VALUE_INJECTION);

            point.SetValue(target, value, null);
        }

        /// <summary>
        /// The perform constructor injection.
        /// </summary>
        /// <param name="target">
        /// The target.
        /// </param>
        /// <param name="reflection">
        /// The reflection.
        /// </param>
        /// <returns>
        /// The <see cref="object"/>.
        /// </returns>
        private object performConstructorInjection(object target, IReflectedClass reflection)
        {
            this.failIf(
                target == null, 
                "Attempt to perform constructor injection into a null object", 
                InjectionExceptionType.NULL_TARGET);
            this.failIf(
                reflection == null, 
                "Attempt to perform constructor injection without a reflection", 
                InjectionExceptionType.NULL_REFLECTION);

            ConstructorInfo constructor = reflection.constructor;
            this.failIf(
                constructor == null, 
                "Attempt to construction inject a null constructor", 
                InjectionExceptionType.NULL_CONSTRUCTOR);

            Type[] constructorParameters = reflection.constructorParameters;
            object[] values = new object[constructorParameters.Length];
            int i = 0;
            foreach (Type type in constructorParameters)
            {
                values[i] = this.getValueInjection(type, null, target);
                i++;
            }

            if (values.Length == 0)
            {
                return target;
            }

            object constructedObj = constructor.Invoke(values);
            return (constructedObj == null) ? target : constructedObj;
        }

        /// <summary>
        /// The perform setter injection.
        /// </summary>
        /// <param name="target">
        /// The target.
        /// </param>
        /// <param name="reflection">
        /// The reflection.
        /// </param>
        private void performSetterInjection(object target, IReflectedClass reflection)
        {
            this.failIf(target == null, "Attempt to inject into a null object", InjectionExceptionType.NULL_TARGET);
            this.failIf(
                reflection == null, "Attempt to inject without a reflection", InjectionExceptionType.NULL_REFLECTION);
            this.failIf(
                reflection.setters.Length != reflection.setterNames.Length, 
                "Attempt to perform setter injection with mismatched names.\nThere must be exactly as many names as setters.", 
                InjectionExceptionType.SETTER_NAME_MISMATCH);

            int aa = reflection.setters.Length;
            for (int a = 0; a < aa; a++)
            {
                KeyValuePair<Type, PropertyInfo> pair = reflection.setters[a];
                object value = this.getValueInjection(pair.Key, reflection.setterNames[a], target);
                this.injectValueIntoPoint(value, target, pair.Value);
            }
        }

        /// <summary>
        /// The post inject.
        /// </summary>
        /// <param name="target">
        /// The target.
        /// </param>
        /// <param name="reflection">
        /// The reflection.
        /// </param>
        private void postInject(object target, IReflectedClass reflection)
        {
            this.failIf(target == null, "Attempt to PostConstruct a null target", InjectionExceptionType.NULL_TARGET);
            this.failIf(
                reflection == null, 
                "Attempt to PostConstruct without a reflection", 
                InjectionExceptionType.NULL_REFLECTION);

            MethodInfo[] postConstructors = reflection.postConstructors;
            if (postConstructors != null)
            {
                foreach (MethodInfo method in postConstructors)
                {
                    method.Invoke(target, null);
                }
            }
        }

        #endregion
    }
}