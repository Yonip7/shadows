// --------------------------------------------------------------------------------------------------------------------
// <copyright file="InjectionExceptionType.cs" company="">
//   
// </copyright>
// <summary>
//   The injection exception type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace strange.extensions.injector.api
{
    /// <summary>
    /// The injection exception type.
    /// </summary>
    public enum InjectionExceptionType
    {
        /// <summary>
        /// The circula r_ dependency.
        /// </summary>
        /// A circular dependency was found.
        /// This Exception was raised to prevent a stack overflow.
        CIRCULAR_DEPENDENCY, 

        /// <summary>
        /// The illega l_ bindin g_ value.
        /// </summary>
        /// The value of a binding does not extend or implement the binding type.
        ILLEGAL_BINDING_VALUE, 

        /// <summary>
        /// The n o_ binder.
        /// </summary>
        /// No InjectionBinder found.
        NO_BINDER, 

        /// <summary>
        /// The n o_ reflector.
        /// </summary>
        /// No ReflectionBinder found.
        NO_REFLECTOR, 

        /// <summary>
        /// The n o_ factory.
        /// </summary>
        /// No InjectorFactory found.
        NO_FACTORY, 

        /// <summary>
        /// The no t_ instantiable.
        /// </summary>
        /// The provided binding is not an instantiable class.
        NOT_INSTANTIABLE, 

        /// <summary>
        /// The nul l_ binding.
        /// </summary>
        /// The requested Binding was null or couldn't be found.
        NULL_BINDING, 

        /// <summary>
        /// The nul l_ constructor.
        /// </summary>
        /// During an attempt to construct, no constructor was found.
        NULL_CONSTRUCTOR, 

        /// <summary>
        /// The nul l_ injectio n_ point.
        /// </summary>
        /// During setter injection the requested setter resolved to null.
        NULL_INJECTION_POINT, 

        /// <summary>
        /// The nul l_ reflection.
        /// </summary>
        /// No reflection was provided for the requested class.
        NULL_REFLECTION, 

        /// <summary>
        /// The nul l_ target.
        /// </summary>
        /// The instance being injected into resolved to null.
        NULL_TARGET, 

        /// <summary>
        /// The nul l_ valu e_ injection.
        /// </summary>
        /// The value being injected into the target resolved to null.
        NULL_VALUE_INJECTION, 

        /// <summary>
        /// The sette r_ nam e_ mismatch.
        /// </summary>
        /// The list of setters and setter names must have exactly the same number of entries.
        /// Two lists are required because Unity does not at present support Tuple.
        /// Seeing this error likely indicates a problem with the Reflector (it's not you, it's me).
        SETTER_NAME_MISMATCH, 

        /// <summary>
        /// The missin g_ cros s_ contex t_ injector.
        /// </summary>
        /// The requested cross-context injector returned null
        MISSING_CROSS_CONTEXT_INJECTOR, 
    }
}