// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Command.cs" company="">
//   
// </copyright>
// <summary>
//   The command.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace strange.extensions.command.impl
{
    using strange.extensions.command.api;
    using strange.extensions.injector.api;

    /// <summary>
    /// The command.
    /// </summary>
    public class Command : ICommand
    {
        #region Fields

        /// <summary>
        /// The _retain.
        /// </summary>
        protected bool _retain = false;

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets a value indicating whether cancelled.
        /// </summary>
        public bool cancelled { get; set; }

        /// <summary>
        /// The command binder.
        /// </summary>
        /// Back reference to the CommandBinder that instantiated this Commmand
        [Inject]
        public ICommandBinder commandBinder { get; set; }

        /// <summary>
        /// Gets or sets the data.
        /// </summary>
        public object data { get; set; }

        /// <summary>
        /// The injection binder.
        /// </summary>
        /// The InjectionBinder for this Context
        [Inject]
        public IInjectionBinder injectionBinder { get; set; }

        /// <summary>
        /// Gets a value indicating whether retain.
        /// </summary>
        public bool retain
        {
            get
            {
                return this._retain;
            }
        }

        /// <summary>
        /// Gets or sets the sequence id.
        /// </summary>
        public int sequenceId { get; set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The cancel.
        /// </summary>
        public void Cancel()
        {
            this.cancelled = true;
        }

        /// <summary>
        /// The execute.
        /// </summary>
        /// <exception cref="CommandException">
        /// </exception>
        public virtual void Execute()
        {
            throw new CommandException(
                "You must override the Execute method in every Command", CommandExceptionType.EXECUTE_OVERRIDE);
        }

        /// <summary>
        /// The fail.
        /// </summary>
        public void Fail()
        {
            if (this.commandBinder != null)
            {
                this.commandBinder.Stop(this);
            }
        }

        /// <summary>
        /// The release.
        /// </summary>
        public void Release()
        {
            this._retain = false;
            if (this.commandBinder != null)
            {
                this.commandBinder.ReleaseCommand(this);
            }
        }

        /// <summary>
        /// The retain.
        /// </summary>
        public void Retain()
        {
            this._retain = true;
        }

        #endregion
    }
}