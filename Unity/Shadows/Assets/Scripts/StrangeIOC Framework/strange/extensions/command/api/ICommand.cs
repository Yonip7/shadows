// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ICommand.cs" company="">
//   
// </copyright>
// <summary>
//   The Command interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace strange.extensions.command.api
{
    /// <summary>
    /// The Command interface.
    /// </summary>
    public interface ICommand
    {
        #region Public Properties

        /// <summary>
        /// The cancelled.
        /// </summary>
        /// The property set to true by a Cancel() call.
        /// Use cancelled internally to determine if further execution is warranted, especially in
        /// asynchronous calls.
        bool cancelled { get; set; }

        /// <summary>
        /// The data.
        /// </summary>
        /// A payload injected into the Command. Most commonly, this an IEvent.
        object data { get; set; }

        /// <summary>
        /// The retain.
        /// </summary>
        /// The property set by `Retain` and `Release` to indicate whether the Command should be cleaned up on completion of the `Execute()` method.
        bool retain { get; }

        // The ordered id of this Command, used in sequencing to find the next Command.
        /// <summary>
        /// Gets or sets the sequence id.
        /// </summary>
        int sequenceId { get; set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The cancel.
        /// </summary>
        /// Inform the Command that further Execution has been terminated
        void Cancel();

        /// <summary>
        /// The execute.
        /// </summary>
        /// Override this! `Execute()` is where you place the logic for your Command.
        void Execute();

        /// <summary>
        /// The fail.
        /// </summary>
        /// Inidcates that the Command failed
        /// Used in sequential command groups to terminate the sequence
        void Fail();

        /// <summary>
        /// The release.
        /// </summary>
        /// Allows a previous Retained Command to be disposed.
        void Release();

        /// <summary>
        /// The retain.
        /// </summary>
        /// Keeps the Command in memory. Use only in conjunction with `Release()`
        void Retain();

        #endregion
    }
}