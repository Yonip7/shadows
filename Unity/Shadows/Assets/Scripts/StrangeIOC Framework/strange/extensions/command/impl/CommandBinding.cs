// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CommandBinding.cs" company="">
//   
// </copyright>
// <summary>
//   The command binding.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace strange.extensions.command.impl
{
    using strange.extensions.command.api;
    using strange.framework.impl;

    /// <summary>
    /// The command binding.
    /// </summary>
    public class CommandBinding : Binding, ICommandBinding
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="CommandBinding"/> class.
        /// </summary>
        public CommandBinding()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CommandBinding"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver.
        /// </param>
        public CommandBinding(Binder.BindingResolver resolver)
            : base(resolver)
        {
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets a value indicating whether is one off.
        /// </summary>
        public bool isOneOff { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is sequence.
        /// </summary>
        public bool isSequence { get; set; }

        #endregion

        // Everything below this point is simply facade on Binding to ensure fluent interface
        #region Public Methods and Operators

        /// <summary>
        /// The bind.
        /// </summary>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// The <see cref="ICommandBinding"/>.
        /// </returns>
        public ICommandBinding Bind<T>()
        {
            return this.Key<T>();
        }

        /// <summary>
        /// The bind.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <returns>
        /// The <see cref="ICommandBinding"/>.
        /// </returns>
        public ICommandBinding Bind(object key)
        {
            return this.Key(key);
        }

        /// <summary>
        /// The in parallel.
        /// </summary>
        /// <returns>
        /// The <see cref="ICommandBinding"/>.
        /// </returns>
        public ICommandBinding InParallel()
        {
            this.isSequence = false;
            return this;
        }

        /// <summary>
        /// The in sequence.
        /// </summary>
        /// <returns>
        /// The <see cref="ICommandBinding"/>.
        /// </returns>
        public ICommandBinding InSequence()
        {
            this.isSequence = true;
            return this;
        }

        /// <summary>
        /// The key.
        /// </summary>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// The <see cref="ICommandBinding"/>.
        /// </returns>
        public new ICommandBinding Key<T>()
        {
            return base.Key<T>() as ICommandBinding;
        }

        /// <summary>
        /// The key.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <returns>
        /// The <see cref="ICommandBinding"/>.
        /// </returns>
        public new ICommandBinding Key(object key)
        {
            return base.Key(key) as ICommandBinding;
        }

        /// <summary>
        /// The named.
        /// </summary>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// The <see cref="ICommandBinding"/>.
        /// </returns>
        public new ICommandBinding Named<T>()
        {
            return base.Named<T>() as ICommandBinding;
        }

        /// <summary>
        /// The named.
        /// </summary>
        /// <param name="o">
        /// The o.
        /// </param>
        /// <returns>
        /// The <see cref="ICommandBinding"/>.
        /// </returns>
        public new ICommandBinding Named(object o)
        {
            return base.Named(o) as ICommandBinding;
        }

        /// <summary>
        /// The once.
        /// </summary>
        /// <returns>
        /// The <see cref="ICommandBinding"/>.
        /// </returns>
        public ICommandBinding Once()
        {
            this.isOneOff = true;
            return this;
        }

        /// <summary>
        /// The to.
        /// </summary>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// The <see cref="ICommandBinding"/>.
        /// </returns>
        public new ICommandBinding To<T>()
        {
            return base.To<T>() as ICommandBinding;
        }

        /// <summary>
        /// The to.
        /// </summary>
        /// <param name="o">
        /// The o.
        /// </param>
        /// <returns>
        /// The <see cref="ICommandBinding"/>.
        /// </returns>
        public new ICommandBinding To(object o)
        {
            return base.To(o) as ICommandBinding;
        }

        /// <summary>
        /// The to name.
        /// </summary>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// The <see cref="ICommandBinding"/>.
        /// </returns>
        public new ICommandBinding ToName<T>()
        {
            return base.ToName<T>() as ICommandBinding;
        }

        /// <summary>
        /// The to name.
        /// </summary>
        /// <param name="o">
        /// The o.
        /// </param>
        /// <returns>
        /// The <see cref="ICommandBinding"/>.
        /// </returns>
        public new ICommandBinding ToName(object o)
        {
            return base.ToName(o) as ICommandBinding;
        }

        #endregion
    }
}