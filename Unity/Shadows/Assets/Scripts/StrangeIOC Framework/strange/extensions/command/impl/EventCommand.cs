// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EventCommand.cs" company="">
//   
// </copyright>
// <summary>
//   The event command.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace strange.extensions.command.impl
{
    using strange.extensions.context.api;
    using strange.extensions.dispatcher.eventdispatcher.api;

    /// <summary>
    /// The event command.
    /// </summary>
    public class EventCommand : Command
    {
        #region Public Properties

        /// <summary>
        /// Gets or sets the dispatcher.
        /// </summary>
        [Inject(ContextKeys.CONTEXT_DISPATCHER)]
        public IEventDispatcher dispatcher { get; set; }

        /// <summary>
        /// Gets or sets the evt.
        /// </summary>
        [Inject]
        public IEvent evt { get; set; }

        #endregion
    }
}