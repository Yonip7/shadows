// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CommandException.cs" company="">
//   
// </copyright>
// <summary>
//   The command exception.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace strange.extensions.command.impl
{
    using System;

    using strange.extensions.command.api;

    /// <summary>
    /// The command exception.
    /// </summary>
    public class CommandException : Exception
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="CommandException"/> class.
        /// </summary>
        public CommandException()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CommandException"/> class. 
        /// The command exception.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        /// <param name="exceptionType">
        /// The exception Type.
        /// </param>
        /// Constructs a CommandException with a message and CommandExceptionType
        public CommandException(string message, CommandExceptionType exceptionType)
            : base(message)
        {
            this.type = exceptionType;
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        public CommandExceptionType type { get; set; }

        #endregion
    }
}