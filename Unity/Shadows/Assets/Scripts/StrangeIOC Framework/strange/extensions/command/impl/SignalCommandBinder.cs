// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SignalCommandBinder.cs" company="">
//   
// </copyright>
// <summary>
//   The signal command binder.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace strange.extensions.command.impl
{
    using System;
    using System.Collections.Generic;

    using strange.extensions.command.api;
    using strange.extensions.injector.api;
    using strange.extensions.injector.impl;
    using strange.extensions.signal.api;
    using strange.extensions.signal.impl;
    using strange.framework.api;

    /// <summary>
    /// The signal command binder.
    /// </summary>
    public class SignalCommandBinder : CommandBinder
    {
        #region Public Methods and Operators

        /// <summary>
        /// The bind.
        /// </summary>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// The <see cref="ICommandBinding"/>.
        /// </returns>
        public override ICommandBinding Bind<T>()
        {
            IInjectionBinding binding = this.injectionBinder.GetBinding<T>();
            if (binding == null)
            {
                // If this isn't injected yet, inject a new one as a singleton
                this.injectionBinder.Bind<T>().ToSingleton();
            }

            T signal = (T)this.injectionBinder.GetInstance<T>();
            return base.Bind(signal);
        }

        /// <summary>
        /// The on remove.
        /// </summary>
        public override void OnRemove()
        {
            foreach (object key in this.bindings.Keys)
            {
                IBaseSignal signal = (IBaseSignal)key;
                if (signal != null)
                {
                    signal.RemoveListener(ReactTo);
                }
            }
        }

        /// <summary>
        /// The resolve binding.
        /// </summary>
        /// <param name="binding">
        /// The binding.
        /// </param>
        /// <param name="key">
        /// The key.
        /// </param>
        public override void ResolveBinding(IBinding binding, object key)
        {
            base.ResolveBinding(binding, key);

            if (this.bindings.ContainsKey(key))
            {
                // If this key already exists, don't bind this again
                IBaseSignal signal = (IBaseSignal)key;
                signal.AddListener(ReactTo); // Do normal bits, then assign the commandlistener to be reactTo
            }
        }

        /// <summary>Unbind by Signal Type</summary>
        /// <exception cref="InjectionException">If there is no binding for this type.</exception>
        public override void Unbind<T>()
        {
            ICommandBinding binding = (ICommandBinding)this.injectionBinder.GetBinding<T>();
            if (binding != null)
            {
                T signal = (T)this.injectionBinder.GetInstance<T>();
                this.Unbind(signal, null);
            }
        }

        /// <summary>
        /// Unbind by Signal Instance
        /// </summary>
        /// <param name="key">
        /// Instance of IBaseSignal
        /// </param>
        /// <param name="name">
        /// The name.
        /// </param>
        public override void Unbind(object key, object name)
        {
            if (this.bindings.ContainsKey(key))
            {
                IBaseSignal signal = (IBaseSignal)key;
                signal.RemoveListener(ReactTo);
            }

            base.Unbind(key, name);
        }

        #endregion

        #region Methods

        /// <summary>
        /// The create command for signal.
        /// </summary>
        /// <param name="cmd">
        /// The cmd.
        /// </param>
        /// <param name="data">
        /// The data.
        /// </param>
        /// <param name="signalTypes">
        /// The signal Types.
        /// </param>
        /// Create a Command and bind its injectable parameters to the Signal types
        /// <returns>
        /// The <see cref="ICommand"/>.
        /// </returns>
        protected ICommand createCommandForSignal(object cmd, object data, List<Type> signalTypes)
        {
            this.injectionBinder.Bind<ICommand>().To(cmd);
            ICommand command = null;
            if (data != null)
            {
                object[] signalData = (object[])data;

                // Iterate each signal type, in order. 
                // Iterate values and find a match
                // If we cannot find a match, throw an error
                HashSet<Type> injectedTypes = new HashSet<Type>();
                List<object> values = new List<object>(signalData);

                foreach (Type type in signalTypes)
                {
                    if (!injectedTypes.Contains(type))
                    {
                        // Do not allow more than one injection of the same Type
                        bool foundValue = false;
                        foreach (object value in values)
                        {
                            if (value != null)
                            {
                                if (type.IsAssignableFrom(value.GetType()))
                                {
                                    // IsAssignableFrom lets us test interfaces as well
                                    this.injectionBinder.Bind(type).ToValue(value).ToInject(false);
                                    injectedTypes.Add(type);
                                    values.Remove(value);
                                    foundValue = true;
                                    break;
                                }
                            }
                            else
                            {
                                // Do not allow null injections
                                throw new SignalException(
                                    "SignalCommandBinder attempted to bind a null value from a signal to Command: "
                                    + cmd.GetType() + " to type: " + type, 
                                    SignalExceptionType.COMMAND_NULL_INJECTION);
                            }
                        }

                        if (!foundValue)
                        {
                            throw new SignalException(
                                "Could not find an unused injectable value to inject in to Command: " + cmd.GetType()
                                + " for Type: " + type, 
                                SignalExceptionType.COMMAND_VALUE_NOT_FOUND);
                        }
                    }
                    else
                    {
                        throw new SignalException(
                            "SignalCommandBinder: You have attempted to map more than one value of type: " + type
                            + " in Command: " + cmd.GetType()
                            + ". Only the first value of a type will be injected. You may want to place your values in a VO, instead.", 
                            SignalExceptionType.COMMAND_VALUE_CONFLICT);
                    }
                }

                command = this.injectionBinder.GetInstance<ICommand>() as ICommand;
                command.data = data;
                    
                    // Just to support swapping from EventCommand to SignalCommand more easily. No reason not to.
                foreach (Type typeToRemove in signalTypes)
                {
                    // clean up these bindings
                    this.injectionBinder.Unbind(typeToRemove);
                }
            }
            else
            {
                command = this.injectionBinder.GetInstance<ICommand>() as ICommand;
                command.data = data;
                    
                    // Just to support swapping from EventCommand to SignalCommand more easily. No reason not to.
            }

            this.injectionBinder.Unbind<ICommand>();
            return command;
        }

        /// <summary>
        /// The invoke command.
        /// </summary>
        /// <param name="cmd">
        /// The cmd.
        /// </param>
        /// <param name="binding">
        /// The binding.
        /// </param>
        /// <param name="data">
        /// The data.
        /// </param>
        /// <param name="depth">
        /// The depth.
        /// </param>
        /// <returns>
        /// The <see cref="ICommand"/>.
        /// </returns>
        protected override ICommand invokeCommand(Type cmd, ICommandBinding binding, object data, int depth)
        {
            IBaseSignal signal = (IBaseSignal)binding.key;
            ICommand command = this.createCommandForSignal(cmd, data, signal.GetTypes());
                
                // Special signal-only command creation
            command.sequenceId = depth;
            this.trackCommand(command, binding);
            this.executeCommand(command);
            return command;
        }

        #endregion
    }
}