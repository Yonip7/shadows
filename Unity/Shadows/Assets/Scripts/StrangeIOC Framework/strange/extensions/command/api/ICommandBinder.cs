// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ICommandBinder.cs" company="">
//   
// </copyright>
// <summary>
//   The CommandBinder interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace strange.extensions.command.api
{
    using strange.framework.api;

    /// <summary>
    /// The CommandBinder interface.
    /// </summary>
    public interface ICommandBinder : IBinder
    {
        #region Public Methods and Operators

        /// <summary>
        /// The bind.
        /// </summary>
        /// Bind a trigger Key by generic Type
        /// <returns>
        /// The <see cref="ICommandBinding"/>.
        /// </returns>
        new ICommandBinding Bind<T>();

        /// <summary>
        /// The bind.
        /// </summary>
        /// <param name="value">
        /// The value.
        /// </param>
        /// Bind a trigger Key by value
        /// <returns>
        /// The <see cref="ICommandBinding"/>.
        /// </returns>
        new ICommandBinding Bind(object value);

        /// <summary>
        /// The react to.
        /// </summary>
        /// <param name="trigger">
        /// The trigger.
        /// </param>
        /// Trigger a key that unlocks one or more Commands
        void ReactTo(object trigger);

        /// <summary>
        /// The react to.
        /// </summary>
        /// <param name="trigger">
        /// The trigger.
        /// </param>
        /// <param name="data">
        /// The data.
        /// </param>
        /// Trigger a key that unlocks one or more Commands and provide a data injection to that Command
        void ReactTo(object trigger, object data);

        /// <summary>
        /// The release command.
        /// </summary>
        /// <param name="command">
        /// The command.
        /// </param>
        /// Release a previously retained Command.
        /// By default, a Command is garbage collected at the end of its `Execute()` method. 
        /// But the Command can be retained for asynchronous calls.
        void ReleaseCommand(ICommand command);

        /// <summary>
        /// The stop.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// Called to halt execution of a currently running command group
        void Stop(object key);

        #endregion
    }
}