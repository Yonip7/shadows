// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ICommandBinding.cs" company="">
//   
// </copyright>
// <summary>
//   The CommandBinding interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace strange.extensions.command.api
{
    using strange.framework.api;

    /// <summary>
    /// The CommandBinding interface.
    /// </summary>
    public interface ICommandBinding : IBinding
    {
        #region Public Properties

        /// <summary>
        /// The is one off.
        /// </summary>
        /// Get/set the property set to `true` by `Once()`
        bool isOneOff { get; set; }

        /// <summary>
        /// The is sequence.
        /// </summary>
        /// Get/set the propterty set by InSequence() and InParallel()
        bool isSequence { get; set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The in parallel.
        /// </summary>
        /// Declares that the bound commands will be run in parallel.
        /// Parallel is the default. There is no need to call this unless you're changing from sequence.
        /// <returns>
        /// The <see cref="ICommandBinding"/>.
        /// </returns>
        ICommandBinding InParallel();

        /// <summary>
        /// The in sequence.
        /// </summary>
        /// Declares that the bound commands will be run as a sequence, rather than in parallel
        /// <returns>
        /// The <see cref="ICommandBinding"/>.
        /// </returns>
        ICommandBinding InSequence();

        /// <summary>
        /// The key.
        /// </summary>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// The <see cref="ICommandBinding"/>.
        /// </returns>
        new ICommandBinding Key<T>();

        /// <summary>
        /// The key.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <returns>
        /// The <see cref="ICommandBinding"/>.
        /// </returns>
        new ICommandBinding Key(object key);

        /// <summary>
        /// The named.
        /// </summary>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// The <see cref="ICommandBinding"/>.
        /// </returns>
        new ICommandBinding Named<T>();

        /// <summary>
        /// The named.
        /// </summary>
        /// <param name="o">
        /// The o.
        /// </param>
        /// <returns>
        /// The <see cref="ICommandBinding"/>.
        /// </returns>
        new ICommandBinding Named(object o);

        /// <summary>
        /// The once.
        /// </summary>
        /// Declares that the Binding is a one-off. As soon as it's satisfied, it will be unmapped.
        /// <returns>
        /// The <see cref="ICommandBinding"/>.
        /// </returns>
        ICommandBinding Once();

        /// <summary>
        /// The to.
        /// </summary>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// The <see cref="ICommandBinding"/>.
        /// </returns>
        new ICommandBinding To<T>();

        /// <summary>
        /// The to.
        /// </summary>
        /// <param name="o">
        /// The o.
        /// </param>
        /// <returns>
        /// The <see cref="ICommandBinding"/>.
        /// </returns>
        new ICommandBinding To(object o);

        /// <summary>
        /// The to name.
        /// </summary>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// The <see cref="ICommandBinding"/>.
        /// </returns>
        new ICommandBinding ToName<T>();

        /// <summary>
        /// The to name.
        /// </summary>
        /// <param name="o">
        /// The o.
        /// </param>
        /// <returns>
        /// The <see cref="ICommandBinding"/>.
        /// </returns>
        new ICommandBinding ToName(object o);

        #endregion
    }
}