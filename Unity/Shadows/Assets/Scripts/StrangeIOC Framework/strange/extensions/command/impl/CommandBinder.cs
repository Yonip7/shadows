// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CommandBinder.cs" company="">
//   
// </copyright>
// <summary>
//   The command binder.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace strange.extensions.command.impl
{
    using System;
    using System.Collections.Generic;

    using strange.extensions.command.api;
    using strange.extensions.dispatcher.api;
    using strange.extensions.injector.api;
    using strange.framework.api;
    using strange.framework.impl;

    /// <summary>
    /// The command binder.
    /// </summary>
    public class CommandBinder : Binder, ICommandBinder, ITriggerable
    {
        #region Fields

        /// <summary>
        /// The active commands.
        /// </summary>
        /// Tracker for parallel commands in progress
        protected HashSet<ICommand> activeCommands = new HashSet<ICommand>();

        /// <summary>
        /// The active sequences.
        /// </summary>
        /// Tracker for sequences in progress
        protected Dictionary<ICommand, ICommandBinding> activeSequences = new Dictionary<ICommand, ICommandBinding>();

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets the injection binder.
        /// </summary>
        [Inject]
        public IInjectionBinder injectionBinder { get; set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The bind.
        /// </summary>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// The <see cref="ICommandBinding"/>.
        /// </returns>
        public new virtual ICommandBinding Bind<T>()
        {
            return base.Bind<T>() as ICommandBinding;
        }

        /// <summary>
        /// The bind.
        /// </summary>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <returns>
        /// The <see cref="ICommandBinding"/>.
        /// </returns>
        public new virtual ICommandBinding Bind(object value)
        {
            return base.Bind(value) as ICommandBinding;
        }

        /// <summary>
        /// The get raw binding.
        /// </summary>
        /// <returns>
        /// The <see cref="IBinding"/>.
        /// </returns>
        public override IBinding GetRawBinding()
        {
            return new CommandBinding(this.resolver);
        }

        /// <summary>
        /// The react to.
        /// </summary>
        /// <param name="trigger">
        /// The trigger.
        /// </param>
        public virtual void ReactTo(object trigger)
        {
            this.ReactTo(trigger, null);
        }

        /// <summary>
        /// The react to.
        /// </summary>
        /// <param name="trigger">
        /// The trigger.
        /// </param>
        /// <param name="data">
        /// The data.
        /// </param>
        public virtual void ReactTo(object trigger, object data)
        {
            ICommandBinding binding = this.GetBinding(trigger) as ICommandBinding;
            if (binding != null)
            {
                if (binding.isSequence)
                {
                    this.next(binding, data, 0);
                }
                else
                {
                    object[] values = binding.value as object[];
                    int aa = values.Length + 1;
                    for (int a = 0; a < aa; a++)
                    {
                        this.next(binding, data, a);
                    }
                }
            }
        }

        /// <summary>
        /// The release command.
        /// </summary>
        /// <param name="command">
        /// The command.
        /// </param>
        public void ReleaseCommand(ICommand command)
        {
            if (command.retain == false)
            {
                if (this.activeCommands.Contains(command))
                {
                    this.activeCommands.Remove(command);
                }
                else if (this.activeSequences.ContainsKey(command))
                {
                    ICommandBinding binding = this.activeSequences[command];
                    object data = command.data;
                    this.activeSequences.Remove(command);
                    this.next(binding, data, command.sequenceId + 1);
                }
            }
        }

        /// <summary>
        /// The stop.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        public virtual void Stop(object key)
        {
            if (key is ICommand && this.activeSequences.ContainsKey(key as ICommand))
            {
                this.removeSequence(key as ICommand);
            }
            else
            {
                ICommandBinding binding = this.GetBinding(key) as ICommandBinding;
                if (binding != null)
                {
                    if (this.activeSequences.ContainsValue(binding))
                    {
                        foreach (KeyValuePair<ICommand, ICommandBinding> sequence in this.activeSequences)
                        {
                            if (sequence.Value == binding)
                            {
                                ICommand command = sequence.Key;
                                this.removeSequence(command);
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// The trigger.
        /// </summary>
        /// <param name="data">
        /// The data.
        /// </param>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool Trigger<T>(object data)
        {
            return this.Trigger(typeof(T), data);
        }

        /// <summary>
        /// The trigger.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <param name="data">
        /// The data.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool Trigger(object key, object data)
        {
            this.ReactTo(key, data);
            return true;
        }

        #endregion

        #region Methods

        /// <summary>
        /// The create command.
        /// </summary>
        /// <param name="cmd">
        /// The cmd.
        /// </param>
        /// <param name="data">
        /// The data.
        /// </param>
        /// <returns>
        /// The <see cref="ICommand"/>.
        /// </returns>
        /// <exception cref="CommandException">
        /// </exception>
        protected virtual ICommand createCommand(object cmd, object data)
        {
            this.injectionBinder.Bind<ICommand>().To(cmd);
            ICommand command = this.injectionBinder.GetInstance<ICommand>() as ICommand;

            if (command == null)
            {
                string msg = "A Command ";
                if (data != null)
                {
                    msg += "tied to data " + data;
                }

                msg +=
                    " could not be instantiated.\nThis might be caused by a null pointer during instantiation or failing to override Execute (generally you shouldn't have constructor code in Commands).";
                throw new CommandException(msg, CommandExceptionType.BAD_CONSTRUCTOR);
            }

            command.data = data;
            this.injectionBinder.Unbind<ICommand>();
            return command;
        }

        /// <summary>
        /// The execute command.
        /// </summary>
        /// <param name="command">
        /// The command.
        /// </param>
        protected void executeCommand(ICommand command)
        {
            if (command == null)
            {
                return;
            }

            command.Execute();
        }

        /// <summary>
        /// The invoke command.
        /// </summary>
        /// <param name="cmd">
        /// The cmd.
        /// </param>
        /// <param name="binding">
        /// The binding.
        /// </param>
        /// <param name="data">
        /// The data.
        /// </param>
        /// <param name="depth">
        /// The depth.
        /// </param>
        /// <returns>
        /// The <see cref="ICommand"/>.
        /// </returns>
        protected virtual ICommand invokeCommand(Type cmd, ICommandBinding binding, object data, int depth)
        {
            ICommand command = this.createCommand(cmd, data);
            command.sequenceId = depth;
            this.trackCommand(command, binding);
            this.executeCommand(command);
            return command;
        }

        /// <summary>
        /// The track command.
        /// </summary>
        /// <param name="command">
        /// The command.
        /// </param>
        /// <param name="binding">
        /// The binding.
        /// </param>
        protected void trackCommand(ICommand command, ICommandBinding binding)
        {
            if (binding.isSequence)
            {
                this.activeSequences[command] = binding;
            }
            else
            {
                this.activeCommands.Add(command);
            }
        }

        /// <summary>
        /// The next.
        /// </summary>
        /// <param name="binding">
        /// The binding.
        /// </param>
        /// <param name="data">
        /// The data.
        /// </param>
        /// <param name="depth">
        /// The depth.
        /// </param>
        private void next(ICommandBinding binding, object data, int depth)
        {
            object[] values = binding.value as object[];
            if (depth < values.Length)
            {
                Type cmd = values[depth] as Type;
                ICommand command = this.invokeCommand(cmd, binding, data, depth);
                this.ReleaseCommand(command);
            }
            else
            {
                if (binding.isOneOff)
                {
                    Unbind(binding);
                }
            }
        }

        /// <summary>
        /// The remove sequence.
        /// </summary>
        /// <param name="command">
        /// The command.
        /// </param>
        private void removeSequence(ICommand command)
        {
            if (this.activeSequences.ContainsKey(command))
            {
                command.Cancel();
                this.activeSequences.Remove(command);
            }
        }

        #endregion
    }
}