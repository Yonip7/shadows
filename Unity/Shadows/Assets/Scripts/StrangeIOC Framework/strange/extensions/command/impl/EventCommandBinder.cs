// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EventCommandBinder.cs" company="">
//   
// </copyright>
// <summary>
//   The event command binder.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace strange.extensions.command.impl
{
    using strange.extensions.command.api;
    using strange.extensions.dispatcher.eventdispatcher.api;

    /// <summary>
    /// The event command binder.
    /// </summary>
    public class EventCommandBinder : CommandBinder
    {
        #region Methods

        /// <summary>
        /// The create command.
        /// </summary>
        /// <param name="cmd">
        /// The cmd.
        /// </param>
        /// <param name="data">
        /// The data.
        /// </param>
        /// <returns>
        /// The <see cref="ICommand"/>.
        /// </returns>
        protected override ICommand createCommand(object cmd, object data)
        {
            this.injectionBinder.Bind<ICommand>().To(cmd);
            if (data is IEvent)
            {
                this.injectionBinder.Bind<IEvent>().ToValue(data).ToInject(false);
            }

            ICommand command = this.injectionBinder.GetInstance<ICommand>() as ICommand;
            if (command == null)
            {
                string msg = "A Command ";
                if (data is IEvent)
                {
                    IEvent evt = (IEvent)data;
                    msg += "tied to event " + evt.type;
                }

                msg +=
                    " could not be instantiated.\nThis might be caused by a null pointer during instantiation or failing to override Execute (generally you shouldn't have constructor code in Commands).";
                throw new CommandException(msg, CommandExceptionType.BAD_CONSTRUCTOR);
            }

            command.data = data;
            if (data is IEvent)
            {
                this.injectionBinder.Unbind<IEvent>();
            }

            this.injectionBinder.Unbind<ICommand>();
            return command;
        }

        #endregion
    }
}