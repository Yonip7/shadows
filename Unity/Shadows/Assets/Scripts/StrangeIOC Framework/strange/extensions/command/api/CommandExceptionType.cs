// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CommandExceptionType.cs" company="">
//   
// </copyright>
// <summary>
//   The command exception type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace strange.extensions.command.api
{
    /// <summary>
    /// The command exception type.
    /// </summary>
    public enum CommandExceptionType
    {
        /// <summary>
        /// The execut e_ override.
        /// </summary>
        /// Commands must always override the Execute() method.
        EXECUTE_OVERRIDE, 

        /// <summary>
        /// The nul l_ binding.
        /// </summary>
        /// Binding wasn't found
        NULL_BINDING, 

        /// <summary>
        /// The ba d_ constructor.
        /// </summary>
        /// Something went wrong during construction, so the Command resolved to null
        BAD_CONSTRUCTOR
    }
}