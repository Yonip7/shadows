// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ReflectionBinder.cs" company="">
//   
// </copyright>
// <summary>
//   The reflection binder.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace strange.extensions.reflector.impl
{
    using System;
    using System.Collections.Generic;
    using System.Reflection;

    using strange.extensions.reflector.api;
    using strange.framework.api;

    /// <summary>
    /// The reflection binder.
    /// </summary>
    public class ReflectionBinder : strange.framework.impl.Binder, IReflectionBinder
    {
        #region Public Methods and Operators

        /// <summary>
        /// The get.
        /// </summary>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// The <see cref="IReflectedClass"/>.
        /// </returns>
        public IReflectedClass Get<T>()
        {
            return this.Get(typeof(T));
        }

        /// <summary>
        /// The get.
        /// </summary>
        /// <param name="type">
        /// The type.
        /// </param>
        /// <returns>
        /// The <see cref="IReflectedClass"/>.
        /// </returns>
        public IReflectedClass Get(Type type)
        {
            IBinding binding = this.GetBinding(type);
            IReflectedClass retv;
            if (binding == null)
            {
                binding = this.GetRawBinding();
                IReflectedClass reflected = new ReflectedClass();
                this.mapPreferredConstructor(reflected, binding, type);
                this.mapPostConstructors(reflected, binding, type);
                this.mapSetters(reflected, binding, type);
                binding.Key(type).To(reflected);
                retv = binding.value as IReflectedClass;
                retv.preGenerated = false;
            }
            else
            {
                retv = binding.value as IReflectedClass;
                retv.preGenerated = true;
            }

            return retv;
        }

        /// <summary>
        /// The get raw binding.
        /// </summary>
        /// <returns>
        /// The <see cref="IBinding"/>.
        /// </returns>
        public override IBinding GetRawBinding()
        {
            IBinding binding = base.GetRawBinding();
            binding.valueConstraint = BindingConstraintType.ONE;
            return binding;
        }

        #endregion

        #region Methods

        /// <summary>
        /// The add.
        /// </summary>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <param name="list">
        /// The list.
        /// </param>
        /// <returns>
        /// The <see cref="object[]"/>.
        /// </returns>
        private object[] Add(object value, object[] list)
        {
            object[] tempList = list;
            int len = tempList.Length;
            list = new object[len + 1];
            tempList.CopyTo(list, 0);
            list[len] = value;
            return list;
        }

        /**
		 * Add an item to a list
		 */

        /// <summary>
        /// The add kv.
        /// </summary>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <param name="list">
        /// The list.
        /// </param>
        /// <returns>
        /// The <see cref="KeyValuePair"/>.
        /// </returns>
        private KeyValuePair<Type, PropertyInfo>[] AddKV(
            KeyValuePair<Type, PropertyInfo> value, KeyValuePair<Type, PropertyInfo>[] list)
        {
            KeyValuePair<Type, PropertyInfo>[] tempList = list;
            int len = tempList.Length;
            list = new KeyValuePair<Type, PropertyInfo>[len + 1];
            tempList.CopyTo(list, 0);
            list[len] = value;
            return list;
        }

        // Look for a constructor in the order:
        // 1. Only one (just return it, since it's our only option)
        // 2. Tagged with [Construct] tag
        // 3. The constructor with the fewest parameters
        /// <summary>
        /// The find preferred constructor.
        /// </summary>
        /// <param name="type">
        /// The type.
        /// </param>
        /// <returns>
        /// The <see cref="ConstructorInfo"/>.
        /// </returns>
        private ConstructorInfo findPreferredConstructor(Type type)
        {
            ConstructorInfo[] constructors =
                type.GetConstructors(
                    BindingFlags.FlattenHierarchy | BindingFlags.Public | BindingFlags.Instance
                    | BindingFlags.InvokeMethod);
            if (constructors.Length == 1)
            {
                return constructors[0];
            }

            int len;
            int shortestLen = int.MaxValue;
            ConstructorInfo shortestConstructor = null;
            foreach (ConstructorInfo constructor in constructors)
            {
                object[] taggedConstructors = constructor.GetCustomAttributes(typeof(Construct), true);
                if (taggedConstructors.Length > 0)
                {
                    return constructor;
                }

                len = constructor.GetParameters().Length;
                if (len < shortestLen)
                {
                    shortestLen = len;
                    shortestConstructor = constructor;
                }
            }

            return shortestConstructor;
        }

        /// <summary>
        /// The map post constructors.
        /// </summary>
        /// <param name="reflected">
        /// The reflected.
        /// </param>
        /// <param name="binding">
        /// The binding.
        /// </param>
        /// <param name="type">
        /// The type.
        /// </param>
        private void mapPostConstructors(IReflectedClass reflected, IBinding binding, Type type)
        {
            MethodInfo[] postConstructors = new MethodInfo[0];
            MethodInfo[] methods =
                type.GetMethods(
                    BindingFlags.FlattenHierarchy | BindingFlags.Public | BindingFlags.Instance
                    | BindingFlags.InvokeMethod);
            foreach (MethodInfo method in methods)
            {
                object[] tagged = method.GetCustomAttributes(typeof(PostConstruct), true);
                if (tagged.Length > 0)
                {
                    MethodInfo[] tempList = postConstructors;
                    int len = tempList.Length;
                    postConstructors = new MethodInfo[len + 1];
                    tempList.CopyTo(postConstructors, 0);
                    postConstructors[len] = method;
                }
            }

            reflected.postConstructors = postConstructors;
        }

        /// <summary>
        /// The map preferred constructor.
        /// </summary>
        /// <param name="reflected">
        /// The reflected.
        /// </param>
        /// <param name="binding">
        /// The binding.
        /// </param>
        /// <param name="type">
        /// The type.
        /// </param>
        /// <exception cref="ReflectionException">
        /// </exception>
        private void mapPreferredConstructor(IReflectedClass reflected, IBinding binding, Type type)
        {
            ConstructorInfo constructor = this.findPreferredConstructor(type);
            if (constructor == null)
            {
                throw new ReflectionException(
                    "The reflector requires concrete classes.\nType " + type
                    + " has no constructor. Is it an interface?", 
                    ReflectionExceptionType.CANNOT_REFLECT_INTERFACE);
            }

            ParameterInfo[] parameters = constructor.GetParameters();

            Type[] paramList = new Type[parameters.Length];
            int i = 0;
            foreach (ParameterInfo param in parameters)
            {
                Type paramType = param.ParameterType;
                paramList[i] = paramType;
                i++;
            }

            reflected.constructor = constructor;
            reflected.constructorParameters = paramList;
        }

        /// <summary>
        /// The map setters.
        /// </summary>
        /// <param name="reflected">
        /// The reflected.
        /// </param>
        /// <param name="binding">
        /// The binding.
        /// </param>
        /// <param name="type">
        /// The type.
        /// </param>
        private void mapSetters(IReflectedClass reflected, IBinding binding, Type type)
        {
            KeyValuePair<Type, PropertyInfo>[] pairs = new KeyValuePair<Type, PropertyInfo>[0];
            object[] names = new object[0];

            MemberInfo[] members = type.FindMembers(
                MemberTypes.Property, 
                BindingFlags.FlattenHierarchy | BindingFlags.SetProperty | BindingFlags.Public | BindingFlags.Instance, 
                null, 
                null);

            foreach (MemberInfo member in members)
            {
                object[] injections = member.GetCustomAttributes(typeof(Inject), true);
                if (injections.Length > 0)
                {
                    Inject attr = injections[0] as Inject;
                    PropertyInfo point = member as PropertyInfo;
                    Type pointType = point.PropertyType;
                    KeyValuePair<Type, PropertyInfo> pair = new KeyValuePair<Type, PropertyInfo>(pointType, point);
                    pairs = this.AddKV(pair, pairs);

                    object bindingName = attr.name;
                    names = this.Add(bindingName, names);
                }
            }

            reflected.setters = pairs;
            reflected.setterNames = names;
        }

        #endregion

        /**
		 * Add an item to a list
		 */
    }
}