// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ReflectionExceptionType.cs" company="">
//   
// </copyright>
// <summary>
//   The reflection exception type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace strange.extensions.reflector.api
{
    /// <summary>
    /// The reflection exception type.
    /// </summary>
    public enum ReflectionExceptionType
    {
        /// <summary>
        /// The canno t_ reflec t_ interface.
        /// </summary>
        /// The reflector requires a constructor, which Interfaces don't provide.
        CANNOT_REFLECT_INTERFACE
    }
}