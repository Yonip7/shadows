// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ReflectedClass.cs" company="">
//   
// </copyright>
// <summary>
//   The reflected class.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace strange.extensions.reflector.impl
{
    using System;
    using System.Collections.Generic;
    using System.Reflection;

    using strange.extensions.reflector.api;

    /// <summary>
    /// The reflected class.
    /// </summary>
    public class ReflectedClass : IReflectedClass
    {
        #region Public Properties

        /// <summary>
        /// Gets or sets the constructor.
        /// </summary>
        public ConstructorInfo constructor { get; set; }

        /// <summary>
        /// Gets or sets the constructor parameters.
        /// </summary>
        public Type[] constructorParameters { get; set; }

        /// <summary>
        /// Gets or sets the post constructors.
        /// </summary>
        public MethodInfo[] postConstructors { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether pre generated.
        /// </summary>
        public bool preGenerated { get; set; }

        /// <summary>
        /// Gets or sets the setter names.
        /// </summary>
        public object[] setterNames { get; set; }

        /// <summary>
        /// Gets or sets the setters.
        /// </summary>
        public KeyValuePair<Type, PropertyInfo>[] setters { get; set; }

        #endregion
    }
}