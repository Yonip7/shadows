// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IReflectedClass.cs" company="">
//   
// </copyright>
// <summary>
//   The ReflectedClass interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace strange.extensions.reflector.api
{
    using System;
    using System.Collections.Generic;
    using System.Reflection;

    /// <summary>
    /// The ReflectedClass interface.
    /// </summary>
    public interface IReflectedClass
    {
        #region Public Properties

        /// <summary>
        /// The constructor.
        /// </summary>
        /// Get/set the preferred constructor
        ConstructorInfo constructor { get; set; }

        /// <summary>
        /// The constructor parameters.
        /// </summary>
        /// Get/set the preferred constructor's list of parameters
        Type[] constructorParameters { get; set; }

        /// <summary>
        /// The post constructors.
        /// </summary>
        /// Get/set any PostConstructors. This includes inherited PostConstructors.
        MethodInfo[] postConstructors { get; set; }

        /// <summary>
        /// The pre generated.
        /// </summary>
        /// For testing. Allows a unit test to assert whether the binding was
        /// generated on the current call, or on a prior one.
        bool preGenerated { get; set; }

        /// <summary>
        /// Gets or sets the setter names.
        /// </summary>
        object[] setterNames { get; set; }

        /// <summary>
        /// The setters.
        /// </summary>
        /// Get/set the list of setter injections. This includes inherited setters.
        KeyValuePair<Type, PropertyInfo>[] setters { get; set; }

        #endregion
    }
}