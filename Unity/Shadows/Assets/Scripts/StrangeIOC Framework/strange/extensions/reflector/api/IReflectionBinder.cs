// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IReflectionBinder.cs" company="">
//   
// </copyright>
// <summary>
//   The ReflectionBinder interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace strange.extensions.reflector.api
{
    using System;

    /// <summary>
    /// The ReflectionBinder interface.
    /// </summary>
    public interface IReflectionBinder
    {
        #region Public Methods and Operators

        /// <summary>
        /// The get.
        /// </summary>
        /// <param name="type">
        /// The type.
        /// </param>
        /// Get a binding based on the provided Type
        /// <returns>
        /// The <see cref="IReflectedClass"/>.
        /// </returns>
        IReflectedClass Get(Type type);

        /// <summary>
        /// The get.
        /// </summary>
        /// Get a binding based on the provided Type generic.
        /// <returns>
        /// The <see cref="IReflectedClass"/>.
        /// </returns>
        IReflectedClass Get<T>();

        #endregion
    }
}