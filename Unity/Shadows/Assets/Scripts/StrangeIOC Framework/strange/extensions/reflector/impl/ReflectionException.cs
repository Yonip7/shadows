// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ReflectionException.cs" company="">
//   
// </copyright>
// <summary>
//   The reflection exception.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace strange.extensions.reflector.impl
{
    using System;

    using strange.extensions.reflector.api;

    /// <summary>
    /// The reflection exception.
    /// </summary>
    public class ReflectionException : Exception
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ReflectionException"/> class.
        /// </summary>
        public ReflectionException()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ReflectionException"/> class. 
        /// The reflection exception.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        /// <param name="exceptionType">
        /// The exception Type.
        /// </param>
        /// Constructs a ReflectionException with a message and ReflectionExceptionType
        public ReflectionException(string message, ReflectionExceptionType exceptionType)
            : base(message)
        {
            this.type = exceptionType;
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        public ReflectionExceptionType type { get; set; }

        #endregion
    }
}