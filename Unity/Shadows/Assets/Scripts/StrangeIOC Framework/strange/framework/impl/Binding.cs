﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Binding.cs" company="">
//   
// </copyright>
// <summary>
//   The binding.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace strange.framework.impl
{
    using System;

    using strange.framework.api;

    /// <summary>
    /// The binding.
    /// </summary>
    public class Binding : IBinding
    {
        #region Fields

        /// <summary>
        /// The resolver.
        /// </summary>
        public Binder.BindingResolver resolver;

        /// <summary>
        /// The _key.
        /// </summary>
        protected ISemiBinding _key;

        /// <summary>
        /// The _name.
        /// </summary>
        protected ISemiBinding _name;

        /// <summary>
        /// The _value.
        /// </summary>
        protected ISemiBinding _value;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="Binding"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver.
        /// </param>
        public Binding(Binder.BindingResolver resolver)
        {
            this.resolver = resolver;

            this._key = new SemiBinding();
            this._value = new SemiBinding();
            this._name = new SemiBinding();

            this.keyConstraint = BindingConstraintType.ONE;
            this.nameConstraint = BindingConstraintType.ONE;
            this.valueConstraint = BindingConstraintType.MANY;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Binding"/> class.
        /// </summary>
        public Binding()
            : this(null)
        {
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets the key.
        /// </summary>
        public object key
        {
            get
            {
                return this._key.value;
            }
        }

        /// <summary>
        /// Gets or sets the key constraint.
        /// </summary>
        public Enum keyConstraint
        {
            get
            {
                return this._key.constraint;
            }

            set
            {
                this._key.constraint = value;
            }
        }

        /// <summary>
        /// Gets the name.
        /// </summary>
        public object name
        {
            get
            {
                return (this._name.value == null) ? BindingConst.NULLOID : this._name.value;
            }
        }

        /// <summary>
        /// Gets or sets the name constraint.
        /// </summary>
        public Enum nameConstraint
        {
            get
            {
                return this._name.constraint;
            }

            set
            {
                this._name.constraint = value;
            }
        }

        /// <summary>
        /// Gets the value.
        /// </summary>
        public object value
        {
            get
            {
                return this._value.value;
            }
        }

        /// <summary>
        /// Gets or sets the value constraint.
        /// </summary>
        public Enum valueConstraint
        {
            get
            {
                return this._value.constraint;
            }

            set
            {
                this._value.constraint = value;
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The key.
        /// </summary>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// The <see cref="IBinding"/>.
        /// </returns>
        public virtual IBinding Key<T>()
        {
            return this.Key(typeof(T));
        }

        /// <summary>
        /// The key.
        /// </summary>
        /// <param name="o">
        /// The o.
        /// </param>
        /// <returns>
        /// The <see cref="IBinding"/>.
        /// </returns>
        public virtual IBinding Key(object o)
        {
            this._key.Add(o);
            return this;
        }

        /// <summary>
        /// The named.
        /// </summary>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// The <see cref="IBinding"/>.
        /// </returns>
        public virtual IBinding Named<T>()
        {
            return this.Named(typeof(T));
        }

        /// <summary>
        /// The named.
        /// </summary>
        /// <param name="o">
        /// The o.
        /// </param>
        /// <returns>
        /// The <see cref="IBinding"/>.
        /// </returns>
        public virtual IBinding Named(object o)
        {
            if (this._name.value == o)
            {
                return this;
            }

            return null;
        }

        /// <summary>
        /// The remove key.
        /// </summary>
        /// <param name="o">
        /// The o.
        /// </param>
        public virtual void RemoveKey(object o)
        {
            this._key.Remove(o);
        }

        /// <summary>
        /// The remove name.
        /// </summary>
        /// <param name="o">
        /// The o.
        /// </param>
        public virtual void RemoveName(object o)
        {
            this._name.Remove(o);
        }

        /// <summary>
        /// The remove value.
        /// </summary>
        /// <param name="o">
        /// The o.
        /// </param>
        public virtual void RemoveValue(object o)
        {
            this._value.Remove(o);
        }

        /// <summary>
        /// The to.
        /// </summary>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// The <see cref="IBinding"/>.
        /// </returns>
        public virtual IBinding To<T>()
        {
            return this.To(typeof(T));
        }

        /// <summary>
        /// The to.
        /// </summary>
        /// <param name="o">
        /// The o.
        /// </param>
        /// <returns>
        /// The <see cref="IBinding"/>.
        /// </returns>
        public virtual IBinding To(object o)
        {
            this._value.Add(o);
            if (this.resolver != null)
            {
                this.resolver(this);
            }

            return this;
        }

        /// <summary>
        /// The to name.
        /// </summary>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// The <see cref="IBinding"/>.
        /// </returns>
        public virtual IBinding ToName<T>()
        {
            return this.ToName(typeof(T));
        }

        /// <summary>
        /// The to name.
        /// </summary>
        /// <param name="o">
        /// The o.
        /// </param>
        /// <returns>
        /// The <see cref="IBinding"/>.
        /// </returns>
        public virtual IBinding ToName(object o)
        {
            object toName = (o == null) ? BindingConst.NULLOID : o;
            this._name.Add(toName);
            if (this.resolver != null)
            {
                this.resolver(this);
            }

            return this;
        }

        #endregion
    }
}