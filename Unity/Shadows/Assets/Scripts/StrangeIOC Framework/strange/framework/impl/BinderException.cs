// --------------------------------------------------------------------------------------------------------------------
// <copyright file="BinderException.cs" company="">
//   
// </copyright>
// <summary>
//   The binder exception.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace strange.framework.impl
{
    using System;

    using strange.framework.api;

    /// <summary>
    /// The binder exception.
    /// </summary>
    public class BinderException : Exception
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="BinderException"/> class.
        /// </summary>
        public BinderException()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BinderException"/> class. 
        /// The binder exception.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        /// <param name="exceptionType">
        /// The exception Type.
        /// </param>
        /// Constructs a BinderException with a message and BinderExceptionType
        public BinderException(string message, BinderExceptionType exceptionType)
            : base(message)
        {
            this.type = exceptionType;
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        public BinderExceptionType type { get; set; }

        #endregion
    }
}