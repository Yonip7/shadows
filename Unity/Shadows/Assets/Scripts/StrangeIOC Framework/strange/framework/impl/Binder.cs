// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Binder.cs" company="">
//   
// </copyright>
// <summary>
//   The binder.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace strange.framework.impl
{
    using System.Collections.Generic;

    using strange.framework.api;

    /// <summary>
    /// The binder.
    /// </summary>
    public class Binder : IBinder
    {
        #region Fields

        /// <summary>
        /// The bindings.
        /// </summary>
        /// Dictionary of all bindings
        /// Two-layer keys. First key to individual Binding keys,
        /// then to Binding names. (This wouldn't be required if
        /// Unity supported Tuple or HashSet.)
        protected Dictionary<object, Dictionary<object, IBinding>> bindings;

        /// <summary>
        /// The conflicts.
        /// </summary>
        protected Dictionary<object, Dictionary<IBinding, object>> conflicts;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="Binder"/> class.
        /// </summary>
        public Binder()
        {
            this.bindings = new Dictionary<object, Dictionary<object, IBinding>>();
            this.conflicts = new Dictionary<object, Dictionary<IBinding, object>>();
        }

        #endregion

        #region Delegates

        /// <summary>
        /// The binding resolver.
        /// </summary>
        /// A handler for resolving the nature of a binding during chained commands
        public delegate void BindingResolver(IBinding binding);

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The bind.
        /// </summary>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// The <see cref="IBinding"/>.
        /// </returns>
        public virtual IBinding Bind<T>()
        {
            return this.Bind(typeof(T));
        }

        /// <summary>
        /// The bind.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <returns>
        /// The <see cref="IBinding"/>.
        /// </returns>
        public virtual IBinding Bind(object key)
        {
            IBinding binding;
            binding = this.GetRawBinding();
            binding.Key(key);
            return binding;
        }

        /// <summary>
        /// The get binding.
        /// </summary>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// The <see cref="IBinding"/>.
        /// </returns>
        public virtual IBinding GetBinding<T>()
        {
            return this.GetBinding(typeof(T), null);
        }

        /// <summary>
        /// The get binding.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <returns>
        /// The <see cref="IBinding"/>.
        /// </returns>
        public virtual IBinding GetBinding(object key)
        {
            return this.GetBinding(key, null);
        }

        /// <summary>
        /// The get binding.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// The <see cref="IBinding"/>.
        /// </returns>
        public virtual IBinding GetBinding<T>(object name)
        {
            return this.GetBinding(typeof(T), name);
        }

        /// <summary>
        /// The get binding.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <returns>
        /// The <see cref="IBinding"/>.
        /// </returns>
        /// <exception cref="BinderException">
        /// </exception>
        public virtual IBinding GetBinding(object key, object name)
        {
            if (this.conflicts.Count > 0)
            {
                string conflictSummary = "";
                Dictionary<object, Dictionary<IBinding, object>>.KeyCollection keys = this.conflicts.Keys;
                foreach (object k in keys)
                {
                    if (conflictSummary.Length > 0)
                    {
                        conflictSummary += ", ";
                    }

                    conflictSummary += k.ToString();
                }

                throw new BinderException(
                    "Binder cannot fetch Bindings when the binder is in a conflicted state.\nConflicts: "
                    + conflictSummary, 
                    BinderExceptionType.CONFLICT_IN_BINDER);
            }

            if (this.bindings.ContainsKey(key))
            {
                Dictionary<object, IBinding> dict = this.bindings[key];
                name = (name == null) ? BindingConst.NULLOID : name;
                if (dict.ContainsKey(name))
                {
                    return dict[name];
                }
            }

            return null;
        }

        /// <summary>
        /// The get raw binding.
        /// </summary>
        /// <returns>
        /// The <see cref="IBinding"/>.
        /// </returns>
        public virtual IBinding GetRawBinding()
        {
            return new Binding(this.resolver);
        }

        /// <summary>
        /// The on remove.
        /// </summary>
        public virtual void OnRemove()
        {
        }

        /// <summary>
        /// The remove key.
        /// </summary>
        /// <param name="binding">
        /// The binding.
        /// </param>
        /// <param name="key">
        /// The key.
        /// </param>
        public virtual void RemoveKey(IBinding binding, object key)
        {
            if (binding == null || key == null || this.bindings.ContainsKey(key) == false)
            {
                return;
            }

            Dictionary<object, IBinding> dict = this.bindings[key];
            if (dict.ContainsKey(binding.name))
            {
                IBinding useBinding = dict[binding.name];
                useBinding.RemoveKey(key);
                object[] keys = useBinding.key as object[];
                if (keys != null && keys.Length == 0)
                {
                    dict.Remove(binding.name);
                }
            }
        }

        /// <summary>
        /// The remove name.
        /// </summary>
        /// <param name="binding">
        /// The binding.
        /// </param>
        /// <param name="name">
        /// The name.
        /// </param>
        public virtual void RemoveName(IBinding binding, object name)
        {
            if (binding == null || name == null)
            {
                return;
            }

            object key;
            if (binding.keyConstraint.Equals(BindingConstraintType.ONE))
            {
                key = binding.key;
            }
            else
            {
                object[] keys = binding.key as object[];
                key = keys[0];
            }

            Dictionary<object, IBinding> dict = this.bindings[key];
            if (dict.ContainsKey(name))
            {
                IBinding useBinding = dict[name];
                useBinding.RemoveName(name);
            }
        }

        /// <summary>
        /// The remove value.
        /// </summary>
        /// <param name="binding">
        /// The binding.
        /// </param>
        /// <param name="value">
        /// The value.
        /// </param>
        public virtual void RemoveValue(IBinding binding, object value)
        {
            if (binding == null || value == null)
            {
                return;
            }

            object key = binding.key;
            Dictionary<object, IBinding> dict;
            if (this.bindings.ContainsKey(key))
            {
                dict = this.bindings[key];
                if (dict.ContainsKey(binding.name))
                {
                    IBinding useBinding = dict[binding.name];
                    useBinding.RemoveValue(value);

                    // If result is empty, clean it out
                    object[] values = useBinding.value as object[];
                    if (values == null || values.Length == 0)
                    {
                        dict.Remove(useBinding.name);
                    }
                }
            }
        }

        /// <summary>
        /// The resolve binding.
        /// </summary>
        /// <param name="binding">
        /// The binding.
        /// </param>
        /// <param name="key">
        /// The key.
        /// </param>
        public virtual void ResolveBinding(IBinding binding, object key)
        {
            if (this.conflicts.ContainsKey(key))
            {
                // does the current key have any conflicts?
                Dictionary<IBinding, object> inConflict = this.conflicts[key];
                if (inConflict.ContainsKey(binding))
                {
                    // Am I on the conflict list?
                    object conflictName = inConflict[binding];
                    if (this.isConflictCleared(inConflict, binding))
                    {
                        // Am I now out of conflict?
                        this.clearConflict(key, conflictName, inConflict); // remove all from conflict list.
                    }
                    else
                    {
                        return; // still in conflict
                    }
                }
            }

            object bindingName = (binding.name == null) ? BindingConst.NULLOID : binding.name;
            Dictionary<object, IBinding> dict;
            if (this.bindings.ContainsKey(key))
            {
                dict = this.bindings[key];

                // Will my registration create a new conflict?
                if (dict.ContainsKey(bindingName))
                {
                    if (dict[bindingName] != binding)
                    {
                        // register both conflictees
                        this.registerNameConflict(key, binding, dict[bindingName]);
                        return;
                    }
                }
            }
            else
            {
                dict = new Dictionary<object, IBinding>();
                this.bindings[key] = dict;
            }

            if (dict.ContainsKey(BindingConst.NULLOID) && dict[BindingConst.NULLOID] == binding)
            {
                dict.Remove(BindingConst.NULLOID);
            }

            if (!dict.ContainsKey(bindingName))
            {
                dict.Add(bindingName, binding);
            }
        }

        /// <summary>
        /// The unbind.
        /// </summary>
        /// <typeparam name="T">
        /// </typeparam>
        public virtual void Unbind<T>()
        {
            this.Unbind(typeof(T), null);
        }

        /// <summary>
        /// The unbind.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        public virtual void Unbind(object key)
        {
            this.Unbind(key, null);
        }

        /// <summary>
        /// The unbind.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <typeparam name="T">
        /// </typeparam>
        public virtual void Unbind<T>(object name)
        {
            this.Unbind(typeof(T), name);
        }

        /// <summary>
        /// The unbind.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <param name="name">
        /// The name.
        /// </param>
        public virtual void Unbind(object key, object name)
        {
            if (this.bindings.ContainsKey(key))
            {
                Dictionary<object, IBinding> dict = this.bindings[key];
                object bindingName = (name == null) ? BindingConst.NULLOID : name;
                if (dict.ContainsKey(bindingName))
                {
                    dict.Remove(bindingName);
                }
            }
        }

        /// <summary>
        /// The unbind.
        /// </summary>
        /// <param name="binding">
        /// The binding.
        /// </param>
        public virtual void Unbind(IBinding binding)
        {
            if (binding == null)
            {
                return;
            }

            this.Unbind(binding.key, binding.name);
        }

        #endregion

        #region Methods

        /// <summary>
        /// The clear conflict.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <param name="dict">
        /// The dict.
        /// </param>
        protected void clearConflict(object key, object name, Dictionary<IBinding, object> dict)
        {
            List<IBinding> removalList = new List<IBinding>();

            foreach (KeyValuePair<IBinding, object> kv in dict)
            {
                object v = kv.Value;
                if (v.Equals(name))
                {
                    removalList.Add(kv.Key);
                }
            }

            int aa = removalList.Count;
            for (int a = 0; a < aa; a++)
            {
                dict.Remove(removalList[a]);
            }

            if (dict.Count == 0)
            {
                this.conflicts.Remove(key);
            }
        }

        /// <summary>
        /// The is conflict cleared.
        /// </summary>
        /// <param name="dict">
        /// The dict.
        /// </param>
        /// <param name="binding">
        /// The binding.
        /// </param>
        /// Returns true if the provided binding and the binding in the dict are no longer conflicting
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        protected bool isConflictCleared(Dictionary<IBinding, object> dict, IBinding binding)
        {
            foreach (KeyValuePair<IBinding, object> kv in dict)
            {
                if (kv.Key != binding && kv.Key.name == binding.name)
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// The register name conflict.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <param name="newBinding">
        /// The new Binding.
        /// </param>
        /// <param name="existingBinding">
        /// The existing Binding.
        /// </param>
        /// Take note of bindings that are in conflict.
        /// This occurs routinely during fluent binding, but will spark an error if
        /// GetBinding is called while this Binder still has conflicts.
        protected void registerNameConflict(object key, IBinding newBinding, IBinding existingBinding)
        {
            Dictionary<IBinding, object> dict;
            if (this.conflicts.ContainsKey(key) == false)
            {
                dict = new Dictionary<IBinding, object>();
                this.conflicts[key] = dict;
            }
            else
            {
                dict = this.conflicts[key];
            }

            dict[newBinding] = newBinding.name;
            dict[existingBinding] = newBinding.name;
        }

        /// <summary>
        /// The resolver.
        /// </summary>
        /// <param name="binding">
        /// The binding.
        /// </param>
        /// The default handler for resolving bindings during chained commands
        protected virtual void resolver(IBinding binding)
        {
            object key = binding.key;
            if (binding.keyConstraint.Equals(BindingConstraintType.ONE))
            {
                this.ResolveBinding(binding, key);
            }
            else
            {
                object[] keys = key as object[];
                int aa = keys.Length;
                for (int a = 0; a < aa; a++)
                {
                    this.ResolveBinding(binding, keys[a]);
                }
            }
        }

        /// <summary>
        /// The splice value at.
        /// </summary>
        /// <param name="splicePos">
        /// The splice pos.
        /// </param>
        /// <param name="objectValue">
        /// The object value.
        /// </param>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// The <see cref="T[]"/>.
        /// </returns>
        protected T[] spliceValueAt<T>(int splicePos, object[] objectValue)
        {
            T[] newList = new T[objectValue.Length - 1];
            int mod = 0;
            int aa = objectValue.Length;
            for (int a = 0; a < aa; a++)
            {
                if (a == splicePos)
                {
                    mod = -1;
                    continue;
                }

                newList[a + mod] = (T)objectValue[a];
            }

            return newList;
        }

        /// <summary>
        /// The splice value at.
        /// </summary>
        /// <param name="splicePos">
        /// The splice Pos.
        /// </param>
        /// <param name="objectValue">
        /// The object Value.
        /// </param>
        /// Remove the item at splicePos from the list objectValue
        /// <returns>
        /// The <see cref="object[]"/>.
        /// </returns>
        protected object[] spliceValueAt(int splicePos, object[] objectValue)
        {
            return this.spliceValueAt<object>(splicePos, objectValue);
        }

        #endregion
    }
}