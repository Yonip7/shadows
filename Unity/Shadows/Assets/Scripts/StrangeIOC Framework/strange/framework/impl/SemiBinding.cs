// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SemiBinding.cs" company="">
//   
// </copyright>
// <summary>
//   The semi binding.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace strange.framework.impl
{
    using System;

    using strange.framework.api;

    /// <summary>
    /// The semi binding.
    /// </summary>
    public class SemiBinding : ISemiBinding
    {
        #region Fields

        /// <summary>
        /// The object value.
        /// </summary>
        private object[] objectValue;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="SemiBinding"/> class.
        /// </summary>
        public SemiBinding()
        {
            this.constraint = BindingConstraintType.ONE;
            this.uniqueValues = true;
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets the constraint.
        /// </summary>
        public Enum constraint { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether unique values.
        /// </summary>
        public bool uniqueValues { get; set; }

        /// <summary>
        /// Gets the value.
        /// </summary>
        public virtual object value
        {
            get
            {
                if ((BindingConstraintType)this.constraint == BindingConstraintType.ONE)
                {
                    return (this.objectValue == null) ? null : this.objectValue[0];
                }

                return this.objectValue;
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The add.
        /// </summary>
        /// <param name="o">
        /// The o.
        /// </param>
        /// <returns>
        /// The <see cref="ISemiBinding"/>.
        /// </returns>
        public ISemiBinding Add(object o)
        {
            if (this.objectValue == null || (BindingConstraintType)this.constraint == BindingConstraintType.ONE)
            {
                this.objectValue = new object[1];
            }
            else
            {
                if (this.uniqueValues)
                {
                    int aa = this.objectValue.Length;
                    for (int a = 0; a < aa; a++)
                    {
                        object val = this.objectValue[a];
                        if (val.Equals(o))
                        {
                            return this;
                        }
                    }
                }

                object[] tempList = this.objectValue;
                int len = tempList.Length;
                this.objectValue = new object[len + 1];
                tempList.CopyTo(this.objectValue, 0);
            }

            this.objectValue[this.objectValue.Length - 1] = o;

            return this;
        }

        /// <summary>
        /// The remove.
        /// </summary>
        /// <param name="o">
        /// The o.
        /// </param>
        /// <returns>
        /// The <see cref="ISemiBinding"/>.
        /// </returns>
        public ISemiBinding Remove(object o)
        {
            if (o.Equals(this.objectValue) || this.objectValue == null)
            {
                this.objectValue = null;
                return this;
            }

            int aa = this.objectValue.Length;
            for (int a = 0; a < aa; a++)
            {
                object currVal = this.objectValue[a];
                if (o.Equals(currVal))
                {
                    this.spliceValueAt(a);
                    return this;
                }
            }

            return this;
        }

        #endregion

        #region Methods

        /// <summary>
        /// The splice value at.
        /// </summary>
        /// <param name="splicePos">
        /// The splice Pos.
        /// </param>
        /// Remove the value at index splicePos
        protected void spliceValueAt(int splicePos)
        {
            object[] newList = new object[this.objectValue.Length - 1];
            int mod = 0;
            int aa = this.objectValue.Length;
            for (int a = 0; a < aa; a++)
            {
                if (a == splicePos)
                {
                    mod = -1;
                    continue;
                }

                newList[a + mod] = this.objectValue[a];
            }

            this.objectValue = (newList.Length == 0) ? null : newList;
        }

        #endregion
    }
}