// --------------------------------------------------------------------------------------------------------------------
// <copyright file="BinderExceptionType.cs" company="">
//   
// </copyright>
// <summary>
//   The binder exception type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace strange.framework.api
{
    /// <summary>
    /// The binder exception type.
    /// </summary>
    public enum BinderExceptionType
    {
        /// <summary>
        /// The conflic t_ i n_ binder.
        /// </summary>
        /// The binder is being used while one or more Bindings are in conflict
        CONFLICT_IN_BINDER
    }
}