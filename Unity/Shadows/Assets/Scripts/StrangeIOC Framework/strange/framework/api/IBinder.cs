// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IBinder.cs" company="">
//   
// </copyright>
// <summary>
//   The Binder interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace strange.framework.api
{
    /// <summary>
    /// The Binder interface.
    /// </summary>
    public interface IBinder
    {
        #region Public Methods and Operators

        /// <summary>
        /// The bind.
        /// </summary>
        /// Bind a Binding Key to a class or interface generic
        /// <returns>
        /// The <see cref="IBinding"/>.
        /// </returns>
        IBinding Bind<T>();

        /// <summary>
        /// The bind.
        /// </summary>
        /// <param name="value">
        /// The value.
        /// </param>
        /// Bind a Binding Key to a value
        /// <returns>
        /// The <see cref="IBinding"/>.
        /// </returns>
        IBinding Bind(object value);

        /// <summary>
        /// The get binding.
        /// </summary>
        /// Retrieve a binding based on the provided Type
        /// <returns>
        /// The <see cref="IBinding"/>.
        /// </returns>
        IBinding GetBinding<T>();

        /// <summary>
        /// The get binding.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// Retrieve a binding based on the provided object
        /// <returns>
        /// The <see cref="IBinding"/>.
        /// </returns>
        IBinding GetBinding(object key);

        /// <summary>
        /// The get binding.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        /// Retrieve a binding based on the provided Key (generic)/Name combo
        /// <returns>
        /// The <see cref="IBinding"/>.
        /// </returns>
        IBinding GetBinding<T>(object name);

        /// <summary>
        /// The get binding.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <param name="name">
        /// The name.
        /// </param>
        /// Retrieve a binding based on the provided Key/Name combo
        /// <returns>
        /// The <see cref="IBinding"/>.
        /// </returns>
        IBinding GetBinding(object key, object name);

        /// <summary>
        /// The get raw binding.
        /// </summary>
        /// Generate an unpopulated IBinding in whatever concrete form the Binder dictates
        /// <returns>
        /// The <see cref="IBinding"/>.
        /// </returns>
        IBinding GetRawBinding();

        /// <summary>
        /// The on remove.
        /// </summary>
        /// The Binder is being removed
        /// Override this method to clean up remaining bindings
        void OnRemove();

        /// <summary>
        /// The remove key.
        /// </summary>
        /// <param name="binding">
        /// The binding.
        /// </param>
        /// <param name="value">
        /// The value.
        /// </param>
        /// Remove a select key from the given binding
        void RemoveKey(IBinding binding, object value);

        /// <summary>
        /// The remove name.
        /// </summary>
        /// <param name="binding">
        /// The binding.
        /// </param>
        /// <param name="value">
        /// The value.
        /// </param>
        /// Remove a select name from the given binding
        void RemoveName(IBinding binding, object value);

        /// <summary>
        /// The remove value.
        /// </summary>
        /// <param name="binding">
        /// The binding.
        /// </param>
        /// <param name="value">
        /// The value.
        /// </param>
        /// Remove a select value from the given binding
        void RemoveValue(IBinding binding, object value);

        /// <summary>
        /// Places individual Bindings into the bindings Dictionary as part of the resolving process
        /// </summary>
        /// <param name="binding">
        /// The binding.
        /// </param>
        /// <param name="key">
        /// The key.
        /// </param>
        /// Note that while some Bindings may store multiple keys, each key takes a unique position in the
        /// bindings Dictionary.
        /// 
        /// Conflicts in the course of fluent binding are expected, but GetBinding
        /// will throw an error if there are any unresolved conflicts.
        void ResolveBinding(IBinding binding, object key);

        /// <summary>
        /// The unbind.
        /// </summary>
        /// Remove a binding based on the provided Key (generic)
        void Unbind<T>();

        /// <summary>
        /// The unbind.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        /// Remove a binding based on the provided Key (generic) / Name combo
        void Unbind<T>(object name);

        /// <summary>
        /// The unbind.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// Remove a binding based on the provided Key
        void Unbind(object key);

        /// <summary>
        /// The unbind.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <param name="name">
        /// The name.
        /// </param>
        /// Remove a binding based on the provided Key / Name combo
        void Unbind(object key, object name);

        /// <summary>
        /// The unbind.
        /// </summary>
        /// <param name="binding">
        /// The binding.
        /// </param>
        /// Remove the provided binding from the Binder
        void Unbind(IBinding binding);

        #endregion
    }
}