// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IBinding.cs" company="">
//   
// </copyright>
// <summary>
//   The Binding interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace strange.framework.api
{
    using System;

    /// <summary>
    /// The Binding interface.
    /// </summary>
    public interface IBinding
    {
        /**
		 * Tie this binding to a Type key
		 */
        #region Public Properties

        /// <summary>
        /// The key.
        /// </summary>
        /// Get the binding''s key
        object key { get; }

        /// <summary>
        /// The key constraint.
        /// </summary>
        /// Get or set a MANY or ONE constraint on the Key
        Enum keyConstraint { get; set; }

        /// <summary>
        /// The name.
        /// </summary>
        /// Get the binding's name
        object name { get; }

        /// <summary>
        /// The value.
        /// </summary>
        /// Get the binding's value
        object value { get; }

        /// <summary>
        /// The value constraint.
        /// </summary>
        /// Get or set a MANY or ONE constraint on the Value
        Enum valueConstraint { get; set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The key.
        /// </summary>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// The <see cref="IBinding"/>.
        /// </returns>
        IBinding Key<T>();

        /**
		 * Tie this binding to a value key, such as a string or class instance
		 */

        /// <summary>
        /// The key.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <returns>
        /// The <see cref="IBinding"/>.
        /// </returns>
        IBinding Key(object key);

        /**
		 * Set the Binding's value to a Type
		 **/

        /**
		 * Retrieve a binding if the supplied name matches, by Type
		 */

        /// <summary>
        /// The named.
        /// </summary>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// The <see cref="IBinding"/>.
        /// </returns>
        IBinding Named<T>();

        /**
		 * Retrieve a binding if the supplied name matches, by value
		 */

        /// <summary>
        /// The named.
        /// </summary>
        /// <param name="o">
        /// The o.
        /// </param>
        /// <returns>
        /// The <see cref="IBinding"/>.
        /// </returns>
        IBinding Named(object o);

        /**
		 * Remove a specific key from the binding.
		 */

        /// <summary>
        /// The remove key.
        /// </summary>
        /// <param name="o">
        /// The o.
        /// </param>
        void RemoveKey(object o);

        /**
		 * Remove a specific value from the binding
		 */

        /**
		 * Remove a name from the binding
		 */

        /// <summary>
        /// The remove name.
        /// </summary>
        /// <param name="o">
        /// The o.
        /// </param>
        void RemoveName(object o);

        /// <summary>
        /// The remove value.
        /// </summary>
        /// <param name="o">
        /// The o.
        /// </param>
        void RemoveValue(object o);

        /// <summary>
        /// The to.
        /// </summary>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// The <see cref="IBinding"/>.
        /// </returns>
        IBinding To<T>();

        /**
		 * Set the Binding's value to a value, such as a string or class instance
		 */

        /// <summary>
        /// The to.
        /// </summary>
        /// <param name="o">
        /// The o.
        /// </param>
        /// <returns>
        /// The <see cref="IBinding"/>.
        /// </returns>
        IBinding To(object o);

        /**
		 * Qualify a binding using a marker type
		 */

        /// <summary>
        /// The to name.
        /// </summary>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// The <see cref="IBinding"/>.
        /// </returns>
        IBinding ToName<T>();

        /**
		 * Qualify a binding using a value, such as a string or class instance
		 */

        /// <summary>
        /// The to name.
        /// </summary>
        /// <param name="o">
        /// The o.
        /// </param>
        /// <returns>
        /// The <see cref="IBinding"/>.
        /// </returns>
        IBinding ToName(object o);

        #endregion
    }
}