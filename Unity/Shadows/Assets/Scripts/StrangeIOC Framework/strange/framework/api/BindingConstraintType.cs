// --------------------------------------------------------------------------------------------------------------------
// <copyright file="BindingConstraintType.cs" company="">
//   
// </copyright>
// <summary>
//   The binding constraint type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace strange.framework.api
{
    /// <summary>
    /// The binding constraint type.
    /// </summary>
    public enum BindingConstraintType
    {
        /// <summary>
        /// The one.
        /// </summary>
        /// Constrains a SemiBinding to carry no more than one item in its Value
        ONE, 

        /// <summary>
        /// The many.
        /// </summary>
        /// Constrains a SemiBinding to carry a list of items in its Value
        MANY
    }
}