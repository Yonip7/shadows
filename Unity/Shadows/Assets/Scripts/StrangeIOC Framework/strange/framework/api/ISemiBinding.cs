// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ISemiBinding.cs" company="">
//   
// </copyright>
// <summary>
//   The SemiBinding interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace strange.framework.api
{
    using System;

    /// <summary>
    /// The SemiBinding interface.
    /// </summary>
    public interface ISemiBinding
    {
        #region Public Properties

        /// <summary>
        /// The constraint.
        /// </summary>
        /// Set or get the constraint.
        Enum constraint { get; set; }

        /// <summary>
        /// The unique values.
        /// </summary>
        /// A secondary constraint that ensures that this SemiBinding will never multiple values equivalent to each other.
        bool uniqueValues { get; set; }

        /// <summary>
        /// The value.
        /// </summary>
        /// Retrieve the value of this SemiBinding. If the constraint is MANY, the value will be an Array.
        object value { get; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The add.
        /// </summary>
        /// <param name="o">
        /// The o.
        /// </param>
        /// Add a value to this SemiBinding.
        /// <returns>
        /// The <see cref="ISemiBinding"/>.
        /// </returns>
        ISemiBinding Add(object o);

        /// <summary>
        /// The remove.
        /// </summary>
        /// <param name="o">
        /// The o.
        /// </param>
        /// Remove a value from this SemiBinding.
        /// <returns>
        /// The <see cref="ISemiBinding"/>.
        /// </returns>
        ISemiBinding Remove(object o);

        #endregion
    }
}