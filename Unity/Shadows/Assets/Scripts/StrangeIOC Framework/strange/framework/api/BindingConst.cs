// --------------------------------------------------------------------------------------------------------------------
// <copyright file="BindingConst.cs" company="">
//   
// </copyright>
// <summary>
//   The binding const.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace strange.framework.api
{
    /// <summary>
    /// The binding const.
    /// </summary>
    public enum BindingConst
    {
        /// <summary>
        /// The nulloid.
        /// </summary>
        /// Null is an acceptable binding, but dictionaries choke on it, so we map null to this instead.
        NULLOID
    }
}