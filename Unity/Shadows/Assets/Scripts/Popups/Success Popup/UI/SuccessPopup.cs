﻿using System.Collections.Generic;
using UnityEngine;

public class SuccessPopup : PopupScreenAbstract
{
    public delegate void LevelsButtonClickedDelegate();
    public event LevelsButtonClickedDelegate LevelsButtonWasClicked;

    public delegate void NextLevelButtonClickedDelegate();
    public event NextLevelButtonClickedDelegate NextLevelButtonWasClicked;

    public delegate void RestartButtonClickedDelegate();
    public event RestartButtonClickedDelegate RestartButtonWasClicked;

    #region Texts

        public enum eTexts
        {
            Body,
            Headline,
            RestartButton,
            LevelsButton,
            NextButton
        }

        public MLTextMesh mr_HeadlineText;
        public MLTextMesh mr_BodyText;
        public MLTextMesh mr_RestartButtonText;
        public MLTextMesh mr_LevelsButtonText;
        public MLTextMesh mr_NextButtonText;

        private Dictionary<eTexts, string> m_texts;

        public override void ResetPopup(object i_InitData = null)
        {
            this.initTextsFromJSON();
            this.assignTexts();
        }

        private void initTextsFromJSON()
        {
            m_texts = new Dictionary<eTexts, string>();
            m_texts[eTexts.Headline] = this.getHeadlineText();
            m_texts[eTexts.Body] = this.getBodyText();
            m_texts[eTexts.RestartButton] = TextService.GetTextById("3.5.1.2");
            m_texts[eTexts.LevelsButton] = TextService.GetTextById("3.5.1.1");
            m_texts[eTexts.NextButton] = TextService.GetTextById("3.5.1.3");
        }

        private void assignTexts()
        {
            mr_HeadlineText.ChangeText(this.getTextByType(eTexts.Headline));
            mr_BodyText.ChangeText(this.getTextByType(eTexts.Body));
            mr_RestartButtonText.ChangeText(this.getTextByType(eTexts.RestartButton));
            mr_LevelsButtonText.ChangeText(this.getTextByType(eTexts.LevelsButton));
            mr_NextButtonText.ChangeText(this.getTextByType(eTexts.NextButton));
        }

        private string getTextByType(eTexts i_TextType)
        {
            return m_texts[i_TextType];
        }

        private string getHeadlineText()
        {
            int randomTextIndex;
            List<string> availableStrings = new List<string>();
            availableStrings.Add(TextService.GetTextById("3.5.2.1"));
            availableStrings.Add(TextService.GetTextById("3.5.2.3"));
            availableStrings.Add(TextService.GetTextById("3.5.2.5"));
            availableStrings.Add(TextService.GetTextById("3.5.2.7"));

            randomTextIndex = Random.Range(0, availableStrings.Count);

            return availableStrings[randomTextIndex];
        }

        private string getBodyText()
        {
            int randomTextIndex;
            List<string> availableStrings = new List<string>();
            availableStrings.Add(TextService.GetTextById("3.5.2.2"));
            availableStrings.Add(TextService.GetTextById("3.5.2.4"));
            availableStrings.Add(TextService.GetTextById("3.5.2.6"));
            availableStrings.Add(TextService.GetTextById("3.5.2.8"));

            randomTextIndex = Random.Range(0, availableStrings.Count);

            return availableStrings[randomTextIndex];
        }

    #endregion

    #region Buttons Handling

    public void RestartButtonWasClickedHandler()
        {
            if (RestartButtonWasClicked != null)
            {
                RestartButtonWasClicked();
            }
        }

        public void LevelsButtonWasClickedHandler()
        {
            if (LevelsButtonWasClicked != null)
            {
                LevelsButtonWasClicked();
            }
        }

        public void NextLevelButtonWasClickedHandler()
        {
            if (NextLevelButtonWasClicked != null)
            {
                NextLevelButtonWasClicked();
            }
        }

    #endregion
}
