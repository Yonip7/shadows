﻿using strange.extensions.mediation.impl;

public class SuccessPopupViewMediator : EventMediator {

    [Inject]
    public SuccessPopup
        SuccessPopup { get; set; }

    [Inject]
    public SuccessRestartButtonWasClickedSignal
        SuccessRestartButtonWasClickedSignal { get; set; }

    [Inject]
    public SuccessLevelsButtonWasClickedSignal
        SuccessLevelsButtonWasClickedSignal { get; set; }

    [Inject]
    public SuccessNextButtonWasClickedSignal
        SuccessNextButtonWasClickedSignal { get; set; }

    public override void OnRegister()
    {
        this.listenToModelEvents();
        this.listenToViewEvents();
    }

    private void listenToViewEvents()
    {
        SuccessPopup.RestartButtonWasClicked += this.handleRestartButtonClicked;
        SuccessPopup.LevelsButtonWasClicked += this.handleLevelsButtonClicked;
        SuccessPopup.NextLevelButtonWasClicked += this.handleNextButtonClicked;
    }

    private void listenToModelEvents()
    {
        //todo
    }

    private void handleRestartButtonClicked()
    {
        SuccessRestartButtonWasClickedSignal.Dispatch();
    }

    private void handleLevelsButtonClicked()
    {
        SuccessLevelsButtonWasClickedSignal.Dispatch();
    }

    private void handleNextButtonClicked()
    {
        SuccessNextButtonWasClickedSignal.Dispatch();
    }
}
