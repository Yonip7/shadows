﻿using strange.extensions.mediation.impl;

public class PopupManagerViewMediator : EventMediator
{
    [Inject]
    public PopupManager PopupManager { get; set; }

    [Inject]
    public MoveToSuccessPopupSignal MoveToSuccessPopupSignal { get; set; }

    [Inject]
    public SuccessRestartButtonWasClickedSignal
        SuccessRestartButtonWasClickedSignal { get; set; }

    [Inject]
    public SuccessLevelsButtonWasClickedSignal
        SuccessLevelsButtonWasClickedSignal { get; set; }

    [Inject]
    public SuccessNextButtonWasClickedSignal
        SuccessNextButtonWasClickedSignal { get; set; }

    public override void OnRegister()
    {
        this.listenToModelEvents();
        this.listenToViewEvents();
    }

    private void listenToModelEvents()
    {
        MoveToSuccessPopupSignal.AddListener(this.onMoveToSuccessPopup);
        SuccessRestartButtonWasClickedSignal.AddListener(this.onSuccessPopupNextButtonClicked);
        SuccessLevelsButtonWasClickedSignal.AddListener(this.onSuccessPopupLevelsButtonClicked);
        SuccessNextButtonWasClickedSignal.AddListener(this.onSuccessPopupNextButtonClicked);
    }

    private void onSuccessPopupNextButtonClicked()
    {
        this.StartCoroutine(PopupManager.HideLastPopupEnumrator());
    }

    private void onSuccessPopupRestartButtonClicked()
    {
        this.StartCoroutine(PopupManager.HideLastPopupEnumrator());
    }

    private void onSuccessPopupLevelsButtonClicked()
    {
        this.StartCoroutine(PopupManager.HideLastPopupEnumrator());
    }

    private void listenToViewEvents()
    {
        //todo
    }

    private void onMoveToSuccessPopup()
    {
        this.StartCoroutine(PopupManager.ShowPopUpEnumerator("SuccessPopup"));
    }
}
