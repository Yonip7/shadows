﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class RulesPopup : PopupScreenAbstract
{

    public const float k_SlideAnimationDuration = 0.6F;

    public enum eSlideDirection
    {
        Left,

        Right,

        Center
    }

    public List<Transform> mr_RulesTransforms;

    public float mr_RulesOffset;

    public int NumberOfRules
    {
        get
        {
            return this.mr_RulesTransforms.Count;
        }
    }

    public float mr_CenterOffset;

    private int m_currentTransformId = 0;

    private Vector3 m_draggingStartPosition;

    private readonly List<Vector3> m_originalPositions = new List<Vector3>();

    private readonly List<Vector3> m_finalPositions = new List<Vector3>();

    
    public override void ResetPopup(object i_InitData = null)
    {
        for (int i = 0; i < this.NumberOfRules; i++)
        {
            this.m_originalPositions.Add(new Vector3(0, 0, 0));
            this.m_finalPositions.Add(new Vector3(0, 0, 0));
        }

        this.ResetRulesPositions();
    }

    public void OnMouseDown()
    {
        this.m_draggingStartPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
    }

    public void OnMouseDrag()
    {
        Vector3 change = new Vector3(
            (Camera.main.ScreenToWorldPoint(Input.mousePosition) - this.m_draggingStartPosition).x, 0, 0);
        foreach (Transform t in this.mr_RulesTransforms)
        {
            t.localPosition = t.localPosition + change;
        }
        this.m_draggingStartPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
    }

    public IEnumerator OnMouseUp()
    {
        this.m_draggingStartPosition = new Vector3(0, 0, 0);


        float movementDistance = this.mr_RulesTransforms[this.m_currentTransformId].localPosition.x;
        eSlideDirection direction;

        if (Math.Abs(movementDistance) < 25)
        {
            direction = eSlideDirection.Center;
        }
        else if (movementDistance < 0)
        {
            direction = eSlideDirection.Left;
        }
        else
        {
            direction = eSlideDirection.Right;
        }

        yield return StartCoroutine(SlideAnimation(direction));
    }


    /**
     * Slide animator for all rules 
     */
    public IEnumerator SlideAnimation(eSlideDirection direction)
    {
        int slideDirection;
        if (direction == eSlideDirection.Left)
        {
            slideDirection = -1;
        }
        else if (direction == eSlideDirection.Right)
        {
            slideDirection = 1;
        }
        else
        {
            // direction is Center 
            slideDirection = 0;
        }

        float startTime = Time.time;
        Vector3 movementVector = new Vector3(this.mr_RulesOffset * slideDirection - (this.mr_RulesTransforms[this.m_currentTransformId].localPosition.x), 0, 0);

        for (int i = 0; i < this.NumberOfRules; i++)
        {
            this.m_originalPositions[i] = this.mr_RulesTransforms[i].localPosition;
            this.m_finalPositions[i] = this.m_originalPositions[i] + movementVector;
        }

        if (this.m_currentTransformId == 0 && slideDirection == 1)
        {
            this.m_currentTransformId = this.NumberOfRules - 1;
        }
        else if (this.m_currentTransformId == this.NumberOfRules - 1 && slideDirection == -1)
        {
            this.m_currentTransformId = 0;
        }
        else
        {
            this.m_currentTransformId = this.m_currentTransformId - slideDirection;
        }

        if (direction != eSlideDirection.Center)
        {
            // TODO
        }
        while (Time.time < startTime + k_SlideAnimationDuration)
        {
            float prop = (Time.time - startTime) / k_SlideAnimationDuration;
            for (int i = 0; i < this.NumberOfRules; i++)
            {
                this.mr_RulesTransforms[i].localPosition = this.m_originalPositions[i] + movementVector * prop;
            }
            yield return 0;
        }

        for (int i = 0; i < this.NumberOfRules; i++)
        {
            this.mr_RulesTransforms[i].localPosition = this.m_originalPositions[i] + movementVector;
        }


        this.ReplaceLastRule(slideDirection);
    }

    public void NextRule()
    {
        StartCoroutine(nextRule());
    }

    private IEnumerator nextRule()
    {
        yield return StartCoroutine(this.SlideAnimation(eSlideDirection.Left));
    }

    public void PreviousRule()
    {
        StartCoroutine(previousRule());
    }

    private IEnumerator previousRule()
    {
        yield return StartCoroutine(this.SlideAnimation(eSlideDirection.Right));
    }

    /**
     * 
     */
    public void ReplaceLastRule(int i_SlideDirection)
    {

        if (i_SlideDirection == 0) return;

        int chosenRuleIndex = 0;
        Vector3 minVec = this.mr_RulesTransforms[0].localPosition;
        Vector3 maxVec = this.mr_RulesTransforms[0].localPosition;

        for (int i = 1; i < this.NumberOfRules; i++)
        {
            if (this.mr_RulesTransforms[i].transform.localPosition.x < minVec.x)
            {
                minVec = this.mr_RulesTransforms[i].transform.localPosition;
                if (i_SlideDirection == -1)
                {
                    chosenRuleIndex = i;
                }
                continue;
            }

            if (this.mr_RulesTransforms[i].transform.localPosition.x > maxVec.x)
            {
                if (i_SlideDirection == 1)
                {
                    chosenRuleIndex = i;
                }
                maxVec = this.mr_RulesTransforms[i].transform.localPosition;
            }
        }

        if (i_SlideDirection == -1)
        {
            this.mr_RulesTransforms[chosenRuleIndex].transform.localPosition = maxVec + new Vector3(this.mr_RulesOffset, 0, 0);
        }
        else
        {
            this.mr_RulesTransforms[chosenRuleIndex].transform.localPosition = minVec - new Vector3(this.mr_RulesOffset, 0, 0);
        }
    }

    public void ResetRulesPositions()
    {

        int middle = this.NumberOfRules / 2;
        int mod = this.NumberOfRules % 2;

        if (mod == 1)
        {
            for (int i = 1; i < middle + 1; i++)
            {
                this.mr_RulesTransforms[i].localPosition = new Vector3(this.mr_RulesOffset * i, 0, 0);
                this.mr_RulesTransforms[this.NumberOfRules - i].localPosition = new Vector3(-1 * this.mr_RulesOffset * i, 0, 0);
            }
            this.mr_RulesTransforms[0].localPosition = new Vector3(0, 0, 0);
        }
        else
        {
            for (int i = 0; i < middle; i++)
            {
                this.mr_RulesTransforms[i].localPosition = new Vector3(this.mr_RulesOffset * i, 0, 0);
                this.mr_RulesTransforms[this.NumberOfRules - i - 1].localPosition = new Vector3(-1 * this.mr_RulesOffset * (i + 1), 0, 0);
            }
        }
        this.m_currentTransformId = 0;
    }
}
