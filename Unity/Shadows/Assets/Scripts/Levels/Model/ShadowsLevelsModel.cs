﻿using System.Collections.Generic;
using Assets.Scripts.strange.extensions;

public class ShadowsLevelsModel : Model
{
    [Inject]
    public LevelsInitCompletedSignal LevelsInitCompletedSignal { get; set; }

    private Dictionary<int, string> m_levelsDictionary;

    private int m_currentLevel;

    private const int k_firstLevel = 1;

    public void Init(Dictionary<int, string> i_LevelsDictionary)
    {
        this.m_levelsDictionary = i_LevelsDictionary;
        this.m_currentLevel = k_firstLevel;
        LevelsInitCompletedSignal.Dispatch(this.GetNumOfLevelCards());
    }

    public string GetCurrentLevel()
    {
        if (this.m_levelsDictionary.ContainsKey(this.m_currentLevel))
        {
            return m_levelsDictionary[this.m_currentLevel];
        }

        return null;
    }

    public int GetCurrentLevelIndex()
    {
        return this.m_currentLevel;
    }

    public void MoveToNextLevel()
    {
        this.m_currentLevel++;
    }

    public void MoveToLevel(int i_LevelIndex)
    {
        this.m_currentLevel = i_LevelIndex;
    }

    public int GetNumOfLevelCards()
    {
        return this.m_levelsDictionary.Count;
    }
}
