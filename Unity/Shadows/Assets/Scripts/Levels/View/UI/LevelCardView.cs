﻿using UnityEngine;
using System.Collections;

public class LevelCardView : MonoBehaviour
{
    public delegate void LevelCardClickedDelegate(LevelCardView i_LevelCard);
    public event LevelCardClickedDelegate LevelCardWasClicked;

    public GameObject mr_Normal;
    public GameObject mr_MouseOver;
    public GameObject mr_Pressed;
    public GameObject mr_Thumbnail;
    public GameObject mr_Completed;
    public MLTextMesh mr_LevelNumberInCategory;

    public LevelsView.eDifficulties Difficulty { get; private set; }

    public int LevelNumber { get; private set; }

    public void Init(int i_LevelNumber, LevelsView.eDifficulties i_Difficulty)
    {
        mr_Normal.SetActive(true);
        mr_MouseOver.SetActive(false);
        mr_Pressed.SetActive(false);

        mr_Thumbnail.SetActive(true);
        mr_Completed.SetActive(false);

        int difficultyIndex = (int)i_Difficulty;

        int levelNumberInCategory = i_LevelNumber - 
            LevelsView.k_NumOfLevelCardsInDifficulty * difficultyIndex;

        this.mr_LevelNumberInCategory.ChangeText(levelNumberInCategory.ToString());

        this.Difficulty = i_Difficulty;
        this.LevelNumber = i_LevelNumber;
    }

    public void OnMouseEnter()
    {
        mr_Normal.SetActive(false);
        mr_MouseOver.SetActive(true);
        mr_Pressed.SetActive(false);
    }

    public void OnMouseExit()
    {
        mr_Normal.SetActive(true);
        mr_MouseOver.SetActive(false);
        mr_Pressed.SetActive(false);
    }

    public void OnMouseDown()
    {
        mr_Pressed.SetActive(true);
        mr_Normal.SetActive(false);
        mr_MouseOver.SetActive(false);
    }

    public void OnMouseUp()
    {
        mr_Normal.SetActive(true);
        mr_Pressed.SetActive(false);

        if (LevelCardWasClicked != null)
        {
            LevelCardWasClicked(this);
        }
    }

    public void MarkAsCompleted()
    {
        mr_Completed.gameObject.SetActive(true);
    }
}
