﻿using UnityEngine;
using System.Collections;

public class LevelCategoryView : MonoBehaviour
{
    public delegate void CategoryClickedDelegate(LevelCategoryView i_Category);
    public event CategoryClickedDelegate CategoryWasClicked;

    public MLTextMesh mr_CategoryText;

    public Material mr_NormalMaterial;
    public Material mr_PressedMaterial;
    public Material mr_MouseOverMaterial;

    public GameObject mr_UnderLine;

    public LevelsView.eDifficulties Difficulty { get; private set; }

    private bool m_isSelected;

    public void Init(LevelsView.eDifficulties i_Difficulty)
    {
        this.Difficulty = i_Difficulty;
        mr_CategoryText.renderer.material = mr_NormalMaterial;
    }

    public void SelectCategory()
    {
        mr_CategoryText.renderer.material = mr_PressedMaterial;
        mr_UnderLine.gameObject.SetActive(true);
        this.m_isSelected = true;
    }

    public void UnselectCategory()
    {
        mr_CategoryText.renderer.material = mr_NormalMaterial;
        mr_UnderLine.gameObject.SetActive(false);
        this.m_isSelected = false;
    }

    public void OnMouseDown()
    {
        mr_CategoryText.renderer.material = mr_PressedMaterial;
    }

    public void OnMouseUp()
    {
        if (this.CategoryWasClicked != null)
        {
            this.CategoryWasClicked(this);
        }
    }

    public void OnMouseEnter()
    {
        mr_CategoryText.renderer.material = mr_MouseOverMaterial;
    }

    public void OnMouseExit()
    {
        if (this.m_isSelected)
        {
            mr_CategoryText.renderer.material = mr_PressedMaterial;
        }
        else
        {
            mr_CategoryText.renderer.material = mr_NormalMaterial;
        }
    }

}
