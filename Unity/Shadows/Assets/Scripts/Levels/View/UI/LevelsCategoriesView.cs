﻿using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class LevelsCategoriesView : MonoBehaviour
{
    public delegate void DifficultyClickedDelegate(LevelsView.eDifficulties i_Difficulty);
    public event DifficultyClickedDelegate DifficultyWasClicked;

    public List<LevelCategoryView> mr_CategoriesList;

    public void Start()
    {
//        foreach (LevelCategoryView levelCategoryView in this.mr_CategoriesList)
//        {
//            levelCategoryView.gameObject.SetActive(false);
//        }
    }

    public void Init()
    {
        int categoryIndex = (int)LevelsView.eDifficulties.Beginner;

        foreach (LevelCategoryView levelCategoryView in this.mr_CategoriesList)
        {
            levelCategoryView.Init((LevelsView.eDifficulties)categoryIndex);
            this.listenToCatergory(levelCategoryView);
            categoryIndex++;
        }

//        foreach (LevelCategoryView levelCategoryView in this.mr_CategoriesList)
//        {
//            levelCategoryView.gameObject.SetActive(true);
//        }

        LevelCategoryView firstCategory = mr_CategoriesList.First();
        //firstCategory.SelectCategory();
        this.handleCategorySelection(firstCategory);
    }

    private void listenToCatergory(LevelCategoryView i_Catergory)
    {
        i_Catergory.CategoryWasClicked += this.handleCategorySelection;
    }

    private void handleCategorySelection(LevelCategoryView i_Category)
    {
        foreach (LevelCategoryView levelCategoryView in this.mr_CategoriesList)
        {
            if (levelCategoryView == i_Category)
            {
                levelCategoryView.SelectCategory();
                this.dispatchDiffcultyWasClickedEvent(levelCategoryView.Difficulty);
            }
            else
            {
                levelCategoryView.UnselectCategory();
            }
        }
    }

    private void dispatchDiffcultyWasClickedEvent(LevelsView.eDifficulties i_Difficulty)
    {
        if (DifficultyWasClicked != null)
        {
            DifficultyWasClicked(i_Difficulty);
        }
    }
}
