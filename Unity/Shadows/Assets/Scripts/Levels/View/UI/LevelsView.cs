﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using strange.extensions.mediation.impl;
using UnityEngine;

public class LevelsView : View
{
    public delegate void LevelCardClickedDelegate(int i_LevelCardNumber);
    public event LevelCardClickedDelegate LevelCardWasClicked;

    public enum eDifficulties 
    {
        Beginner = 0,
        Advanced,
        Expert
    }

    public enum eTexts
    {
        PlayButton,
        BeginnerCategory,
        AdvancedCategory,
        ExpertCategory,
    }

    public LevelsCategoriesView mr_CategoriesView;

    public List<GameObject> mr_DifficultyContainers;

    public LevelCardView mr_LevelCardViewPrefab;

    public ArenaUIButton mr_PlayButton;

    public MLTextMesh mr_PlayButtonText;
    public MLTextMesh mr_BeginnerCategoryText;
    public MLTextMesh mr_AdvancedCategoryText;
    public MLTextMesh mr_ExpertCategoryText;

    public SystemButtons mr_SystemButtons;

    private Dictionary<eTexts, string> m_texts;

    private List<LevelCardView> m_levelCards; 

    private eDifficulties m_currentDifficulty;

    private int m_currentLevelCardNumber;

    private int m_numOfLevelCards;

    public const int k_NumOfLevelCardsInDifficulty = 10;

    private const int k_numOfLinesPerDifficulty = 2;
    
    private readonly Vector3 k_firstLevelCardPosition = new Vector3(65, -225, 0);

    private const int k_firstLevelCardIndex = 1;

    private const float k_distanceBetweenLevelCardsInX = 240f;
    private const float k_distanceBetweenLevelCardsInY = 240f;

    public void Init(int i_NumOfLevelCards)
    {
       this.initTexts();
       mr_SystemButtons.Init();

        //todo:Get from model
        this.m_numOfLevelCards = i_NumOfLevelCards;

        this.m_levelCards = new List<LevelCardView>();

        this.m_currentDifficulty = eDifficulties.Beginner;
        this.m_currentLevelCardNumber = k_firstLevelCardIndex;

        mr_CategoriesView.Init();
        
        this.createLevelCards();
        this.displayCorrectDifficulty();
        this.listenToCategoryView();
        //Debug.Log("Levels View initalized!!!");
    }

    private void initTexts()
    {
        this.initTextsFromJSON();
        this.assignTexts();
    }

    public IEnumerator Show()
    {
        //todo: level screen show animation
        this.mr_PlayButton.gameObject.SetActive(true);
        this.mr_CategoriesView.gameObject.SetActive(true);
        this.displayCorrectDifficulty();
        yield break;
    }

    private IEnumerator hide()
    {
        //todo: level screen hide animation
        this.mr_PlayButton.gameObject.SetActive(false);
        this.mr_CategoriesView.gameObject.SetActive(false);

        foreach (GameObject difficultyContainer in mr_DifficultyContainers)
        {
            difficultyContainer.gameObject.SetActive(false);
        }

        yield break;
    }

    private void listenToCategoryView()
    {
        mr_CategoriesView.DifficultyWasClicked += this.handleDifficultySelection;
    }

    private void handleDifficultySelection(eDifficulties i_Difficulty)
    {
        int difficultyIndex = (int)i_Difficulty;
        GameObject difficulty;

        for (int i = 0; i < Enum.GetValues(typeof(eDifficulties)).Length; i++)
        {
            difficulty = mr_DifficultyContainers[i];

            if (i == difficultyIndex)
            {
                difficulty.gameObject.SetActive(true);
            }
            else
            {
                difficulty.gameObject.SetActive(false);
            }
        }

        this.m_currentDifficulty = i_Difficulty;
    }

    private void listenToLevelCard(LevelCardView i_LevelCard)
    {
        i_LevelCard.LevelCardWasClicked += this.handleLevelCardWasClicked;
    }

    private void handleLevelCardWasClicked(LevelCardView i_Levelcard)
    {
        this.StartCoroutine(this.handleLevelCardWasClickedEnumerator(i_Levelcard));
    }

    private IEnumerator handleLevelCardWasClickedEnumerator(LevelCardView i_Levelcard)
    {
        Debug.Log("Level Card: " + i_Levelcard.LevelNumber + " Was Clicked!!!");

        yield return this.StartCoroutine(this.hide());

        if (LevelCardWasClicked != null)
        {
            LevelCardWasClicked(i_Levelcard.LevelNumber);
        }

        this.m_currentLevelCardNumber = i_Levelcard.LevelNumber;
    }

    private void displayCorrectDifficulty()
    {
        foreach (eDifficulties difficulty in Enum.GetValues(typeof(eDifficulties)))
        {
            if (difficulty == this.m_currentDifficulty)
            {
                mr_DifficultyContainers[(int)difficulty].SetActive(true);
            }
            else
            {
                mr_DifficultyContainers[(int)difficulty].SetActive(false);
            }
        }
    }

    private void createLevelCards()
    {
        foreach (eDifficulties difficulty in Enum.GetValues(typeof(eDifficulties)))
        {
            this.createDifficulty(difficulty);
        }
    }

    private void createDifficulty(eDifficulties i_Difficulty)
    {
        int levelNumber;

        int difficultyIndex = (int)i_Difficulty;

        int numOfCardsLeftToCreate = this.m_numOfLevelCards - k_NumOfLevelCardsInDifficulty * difficultyIndex;

        int numOfCardsPerLine = k_NumOfLevelCardsInDifficulty /
            k_numOfLinesPerDifficulty;

        for (int i = 0; i < k_numOfLinesPerDifficulty; i++)
        {
            for (int j = 0; j < numOfCardsPerLine; j++)
            {

                levelNumber = this.m_numOfLevelCards - numOfCardsLeftToCreate + 1;
                numOfCardsLeftToCreate--;

                Debug.Log("Level Num: " + levelNumber);


                if (numOfCardsLeftToCreate > -1)
                {
                    this.createLevelCard(j, i, i_Difficulty, levelNumber);
                }
            }
        }
    }

    private void createLevelCard(int xPosition, int yPosition, eDifficulties i_Difficulty,
        int i_LevelNumber)
    {
        LevelCardView newLevelCard = Instantiate(mr_LevelCardViewPrefab) as LevelCardView;

        GameObject difficultyContainer = mr_DifficultyContainers[(int)i_Difficulty];

        newLevelCard.transform.parent = difficultyContainer.transform;

        float xValue = k_firstLevelCardPosition.x + xPosition * k_distanceBetweenLevelCardsInX;

        float yValue = k_firstLevelCardPosition.y - 
            yPosition * k_distanceBetweenLevelCardsInY;

        float zValue = 0;

        newLevelCard.transform.localPosition = new Vector3(xValue, yValue, zValue);

        newLevelCard.transform.name = i_Difficulty + ":" + i_LevelNumber;

        newLevelCard.Init(i_LevelNumber, i_Difficulty);

        this.m_levelCards.Add(newLevelCard);

        this.listenToLevelCard(newLevelCard);
    }

    public void MarkCurrentLevelCardAsCompleted()
    {
        LevelCardView completedLevelCard =
            m_levelCards.Where(x => x.LevelNumber == m_currentLevelCardNumber).First();

        completedLevelCard.MarkAsCompleted();
    }

    #region Texts

        private void initTextsFromJSON()
        {
            m_texts = new Dictionary<eTexts, string>();
            m_texts[eTexts.PlayButton] = TextService.GetTextById("3.2.1.5");
            m_texts[eTexts.BeginnerCategory] = TextService.GetTextById("3.2.2.1");
            m_texts[eTexts.AdvancedCategory] = TextService.GetTextById("3.2.2.2");
            m_texts[eTexts.ExpertCategory] = TextService.GetTextById("3.2.2.3");
        }

        private void assignTexts()
        {
            mr_PlayButtonText.ChangeText(this.getTextByType(eTexts.PlayButton));
            mr_BeginnerCategoryText.ChangeText(this.getTextByType(eTexts.BeginnerCategory));
            mr_AdvancedCategoryText.ChangeText(this.getTextByType(eTexts.AdvancedCategory));
            mr_ExpertCategoryText.ChangeText(this.getTextByType(eTexts.ExpertCategory));
        }

        private string getTextByType(eTexts i_TextType)
        {
            return m_texts[i_TextType];
        }

    #endregion

    #region Buttons Clicked

        public void PlayButtonClickedHandler()
        {
            this.StartCoroutine(this.playButtonClickedEnumerator());
        }

        private IEnumerator playButtonClickedEnumerator()
        {
            Debug.Log("Level Card: " + this.m_currentLevelCardNumber + " Was Clicked!!!");

            yield return this.StartCoroutine(this.hide());

            if (LevelCardWasClicked != null)
            {
                LevelCardWasClicked(this.m_currentLevelCardNumber);
            }
        }

    #endregion
}
