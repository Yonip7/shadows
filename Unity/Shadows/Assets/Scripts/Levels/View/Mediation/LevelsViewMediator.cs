﻿using strange.extensions.mediation.impl;
using strange.extensions.signal.api;

public class LevelsViewMediator : EventMediator
{
    [Inject]
    public LevelsView LevelsView { get; set; }

    [Inject]
    public LevelSelectedSignal LevelSelectedSignal { get; set; }

    [Inject]
    public LevelsInitCompletedSignal LevelsInitCompletedSignal { get; set; }

    [Inject]
    public MoveToLevelsScreenSignal MoveToLevelsScreenSignal { get; set; }

    [Inject]
    public MoveToSuccessPopupSignal MoveToSuccessPopupSignal { get; set; }


    public override void OnRegister()
    {
        this.listenToModelEvents();
        this.listenToViewEvents();
        LevelsView.gameObject.SetActive(false);
    }

    private void listenToViewEvents()
    {
        LevelsView.LevelCardWasClicked += this.handleLevelCardWasClicked;
    }

    private void handleLevelCardWasClicked(int i_LevelCardNumber)
    {
        LevelsView.gameObject.SetActive(false);
        LevelSelectedSignal.Dispatch(i_LevelCardNumber);
    }

    private void listenToModelEvents()
    {
        LevelsInitCompletedSignal.AddListener(this.onLevelsInitCompleted);
        MoveToLevelsScreenSignal.AddListener(this.onMoveToLevelsScreen);
        MoveToSuccessPopupSignal.AddListener(this.onMoveToSuccessScreen);
    }

    private void onMoveToSuccessScreen()
    {
        LevelsView.MarkCurrentLevelCardAsCompleted();
    }

    private void onMoveToLevelsScreen()
    {
        LevelsView.gameObject.SetActive(true);
        this.StartCoroutine(LevelsView.Show());
    }

    private void onLevelsInitCompleted(int i_NumOfLevelCards)
    {
        LevelsView.Init(i_NumOfLevelCards);
    }
}
