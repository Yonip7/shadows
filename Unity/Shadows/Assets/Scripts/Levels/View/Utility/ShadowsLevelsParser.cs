﻿using Newtonsoft.Json;
using UnityEngine;
using System.Collections;

public class ShadowsLevelsParser : MonoBehaviour
{
    private const string k_levelsJsonPath = "Levels/LevelCardsJSON";

    public void ParseLevels(System.Action<ShadowsLevelsMetaData> i_Action)
    {
        this.StartCoroutine(this.parseLevels(i_Action));
    }

    private IEnumerator parseLevels(System.Action<ShadowsLevelsMetaData> i_Action)
    {
        var resource = Resources.Load(k_levelsJsonPath);
        yield return resource;

        TextAsset levelsAsTextAsset = resource as TextAsset;
        string levelsString = levelsAsTextAsset.text;
        ShadowsLevelsMetaData levels = JsonConvert.DeserializeObject<ShadowsLevelsMetaData>(levelsString);

        Debug.Log("Levels Parsing completed");

        Debug.Log("Num of levels: " + levels.LevelsDictionary.Count);

        if (i_Action != null)
        {
            i_Action(levels);
        }
    }
}
