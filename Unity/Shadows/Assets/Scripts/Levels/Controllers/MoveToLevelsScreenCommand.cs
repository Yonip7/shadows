﻿using strange.extensions.command.impl;

public class MoveToLevelsScreenCommand : Command
{
    [Inject]
    public SuccessLevelsButtonWasClickedSignal
        SuccessLevelsButtonWasClickedSignal { get; set; }

    [Inject]
    public LevelsInitCompletedSignal LevelsInitCompletedSignal { get; set; }

    [Inject]
    public ApplicationModel ApplicationModel { get; set; }

    public override void Execute()
    {
        ApplicationModel.MoveToLevels();
    }
}
