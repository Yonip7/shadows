﻿using System;

using ShadowsModel.Helpers;
using ShadowsModel.Model;

using strange.extensions.command.impl;

public class MoveToLevelCommand : Command
{
    [Inject]
    public ApplicationModel ApplicationModel { get; set; }

    [Inject]
    public ShadowsLevelsModel ShadowsLevelsModel { get; set; }

    [Inject]
    public IShadowsGameModel ShadowsGameModel { get; set; }

    [Inject]
    public int SelectedLevel { get; set; }

    public override void Execute()
    {
        ApplicationModel.MoveToGame();

        ShadowsLevelsModel.MoveToLevel(SelectedLevel);

        BoardConfig newConfig = 
            LayoutParser.parse(ShadowsLevelsModel.GetCurrentLevel());

        LogicalBoard newLogicalBoard = new LogicalBoard(newConfig, newConfig.InitialPosition);

        ShadowsGameModel.LoadLevel(ShadowsLevelsModel.GetCurrentLevelIndex(), newLogicalBoard);
    }
}
