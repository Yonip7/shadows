﻿using UnityEngine;
using System.Collections;

using strange.extensions.mediation.impl;

public class ApplicationRootView : View {

    public void Awake()
    {
        //ShadowsContext context = new ShadowsContext(this);
        //context.Start();
    }
    
    
    public void Start()
    {
        ShadowsContext context = new ShadowsContext(this);
        context.Start();
    }
}
