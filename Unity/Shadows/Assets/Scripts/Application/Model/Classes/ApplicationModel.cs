﻿using Assets.Scripts.strange.extensions;

public class ApplicationModel : Model
{
    [Inject]
    public StartApplicationInitSignal AppInitSignal { get; set; }

    [Inject]
    public MoveToSuccessPopupSignal MoveToSuccessPopupSignal { get; set; }

    [Inject]
    public MoveToLevelsScreenSignal MoveToLevelsScreenSignal { get; set; }

    [Inject]
    public MoveToGameSignal MoveToGameSignal { get; set; }

    public enum eAppStates
    {
        InSplash,
        InLevels,
        InGame,
        SuccessPopup,
        RulesPopup,
        ExitPopup
    }

    private eAppStates m_previousState;

    private eAppStates m_currentState;

    public ApplicationModel()
    {
        m_previousState = eAppStates.InSplash;
        m_currentState = eAppStates.InSplash;
    }

    public void StartApplicationInit()
    {
        this.AppInitSignal.Dispatch(this.m_currentState);
    }

    public void MoveToLevels()
    {
        this.m_previousState = this.m_currentState;
        this.m_currentState = eAppStates.InLevels;
        this.MoveToLevelsScreenSignal.Dispatch();
    }

    public void MoveToSuccessPopup()
    {
        this.m_previousState = this.m_currentState;
        this.m_currentState = eAppStates.SuccessPopup;
        this.MoveToSuccessPopupSignal.Dispatch();
    }

    public void MoveToGame()
    {
        this.m_previousState = this.m_currentState;
        this.m_currentState = eAppStates.InGame;
        this.MoveToGameSignal.Dispatch();
    }

    public void MoveToRulesPopup()
    {
        this.m_previousState = this.m_currentState;
        this.m_currentState = eAppStates.RulesPopup;
        this.MoveToGameSignal.Dispatch();
    }

    public void MoveToExitPopup()
    {
        
    }

    public void ToggleSound()
    {
        
    }
}
