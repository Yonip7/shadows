﻿using strange.extensions.command.impl;

public class SplashExitCommand : Command
{

    [Inject]
    public ApplicationModel ApplicationModel { get; set; }

    public override void Execute()
    {
        this.ApplicationModel.MoveToExitPopup();
    }
}