﻿using strange.extensions.command.impl;

public class SplashPlayCommand : Command {

    [Inject]
    public ApplicationModel ApplicationModel { get; set; }

    public override void Execute()
    {
        this.ApplicationModel.StartApplicationInit();
    }
}
