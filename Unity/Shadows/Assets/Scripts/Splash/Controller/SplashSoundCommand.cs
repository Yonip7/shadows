﻿using strange.extensions.command.impl;

public class SplashSoundCommand : Command
{

    [Inject]
    public ApplicationModel ApplicationModel { get; set; }

    public override void Execute()
    {
        this.ApplicationModel.ToggleSound();
    }
}