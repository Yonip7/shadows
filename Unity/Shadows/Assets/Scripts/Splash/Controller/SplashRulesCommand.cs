﻿using strange.extensions.command.impl;

public class SplashRulesCommand : Command
{

    [Inject]
    public ApplicationModel ApplicationModel { get; set; }

    public override void Execute()
    {
        this.ApplicationModel.MoveToRulesPopup();
    }
}