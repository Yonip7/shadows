﻿using System;
using System.Collections.Generic;
using System.Linq;

using UnityEngine;
using System.Collections;

using strange.extensions.mediation.impl;

using Random = System.Random;

public class SplashView : View
{
    private enum eButtonType
    {
        Play=0,

        Sound,

        Rules,

        Exit
    }

    public delegate void SplashButtonClickedDelegate();

    public event SplashButtonClickedDelegate SplashPlayButtonClicked;

    public event SplashButtonClickedDelegate SplashSoundButtonClicked;

    public event SplashButtonClickedDelegate SplashRulesButtonClicked;

    public event SplashButtonClickedDelegate SplashExitButtonClicked;

    public GameObject mr_MainCamera;

    public GameObject mr_SplashElements;

    public GameObject mr_Clouds;

    public GameObject mr_Buttons;

    public GameObject mr_GameLogo;

    public GameObject mr_Blinks;

    private DateTime lastBlinkTime;

    private int m_currentBlinkingMargin=0;

    public tk2dSpriteAnimator mr_Mimpy;

    private static readonly Vector3 m_cameraStartPosition = new Vector3(-1408, 0, 0);

    private static readonly Vector3 m_cameraEndPosition = new Vector3(373, 0, 0);

    private static readonly Vector3 m_elementsStartPosition = new Vector3(1408, 0, 0);

    private static readonly Vector3 m_elementsEndPosition = new Vector3(-373, 0, 0);

    private static readonly Vector3 m_LogoStartPosition = new Vector3(1085, 550, -3);

    private static readonly Vector3 m_LogoEndPosition = new Vector3(700, 170, -3);

    private Dictionary<eButtonType, AbstractButton> m_Buttons = new Dictionary<eButtonType, AbstractButton>();

    private Dictionary<eButtonType, Vector3> m_ButtonsStartPositions = new Dictionary<eButtonType, Vector3>()
                                                                           {
                                                                               {
                                                                                   eButtonType
                                                                                   .Play
                                                                                   ,
                                                                                   new Vector3
                                                                                   (
                                                                                   1080,
                                                                                   -29.5f,
                                                                                   -2)
                                                                               },
                                                                               {
                                                                                   eButtonType
                                                                                   .Sound
                                                                                   ,
                                                                                   new Vector3
                                                                                   (
                                                                                   606.5f,
                                                                                   -440,
                                                                                   -2)
                                                                               },
                                                                               {
                                                                                   eButtonType
                                                                                   .Rules
                                                                                   ,
                                                                                   new Vector3
                                                                                   (
                                                                                   865,
                                                                                   -435,
                                                                                   -2)
                                                                               },
                                                                               {
                                                                                   eButtonType
                                                                                   .Exit
                                                                                   ,
                                                                                   new Vector3
                                                                                   (
                                                                                   1062,
                                                                                   -430,
                                                                                   -2)
                                                                               }
                                                                           };




    private Dictionary<eButtonType, Vector3> m_ButtonsEndPositions = new Dictionary<eButtonType, Vector3>()
                                                                         {
                                                                             {
                                                                                 eButtonType
                                                                                 .Play
                                                                                 ,
                                                                                 new Vector3
                                                                                 (
                                                                                 722.5f,
                                                                                 -29.5f,
                                                                                 -1)
                                                                             },
                                                                             {
                                                                                 eButtonType
                                                                                 .Sound
                                                                                 ,
                                                                                 new Vector3
                                                                                 (
                                                                                 606.5f,
                                                                                 -221,
                                                                                 -2)
                                                                             },
                                                                             {
                                                                                 eButtonType
                                                                                 .Rules
                                                                                 ,
                                                                                 new Vector3
                                                                                 (
                                                                                 721.5f,
                                                                                 -221,
                                                                                 -2)
                                                                             },
                                                                             {
                                                                                 eButtonType
                                                                                 .Exit
                                                                                 ,
                                                                                 new Vector3
                                                                                 (
                                                                                 836.5f,
                                                                                 -221,
                                                                                 -2)
                                                                             }
                                                                         };


    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.Return))
        {
            this.EnterGameButtonClickedHandler();
        }

        if (Input.GetKeyDown(KeyCode.X))
        {
            this.PlaySplashAnimation();
        }

        this.PlayBlinkingAnimation();
    }

    public void EnterGameButtonClickedHandler()
    {
        if (SplashPlayButtonClicked != null)
        {
            SplashPlayButtonClicked();
        }
    }

    public void RulesButtonClickedHandler()
    {
        if (SplashRulesButtonClicked != null)
        {
            SplashRulesButtonClicked();
        }
    }

    public void ExitButtonClickedHandler()
    {
        if (SplashExitButtonClicked != null)
        {
            SplashExitButtonClicked();
        }
    }

    public void SoundButtonClickedHandler()
    {
        if (SplashSoundButtonClicked != null)
        {
            SplashSoundButtonClicked();
        }
    }

    public void PlaySplashAnimation()
    {
        this.StartCoroutine(this.SplashAnimationEnumerator());
    }


    private IEnumerator SplashAnimationEnumerator()
    {
        yield return this.StartCoroutine(this.Reset());
        Debug.Log("Generic Animator " + GenericAnimator.Instance);
        //yield return this.StartCoroutine(GenericAnimator.Instance.MoveLocal(this.mr_MainCamera, m_cameraStartPosition, m_cameraEndPosition, 4));
        yield return
            this.StartCoroutine(
                GenericAnimator.Instance.MoveLocal(
                    this.mr_SplashElements, m_elementsStartPosition, m_elementsEndPosition, 4));
        this.StartCoroutine(this.PlayCloudsAnimation());
        yield return this.StartCoroutine(this.showMimpy());
        yield return new WaitForSeconds(0.5f);
        this.StartCoroutine(this.playMimpyLoop());
        yield return this.StartCoroutine(this.showLogo());
        this.StartCoroutine(this.AnimateButtons());
       
    }


    private IEnumerator PlayCloudsAnimation()
    {
        int i = 1;
        foreach (Transform cloud in this.mr_Clouds.GetComponentsInChildren<Transform>())
        {
            this.StartCoroutine(
                GenericAnimator.Instance.MoveLocal(
                    cloud.gameObject,
                    cloud.transform.localPosition,
                    cloud.transform.localPosition + new Vector3(-450 * i, 0, 0),
                    3));
            i++;
        }
        yield break;
    }

    private IEnumerator Reset()
    {
        this.InitButtons();

        this.mr_MainCamera.transform.localPosition = m_cameraStartPosition;

        //this.StartCoroutine(GenericAnimator.Instance.Fade(this.mr_Buttons.gameObject, 1, 0, 0));
        this.mr_GameLogo.transform.localPosition = m_LogoStartPosition;
        yield return this.StartCoroutine(GenericAnimator.Instance.Fade(this.mr_GameLogo.gameObject, 1, 0, 0));
    }

    private IEnumerator showLogo()
    {
        this.mr_GameLogo.transform.localScale = new Vector3(2, 2, 1);
        yield return this.StartCoroutine(GenericAnimator.Instance.Fade(this.mr_GameLogo, 1, 1, 1));
        this.StartCoroutine(GenericAnimator.Instance.LocalScale(this.mr_GameLogo, new Vector3(2, 2, 1), Vector3.one, 1));
        yield return
            this.StartCoroutine(
                GenericAnimator.Instance.MoveLocal(this.mr_GameLogo, m_LogoStartPosition, m_LogoEndPosition, 1));
    }

    private IEnumerator showMimpy()
    {
        this.mr_Mimpy.gameObject.SetActive(true);
        this.mr_Mimpy.Play("Entrance");
        yield return new WaitForSeconds(mr_Mimpy.ClipTimeSeconds);
    }

    private IEnumerator playMimpyLoop()
    {
        this.mr_Mimpy.Play("Loop");
        yield break;
    }

    private void InitButtons()
    {
        foreach (var button in this.mr_Buttons.GetComponentsInChildren<AbstractButton>())
        {
            if (button.name == "Play")
            {
                this.m_Buttons.Add(eButtonType.Play, button);
            }
            if (button.name == "Sound")
            {
                this.m_Buttons.Add(eButtonType.Sound, button);
            }
            if (button.name == "Rules")
            {
                this.m_Buttons.Add(eButtonType.Rules, button);
            }
            if (button.name == "Exit")
            {
                this.m_Buttons.Add(eButtonType.Exit, button);
            }
        }

        foreach (KeyValuePair<eButtonType, AbstractButton> pair in m_Buttons)
        {

            pair.Value.transform.localPosition = m_ButtonsStartPositions[pair.Key];
        }
    }

    private IEnumerator AnimateButtons()
    {
        foreach (KeyValuePair<eButtonType, AbstractButton> pair in m_Buttons)
        {
            this.StartCoroutine(
                GenericAnimator.Instance.MoveLocal(
                    pair.Value.gameObject,
                    this.m_ButtonsStartPositions[pair.Key],
                    this.m_ButtonsEndPositions[pair.Key],
                    0.3f));
            yield return
                this.StartCoroutine(
                    GenericAnimator.Instance.LocalScale(pair.Value.gameObject, 2 * Vector3.one, Vector3.one, 0.3f));
        }


    }

    private void PlayBlinkingAnimation()
    {
        if ((DateTime.Now - this.lastBlinkTime).Seconds == this.m_currentBlinkingMargin)
        {
            this.StartCoroutine(this.Blink());
            Random random = new Random();
            this.m_currentBlinkingMargin = random.Next(1, 5);
        }
    }

    private IEnumerator Blink()
    {
        this.mr_Blinks.SetActive(true);
        yield return new WaitForSeconds(0.1f);
        this.mr_Blinks.SetActive(false);
    }

    
    public void EnableSoundButton()
    {
        this.m_Buttons[eButtonType.Sound].EnableButton();
    }

    public void DisableSoundButton()
    {
        this.m_Buttons[eButtonType.Sound].DisableButton();
    }
   

}
