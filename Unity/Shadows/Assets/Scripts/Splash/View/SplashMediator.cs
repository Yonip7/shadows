﻿using Assets.Scripts.View;
using Assets.Sound_Manager.model.events;

using ShadowsModel.Model;

using UnityEngine;

using strange.extensions.mediation.impl;
using Assets.Scripts.Splash.View;

public class SplashMediator : EventMediator
{
    [Inject]
    public SplashView Splash { get; set; }

    [Inject]
    public SplashPlaySignal SplashPlaySignal { get; set; }

    [Inject]
    public SplashExitSignal SplashExitSignal { get; set; }

    [Inject]
    public SplashRulesSignal SplashRulesSignal { get; set; }

    [Inject]
    public SplashSoundSignal SplashSoundSignal { get; set; }

    [Inject]
    public GameInitCompletedSignal GameInitCompletedSignal { get; set; }

    [Inject]
    public SoundMutedSignal SoundMutedSignal { get; set; }

    [Inject]
    public SoundUnMutedSignal SoundUnmutedSignal { get; set; }

    [Inject]
    public SoundsMuteSignalRequest soundsMuteSignalRequest { get; set; }


    public override void OnRegister()
    {
        this.listenToModelEvents();
        this.listenToViewEvents();
        this.Splash.PlaySplashAnimation();
    }

    private void listenToViewEvents()
    {
        this.Splash.SplashPlayButtonClicked += this.splashButtonClickedHandler;
        this.Splash.SplashSoundButtonClicked += this.splashSoundButtonClickedHandler;
        this.Splash.SplashRulesButtonClicked += this.splashRulesButtonClickedHandler;
        this.Splash.SplashExitButtonClicked += this.splashExitButtonClickedHandler;
    }

    private void splashExitButtonClickedHandler()
    {
        this.SplashExitSignal.Dispatch();
    }

    private void splashRulesButtonClickedHandler()
    {
        this.SplashRulesSignal.Dispatch();
    }

    private void splashSoundButtonClickedHandler()
    {
        this.SplashSoundSignal.Dispatch();
    }

    private void listenToModelEvents()
    {
        GameInitCompletedSignal.AddListener(this.onGameInitCompleted);
        SoundMutedSignal.AddListener(this.onSoundMuted);
        SoundUnmutedSignal.AddListener(this.onSoundUnmuted);

    }

    private void onSoundUnmuted()
    {
        this.Splash.EnableSoundButton();
    }

    private void onSoundMuted()
    {
        this.Splash.DisableSoundButton();
    }

    private void splashButtonClickedHandler()
    {
        Debug.Log("arrived at mediator");
        this.SplashPlaySignal.Dispatch();
        
    }

    private void onGameInitCompleted(LogicalBoard i_LogicalBoard)
    {
        Debug.Log("Turned off splash!!!");
        this.Splash.gameObject.SetActive(false);
    }
}
