﻿using System.Collections.Generic;

using UnityEngine;
using System.Collections;

public class RulesButton : MonoBehaviour
{
    #region Texts

        public enum eTexts
        {
            RulesButtonTooltip
        }

        public MLTextMesh mr_RulesTooltipText;


        private Dictionary<eTexts, string> m_texts;

        public void Init()
        {
            this.initTextsFromJSON();
            this.assignTexts();
        }


        private void initTextsFromJSON()
        {
            m_texts = new Dictionary<eTexts, string>();
            m_texts[eTexts.RulesButtonTooltip] = TextService.GetTextById("3.3.1.1");
        }

        private void assignTexts()
        {
            this.mr_RulesTooltipText.ChangeText(this.getTextByType(eTexts.RulesButtonTooltip));
        }

        private string getTextByType(eTexts i_TextType)
        {
            return m_texts[i_TextType];
        }

    #endregion
}
