﻿using System.Collections.Generic;

using UnityEngine;
using System.Collections;

public class SoundsEffectsButton : MonoBehaviour
{
    private ArenaToggleButton m_button;

    #region Texts

    public enum eTexts
    {
        SoundEffectsButtonTooltip
    }

    public MLTextMesh mr_SoundEffectsTooltipText;


    private Dictionary<eTexts, string> m_texts;

    public void Init()
    {
        this.initTextsFromJSON();
        this.assignTexts();
    }


    private void initTextsFromJSON()
    {
        m_texts = new Dictionary<eTexts, string>();
        m_texts[eTexts.SoundEffectsButtonTooltip] = TextService.GetTextById("3.3.1.2");
    }

    private void assignTexts()
    {
        this.mr_SoundEffectsTooltipText.ChangeText(this.getTextByType(eTexts.SoundEffectsButtonTooltip));
    }

    private string getTextByType(eTexts i_TextType)
    {
        return m_texts[i_TextType];
    }

    #endregion

    public void Awake()
    {
        m_button = this.GetComponent<ArenaToggleButton>();
        this.listenToButtonEvents();
    }

    private void listenToButtonEvents()
    {
        this.m_button.MouseUp += this.handleEffectsToggle;
    }

    private void handleEffectsToggle(ArenaToggleButton.eButtonState i_ButtonState)
    {
        if (i_ButtonState == ArenaToggleButton.eButtonState.On)
        {
            this.turnEffectsOn();
        }
        else
        {
            this.turnEffectsOff();
        }
    }

    private void turnEffectsOff()
	{
        SoundManager.StopAndRemoveAllSoundsByTag(SoundManagerStaticInfo.eTags.Effects);
	    SoundManager.SetIsMutedForTag(SoundManagerStaticInfo.eTags.Effects, true);
	}

    private void turnEffectsOn()
    {
        SoundManager.SetIsMutedForTag(SoundManagerStaticInfo.eTags.Effects, false);
    }
}
