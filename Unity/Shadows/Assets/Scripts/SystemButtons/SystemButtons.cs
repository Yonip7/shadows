﻿using UnityEngine;
using System.Collections;

public class SystemButtons : MonoBehaviour, IPauseable
{    
    public ExitButton mr_ExitButton;
    public SoundsEffectsButton mr_SoundsEffectsButton;
    public RulesButton mr_RulesButton;

    public GameObject mr_PauseScreen;

    public void Init()
    {     
        this.InitPauseScreen();
        this.initButtons();
        this.UnPause();
    }

    public void InitPauseScreen()
    {
        mr_PauseScreen.transform.localPosition = new Vector3(0, 0, -10);
    }

    public void Pause()
    {
        mr_PauseScreen.gameObject.SetActive(true);
    }

    public void UnPause()
    {
        mr_PauseScreen.gameObject.SetActive(false);
    }       

    private void initButtons()
    {
        mr_RulesButton.Init();
        mr_SoundsEffectsButton.Init();
        mr_ExitButton.Init();
    }
}
