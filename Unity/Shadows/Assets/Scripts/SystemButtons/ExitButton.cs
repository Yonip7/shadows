﻿using System.Collections.Generic;

using UnityEngine;
using System.Collections;

public class ExitButton : MonoBehaviour 
{
    #region Texts

        public enum eTexts
        {
            ExitButtonTooltip
        }

        public MLTextMesh mr_ExitTooltipText;
    

        private Dictionary<eTexts, string> m_texts;

        public void Init()
        {
            this.initTextsFromJSON();
            this.assignTexts();
        }
    

        private void initTextsFromJSON()
        {
            m_texts = new Dictionary<eTexts, string>();
            m_texts[eTexts.ExitButtonTooltip] = TextService.GetTextById("3.3.1.3");
        }

        private void assignTexts()
        {
            mr_ExitTooltipText.ChangeText(this.getTextByType(eTexts.ExitButtonTooltip));
        }

        private string getTextByType(eTexts i_TextType)
        {
            return m_texts[i_TextType];
        }

    #endregion

}
