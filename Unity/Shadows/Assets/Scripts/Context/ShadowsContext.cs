﻿using Assets.Scripts.Splash.View;
using Assets.Scripts.View;
using Assets.Sound_Manager;
using Assets.Sound_Manager.commands;
using Assets.Sound_Manager.model.events;
using Assets.Sound_Manager.view.events;

using ShadowsModel.Model;

using UnityEngine;

using strange.extensions.command.api;
using strange.extensions.command.impl;
using strange.extensions.context.impl;
//using Assets.Scripts.Context

public class ShadowsContext : MVCSContext {

    public ShadowsContext(MonoBehaviour view)
        : base(view, true)
    {
        
    }

    protected override void mapBindings()
    {
        this.createModelsBindings();
        this.createCommandBindings();
        this.createMediationBindings();
    }

    private void createModelsBindings()
    {
        this.createSignalBindings();
        injectionBinder.Bind<ApplicationModel>().ToSingleton();
        injectionBinder.Bind<IShadowsGameModel>().To<ShadowsGameModel>().ToSingleton();
        injectionBinder.Bind<ShadowsLevelsModel>().ToSingleton();
        injectionBinder.Bind<SoundsModel>().ToSingleton();
    }

    private void createSignalBindings()
    {
        //Game
        injectionBinder.Bind<GameInitCompletedSignal>().ToSingleton();
        injectionBinder.Bind<GameLevelLoadedSignal>().ToSingleton();
        injectionBinder.Bind<GameMovePerformedInModelSignal>().ToSingleton();
        injectionBinder.Bind<MoveToGameSignal>().ToSingleton();
        injectionBinder.Bind<GameResetClickedSignal>().ToSingleton();
        injectionBinder.Bind<GameRestartedInModelSignal>().ToSingleton();

        //Levels
        injectionBinder.Bind<LevelsInitCompletedSignal>().ToSingleton();
        //injectionBinder.Bind<LevelSelectedSignal>().ToSingleton();
        injectionBinder.Bind<MoveToLevelsScreenSignal>().ToSingleton();

        //Success Popup
        injectionBinder.Bind<MoveToSuccessPopupSignal>().ToSingleton();

        //injectionBinder.Bind<SuccessNextButtonWasClickedSignal>().ToSingleton();
        injectionBinder.Bind<SuccessLevelsButtonWasClickedSignal>().ToSingleton();
        //injectionBinder.Bind<SuccessRestartButtonWasClickedSignal>().ToSingleton();

        injectionBinder.Bind<PlaySoundRequest>().ToSingleton();
        injectionBinder.Bind<SoundMutedSignal>().ToSingleton();
        injectionBinder.Bind<SoundUnMutedSignal>().ToSingleton();
       
    }

    private void createCommandBindings()
    {
        //Init
        commandBinder.Bind<SplashPlaySignal>().To<SplashPlayCommand>();
        commandBinder.Bind<SplashRulesSignal>().To<SplashRulesCommand>();
        commandBinder.Bind<SplashSoundSignal>().To<SplashSoundCommand>();
        commandBinder.Bind<SplashExitSignal>().To<SplashExitCommand>();
        commandBinder.Bind<StartApplicationInitSignal>().To<GameInitCommand>();

        //Non Relevant Command
        //commandBinder.Bind<GameInitCompletedSignal>().To<GameLoadLevelCommand>();

        //Levels
        commandBinder.Bind<LevelsInitCompletedSignal>().To<MoveToLevelsScreenCommand>();

        commandBinder.Bind<SuccessLevelsButtonWasClickedSignal>().To<MoveToLevelsScreenCommand>();
        commandBinder.Bind<LevelSelectedSignal>().To<MoveToLevelCommand>();

        //Game 
        commandBinder.Bind<GamePerformMoveSignal>().To<GamePerformMoveCommand>();
        commandBinder.Bind<GameMovePerformedInViewSignal>().To<GameMovePeformedInViewCommand>();
        commandBinder.Bind<SuccessRestartButtonWasClickedSignal>().To<GameRestartCurrentLevelCommand>();
        commandBinder.Bind<SuccessNextButtonWasClickedSignal>().To<GameMoveToNextLevelCommand>();
        commandBinder.Bind<GameResetClickedSignal>().To<GameRestartLevelCommand>();

        //Test Win
        commandBinder.Bind<GameWinTestSignal>().To<GameWinTestCommand>();

        // sounds
        commandBinder.Bind<SoundsMuteSignalRequest>().To<SoundsMuteRequestCommand>();
    }

    private void createMediationBindings()
    {
        mediationBinder.Bind<PopupManager>().To<PopupManagerViewMediator>();
        mediationBinder.Bind<SplashView>().To<SplashMediator>();
        mediationBinder.Bind<LevelsView>().To<LevelsViewMediator>();
        mediationBinder.Bind<GameView>().To<GameViewMediator>();
        mediationBinder.Bind<SuccessPopup>().To<SuccessPopupViewMediator>();
    }

    protected override void addCoreComponents()
    {
        base.addCoreComponents();
        injectionBinder.Unbind<ICommandBinder>();
        injectionBinder.Bind<ICommandBinder>().To<SignalCommandBinder>().ToSingleton();
    }
}
