﻿using UnityEngine;

using strange.extensions.command.impl;

public class GameRestartLevelCommand : Command
{
    [Inject]
    public IShadowsGameModel GameModel { get; set; }

    public override void Execute ()
    {
        Debug.Log("Exeucting restart.");
        this.GameModel.RestartLevel();
    }
}