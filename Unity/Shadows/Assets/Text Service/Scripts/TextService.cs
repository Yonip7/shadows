﻿using System;
using System.IO;

using Newtonsoft.Json;

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TextService : MonoBehaviour
{
    public static TextService Instance { get; private set; }

    private static IDictionary<string, string> s_texts;

    #region Unity Methods
    
    public void Awake ()
    {
        this.setInstance();
    }

    public void Start ()
    {
        this.setInstance();
        
        // HACK
        if (Application.isEditor)
        {
            //this.StartCoroutine(this.testMethod());
        }
    }

    #endregion

    
    #region API Methods

    private IEnumerator parseAndSaveEnumerator(IDictionary<string, string> i_Dictionary, WWW www)
    {
        this.parseAndSave(i_Dictionary, www);
        yield break;
    }

    private void parseAndSave(IDictionary<string, string> i_Dictionary, WWW www)
    {
        i_Dictionary = JsonConvert.DeserializeObject<Dictionary<string, string>>(www.text);

        if (i_Dictionary == null)
        {
            Debug.LogError("JSON parser failed reading " + www.text);
        }
        else
        {
            Debug.Log("Finished parsing text dictionary. Saving " + i_Dictionary.Count + " entries.");
            //s_texts = i_Dictionary;
            foreach (KeyValuePair<string, string> keyValuePair in i_Dictionary)
            {
                s_texts.Add(keyValuePair.Key, keyValuePair.Value);
            }
        }
    }

    /// <summary>
    /// Parses a json file containing texts. Overload that fixes bug in previous implemetation.
    /// </summary>
    /// <param name="i_JsonFileName">Name of the json file.</param>
    public IEnumerator ParseJson(string i_JsonFileName)
    {
        var resource = Resources.Load(i_JsonFileName);
        yield return resource;

        TextAsset fileAsTextAsset = resource as TextAsset;
        string fileContent = fileAsTextAsset.text;

        Dictionary<string, string> newStrings =
            JsonConvert.DeserializeObject<Dictionary<string, string>>(fileContent);

        if (newStrings == null)
        {
            Debug.LogError("JSON parser failed reading " + i_JsonFileName);
        }
        else
        {
            Debug.Log("Finished parsing text dictionary. Saving " + newStrings.Count + " entries.");
            if (s_texts == null)
            {
                s_texts = new Dictionary<string, string>();
            }

            foreach (KeyValuePair<string, string> keyValuePair in newStrings)
            {
                if (!s_texts.ContainsKey(keyValuePair.Key))
                {
                    s_texts.Add(keyValuePair.Key, keyValuePair.Value);
                }
            }
        }
    }
    
    /// <summary>
    /// Generates a json from project text meshes.
    /// Important: Overwrites existing file.
    /// Note: Still in BETA
    /// </summary>
    /// <param name="i_JsonFileName">Name of the json file to create. Note: It will be overwritten if exists.</param>
    public IEnumerator GenerateJsonFromProjectTextMeshes(string i_JsonFileName)
    {

        IDictionary<string, string> dictionaryToWrite = new Dictionary<string, string>();
        yield return this.StartCoroutine(this.populateDictionaryFromProjectTextMeshes(dictionaryToWrite));

        if (dictionaryToWrite.Count > 0)
        {
            string json = null;
            string path = null;

            try
            {
                // HACK Check this when building for web:
                path = Application.streamingAssetsPath + "/" + i_JsonFileName;

                Debug.Log("Serializing JSON...");
                json = JsonConvert.SerializeObject(dictionaryToWrite);

            }
            catch (Exception e)
            {
                Debug.LogError("Exception while serializing JSON: " + e.Message);
            }


            try
            {
                Debug.Log("Writing this JSON to file:\n" + json);
                if (path != null)
                {
                    StreamWriter writer = new StreamWriter(path);
                    writer.Write(json);
                    writer.Close();
                }
                Debug.Log("JSON written to file at: " + path);
            }
            catch (Exception e)
            {
                Debug.LogError("Exception while writing JSON file: " + e.Message);

            }
        }
    }

    /// <summary>
    /// Gets the text by identifier.
    /// </summary>
    /// <param name="i_Id">The identifier.</param>
    /// <returns>string</returns>
    public static string GetTextById ( string i_Id )
    {
        string result = null;

        if (s_texts != null && s_texts.ContainsKey(i_Id))
        {
            result = s_texts[i_Id];
        }
        else
        {
            Debug.LogError("Text not found for id " + i_Id);
        }
        return result;
    }

    #endregion


    #region Private and auxiliary methods

    /// <summary>
    /// Sets the instance.
    /// </summary>
    private void setInstance ()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Debug.LogWarning("Second instantiation of " + this.GetType() + " blocked.");
        }
    }

    /// <summary>
    /// Populates the dictionary from project text meshes.
    /// </summary>
    /// <returns>A Dictionary</returns>
    private IEnumerator populateDictionaryFromProjectTextMeshes(IDictionary<string, string> i_ResultDictionary)
    {
        Debug.Log("Creating texts dictionary...");
        

        MLTextMesh[] textMeshes = FindObjectsOfType(typeof(MLTextMesh)) as MLTextMesh[];

        if (textMeshes != null)
        {
            Debug.Log("JSON service found " + textMeshes.Length + " texts in the project.");
            for (int index = 0; index < textMeshes.Length; index++)
            {
                //Set ID
                if (textMeshes[index].mr_TextID == null || textMeshes[index].mr_TextID.Equals(string.Empty)) textMeshes[index].mr_TextID = index.ToString();

                // Add textmesh to dictionary for later use.
                i_ResultDictionary.Add(textMeshes[index].mr_TextID, textMeshes[index].Text);
            }
        }
        Debug.Log("Texts dictionary created.");
        yield break;
    }

    #endregion
}