﻿using UnityEngine;
using System.Collections;

public class PopupManager : PopupManagerAbstract
{    
    public void Awake()
    {
        this.StartCoroutine(this.InitializeEnumerator());
        this.UnPause();
    }

    protected override IEnumerator AnimatePopupIn(GameObject i_Popup)
    {
        yield break;
    }

    protected override IEnumerator AnimatePopupOut(GameObject i_Popup)
    {
        //todo
        yield break;
    }
}
