﻿using UnityEngine;
using System.Collections;

public interface IPopupScreenShowable
{
    IEnumerator ShowEnumerator();
}
