﻿using UnityEngine;
using System.Collections;

public interface IPopupScreenHideable
{
    IEnumerator HideEnumerator();
}
