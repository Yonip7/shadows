﻿public interface IPauseable
{
    void InitPauseScreen();

    void Pause();

    void UnPause();
}
