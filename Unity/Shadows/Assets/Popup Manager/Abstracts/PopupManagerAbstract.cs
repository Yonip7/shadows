﻿using System;
using System.Collections.Generic;
using UnityEngine;
using System.Collections;
using strange.extensions.mediation.impl;

public abstract class PopupManagerAbstract : View, IPauseable
{
    public const float k_FadeInOutLength = 0.7f;

    private const int k_depthOfPause = 40;

    public GameObject mr_PopupScreens;

    public Transform mr_PopupScreensPause;

    public GameObject mr_Overlay;

    protected Stack<GameObject> m_ActivePopupsStack;

    private int k_multiplierBetweenPopups = 10;

    public IEnumerator InitializeEnumerator()
    {
        this.InitPauseScreen();
        this.UnPause();
        this.changeOverlayState(false);

        this.m_ActivePopupsStack = new Stack<GameObject>();
        foreach (Transform screen in this.mr_PopupScreens.transform)
        {
            screen.gameObject.SetActive(false);
        }
        yield break;
    }

    public IEnumerator HideLastPopupEnumrator()
    {
        this.mr_PopupScreensPause.Translate(0, 0, -k_depthOfPause);

        GameObject popupScreenObject;
        try
        {
            popupScreenObject = this.m_ActivePopupsStack.Pop();
        }
        catch (Exception)
        {

            popupScreenObject = null;
        }

        if (popupScreenObject != null)
        {

            PopupScreenAbstract popupScreen = popupScreenObject.GetComponent<PopupScreenAbstract>();

            if (popupScreen != null && popupScreen is IPopupScreenHideable)
            {
                yield return StartCoroutine((popupScreen as IPopupScreenHideable).HideEnumerator());
            }

            if (popupScreen != null)
            {

                yield return this.StartCoroutine(this.AnimatePopupOut(popupScreenObject));

                popupScreenObject.SetActive(false);
            }
        }
        this.mr_PopupScreensPause.Translate(0, 0, k_depthOfPause);
        this.changeOverlayState(false);
        this.UnPause();
    }

    protected abstract IEnumerator AnimatePopupIn(GameObject i_Popup);

    protected abstract IEnumerator AnimatePopupOut(GameObject i_Popup);

    public IEnumerator ShowPopUpEnumerator(string i_WantedPopupScreen, object i_Args = null)
    {
        this.Pause();
        this.changeOverlayState(true);

        GameObject popupScreenObject = this.mr_PopupScreens.transform.FindChild(i_WantedPopupScreen).gameObject;
        PopupScreenAbstract popupScreen = popupScreenObject.GetComponent<PopupScreenAbstract>();

        this.m_ActivePopupsStack.Push(popupScreenObject);
        popupScreenObject.transform.localPosition = new Vector3(
            popupScreenObject.transform.localPosition.x,
            popupScreenObject.transform.localPosition.y,
            this.m_ActivePopupsStack.Count * (- k_multiplierBetweenPopups));

        if (popupScreen != null)
        {
            popupScreen.ResetPopup(i_Args);
        }

        yield return this.StartCoroutine(this.AnimatePopupIn(popupScreenObject));

        if (popupScreen != null && popupScreen is IPopupScreenShowable)
        {
            popupScreen.gameObject.SetActive(true);
            yield return StartCoroutine((popupScreen as IPopupScreenShowable).ShowEnumerator());
        }

        if (popupScreen is SystemPopUpAbstract)
        {
            //this.mr_PopupScreensPause.localPosition = new Vector3(0, 0, -1);
            this.Pause();
            yield break;
        }
        else
        {
            //this.mr_PopupScreensPause.localPosition = new Vector3(0, 0, 0);
            this.UnPause();
        }

        popupScreenObject.SetActive(true);
    }

    public void InitPauseScreen()
    {
        this.mr_PopupScreensPause.localPosition = new Vector3(0, 0, -50);
    }

    public void Pause()
    {
        this.mr_PopupScreensPause.gameObject.SetActive(true);
    }

    public void UnPause()
    {
        this.mr_PopupScreensPause.gameObject.SetActive(false);
    }


    public bool IsActivePopUpExisits()
    {
        Debug.Log("Stack size now: " + this.m_ActivePopupsStack.Count);
        return this.m_ActivePopupsStack.Count > 0;
    }

    public void AddPopupScreen(PopupScreenAbstract i_PopupScreen)
    {
        i_PopupScreen.transform.parent = mr_PopupScreens.transform;
        i_PopupScreen.transform.localPosition = new Vector3();
    }

    private void changeOverlayState(bool i_OverlayState)
    {
        mr_Overlay.gameObject.SetActive(i_OverlayState);
    }
}
