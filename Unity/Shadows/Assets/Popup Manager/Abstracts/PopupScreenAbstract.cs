﻿using strange.extensions.mediation.impl;

public abstract class PopupScreenAbstract : View
{
    public abstract void ResetPopup(object i_InitData = null);
}
