﻿using UnityEngine;
using System.Collections;

public abstract class SystemPopUpAbstract : PopupScreenAbstract , IPopupScreenHideable, IPopupScreenShowable
{
    private const float k_contentFadeAnimationDuration = 0.5F;

    private const float k_overlayFadeAnimationDuration = 0.25F;

    public MLTextMesh mr_Body;

    public MLTextMesh mr_DeclineButtonText;

    public MLTextMesh mr_ConfirmButtonText;

    public GameObject mr_OverlayToFade;

    public GameObject mr_ContentToFade;


    public IEnumerator HideEnumerator()
    {
        yield return StartCoroutine(GenericAnimator.Instance.Fade(this.mr_ContentToFade, 1, 0, k_contentFadeAnimationDuration));
        yield return StartCoroutine(GenericAnimator.Instance.Fade(this.mr_OverlayToFade, 1, 0, k_overlayFadeAnimationDuration));
    }

    public IEnumerator ShowEnumerator()
    {
        StartCoroutine(GenericAnimator.Instance.Fade(this.mr_ContentToFade, 0, 0, 0));
        yield return StartCoroutine(GenericAnimator.Instance.Fade(this.mr_OverlayToFade, 0, 1, k_overlayFadeAnimationDuration));
        yield return StartCoroutine(GenericAnimator.Instance.Fade(this.mr_ContentToFade, 0, 1, k_contentFadeAnimationDuration));

    }
}
