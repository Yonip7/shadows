﻿using Assets.Scripts.strange.extensions;

namespace Assets.Sound_Manager
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    using Assets.Sound_Manager.model.events;

    using Newtonsoft.Json;

    using UnityEngine;

    public class SoundsModel : Model
    {
        [Inject]
        public SoundsMuted m_soundMuted { get; set; }

        [Inject]
        public SoundsMuted m_soundUnmuted { get; set; }

        private bool m_isMuted;
        public bool isMuted
        {
            get
            {
                return m_isMuted;
            }
            set
            {
                if (m_isMuted != value)
                {
                    m_isMuted = value;
                    if (m_isMuted)
                    {
                        m_soundMuted.Dispatch();  
                    }
                    else
                    {
                        m_soundUnmuted.Dispatch();
                    }
                }
            }
        }

        public void Mute()
        {
            this.isMuted = true;
        }

        public void Unmute()
        {
            this.isMuted = false;
        }

       
    }
}
