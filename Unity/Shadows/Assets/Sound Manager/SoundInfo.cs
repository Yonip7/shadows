﻿
public class SoundInfo
{
    public string SoundId { get; set; }

    public string SoundFileName { get; set; }

    public float Volume { get; set; }

    public float Pitch { get; set; }
}
