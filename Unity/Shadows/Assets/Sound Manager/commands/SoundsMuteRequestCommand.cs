﻿using strange.extensions.command.impl;

namespace Assets.Sound_Manager.commands
{

    public class SoundsMuteRequestCommand : Command
    {
        [Inject]
        public SoundsModel soundsModel { get; set; }

        public override void Execute()
        {
            base.Execute();
            soundsModel.Mute();
        }
    }
}
