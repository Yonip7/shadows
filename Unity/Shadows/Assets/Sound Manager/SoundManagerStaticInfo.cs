﻿
public class SoundManagerStaticInfo
{
    public enum eSoundIds
    {
        // General
        SplashAndGameMusic = 0,
        SuccessMusic = 14,

        //Game
        DoorOpen = 1,
        DoorClose = 2,
        MoveCounters = 3,
        MovesDigitsSwitch = 4,
        MovesDigitsSwitchOverOptimal = 5,
        FurnitureUp1 = 6,
        FurnitureUp2 = 7,
        FurnitureUp3 = 8,
        FurnitureUp4 = 9,
        PedroUp1 = 10,
        PedroUp2 = 11,
        PedroUp3 = 12,
        PedroUp4 = 13,
        ButtonClick = 15,
        ButtonOver = 16,
        HintBoardPieceGlow = 17,
        HintBoardPieceMove = 18,
        HintButtonGlow = 19,
        Restart = 20,
        MessageIn = 21,
        MessageOut = 22,
        OnTheRightTrack = 23,
        TransitionStar = 24
    }

    public enum eTags
    {
        Music,
        Effects
    }
}
