﻿using strange.extensions.mediation.impl;
using Assets.Sound_Manager.view.events;

namespace Assets.Sound_Manager
{
    using Assets.Sound_Manager.model.events;

    public class SoundsManagerMediator : Mediator
    {
        [Inject]
        public SoundManager m_soundManager { get; set; }

        [Inject]
        public PlaySoundRequest m_playSoundRequest { get; set; }

        [Inject]
        public SoundsMuted SoundsMuted { get; set; }

        public override void OnRegister()
        {
            base.OnRegister();
            m_playSoundRequest.AddListener(onPlaySoundRequest);
            SoundsMuted.AddListener(onSoundsMuted);
        }

        private void onSoundsMuted()
        {
            
        }

        private void onPlaySoundRequest(SoundManagerStaticInfo.eSoundIds i_Id, SoundManagerStaticInfo.eTags i_Tag, bool i_IsLoop)
        {
            m_soundManager.PlaySound(i_Id, i_Tag, i_IsLoop);
        }
    }
}
