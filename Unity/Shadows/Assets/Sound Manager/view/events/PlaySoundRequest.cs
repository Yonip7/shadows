﻿using strange.extensions.signal.impl;

namespace Assets.Sound_Manager.view.events
{
    public class PlaySoundRequest : Signal<SoundManagerStaticInfo.eSoundIds, SoundManagerStaticInfo.eTags, bool>
    {
    }
}
