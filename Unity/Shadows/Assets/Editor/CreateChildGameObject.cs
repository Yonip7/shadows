using UnityEngine;
using UnityEditor;

//[RequireComponent (typeof(tk2dSprite))]
public class CreateChildGameObject :MonoBehaviour {
 
    [MenuItem("GameObject/Add Child GameObject %# m")]
    public static void AddChildGameObject ()
    {
        GameObject newChildGameObject = new GameObject("Child GameObject");
        newChildGameObject.transform.parent = Selection.activeTransform;
        newChildGameObject.transform.localPosition = Vector3.zero;
    }

    [MenuItem("GameObject/Add Child GameObject", true)]
    public static bool CheckAddChildGameObjectStatus ()
    {
        return Selection.activeTransform != null;
    }

    [MenuItem("GameObject/Add Child GameObject with Sprite %# k")]
    public static void AddChildWithSimpleSprite ()
    {
        GameObject newChildGameObject = new GameObject("Child GameObject + Sprite");
        newChildGameObject.transform.parent = Selection.activeTransform;
        newChildGameObject.transform.localPosition = Vector3.zero;
        newChildGameObject.AddComponent<tk2dSprite>();
    }
    
    [MenuItem("GameObject/Add Child GameObject with Sprite", true)]
    public static bool CheckAddChildWithSpriteStatus ()
    {
        return Selection.activeTransform != null;
    }

    [MenuItem("GameObject/Add Child GameObject with Button %# l")]
    public static void AddChildWithButton()
    {
        GameObject newChildGameObject = new GameObject("Child GameObject + Button");
        newChildGameObject.transform.parent = Selection.activeTransform;
        newChildGameObject.transform.localPosition = Vector3.zero;
        newChildGameObject.AddComponent<UIButton>();
        newChildGameObject.GetComponent<UIButton>().doNotTrimImages = true;
        newChildGameObject.GetComponent<UIButton>().anchor = SpriteRoot.ANCHOR_METHOD.UPPER_LEFT;
    }

    [MenuItem("GameObject/Add Child GameObject with Button", true)]
    public static bool CheckAddChildWithButtonStatus()
    {
        return Selection.activeTransform != null;
    }
}
