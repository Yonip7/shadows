﻿using UnityEngine;
using System.Collections;

public class ArenaButtonsTooltipController : MonoBehaviour
{
   public MLTextMesh mr_TooltipText;
   public SpriteRenderer mr_TooltipPic;

   public void Awake()
   {
       this.HideTooltip();
   }

    public void InitText()
    {
        this.mr_TooltipText.ChangeText(TextService.GetTextById(mr_TooltipText.mr_TextID));
    }

    public void ShowTooltip()
   {
       mr_TooltipText.gameObject.SetActive(true);
       mr_TooltipPic.gameObject.SetActive(true);
   }

   public void HideTooltip()
   {
       mr_TooltipText.gameObject.SetActive(false);
       mr_TooltipPic.gameObject.SetActive(false);
   }

   public void ChangeText(string i_GivenText)
   {
       mr_TooltipText.ChangeText(i_GivenText);
   }
}
