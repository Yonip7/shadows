﻿using System.Collections.Generic;

using UnityEngine;
using System.Collections;

public class ArenaRadioButton : MonoBehaviour
{
    public enum eButtonState
    {
        Normal,
        Pressed,
        Disabled
    }

    public enum eButtonPivots
    {
        Center = 0,
        TopLeft,
        Top,
        TopRight,
        Left,
        Right,
        BotLeft,
        Bot,
        BotRight
    }

    public eButtonPivots mr_ButtonPivot;

    public delegate void ButtonChangedStateEventExecuted(eButtonState i_ButtonState);
    public event ButtonChangedStateEventExecuted ButtonChangedStateEvent;

    public SpriteRenderer mr_NormalState;

    public SpriteRenderer mr_OverState;

    public SpriteRenderer mr_PressedState;

    public SpriteRenderer mr_DisabledState;

    private eButtonState m_buttonState;

    public List<ArenaRadioButton> mr_ListOfButtonsToNotify;

    public ArenaButtonsTooltipController mr_Tooltip;

    public bool m_IsRadioButtonActive;

    public bool mr_IsTooltipActive;

    public bool mr_IsButtonDisabled;

    #region Method invocation

        //Method invocation
        public GameObject mr_ScriptContainingMethodToInvoke;
        public string mr_MethodNameToInvoke;

    #endregion

    private bool m_isButtonDisabled;

    private const int k_zValueOfButtons = -10;

    private bool m_isPressed;

    public void Start()
    {
        m_isPressed = false;
        this.setButtonInitialState();
        this.initButtonBoxCollider();
        //this.setButtonToCorrectState();
    }

    private void setButtonInitialState()
    {
        if (mr_IsButtonDisabled)
        {
            if (mr_DisabledState != null)
            {
                m_isButtonDisabled = true;
                m_buttonState = eButtonState.Disabled;
                this.ChangeButtonToGivenState(eButtonState.Disabled);
            }
            else
            {
                m_buttonState = eButtonState.Normal;
                this.ChangeButtonToGivenState(eButtonState.Normal);
                this.initRadioButtonSettings();
            }
        }
        else
        {
            m_buttonState = eButtonState.Normal;
            this.ChangeButtonToGivenState(eButtonState.Normal);
            this.initRadioButtonSettings();
        }
    }

    private void initButtonBoxCollider()
    {
        float widthOfSprite = mr_NormalState.sprite.rect.xMax;
        float heightOfSprite = mr_NormalState.sprite.rect.yMax;
        BoxCollider buttonBoxCollider =
            this.gameObject.AddComponent<BoxCollider>();
        buttonBoxCollider.size =
            new Vector3(widthOfSprite, heightOfSprite, k_zValueOfButtons);
        this.adjustColliderAccordingToSpriteOffset(buttonBoxCollider);
    }

    private void adjustColliderAccordingToSpriteOffset(BoxCollider i_ButtonBoxCollider)
    {
        float basicAdjustmentDeltaInWidth = mr_NormalState.sprite.rect.xMax / 2;
        float basicAdjustmentDeltaInHeight = mr_NormalState.sprite.rect.yMax / 2;

        float totalAdjustmentDeltaInWidth = 0;
        float totalAdjustmentDeltaInHeight = 0;

        switch (mr_ButtonPivot)
        {
            case eButtonPivots.Center:
                break;

            case eButtonPivots.TopLeft:
                totalAdjustmentDeltaInWidth = basicAdjustmentDeltaInWidth;
                totalAdjustmentDeltaInHeight = -basicAdjustmentDeltaInHeight;
                break;
        }

        i_ButtonBoxCollider.center =
            new Vector3(totalAdjustmentDeltaInWidth, totalAdjustmentDeltaInHeight, 0);
    }

    private void initRadioButtonSettings()
    {
        if (m_IsRadioButtonActive)
        {
            m_buttonState = eButtonState.Pressed;
            this.ChangeButtonToGivenState(eButtonState.Pressed);
        }
        else
        {
            m_buttonState = eButtonState.Normal;
            this.ChangeButtonToGivenState(eButtonState.Normal);
        }
    }

    public void ToggleButtonStateChange()
    {
        if (m_buttonState == eButtonState.Normal)
        {
            this.ChangeButtonToGivenState(eButtonState.Pressed);
            m_buttonState = eButtonState.Pressed;
        }
        else
        {
            if (m_buttonState == eButtonState.Pressed)
            {
                this.ChangeButtonToGivenState(eButtonState.Normal);
                m_buttonState = eButtonState.Normal;
            }
        }
    }

    public void OnMouseDown()
    {
        m_isPressed = true;
    }

    public void OnMouseUp()
    {
        if (!m_isButtonDisabled && m_isPressed)
        {
            if (m_buttonState == eButtonState.Normal)
            {
                //this.m_IsRadioButtonActive = !this.m_IsRadioButtonActive;
                this.ToggleButtonStateChange();
                this.notifyButtonsOfStateChange();

                if (ButtonChangedStateEvent != null)
                {
                    ButtonChangedStateEvent(this.m_buttonState);
                }

                this.invokeGivenMethod();
            }

            if (mr_IsTooltipActive)
            {
                mr_Tooltip.HideTooltip();
            }
        }
        m_isPressed = false;
    }

    public void OnMouseEnter()
    {
        if (!m_isButtonDisabled && !this.mr_PressedState.gameObject.activeInHierarchy)
        {
            if (mr_OverState != null)
            {
                mr_OverState.gameObject.SetActive(true);
                this.mr_NormalState.gameObject.SetActive(false);
                this.mr_PressedState.gameObject.SetActive(false);
            }

            if (mr_DisabledState != null)
            {
                mr_DisabledState.gameObject.SetActive(false);
            }

            if (mr_IsTooltipActive)
            {
                mr_Tooltip.ShowTooltip();
            }
        }
    }

    public void OnMouseExit()
    {
        if (!m_isButtonDisabled && !this.mr_PressedState.gameObject.activeInHierarchy)
        {
            if (mr_OverState != null)
            {
                mr_OverState.gameObject.SetActive(false);
            }

            if (mr_DisabledState != null)
            {
                mr_DisabledState.gameObject.SetActive(false);
            }

            if (mr_IsTooltipActive)
            {
                mr_Tooltip.HideTooltip();
            }

            this.setButtonToCorrectState();
        }
        m_isPressed = false;
    }

    private void setButtonToCorrectState()
    {
        if (m_buttonState == eButtonState.Normal)
        {
            this.ChangeButtonToGivenState(eButtonState.Normal);
        }

        if (m_buttonState == eButtonState.Pressed)
        {
            this.ChangeButtonToGivenState(eButtonState.Pressed);
        }
    }

    public void DisableButton()
    {
        if (mr_DisabledState != null)
        {
            if (mr_DisabledState != null)
            {
                mr_DisabledState.gameObject.SetActive(true);
            }

            this.mr_NormalState.gameObject.SetActive(false);

            if (mr_OverState != null)
            {
                mr_OverState.gameObject.SetActive(false);
            }

            this.mr_PressedState.gameObject.SetActive(false);
            this.m_isButtonDisabled = true;
        }
    }

    public void EnableButton()
    {
        if (mr_DisabledState != null)
        {
            this.mr_NormalState.gameObject.SetActive(true);
            this.mr_PressedState.gameObject.SetActive(false);

            if (mr_OverState != null)
            {
                mr_OverState.gameObject.SetActive(false);
            }

            if (mr_DisabledState != null)
            {
                mr_DisabledState.gameObject.SetActive(false);
            }

            this.m_isButtonDisabled = false;
        }
    }

    public void ChangeButtonToGivenState(eButtonState i_ButtonState)
    {
        switch (i_ButtonState)
        {
            case eButtonState.Normal:
                this.mr_NormalState.gameObject.SetActive(true);
                this.mr_PressedState.gameObject.SetActive(false);

                if (mr_OverState != null)
                {
                    mr_OverState.gameObject.SetActive(false);
                }

                if (mr_DisabledState != null)
                {
                    mr_DisabledState.gameObject.SetActive(false);
                }

                this.m_buttonState = eButtonState.Normal;

                break;

            case eButtonState.Pressed:
                this.mr_PressedState.gameObject.SetActive(true);
                this.mr_NormalState.gameObject.SetActive(false);

                if (mr_OverState != null)
                {
                    mr_OverState.gameObject.SetActive(false);
                }

                if (mr_DisabledState != null)
                {
                    mr_DisabledState.gameObject.SetActive(false);
                }

                this.m_buttonState = eButtonState.Pressed;

                break;

            case eButtonState.Disabled:

                if (mr_DisabledState != null)
                {
                    mr_DisabledState.gameObject.SetActive(true);
                    this.m_buttonState = eButtonState.Disabled;
                }

                if (mr_OverState != null)
                {
                    mr_OverState.gameObject.SetActive(false);
                }

                this.mr_NormalState.gameObject.SetActive(false);
                this.mr_PressedState.gameObject.SetActive(false);
                break;
        }
    }

    private void notifyButtonsOfStateChange()
    {
        if (mr_ListOfButtonsToNotify != null)
        {
            foreach (ArenaRadioButton button in mr_ListOfButtonsToNotify)
            {
                button.m_IsRadioButtonActive = true;
                button.ChangeButtonToGivenState(eButtonState.Normal);
            }
            this.m_IsRadioButtonActive = false;
        }
    }

    private void invokeGivenMethod()
    {
        if (!mr_MethodNameToInvoke.Equals("") && mr_ScriptContainingMethodToInvoke != null)
        {
            MonoBehaviour scriptContainingMethodToInvoke =
                mr_ScriptContainingMethodToInvoke.GetComponent<MonoBehaviour>();

            scriptContainingMethodToInvoke.Invoke(mr_MethodNameToInvoke, 0);
        }
        else
        {
            if (!mr_MethodNameToInvoke.Equals(""))
            {
                Debug.LogError("No such script or method exist!!!");
            }
        }
    }
}
