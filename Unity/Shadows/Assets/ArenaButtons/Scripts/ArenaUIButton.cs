﻿using UnityEngine;
using System.Collections;

public class ArenaUIButton : AbstractButton
{
    #region Events

        public delegate void MouseDownEvent();
        public event MouseDownEvent MouseDown;

        public delegate void MouseUpEvent();
        public event MouseUpEvent MouseUp;

        public delegate void MouseEnterEvent();
        public event MouseEnterEvent MouseEnter;

        public delegate void MouseExitEvent();
        public event MouseExitEvent MouseExit;

    #endregion

    public delegate void ButtonChangedStateEventExecuted(eButtonState i_ButtonState);
    public event ButtonChangedStateEventExecuted ButtonChangedStateEvent;

    public enum eButtonPivots 
    {
        Center = 0,
        TopLeft,
        Top,
        TopRight,
        Left,
        Right,
        BotLeft,
        Bot,
        BotRight
    }

    public eButtonPivots mr_ButtonPivot;

    public SpriteRenderer mr_NormalState;

    public SpriteRenderer mr_OverState;

    public SpriteRenderer mr_PressedState;

    public SpriteRenderer mr_DisabledState;

    public ArenaButtonsTooltipController mr_Tooltip;

    public bool mr_IsTooltipActive;

    public bool mr_IsButtonDisabled;

    public bool mr_IsContinousAction;

    #region Method invocation

        //Method invocation
        public GameObject mr_ScriptContainingMethodToInvoke;
        public string mr_MethodNameToInvoke;

    #endregion

    private eButtonState m_buttonState;

    private bool m_isButtonDisabled;

    private const int k_zValueOfButtons = -10;

    private bool m_cursorOnButton;

    public MLTextMesh mr_ButtonText;

    private Color k_defaultNormalColor = Color.black;
    private Color k_defaultPressedColor = Color.white;

    public enum eButtonState
    {
        Normal,
        Over,
        Pressed,
        Disabled
    }

    public void Start()
    {
        this.setButtonInitialState();
        this.initButtonBoxCollider();
    }


    public eButtonState ButtonState
    {
        get
        {
            return this.m_buttonState;
        }

    }

    private void setButtonInitialState()
    {
        if (this.m_buttonState == eButtonState.Disabled
            || mr_IsButtonDisabled)
        {
            if (mr_DisabledState != null)
            {
                m_isButtonDisabled = true;
                m_buttonState = eButtonState.Disabled;
                this.ChangeButtonToGivenState(eButtonState.Disabled);
            }
            else
            {
                m_buttonState = eButtonState.Normal;
                this.ChangeButtonToGivenState(eButtonState.Normal);
            }
        }
        else
        {
            m_buttonState = eButtonState.Normal;
            this.ChangeButtonToGivenState(eButtonState.Normal);
        }
    }

    private void initButtonBoxCollider()
    {
        float widthOfSprite = this.mr_NormalState.sprite.rect.xMax;
        float heightOfSprite = this.mr_NormalState.sprite.rect.yMax;
        BoxCollider buttonBoxCollider =
            this.gameObject.AddComponent<BoxCollider>();
        buttonBoxCollider.size =
            new Vector3(widthOfSprite, heightOfSprite, k_zValueOfButtons);
        this.adjustColliderAccordingToSpriteOffset(buttonBoxCollider);
    }

    private void adjustColliderAccordingToSpriteOffset(BoxCollider i_ButtonBoxCollider)
    {
        float basicAdjustmentDeltaInWidth = this.mr_NormalState.sprite.rect.xMax / 2;
        float basicAdjustmentDeltaInHeight = this.mr_NormalState.sprite.rect.yMax / 2;

        float totalAdjustmentDeltaInWidth = 0;
        float totalAdjustmentDeltaInHeight = 0;

        switch (mr_ButtonPivot)
        {
            case eButtonPivots.Center:
                    break;

            case eButtonPivots.TopLeft:
                    totalAdjustmentDeltaInWidth = basicAdjustmentDeltaInWidth;
                    totalAdjustmentDeltaInHeight = - basicAdjustmentDeltaInHeight;
                    break;
        }

        i_ButtonBoxCollider.center =
            new Vector3(totalAdjustmentDeltaInWidth, totalAdjustmentDeltaInHeight, 0);
    }

    public void OnMouseDown()
    {
        if (!m_isButtonDisabled)
        {
            this.ChangeButtonToGivenState(eButtonState.Pressed);

            if (this.MouseDown != null)
            {
                this.MouseDown();
            }
        }
    }

    public void OnMouseDrag ()
    {
        if (!m_isButtonDisabled && this.mr_IsContinousAction)
        {
            if (this.MouseDown != null)
            {
                this.MouseDown();
            }
            this.invokeGivenMethod();
        }
    }

    private bool isCursorOnButton()
    {
        bool cursorOnButton;

        if (Application.isWebPlayer || Application.isEditor)
        {
            cursorOnButton = m_cursorOnButton;
        }
        //Redundant - No Cursor on Android or iOS
        else
        {
            cursorOnButton = true;
        }

        return cursorOnButton;
    }

    public void OnMouseUp()
    {
        if (!m_isButtonDisabled && this.isPressed())
        {
            if (this.isCursorOnButton())
            {
                this.ChangeButtonToGivenState(eButtonState.Normal);

                Debug.Log("Arena UI Button Clicked!!!");

                if (ButtonChangedStateEvent != null)
                {
                    ButtonChangedStateEvent(this.m_buttonState);
                }

                if (mr_IsTooltipActive)
                {
                    mr_Tooltip.HideTooltip();
                }

                this.invokeGivenMethod();

                if (this.MouseUp != null)
                {
                    this.MouseUp();
                }
            }
        }
    }

    private bool isPressed()
    {
        return m_buttonState == eButtonState.Pressed;
    }

    public void OnMouseEnter()
    {
        if (!m_isButtonDisabled && !this.mr_PressedState.gameObject.activeInHierarchy)
        {
            if (mr_OverState != null)
            {
                mr_OverState.gameObject.SetActive(true);
                this.mr_NormalState.gameObject.SetActive(false);
                this.mr_PressedState.gameObject.SetActive(false);
            }

            if (mr_DisabledState != null)
            {
                mr_DisabledState.gameObject.SetActive(false);
            }

            if (mr_IsTooltipActive)
            {
                mr_Tooltip.gameObject.SetActive(true);
                mr_Tooltip.ShowTooltip();
            }

            this.m_cursorOnButton = true;

            if (MouseEnter != null)
            {
                MouseEnter();
            }
        }
    }

    public void OnMouseExit()
    {
        if (!m_isButtonDisabled)
        {
            if (mr_OverState != null)
            {
                mr_OverState.gameObject.SetActive(false);
            }

            if (mr_DisabledState != null)
            {
                mr_DisabledState.gameObject.SetActive(false);
            }

            if (mr_IsTooltipActive)
            {
                mr_Tooltip.HideTooltip();
            }

            this.ChangeButtonToGivenState(eButtonState.Normal);

            this.m_cursorOnButton = false;

            if (MouseExit != null)
            {
                MouseExit();
            }
        }
    }

    private void setButtonToCorrectState()
    {
        if (m_buttonState == eButtonState.Normal)
        {
            this.ChangeButtonToGivenState(eButtonState.Normal);
        }

        if (m_buttonState == eButtonState.Pressed)
        {
            this.ChangeButtonToGivenState(eButtonState.Pressed);
        }
    }

    public override void DisableButton()
    {
        if (mr_DisabledState != null)
        {
            if (mr_DisabledState != null)
            {
                mr_DisabledState.gameObject.SetActive(true);
            }

            this.mr_NormalState.gameObject.SetActive(false);

            if (mr_OverState != null)
            {
                mr_OverState.gameObject.SetActive(false);
            }

            this.mr_PressedState.gameObject.SetActive(false);
            this.m_isButtonDisabled = true;
            this.m_buttonState = eButtonState.Disabled;
        }
    }

    public override void EnableButton()
    {
        if (mr_DisabledState != null)
        {
            this.mr_NormalState.gameObject.SetActive(true);
            this.mr_PressedState.gameObject.SetActive(false);

            if (mr_OverState != null)
            {
                mr_OverState.gameObject.SetActive(false);
            }

            if (mr_DisabledState != null)
            {
                mr_DisabledState.gameObject.SetActive(false);
            }

            this.m_isButtonDisabled = false;
            this.m_buttonState = eButtonState.Normal;
        }
    }

    public void ChangeButtonToGivenState(eButtonState i_ButtonState)
    {
        switch (i_ButtonState)
        {
            case eButtonState.Normal:
                this.mr_NormalState.gameObject.SetActive(true);
                this.mr_PressedState.gameObject.SetActive(false);

                if (mr_OverState != null)
                {
                    mr_OverState.gameObject.SetActive(false);
                }

                if (mr_DisabledState != null)
                {
                    mr_DisabledState.gameObject.SetActive(false);
                }

                this.m_buttonState = eButtonState.Normal;

                break;

            case eButtonState.Pressed:
                this.mr_PressedState.gameObject.SetActive(true);
                this.mr_NormalState.gameObject.SetActive(false);

                if (mr_OverState != null)
                {
                    mr_OverState.gameObject.SetActive(false);
                }

                if (mr_DisabledState != null)
                {
                    mr_DisabledState.gameObject.SetActive(false);
                }

                this.m_buttonState = eButtonState.Pressed;

                break;

            case eButtonState.Disabled:

                if (mr_DisabledState != null)
                {
                    mr_DisabledState.gameObject.SetActive(true);
                    this.m_buttonState = eButtonState.Disabled;
                }

                if (mr_OverState != null)
                {
                    mr_OverState.gameObject.SetActive(false);
                }

                this.mr_NormalState.gameObject.SetActive(false);
                this.mr_PressedState.gameObject.SetActive(false);
                break;
        }

        this.changeTextColorAccordingToButtonState();
    }

    private void changeTextColorAccordingToButtonState()
    {
        if (mr_ButtonText != null)
        {
            switch (m_buttonState)
            {
                case eButtonState.Normal:
                        this.mr_ButtonText.TextMesh.color = k_defaultNormalColor;
                        break;

                case eButtonState.Pressed:
                        this.mr_ButtonText.TextMesh.color = k_defaultPressedColor;
                        break;
            }
        }
    }

    private void invokeGivenMethod()
    {
        if (!mr_MethodNameToInvoke.Equals("") && mr_ScriptContainingMethodToInvoke != null)
        {
            MonoBehaviour scriptContainingMethodToInvoke =
                mr_ScriptContainingMethodToInvoke.GetComponent<MonoBehaviour>();

            scriptContainingMethodToInvoke.Invoke(mr_MethodNameToInvoke, 0);
        }
        else
        {
            if (!mr_MethodNameToInvoke.Equals(""))
            {
                Debug.LogError("No such script or method exist!!!");
            }
        }
    }

    public void ShowButton()
    {
        mr_NormalState.gameObject.SetActive(true);
        mr_OverState.gameObject.SetActive(true);
        mr_PressedState.gameObject.SetActive(true);
        mr_DisabledState.gameObject.SetActive(true);

        this.EnableButton();
    }

    public void HideButton()
    {
        this.DisableButton();
        mr_NormalState.gameObject.SetActive(false);
        mr_OverState.gameObject.SetActive(false);
        mr_PressedState.gameObject.SetActive(false);
        mr_DisabledState.gameObject.SetActive(false);
    }
}
