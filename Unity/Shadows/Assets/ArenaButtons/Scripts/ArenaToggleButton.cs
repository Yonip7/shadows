﻿using UnityEngine;
using System.Collections;

public class ArenaToggleButton : AbstractButton
{

    public delegate void MouseDownEvent();
    public event MouseDownEvent MouseDown;

    public delegate void MouseUpEvent(eButtonState i_ButtonState);
    public event MouseUpEvent MouseUp;

    public delegate void ButtonChangedStateEventExecuted(eButtonState i_ButtonState);
    public event ButtonChangedStateEventExecuted ButtonChangedStateEvent;

    public enum eButtonPivots
    {
        Center = 0,
        TopLeft,
        Top,
        TopRight,
        Left,
        Right,
        BotLeft,
        Bot,
        BotRight
    }

    public eButtonPivots mr_ButtonPivot;

    public SpriteRenderer mr_OnState;

    public SpriteRenderer mr_OverState;

    public SpriteRenderer mr_OverOffState;

    public SpriteRenderer mr_OffState;

    public SpriteRenderer mr_DisabledState;

    public ArenaButtonsTooltipController mr_Tooltip;

    public bool mr_IsTooltipActive;

    public bool mr_IsButtonDisabled;

    #region Method invocation

        //Method invocation
        public GameObject mr_ScriptContainingMethodToInvoke;
        public string mr_MethodNameToInvoke;

    #endregion

    private eButtonState m_buttonState;

    private bool m_isButtonDisabled;

    private const int k_zValueOfButtons = -10;

    private bool m_isPressed;

    public enum eButtonState
    {
        On,
        Off,
        Disabled
    }

    public eButtonState ButtonState
    {
        get
        {
            return m_buttonState;
        }
    }

    public void Start()
    {
        m_isPressed = false;
        this.setButtonInitialState();
        this.initButtonBoxCollider();
        this.setButtonToCorrectState();
    }

    private void setButtonInitialState()
    {
        if (mr_IsButtonDisabled)
        {
            if (mr_DisabledState != null)
            {
                m_isButtonDisabled = true;
                this.ChangeButtonToGivenState(eButtonState.Disabled);
                m_buttonState = eButtonState.Disabled;
            }
            else
            {
                this.ChangeButtonToGivenState(eButtonState.On);
                m_buttonState = eButtonState.On;
            }
        }
        else
        {
            this.ChangeButtonToGivenState(eButtonState.On);
            m_buttonState = eButtonState.On;
        }
    }

    private void initButtonBoxCollider()
    {
        float widthOfSprite = this.mr_OnState.sprite.rect.xMax;
        float heightOfSprite = this.mr_OnState.sprite.rect.yMax;
        BoxCollider buttonBoxCollider =
            this.gameObject.AddComponent<BoxCollider>();
        buttonBoxCollider.size =
            new Vector3(widthOfSprite, heightOfSprite, k_zValueOfButtons);
        this.adjustColliderAccordingToSpriteOffset(buttonBoxCollider);
    }

    private void adjustColliderAccordingToSpriteOffset(BoxCollider i_ButtonBoxCollider)
    {
        float basicAdjustmentDeltaInWidth = this.mr_OnState.sprite.rect.xMax / 2;
        float basicAdjustmentDeltaInHeight = this.mr_OnState.sprite.rect.yMax / 2;

        float totalAdjustmentDeltaInWidth = 0;
        float totalAdjustmentDeltaInHeight = 0;

        switch (mr_ButtonPivot)
        {
            case eButtonPivots.Center:
                break;

            case eButtonPivots.TopLeft:
                totalAdjustmentDeltaInWidth = basicAdjustmentDeltaInWidth;
                totalAdjustmentDeltaInHeight = -basicAdjustmentDeltaInHeight;
                break;
        }

        i_ButtonBoxCollider.center =
            new Vector3(totalAdjustmentDeltaInWidth, totalAdjustmentDeltaInHeight, 0);
    }

    public void ToggleButtonStateChange()
    {
        if (m_buttonState == eButtonState.On)
        {
            this.ChangeButtonToGivenState(eButtonState.Off);
            m_buttonState = eButtonState.Off;
        }
        else
        {
            if (m_buttonState == eButtonState.Off)
            {
                this.ChangeButtonToGivenState(eButtonState.On);
                m_buttonState = eButtonState.On;
            }
        }
    }

    public void OnMouseDown()
    {
        if (!m_isButtonDisabled)
        {
            m_isPressed = true;
            if (this.MouseDown != null)
            {
                this.MouseDown();
            }
        }
    }

    public void OnMouseUp()
    {
        if (!m_isButtonDisabled && m_isPressed)
        {
            this.ToggleButtonStateChange();

            if (ButtonChangedStateEvent != null)
            {
                ButtonChangedStateEvent(this.m_buttonState);
            }

            if (mr_IsTooltipActive)
            {
                mr_Tooltip.HideTooltip();
            }

            if (this.MouseUp != null)
            {
                this.MouseUp(this.m_buttonState);
            }

            this.invokeGivenMethod();
        }
        m_isPressed = false;
    }

    public void OnMouseEnter()
    {
        if (!m_isButtonDisabled)
        {
            if (mr_OverState != null)
            {
                if (m_buttonState == eButtonState.On)
                {
                    mr_OverState.gameObject.SetActive(true);
                    this.mr_OnState.gameObject.SetActive(false);
                    this.mr_OffState.gameObject.SetActive(false);

                    if (mr_OverOffState != null)
                    {
                        mr_OverOffState.gameObject.SetActive(false);
                    }
                }
            }

            if (mr_OverOffState != null)
            {
                if (m_buttonState == eButtonState.Off)
                {
                    mr_OverOffState.gameObject.SetActive(true);
                    this.mr_OnState.gameObject.SetActive(false);
                    this.mr_OffState.gameObject.SetActive(false);

                    if (mr_OverState != null)
                    {
                        this.mr_OverState.gameObject.SetActive(false);
                    }
                }
            }

            if (mr_DisabledState != null)
            {
                mr_DisabledState.gameObject.SetActive(false);
            }

            if (mr_IsTooltipActive)
            {
                mr_Tooltip.ShowTooltip();
            }
        }
    }

    public void OnMouseExit()
    {
        if (!m_isButtonDisabled && !this.mr_OffState.gameObject.activeInHierarchy)
        {
            if (mr_OverState != null)
            {
                mr_OverState.gameObject.SetActive(false);
            }
            
            if (mr_DisabledState != null)
            {
                mr_DisabledState.gameObject.SetActive(false);
            }
            this.setButtonToCorrectState();

            if (mr_IsTooltipActive)
            {
                mr_Tooltip.HideTooltip();
            }
        }
        m_isPressed = false;
    }

    private void setButtonToCorrectState()
    {
        if (m_buttonState == eButtonState.On)
        {
            this.ChangeButtonToGivenState(eButtonState.On);
            m_buttonState = eButtonState.On;
        }
        else
        {
            if (m_buttonState == eButtonState.Off)
            {
                this.ChangeButtonToGivenState(eButtonState.Off);
                m_buttonState = eButtonState.Off;
            }
        }
    }

    public override void DisableButton()
    {
        if (mr_DisabledState != null)
        {
            if (mr_DisabledState != null)
            {
                mr_DisabledState.gameObject.SetActive(true);
            }
            
            this.mr_OnState.gameObject.SetActive(false);

            if (mr_OverState != null)
            {
                mr_OverState.gameObject.SetActive(false);
            }
            
            this.mr_OffState.gameObject.SetActive(false);
            this.m_isButtonDisabled = true;
        }
    }

    public override void EnableButton()
    {
        if (mr_DisabledState != null)
        {
            this.mr_OnState.gameObject.SetActive(true);
            this.mr_OffState.gameObject.SetActive(false);

            if (mr_OverState != null)
            {
                mr_OverState.gameObject.SetActive(false);
            }
            
            if (mr_DisabledState != null)
            {
                mr_DisabledState.gameObject.SetActive(false);
            }
            
            this.m_isButtonDisabled = false;
        }
    }

    public void ChangeButtonToGivenState(eButtonState i_ButtonState)
    {
        switch (i_ButtonState)
        {
            case eButtonState.On:
                this.mr_OnState.gameObject.SetActive(true);
                this.mr_OffState.gameObject.SetActive(false);

                if (mr_OverState != null)
                {
                    mr_OverState.gameObject.SetActive(false);
                }

                if (mr_OverOffState != null)
                {
                    mr_OverOffState.gameObject.SetActive(false);
                }

                if (mr_DisabledState != null)
                {
                    mr_DisabledState.gameObject.SetActive(false);
                }
                
                break;

            case eButtonState.Off:
                this.mr_OffState.gameObject.SetActive(true);
                this.mr_OnState.gameObject.SetActive(false);

                if (mr_OverState != null)
                {
                    mr_OverState.gameObject.SetActive(false);
                }

                if (mr_OverOffState != null)
                {
                    mr_OverOffState.gameObject.SetActive(false);
                }
               
                if (mr_DisabledState != null)
                {
                    mr_DisabledState.gameObject.SetActive(false);
                }
                
                break;

            case eButtonState.Disabled:

                if (mr_DisabledState != null)
                {
                    mr_DisabledState.gameObject.SetActive(true);
                }

                if (mr_OverState != null)
                {
                    mr_OverState.gameObject.SetActive(false);
                }

                if (mr_OverOffState != null)
                {
                    mr_OverOffState.gameObject.SetActive(false);
                }

                this.mr_OnState.gameObject.SetActive(false);
                this.mr_OffState.gameObject.SetActive(false);
                break;
        }
    }

    private void invokeGivenMethod()
    {
        if (!mr_MethodNameToInvoke.Equals("") && mr_ScriptContainingMethodToInvoke != null)
        {
            MonoBehaviour scriptContainingMethodToInvoke =
                mr_ScriptContainingMethodToInvoke.GetComponent<MonoBehaviour>();

            scriptContainingMethodToInvoke.Invoke(mr_MethodNameToInvoke, 0);
        }
        else
        {
            if (!mr_MethodNameToInvoke.Equals(""))
            {
                Debug.LogError("No such script or method exist!!!");
            }
        }
    }
}
